package verification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import currencyConvertion.currencyConveter;
import excelReader.ExcelReader;
import readAndWrite.hotel;
import readXml.resHotelAvailability;

public class hotelBedsPaymentResponse {

	public void verify(resHotelAvailability availability, hotel hotel, String defaultCode, Map<String, String> currency, StringBuffer ReportPrinter) throws FileNotFoundException
	{
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/*System.out.println(defaultCode);
	System.out.println(availability.getTotalAmount());
	System.out.println(hotel.getCurrencyCode());
	System.out.println(availability.getCurrencyCode());
	System.out.println("Supplier to usd : " + currency.get(availability.getCurrencyCode()));
	System.out.println("Usd to customer : " + currency.get(hotel.getCurrencyCode()));*/
		
		
		
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~		
		ExcelReader newReader = new ExcelReader();
		FileInputStream stream =new FileInputStream(new File("excel/searchDetails.xls"));
		
		ArrayList<Map<Integer, String>> ContentList = new ArrayList<Map<Integer,String>>();
		ContentList = newReader.contentReading(stream);
		
		Map<Integer, String>  content = new HashMap<Integer, String>();
		content   = ContentList.get(0);
		
		Iterator<Map.Entry<Integer, String>>   mapiterator = content.entrySet().iterator();
		
		
			Map.Entry<Integer, String> Entry = (Map.Entry<Integer, String>)mapiterator.next();
			String Content = Entry.getValue();
			String[] searchDetails = Content.split(",");
		
		String checkin = null;
		String checkout = null;
		try {
			checkin = formatDate(hotel.getCheckin());
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			checkout = formatDate(hotel.getCheckOut());
		} catch (Exception e) {
			e.printStackTrace();
		}
		ArrayList<String> 	webRoomType	= new ArrayList<String>();
		ArrayList<String> 	xmlRoomType	= new ArrayList<String>();
		for(int i = 0 ; i < hotel.getRoom().size() ; i++)
		{
			webRoomType.add(hotel.getRoom().get(i).getRoomType());
		}
		for(int i = 0 ; i < availability.getHotelRoom().size() ; i++)
		{
			xmlRoomType.add(availability.getHotelRoom().get(i).getRoom());
		}
		currencyConveter	converter	= 	new currencyConveter();
		String value	=	converter.convert(defaultCode, availability.getTotalAmount(), hotel.getCurrencyCode(), currency, availability.getCurrencyCode());
		float	cv		= 0 ;
				cv		= Float.parseFloat(value);
				cv		= cv + (cv/100)*10;
		BigDecimal roundfinalPrice 	= new BigDecimal(cv).setScale(0,BigDecimal.ROUND_CEILING);

		int webValue	= 	Integer.parseInt(hotel.getPay().getSubTotal());
			value		= 	String.valueOf(roundfinalPrice);
		int	cValue		= 	Integer.parseInt(value);
		ArrayList<String> adult		= new ArrayList<String>();
		ArrayList<String> child		= new ArrayList<String>();
		ArrayList<String> webAdult	= new ArrayList<String>();
		ArrayList<String> webChild	= new ArrayList<String>();
		for(int i = 0 ; i < hotel.getRoom().size() ; i++)
		{
			adult.add(availability.getRoomAvailability().get(i).getAdultCount());
			child.add(availability.getRoomAvailability().get(i).getChildCount());
		}
		for(int i = 0 ; i < hotel.getRoom().size() ; i++)
		{
			webAdult.add(hotel.getRoom().get(i).getAdultCount());
			System.out.println(hotel.getRoom().get(i).getAdultCount());
			webChild.add(hotel.getRoom().get(i).getChildCount());
		}
		ReportPrinter.append("<span>(7) Payment verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:800px>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
		
		if(hotel.getHotelName().equals(availability.getHotelInfoname()))
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotel.getHotelName() +"</td>"
									+ 	"<td>"+ availability.getHotelInfoname() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotel.getHotelName() +"</td>"
									+ 	"<td>"+ availability.getHotelInfoname() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(webRoomType.equals(xmlRoomType))
		{
			ReportPrinter.append	(	"<tr><td>2</td>"
									+ 	"<td>Verify room type</td>"
									+	"<td>"+ webRoomType +"</td>"
									+ 	"<td>"+ xmlRoomType +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>2</td>"
									+ 	"<td>Verify room type</td>"
									+	"<td>"+ webRoomType +"</td>"
									+ 	"<td>"+ xmlRoomType +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(checkin.equals(availability.getDateFromDate()))
		{
			ReportPrinter.append	(	"<tr><td>3</td>"
									+ 	"<td>Verify checkin date</td>"
									+	"<td>"+ checkin +"</td>"
									+ 	"<td>"+ availability.getDateFromDate() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>3</td>"
									+ 	"<td>Verify checkin date</td>"
									+	"<td>"+ checkin +"</td>"
									+ 	"<td>"+ availability.getDateFromDate() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
		}
		if(checkout.equals(availability.getDateToDate()))
		{
			ReportPrinter.append	(	"<tr><td>4</td>"
									+ 	"<td>Verify checkout date</td>"
									+	"<td>"+ checkin +"</td>"
									+ 	"<td>"+ availability.getDateFromDate() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>4</td>"
									+ 	"<td>Verify checkout date</td>"
									+	"<td>"+ checkin +"</td>"
									+ 	"<td>"+ availability.getDateFromDate() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(cValue + 2 >= webValue && webValue >= cValue - 2 )
		{
			ReportPrinter.append	(	"<tr><td>5</td>"
									+ 	"<td>Verify sub total</td>"
									+	"<td>"+ cValue +"</td>"
									+ 	"<td>"+ webValue +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>5</td>"
									+ 	"<td>Verify sub total</td>"
									+	"<td>"+ cValue +"</td>"
									+ 	"<td>"+ webValue +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
		}
		String adultCount  = searchDetails[5].replace("/", ",");
		adultCount			= "[".concat(adultCount).concat("]");
		System.out.println(adultCount);
		System.out.println(adult);
		String adultString = adult.toString().replace(" ", "").trim();
		System.out.println(adultString);
		if(adultString.contains(adultCount.trim()))
		{
			ReportPrinter.append	(	"<tr><td>6</td>"
									+ 	"<td>Verify adult count</td>"
									+	"<td>"+ adultCount +"</td>"
									+ 	"<td>"+ adult +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>6</td>"
									+ 	"<td>Verify adult count</td>"
									+	"<td>"+ adultCount +"</td>"
									+ 	"<td>"+ adult +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
		}
		String childCount	= searchDetails[6].replace("/", ",");
		childCount			= "[".concat(childCount).concat("]");
		String childString	= child.toString().replace(" ", "").trim();	
		if(childString.contains(childCount.trim()))
		{
			ReportPrinter.append	(	"<tr><td>7</td>"
									+ 	"<td>Verify child count</td>"
									+	"<td>"+ childCount +"</td>"
									+ 	"<td>"+ child +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>7</td>"
									+ 	"<td>Verify child count</td>"
									+	"<td>"+ adultCount +"</td>"
									+ 	"<td>"+ child +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}
	public String formatDate(String date) 
	{
		System.out.println(date);
		DateFormat originalFormat = new SimpleDateFormat("dd MMM yy");
		DateFormat targetFormat = new SimpleDateFormat("yyyyMMdd");
		Date date1;
		String formattedDate = null;
		try {
			date1 = originalFormat.parse(date);
			formattedDate = targetFormat.format(date1); 
			System.out.println(formattedDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formattedDate;
	}
}
