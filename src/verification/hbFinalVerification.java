package verification;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.omg.CORBA.OMGVMCID;

import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import readConfirmation.conhotel;
import readConfirmation.roomOccupancy;
import readXml.availableRoom;
import readXml.hbPage1Req;
import readXml.hotelAvailability;
import readXml.resHotelAvailability;

public class hbFinalVerification {
	private static Map<String, String>  txtProperty = new HashMap<String, String>();
	
	public void inv13ReqAgainstConWeb(hbPage1Req reqAvailabilityFinalReq, conhotel conHotel, StringBuffer ReportPrinter)
	{
		Properties properties = new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("D://workSpace//hotelXmlValidation//profitMarkUps.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		int cnt=1;
		ReportPrinter.append("<span>(7) Pre reservation req against res</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		String newDateString = formatDate(conHotel.getCheckIn());
		if(reqAvailabilityFinalReq.getCheckin().contains(newDateString))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getCheckin() +"</td>"
					+ 	"<td>"+ conHotel.getCheckIn() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getCheckin() +"</td>"
					+ 	"<td>"+ conHotel.getCheckIn() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		newDateString =formatDate(conHotel.getCheckout());
		if(reqAvailabilityFinalReq.getCheckout().contains(newDateString))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getCheckout() +"</td>"
					+ 	"<td>"+ conHotel.getCheckout() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getCheckout() +"</td>"
					+ 	"<td>"+ conHotel.getCheckout() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		System.out.println(conHotel.getRoom().size());
		for(int i = 0 ; i < conHotel.getRoom().size() ; i++)
		{
			System.out.println(reqAvailabilityFinalReq.getGuest().get(i).getName());
			System.out.println(conHotel.getCustomer().getFirstName());
			try {
				System.out.println(conHotel.getOccypancy().get(i).get(1).getFirstName());
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				System.out.println(conHotel.getOccypancy2().get(i).getFirstName());
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println(conHotel.getOccypancy().get(i).get(0).getFirstName());
			System.out.println(reqAvailabilityFinalReq.getGuest().get(i).getName());
			
			if(reqAvailabilityFinalReq.getGuest().get(i).getName().equals(conHotel.getOccypancy().get(i).get(0).getFirstName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getName() +"</td>"
						+ 	"<td>"+ conHotel.getOccypancy().get(i).get(0).getFirstName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer name</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getName() +"</td>"
						+ 	"<td>"+ conHotel.getOccypancy().get(i).get(0).getFirstName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			if(reqAvailabilityFinalReq.getGuest().get(i).getLastName().equals(conHotel.getOccypancy().get(i).get(0).getLastName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getLastName() +"</td>"
						+ 	"<td>"+ conHotel.getOccypancy().get(i).get(0).getLastName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer name</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getLastName() +"</td>"
						+ 	"<td>"+ conHotel.getOccypancy().get(i).get(0).getLastName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			System.out.println(reqAvailabilityFinalReq.getGuest().get(i).getBedType());
			System.out.println(conHotel.getRoom().get(i).getBedType());
			if(reqAvailabilityFinalReq.getGuest().get(i).getBedType().contains(conHotel.getRoom().get(i).getBedType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify bed type</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getBedType() +"</td>"
						+ 	"<td>"+ conHotel.getRoom().get(i).getBedType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify bed type</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getBedType() +"</td>"
						+ 	"<td>"+ conHotel.getRoom().get(i).getBedType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			System.out.println(conHotel.getRoom().get(i).getRoom());
			String[] count = conHotel.getRoom().get(i).getRoom().split(",");
			count = count[0].split("s");
			String adult = count[1].trim();
			count = conHotel.getRoom().get(i).getRoom().split(",");
			count = count[1].trim().split(" ");
			count = count[0].split("n");
			String child = count[1];
			
			if(reqAvailabilityFinalReq.getGuest().get(i).getAdultCount().equals(adult))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify adult count per room</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getAdultCount() +"</td>"
						+ 	"<td>"+ adult +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify adult count per room</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getAdultCount() +"</td>"
						+ 	"<td>"+ adult +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			if(reqAvailabilityFinalReq.getGuest().get(i).getChildCount().equals(child))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify child count per room</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getChildCount() +"</td>"
						+ 	"<td>"+ child+"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify child count per room</td>"
						+	"<td>"+ reqAvailabilityFinalReq.getGuest().get(i).getChildCount() +"</td>"
						+ 	"<td>"+ child+"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
		
	}
	
	public void inv13ReqAgainstCon(hbPage1Req reqAvailabilityFinalReq, conhotel conHotel, StringBuffer ReportPrinter)
	{
		int cnt = 1;
		Properties properties = new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitMarkUps.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		
		ReportPrinter.append("<span>(7) Pre reservation req against res</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		System.out.println(reqAvailabilityFinalReq.getCheckin());
		System.out.println(conHotel.getCheckIn());
		
		String newDateString = formatDate(conHotel.getCheckIn());
		if(reqAvailabilityFinalReq.getCheckin().contains(newDateString))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getCheckin() +"</td>"
					+ 	"<td>"+ conHotel.getCheckIn() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getCheckin() +"</td>"
					+ 	"<td>"+ conHotel.getCheckIn() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		newDateString =formatDate(conHotel.getCheckout());
		if(reqAvailabilityFinalReq.getCheckout().contains(newDateString))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getCheckout() +"</td>"
					+ 	"<td>"+ conHotel.getCheckout() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getCheckout() +"</td>"
					+ 	"<td>"+ conHotel.getCheckout() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		System.out.println(conHotel.getRoom().size());
		ArrayList<String> name1 = new ArrayList<String>();
		ArrayList<String> name2 = new ArrayList<String>();
		ArrayList<String> bed1 = new ArrayList<String>();
		ArrayList<String> bed2 = new ArrayList<String>();
		ArrayList<String> adlt1 = new ArrayList<String>();
		ArrayList<String> adlt2 = new ArrayList<String>();
		ArrayList<String> chld2 = new ArrayList<String>();
		ArrayList<String> chld1 = new ArrayList<String>();
		for(int i = 0 ; i < conHotel.getRoom().size() ; i++)
		{
			String nameFull1 = reqAvailabilityFinalReq.getGuest().get(i).getName().concat(" ").concat(reqAvailabilityFinalReq.getGuest().get(i).getLastName());
			String nameFull2 = conHotel.getOccypancy().get(i).get(0).getFirstName().concat(" ").concat(conHotel.getOccypancy().get(i).get(0).getLastName());
			name1.add(nameFull1);
			name2.add(nameFull2);
			
			bed1.add(reqAvailabilityFinalReq.getGuest().get(i).getBedType());
			bed2.add(conHotel.getRoom().get(i).getBedType());

			String[] count = conHotel.getRoom().get(i).getRoom().split(":");
			String[] adult = count[1].trim().split(" ");
			String[] child = count[2].trim().split(" ");

			adlt1.add(reqAvailabilityFinalReq.getGuest().get(i).getAdultCount());
			adlt2.add(adult[0]);
			
			chld1.add(reqAvailabilityFinalReq.getGuest().get(i).getChildCount());
			chld2.add(child[0]);
			
		}
		
		if(name1.containsAll(name2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer name</td>"
					+	"<td>"+ name1 +"</td>"
					+ 	"<td>"+ name2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer name</td>"
					+	"<td>"+ name1 +"</td>"
					+ 	"<td>"+ name2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(bed1.containsAll(bed2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify bed type</td>"
					+	"<td>"+ bed1 +"</td>"
					+ 	"<td>"+ bed2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify bed type</td>"
					+	"<td>"+ bed1 +"</td>"
					+ 	"<td>"+ bed2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(adlt1.containsAll(adlt2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adlt1 +"</td>"
					+ 	"<td>"+ adlt2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adlt1 +"</td>"
					+ 	"<td>"+ adlt2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(chld1.containsAll(chld2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chld1 +"</td>"
					+ 	"<td>"+ chld2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chld1 +"</td>"
					+ 	"<td>"+ chld2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}
	
	public String formatDate(String date)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyy-MM-dd");
		String newDateString = sdf.format(d);
		return newDateString;
	}
	
	public void confirmationError(String error1, String error2, StringBuffer ReportPrinter)
	{
		
		if(error1.equals("") && error2.equals(""))
		{
			
		}
		{
			ReportPrinter.append("<span>Confirmation Error Verification</span>");
			ReportPrinter.append("<table border=1 style=width:100%>"
					+ "<tr><th>Error Message</th>"
					+ "<th>Xml Error</th></tr>");
			
			ReportPrinter.append	(	"<tr><td>"+ error1 +"</td>"
					+ 	"<td>"+ error2 +"</td></tr>");
			
			ReportPrinter.append("</table>");
			ReportPrinter.append	("<b></b><br>"
	                +	 "<hr size=4>");
		}
		
		
	}
	
	public void reqAgainstCon(hotelAvailability reqAvailabilityFinalReq, conhotel conHotel, StringBuffer ReportPrinter)
	{
		Properties properties = new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("D://workSpace//hotelXmlValidation//profitMarkUps.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		int cnt = 1 ;
		ReportPrinter.append("<span>(9) Pre reservation req against res</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		hbRoomAvailability availability = new hbRoomAvailability();
		
		System.out.println(reqAvailabilityFinalReq.getHolderName());
		System.out.println(conHotel.getCustomer().getFirstName());
		if(reqAvailabilityFinalReq.getHolderName().equals(conHotel.getCustomer().getFirstName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer first name</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getHolderName() +"</td>"
					+ 	"<td>"+ conHotel.getCustomer().getFirstName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer first name</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getHolderName() +"</td>"
					+ 	"<td>"+ conHotel.getCustomer().getFirstName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(reqAvailabilityFinalReq.getLastName().equals(conHotel.getCustomer().getLastName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer last name</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getLastName() +"</td>"
					+ 	"<td>"+ conHotel.getCustomer().getLastName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer last name</td>"
					+	"<td>"+ reqAvailabilityFinalReq.getLastName() +"</td>"
					+ 	"<td>"+ conHotel.getCustomer().getLastName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		ArrayList<roomOccupancy>  		occupancy = new ArrayList<roomOccupancy>();
		for(int i = 0 ; i < conHotel.getOccypancy().size() ; i++)
		{
			for(int j = 0 ; j < conHotel.getOccypancy().get(i).size() ; j++)
			{
				roomOccupancy occ = new roomOccupancy();
				occ.setFirstName			(conHotel.getOccypancy().get(i).get(j).getFirstName());
				occ.setLastName				(conHotel.getOccypancy().get(i).get(j).getFirstName());
				occupancy.add(occ);
			}
		}
		try {
			for(int i = 0 ; i < reqAvailabilityFinalReq.getGuestList().size(); i++)
			{
				
				if(reqAvailabilityFinalReq.getGuestList().get(i).getName().contains((CharSequence) occupancy))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify first name</td>"
							+	"<td>"+ reqAvailabilityFinalReq.getGuestList().get(i).getName() +"</td>"
							+ 	"<td>"+ reqAvailabilityFinalReq.getGuestList().get(i).getName() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify first name</td>"
							+	"<td>"+ reqAvailabilityFinalReq.getGuestList().get(i).getName() +"</td>"
							+ 	"<td>"+ reqAvailabilityFinalReq.getGuestList().get(i).getName() +"</td>" // check
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				if(reqAvailabilityFinalReq.getGuestList().get(i).getLastName().contains((CharSequence) occupancy))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify first name</td>"
							+	"<td>"+ reqAvailabilityFinalReq.getGuestList().get(i).getLastName() +"</td>"
							+ 	"<td>"+ reqAvailabilityFinalReq.getGuestList().get(i).getLastName() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify first name</td>"
							+	"<td>"+ reqAvailabilityFinalReq.getGuestList().get(i).getLastName() +"</td>"
							+ 	"<td>"+ reqAvailabilityFinalReq.getGuestList().get(i).getLastName() +"</td>" // check
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
		System.out.println(conHotel.getRoom().size());		
	}
	
	public void inv13ResAgainstCon(resHotelAvailability resAvailabilityFinalRes, conhotel conHotel, StringBuffer ReportPrinter, String defaultCurrency, Map<String, String> currency)
	{

		int cnt = 1 ;
		currencyConveter	cc	= 	new currencyConveter();
		addProfitmarkup		apm	=	new addProfitmarkup();
		System.out.println(resAvailabilityFinalRes.getHotelInfoname());
		System.out.println(conHotel.getError());
	
		try {
			System.out.println(resAvailabilityFinalRes.getHotelDetails().get(0).getPrice());
			System.out.println(conHotel.getPay().getTotal());
			System.out.println(resAvailabilityFinalRes.getHotelDetails().get(0).getCurrencyCode());
			System.out.println(conHotel.getCurrencyCode());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		Properties properties = new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitMarkUps.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		

		if(resAvailabilityFinalRes.getXmlError().equals("Confirm reservation failed. RESERVE_ONLY_AVAIL_VIOLATED"))
		{
			ReportPrinter.append("<br>");
			ReportPrinter.append("<span>Reservation response error</span>");
			ReportPrinter.append("<table border=1 style=width:100%>"
					+ "<tr><th>Error</th></tr>");
			ReportPrinter.append	(	"<tr><td>"+ resAvailabilityFinalRes.getXmlError() +"</td></tr>");
			ReportPrinter.append("</table>");
			
		}
		ReportPrinter.append("<span>(8) Pre reservation req against res</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		
		String convertedValue = "";
		int pmValue = 0 ;
		try {
			convertedValue = cc.convert(defaultCurrency, resAvailabilityFinalRes.getFinalPrice(), conHotel.getCurrencyCode(), currency, resAvailabilityFinalRes.getFinalcurrency());
			System.out.println(convertedValue);
			pmValue = apm.profitMarkup(convertedValue, txtProperty.get("inv13"));
			pmValue = Integer.parseInt(convertedValue) + pmValue;
			} catch (Exception e) {
			// TODO: handle exception
			System.out.println("adding pm error");
		}
		
		
		//System.out.println(resAvailabilityFinalRes.getFinalPrice());
		try {
			if(String.valueOf(pmValue).equals(conHotel.getPay().getTotal()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify total price</td>"
						+	"<td>"+ pmValue +"</td>"
						+ 	"<td>"+ conHotel.getPay().getTotal() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify total price</td>"
						+	"<td>"+ pmValue +"</td>"
						+ 	"<td>"+ conHotel.getPay().getTotal() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		int total = 0 ;
		int totalFinal = 0 ;
		float a = 0;
		for(int i = 0 ; i < resAvailabilityFinalRes.getHotelDetails().size() ; i++)
		{
			a = a + Float.parseFloat(resAvailabilityFinalRes.getHotelDetails().get(i).getPrice());
		}
		System.out.println(a); 
		totalFinal = Integer.parseInt(cc.convert(defaultCurrency, String.valueOf(a), conHotel.getCurrencyCode(), currency, resAvailabilityFinalRes.getHotelDetails().get(0).getCurrencyCode()));
		System.out.println(totalFinal);
		System.out.println(apm.profitMarkup(String.valueOf(totalFinal), txtProperty.get("inv13")));
		System.out.println(conHotel.getPay().getTotal());
		totalFinal = totalFinal +  apm.profitMarkup(String.valueOf(totalFinal), txtProperty.get("inv13"));
		System.out.println(totalFinal);
		
		if(String.valueOf(totalFinal).equals(conHotel.getPay().getTotal()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify total price (addition of price per room)</td>"
					+	"<td>"+ totalFinal +"</td>"
					+ 	"<td>"+ conHotel.getPay().getTotal() +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify total price (addition of price per room)</td>"
					+	"<td>"+ totalFinal +"</td>"
					+ 	"<td>"+ conHotel.getPay().getTotal() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		String totalAddedValue = String.valueOf(totalFinal);
		total = apm.profitMarkup(totalAddedValue, txtProperty.get("inv13"));
		totalAddedValue = String.valueOf(Integer.parseInt(totalAddedValue) + total);
		System.out.println(totalAddedValue);
		String[] supCon = conHotel.getSupConfirmation().split(",");
		
		
		ArrayList<String> reservationId1 	= new ArrayList<String>();
		ArrayList<String> reservationId2 	= new ArrayList<String>();
		ArrayList<String> checkin1 			= new ArrayList<String>();
		ArrayList<String> checkin2 			= new ArrayList<String>();
		ArrayList<String> tax1 				= new ArrayList<String>();
		ArrayList<String> tax2 				= new ArrayList<String>();
		ArrayList<String> status1 			= new ArrayList<String>();
		ArrayList<String> status2 			= new ArrayList<String>();
		ArrayList<String> hotel1 			= new ArrayList<String>();
		ArrayList<String> hotel2 			= new ArrayList<String>();
		ArrayList<String> address1 			= new ArrayList<String>();
		ArrayList<String> address2 			= new ArrayList<String>();
		ArrayList<String> firstName1		= new ArrayList<String>();
		ArrayList<String> firstName2 		= new ArrayList<String>();
		ArrayList<String> lstName1			= new ArrayList<String>();
		ArrayList<String> lstName2 			= new ArrayList<String>();
		ArrayList<String> pricePerRoom1    	= new ArrayList<String>();
		ArrayList<String> pricePerRoom2    	= new ArrayList<String>();
		
		System.out.println(conHotel.getRoom().get(0).getTotalRate());
		System.out.println(resAvailabilityFinalRes.getHotelDetails().get(0).getPrice());
		
		for(int i = 0 ; i < resAvailabilityFinalRes.getHotelDetails().size() ; i++)
		{
		
			pricePerRoom2.add(conHotel.getRoom().get(i).getTotalRate());
			String 		pricePerRoom 	= cc.convert(defaultCurrency, resAvailabilityFinalRes.getHotelDetails().get(i).getPrice(), conHotel.getCurrencyCode(), currency, resAvailabilityFinalRes.getHotelDetails().get(i).getCurrencyCode());
			System.out.println(pricePerRoom);
			int		 	pmVal 			= apm.profitMarkup(pricePerRoom, txtProperty.get("inv13"));
			System.out.println(pmVal);
			System.out.println(pmVal + Integer.parseInt(pricePerRoom));
			pricePerRoom1.add(String.valueOf(pmVal + Integer.parseInt(pricePerRoom)));
			
			reservationId1.add(resAvailabilityFinalRes.getHotelDetails().get(i).getReservationId());
			reservationId2.add(supCon[i].replace("_P", ""));
			
			try {
				System.out.println(conHotel.getCheckIn());
				String dateDromDate = formatDate(conHotel.getCheckIn());
				String[] date = resAvailabilityFinalRes.getHotelDetails().get(i).getFromDate().split("T");
				checkin1.add(date[0]);
				checkin2.add(dateDromDate);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			String dateToDate = formatDate(conHotel.getCheckout());
			
			try {
				tax1.add(resAvailabilityFinalRes.getHotelDetails().get(i).getTotalTax());
				tax2.add(conHotel.getPay().getTotalTaxAndServiceCharges());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				status1.add(resAvailabilityFinalRes.getHotelDetails().get(i).getStatus().toLowerCase());
				if(conHotel.getBookingStatus().equals("ON-REQUEST"))
				{
					status2.add("request");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			String[] hName = null;
			try {
				hName = conHotel.getHotelName().split("View Cancellation Policy");
				System.out.println(hName[0]);
				hotel1.add(resAvailabilityFinalRes.getHotelDetails().get(i).getHotelName());
				hotel2.add(hName[0].trim());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				address1.add(resAvailabilityFinalRes.getHotelDetails().get(i).getAddress());
				address2.add(conHotel.getAddress());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				System.out.println(resAvailabilityFinalRes.getHotelDetails().get(i).getFirstName());
				System.out.println(conHotel.getCustomer().getFirstName());
				firstName1.add(resAvailabilityFinalRes.getHotelDetails().get(i).getFirstName());
				firstName2.add(conHotel.getCustomer().getFirstName());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				lstName1.add(resAvailabilityFinalRes.getHotelDetails().get(i).getLastName());
				lstName2.add(conHotel.getCustomer().getLastName());
			} catch (Exception e) {
				// TODO: handle exception
			}	
		}
		
		if(reservationId1.containsAll(reservationId2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Reservation Id</td>"
					+	"<td>"+ reservationId1 +"</td>"
					+ 	"<td>"+ reservationId2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Reservation Id</td>"
					+	"<td>"+ reservationId1 +"</td>"
					+ 	"<td>"+ reservationId2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(checkin1.containsAll(checkin2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkin date</td>"
					+	"<td>"+ checkin1 +"</td>"
					+ 	"<td>"+ checkin2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ checkin1 +"</td>"
					+ 	"<td>"+ checkin2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(pricePerRoom1.containsAll(pricePerRoom2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
				+ 	"<td>Verify price per room</td>"
				+	"<td>"+ pricePerRoom1 +"</td>"
				+ 	"<td>"+ pricePerRoom2 +"</td>"
				+	"<td class='passed'>Passed</td></tr>");
			
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify price per room</td>"
					+	"<td>"+ pricePerRoom1 +"</td>"
					+ 	"<td>"+ pricePerRoom2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		/*if(tax1.containsAll(tax2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify tax amount</td>"
					+	"<td>"+ tax1 +"</td>"
					+ 	"<td>"+ tax2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify tax amount</td>"
					+	"<td>"+ tax1 +"</td>"
					+ 	"<td>"+ tax2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;*/
		
		if(status1.containsAll(status2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify status</td>"
					+	"<td>"+ status1 +"</td>"
					+ 	"<td>"+ status2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify status</td>"
					+	"<td>"+ status1 +"</td>"
					+ 	"<td>"+ status2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(hotel1.containsAll(hotel2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel name</td>"
					+	"<td>"+ hotel1 +"</td>"
					+ 	"<td>"+ hotel2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel name</td>"
					+	"<td>"+ hotel1 +"</td>"
					+ 	"<td>"+ hotel2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(address1.get(0).trim().toLowerCase().equals(address2.get(0).trim().toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel address</td>"
					+	"<td>"+ address1.get(0) +"</td>"
					+ 	"<td>"+ address2.get(0) +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel address</td>"
					+	"<td>"+ address1.get(0) +"</td>"
					+ 	"<td>"+ address2.get(0) +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		/*if(firstName1.containsAll(firstName2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify First name</td>"
					+	"<td>"+ firstName1 +"</td>"
					+ 	"<td>"+ firstName2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify First name</td>"
					+	"<td>"+ firstName1 +"</td>"
					+ 	"<td>"+ firstName2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;*/
		
		/*if(lstName1.containsAll(lstName2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
				+ 	"<td>Verify Last name</td>"
				+	"<td>"+ lstName1 +"</td>"
				+ 	"<td>"+ lstName2 +"</td>"
				+	"<td class='passed'>Passed</td></tr>");
			
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify last name</td>"
					+	"<td>"+ lstName1 +"</td>"
					+ 	"<td>"+ lstName2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}*/
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}
	
	public void ccresAgainstCon(resHotelAvailability resAvailabilityFinalRes, conhotel conHotel, StringBuffer ReportPrinter, String defaultCurrency, Map<String, String> currency)
	{
		int cnt = 1 ;
		currencyConveter	cc	= 	new currencyConveter();
		addProfitmarkup		apm	=	new addProfitmarkup();
		

			ReportPrinter.append("<span>(10) Pre reservation req against res</span>");
			
			ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");

			String[] hName = conHotel.getHotelName().split("View Cancellation Policy");
			try {
				if(resAvailabilityFinalRes.getHotelInfoname().equals(hName[0].trim()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify Hotel Name</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHotelInfoname() +"</td>"
							+ 	"<td>"+ conHotel.getHotelName() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify Hotel Name</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHotelInfoname() +"</td>"
							+ 	"<td>"+ conHotel.getHotelName() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				if(resAvailabilityFinalRes.getHoldername().toLowerCase().equals(conHotel.getCustomer().getFirstName()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify Customer name</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHoldername() +"</td>"
							+ 	"<td>"+ conHotel.getCustomer().getFirstName() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify Customer name</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHoldername() +"</td>"
							+ 	"<td>"+ conHotel.getCustomer().getFirstName() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
			} catch (Exception e) {
				// TODO: handle exception
			}
		
			try {
				if(resAvailabilityFinalRes.getHolderlastName().toLowerCase().equals(conHotel.getCustomer().getLastName()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify Customer last name</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHolderlastName() +"</td>"
							+ 	"<td>"+ conHotel.getCustomer().getLastName() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify Customer last name</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHolderlastName() +"</td>"
							+ 	"<td>"+ conHotel.getCustomer().getLastName() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				if(resAvailabilityFinalRes.getRoomAvailability().size() == (conHotel.getRoom().size()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify room count</td>"
							+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().size() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().size() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify room count</td>"
							+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().size() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().size() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		//	resAvailabilityFinalRes.geth
			ArrayList<String> roomtype1 	= new ArrayList<String>();
			ArrayList<String> roomtype2 	= new ArrayList<String>();
			ArrayList<String> boardtype1 	= new ArrayList<String>();
			ArrayList<String> boardtype2 	= new ArrayList<String>();
			ArrayList<String> cprtype1 		= new ArrayList<String>();
			ArrayList<String> cprtype2 		= new ArrayList<String>();
			ArrayList<String> adltCount1 	= new ArrayList<String>();
			ArrayList<String> adltCount2 	= new ArrayList<String>();
			ArrayList<String> childCount1 	= new ArrayList<String>();
			ArrayList<String> childCount2 	= new ArrayList<String>();
			ArrayList<String> firstName1 	= new ArrayList<String>();
			ArrayList<String> fisrtName2 	= new ArrayList<String>();
			ArrayList<String> lastName1 	= new ArrayList<String>();
			ArrayList<String> lastName2 	= new ArrayList<String>();
			ArrayList<String> title1 		= new ArrayList<String>();
			ArrayList<String> title2 		= new ArrayList<String>();
			for(int i = 0 ; i < resAvailabilityFinalRes.getRoomAvailability().size() ; i++)
			{
				try {
					System.out.println(resAvailabilityFinalRes.getHotelRoom().get(i).getRoom());
					System.out.println(conHotel.getRoom().get(i).getRoomType());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					roomtype1.add(resAvailabilityFinalRes.getHotelRoom().get(i).getRoom());
					roomtype2.add(conHotel.getRoom().get(i).getRoomType());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					boardtype1.add(resAvailabilityFinalRes.getHotelRoom().get(i).getBoard());
					boardtype2.add(conHotel.getRoom().get(i).getBoardType());
				} catch (Exception e) {
					// TODO: handle exception
				}
				String conValue = "";
				int pmValue = 0 ; 
				try {
					conValue	=	cc.convert(defaultCurrency, resAvailabilityFinalRes.getHotelRoom().get(i).getPrice(), conHotel.getCurrencyCode(), currency, resAvailabilityFinalRes.getCurrencyCode());
					pmValue		=	apm.profitMarkup(conValue, txtProperty.get("hb"));
					pmValue 	= 	pmValue + Integer.parseInt(conValue);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					cprtype1.add(String.valueOf(pmValue));
					cprtype2.add(conHotel.getRoom().get(i).getTotalRate());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					adltCount1.add(resAvailabilityFinalRes.getRoomAvailability().get(i).getAdultCount());
					adltCount2.add(conHotel.getRoom().get(i).getAdultCount());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				try {
					childCount1.add(resAvailabilityFinalRes.getRoomAvailability().get(i).getChildCount());
					childCount2.add(conHotel.getRoom().get(i).getChildCount());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				for(int j = 0 ; j < resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().size() ; j++)
				{
					
					try {
						System.out.println(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getName());
						System.out.println(conHotel.getOccypancy().get(i).get(j).getFirstName());
					} catch (Exception e) {
						// TODO: handle exception
					}
					try {
						firstName1.add(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getName());
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						lastName1.add(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getLastName());
						lastName2.add(conHotel.getOccypancy().get(i).get(j).getLastName());
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						title1.add(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getCustomerType());
						title2.add(conHotel.getOccypancy().get(i).get(j).getTitle());
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
				
				//if(resAvailabilityFinalRes.getRoomAvailability().get(i).getAdultCount().equals(conHotel.getOccypancy().))
			}
			try {
				for(int i = 0 ; i < conHotel.getOccypancy().get(i).size() ; i++)
				{
					try {
						fisrtName2.add(conHotel.getOccypancy().get(0).get(i).getFirstName());
						System.out.println(conHotel.getOccypancy().get(0).get(i).getFirstName());
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			System.out.println("test");
			if(roomtype1.containsAll(roomtype2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ roomtype1 +"</td>"
						+ 	"<td>"+ roomtype2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ roomtype1 +"</td>"
						+ 	"<td>"+ roomtype2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(boardtype1.containsAll(boardtype2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify board type</td>"
						+	"<td>"+ boardtype1 +"</td>"
						+ 	"<td>"+ boardtype2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify board type</td>"
						+	"<td>"+ boardtype1 +"</td>"
						+ 	"<td>"+ boardtype2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(cprtype1.containsAll(cprtype2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify cost per room</td>"
						+	"<td>"+ cprtype1 +"</td>"
						+ 	"<td>"+ cprtype2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify cost per room</td>"
						+	"<td>"+ cprtype1 +"</td>"
						+ 	"<td>"+ cprtype2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(adltCount1.containsAll(adltCount2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify adult count</td>"
						+	"<td>"+ adltCount1 +"</td>"
						+ 	"<td>"+ adltCount2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify adult count</td>"
						+	"<td>"+ adltCount1 +"</td>"
						+ 	"<td>"+ adltCount2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(childCount1.containsAll(childCount2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify child count</td>"
						+	"<td>"+ childCount1 +"</td>"
						+ 	"<td>"+ childCount2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify child count</td>"
						+	"<td>"+ childCount1 +"</td>"
						+ 	"<td>"+ childCount2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(firstName1.containsAll(fisrtName2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify first name</td>"
						+	"<td>"+ firstName1 +"</td>"
						+ 	"<td>"+ fisrtName2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify first name</td>"
						+	"<td>"+ firstName1 +"</td>"
						+ 	"<td>"+ fisrtName2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(lastName1.containsAll(lastName2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify last name</td>"
						+	"<td>"+ lastName1 +"</td>"
						+ 	"<td>"+ lastName2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify last name</td>"
						+	"<td>"+ lastName1 +"</td>"
						+ 	"<td>"+ lastName2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(title1.containsAll(title2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify title</td>"
						+	"<td>"+ title1 +"</td>"
						+ 	"<td>"+ title2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify title</td>"
						+	"<td>"+ title1 +"</td>"
						+ 	"<td>"+ title2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			try {
				String conValue	=	cc.convert(defaultCurrency, resAvailabilityFinalRes.getTotalAmount(), conHotel.getCurrencyCode(), currency, resAvailabilityFinalRes.getCurrencyCode());
				int	pmValue		=	apm.profitMarkup(conValue, txtProperty.get("hb"));
				pmValue = pmValue + Integer.parseInt(conValue);
				if(String.valueOf(pmValue).equals(conHotel.getTotal()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify total</td>"
							+	"<td>"+ pmValue +"</td>"
							+ 	"<td>"+ conHotel.getTotal() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify total</td>"
							+	"<td>"+ pmValue +"</td>"
							+ 	"<td>"+ conHotel.getTotal() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
			} catch (Exception e) {
				// TODO: handle exception
			}
				
				ReportPrinter.append("</table>");
				ReportPrinter.append	("<b></b><br>"
	                    +	 "<hr size=4>");
		
		
	}
	
	public void resAgainstCon(resHotelAvailability resAvailabilityFinalRes, conhotel conHotel, StringBuffer ReportPrinter, String defaultCurrency, Map<String, String> currency)
	{
		int cnt = 1 ;
		currencyConveter	cc	= 	new currencyConveter();
		addProfitmarkup		apm	=	new addProfitmarkup();
		
		if((resAvailabilityFinalRes.getHotelInfoname().equals("") || resAvailabilityFinalRes.getHotelInfoname().equals(null)) && (!conHotel.getError().equals("") && !conHotel.getErrorType().equals("")))
		{
			ReportPrinter.append("<span>Confirmation page error</span>");
			
			ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Error type</th>"
				+ "<th>Error</t></tr>");
			
			ReportPrinter.append	(	"<tr><td>"+ conHotel.getErrorType() +"</td>"
					+ 	"<td>"+ conHotel.getError() +"</td></tr></table>");
		
		}
		else
		{
			ReportPrinter.append("<span>(8) Pre reservation req against res</span>");
			
			ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");

			String[] hName = conHotel.getHotelName().split("View Cancellation Policy");
			if(resAvailabilityFinalRes.getHotelInfoname().equals(hName[0].trim()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Hotel Name</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelInfoname() +"</td>"
						+ 	"<td>"+ conHotel.getHotelName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Hotel Name</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelInfoname() +"</td>"
						+ 	"<td>"+ conHotel.getHotelName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			if(resAvailabilityFinalRes.getHoldername().toLowerCase().equals(conHotel.getCustomer().getFirstName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Customer name</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHoldername() +"</td>"
						+ 	"<td>"+ conHotel.getCustomer().getFirstName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Customer name</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHoldername() +"</td>"
						+ 	"<td>"+ conHotel.getCustomer().getFirstName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			if(resAvailabilityFinalRes.getHolderlastName().toLowerCase().equals(conHotel.getCustomer().getLastName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Customer last name</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHolderlastName() +"</td>"
						+ 	"<td>"+ conHotel.getCustomer().getLastName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Customer last name</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHolderlastName() +"</td>"
						+ 	"<td>"+ conHotel.getCustomer().getLastName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		//	resAvailabilityFinalRes.geth
			for(int i = 0 ; i < resAvailabilityFinalRes.getRoomAvailability().size() ; i++)
			{
				if(resAvailabilityFinalRes.getRoomAvailability().size() == (conHotel.getRoom().size()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify room count</td>"
							+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().size() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().size() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify room count</td>"
							+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().size() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().size() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				try {
					System.out.println(resAvailabilityFinalRes.getHotelRoom().get(i).getRoom());
					System.out.println(conHotel.getRoom().get(i).getRoomType());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				if(resAvailabilityFinalRes.getHotelRoom().get(i).getRoom().equals(conHotel.getRoom().get(i).getRoomType()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify room type</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getRoom() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getRoomType() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify room type</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getRoom() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getRoomType() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				if(resAvailabilityFinalRes.getHotelRoom().get(i).getBoard().equals(conHotel.getRoom().get(i).getBoardType()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify board type</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getBoard() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getBoardType() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify board type</td>"
							+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getBoard() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getBoardType() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				String conValue	=	cc.convert(defaultCurrency, resAvailabilityFinalRes.getHotelRoom().get(i).getPrice(), conHotel.getCurrencyCode(), currency, resAvailabilityFinalRes.getCurrencyCode());
				int	 pmValue	=	apm.profitMarkup(conValue, txtProperty.get("hb"));
				if(String.valueOf(pmValue).equals(conHotel.getRoom().get(i).getTotalRate()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify cost per room</td>"
							+	"<td>"+ pmValue +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getTotalRate() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify cost per room</td>"
							+	"<td>"+ pmValue +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getTotalRate() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				if(resAvailabilityFinalRes.getRoomAvailability().get(i).getAdultCount().equals(conHotel.getRoom().get(i).getAdultCount()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify adult count</td>"
							+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getAdultCount() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getAdultCount() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify adult count</td>"
							+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getAdultCount() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getAdultCount() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				if(resAvailabilityFinalRes.getRoomAvailability().get(i).getChildCount().equals(conHotel.getRoom().get(i).getChildCount()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify child count</td>"
							+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getChildCount() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getChildCount() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify child count</td>"
							+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getChildCount() +"</td>"
							+ 	"<td>"+ conHotel.getRoom().get(i).getChildCount() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				for(int j = 0 ; j < resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().size() ; j++)
				{
					
					try {
						System.out.println(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getName());
						System.out.println(conHotel.getOccypancy().get(i).get(j).getFirstName());
					} catch (Exception e) {
						// TODO: handle exception
					}
					System.out.println(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getName());
					System.out.println(conHotel.getOccypancy2().get(i).getFirstName());
					if(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getName().contains(conHotel.getOccypancy2().get(i).getFirstName()))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify First Name</td>"
								+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getName() +"</td>"
								+ 	"<td>"+ conHotel.getOccypancy2().get(i).getFirstName() +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify First Name</td>"
								+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getName() +"</td>"
								+ 	"<td>"+ conHotel.getOccypancy2().get(i).getFirstName() +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
					cnt++;
					if(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getLastName().equals(conHotel.getOccypancy2().get(i).getLastName()))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify Last Name</td>"
								+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getLastName() +"</td>"
								+ 	"<td>"+ conHotel.getOccypancy2().get(i).getLastName() +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify Last Name</td>"
								+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getLastName() +"</td>"
								+ 	"<td>"+ conHotel.getOccypancy2().get(i).getLastName() +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
					cnt++;
					if(resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getCustomerType().equals(conHotel.getOccypancy2().get(i).getTitle()))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify Title</td>"
								+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getCustomerType() +"</td>"
								+ 	"<td>"+ conHotel.getOccypancy2().get(i).getTitle() +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify Title</td>"
								+	"<td>"+ resAvailabilityFinalRes.getRoomAvailability().get(i).getGuest().get(j).getCustomerType() +"</td>"
								+ 	"<td>"+ conHotel.getOccypancy2().get(i).getTitle()  +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
					cnt++;
				}
				
				//if(resAvailabilityFinalRes.getRoomAvailability().get(i).getAdultCount().equals(conHotel.getOccypancy().))
			}
				String conValue	=	cc.convert(defaultCurrency, resAvailabilityFinalRes.getTotalAmount(), conHotel.getCurrencyCode(), currency, resAvailabilityFinalRes.getCurrencyCode());
				int	pmValue		=	apm.profitMarkup(conValue, txtProperty.get("hb"));
				if(String.valueOf(pmValue).equals(conHotel.getTotal()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify total</td>"
							+	"<td>"+ pmValue +"</td>"
							+ 	"<td>"+ conHotel.getTotal() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify total</td>"
							+	"<td>"+ pmValue +"</td>"
							+ 	"<td>"+ conHotel.getTotal() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				ReportPrinter.append("</table>");
				ReportPrinter.append	("<b></b><br>"
		                +	 "<hr size=4>");
		}
		
	}
}
