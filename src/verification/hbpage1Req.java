package verification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import readAndWrite.readSearchDetails;
import readXml.hbPage1Req;
import readXml.resHotelAvailability;

public class hbpage1Req {

	private static Map<String, String>  txtProperty = new HashMap<String, String>();
	public void verify(hbPage1Req page1Req, readSearchDetails rsd, StringBuffer ReportPrinter, String supplier)
	{
		ReportPrinter.append("<span>(1.4) Search Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
		String[] adultCount = rsd.getRoom().getAdultCount().split(",");
		int cnt = 1;
		System.out.println(page1Req.getHotelId());
		for(int i  = 0 ; i < adultCount.length ; i++)
		{
			//System.out.println(adultCount[i]);
			//System.out.println(page1Req.getGuest().get(i).getAdultCount());
			if(page1Req.getGuest().get(i).getAdultCount().equals(adultCount[i]))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify adult count</td>"
						+	"<td>"+ page1Req.getGuest().get(i).getAdultCount() +"</td>"
						+ 	"<td>"+ adultCount[i] +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify adult Count</td>"
						+	"<td>"+ page1Req.getGuest().get(i).getAdultCount() +"</td>"
						+ 	"<td>"+ adultCount[i] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		}
		
		String[] childCount = rsd.getRoom().getChildCount().split(",");
		for(int i = 0 ; i < childCount.length ; i++)
		{
			//System.out.println(childCount[i]);
			//System.out.println(page1Req.getGuest().get(i).getChildCount());
			if(page1Req.getGuest().get(i).getChildCount().equals(childCount[i]))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Child Count</td>"
						+	"<td>"+ page1Req.getGuest().get(i).getChildCount() +"</td>"
						+ 	"<td>"+ childCount[i] +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Child Count</td>"
						+	"<td>"+ page1Req.getGuest().get(i).getChildCount() +"</td>"
						+ 	"<td>"+ childCount[i] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		}
		
		
		
		
		String day = dateFormat(supplier, rsd.getCheckIn());
		if(page1Req.getCheckin().equals(day))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkin Date</td>"
					+	"<td>"+ page1Req.getCheckin() +"</td>"
					+ 	"<td>"+ day +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkin Date</td>"
					+	"<td>"+ page1Req.getCheckin() +"</td>"
					+ 	"<td>"+ day +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
				day = dateFormat(supplier, rsd.getCheckout());
		if(page1Req.getCheckout().equals(day))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkout Date</td>"
					+	"<td>"+ page1Req.getCheckout() +"</td>"
					+ 	"<td>"+ day +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkout Date</td>"
					+	"<td>"+ page1Req.getCheckout() +"</td>"
					+ 	"<td>"+ day +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		//System.out.println(page1Req.getDestination());
		//System.out.println(rsd.getDestination());
		if((rsd.getDestination().toLowerCase().contains(page1Req.getDestination().toLowerCase())))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Destination</td>"
					+	"<td>"+ page1Req.getDestination() +"</td>"
					+ 	"<td>"+ rsd.getDestination() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Destination</td>"
					+	"<td>"+ page1Req.getDestination() +"</td>"
					+ 	"<td>"+ rsd.getDestination() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		for(int i = 0 ; i < rsd.getRoom().getAge().length() ; i++)
		{
			String ages = rsd.getRoom().getAge().replace(",", " ");
			ages	= ages.trim();
			//System.out.println(ages);
			String[] ages2 = ages.split(" ");
			//String[] age = rsd.getRoom().getAge().split(",");
			try {
				if(page1Req.getGuest().get(0).getCus().get(i).getAge().equals(ages2[i]))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify age</td>"
							+	"<td>"+ page1Req.getGuest().get(0).getCus().get(i).getAge() +"</td>"
							+ 	"<td>"+ ages2[i] +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify age</td>"
							+	"<td>"+ page1Req.getGuest().get(0).getCus().get(i).getAge() +"</td>"
							+ 	"<td>"+ ages2[i] +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
			} 
			catch (Exception e) {
				break;
			}
			
			
		}
		ReportPrinter.append("</table>");
		
		ReportPrinter.append	("<b></b><br>"
                             +	 "<hr size=4>");
	}
	
	public String dateFormat(String supplier, String rsdDate)
	{
		String day = null;
		if(supplier.equals("hotelbeds"))
		{
			String[] checkin 	= rsdDate.split("/");
			String date 		= checkin[1];
			String month 		= checkin[0];
			String year 		= checkin[2];
			day 				= year.concat(month).concat(date);
			//System.out.println(day);
		}
		else if(supplier.equals("inv13"))
		{
			String[] checkin 	= rsdDate.split("/");
			String date 		= checkin[1];
			String month 		= checkin[0];
			String year 		= checkin[2];
			day 				= year.concat("-").concat(month).concat("-").concat(date);
		}
		return day;
	}
	
	public void inv13SearchReqAgainstRes(StringBuffer ReportPrinter, hbPage1Req page1Req, ArrayList<resHotelAvailability> resAvailability)
	{
		ReportPrinter.append("<tr><span color=red>(3)Search Verification xml request aganst response</span></tr>");
		ReportPrinter.append("<p></p>");
		
		int cnt = 1;
		//System.out.println(resAvailability.size());
		//System.out.println(resAvailability.get(0).getHotelRoom().size());
		//System.out.println(resAvailability.get(1).getHotelRoom().size());
		for(int k = 0 ; k < resAvailability.size() ; k++)
		{
			/*System.out.println(resAvailability.get(0).getHotelRoom().get(0).getAvailableRoom().size());
			System.out.println(resAvailability.get(0).getHotelRoom().get(0).getAvailableRoom().get(0).getGuest().get(0).getCustomerAge());
			System.out.println(resAvailability.get(1).getHotelRoom().size());*/
			for(int i = 0 ; i < resAvailability.get(k).getHotelRoom().size() ; i++)
			{
				
				ReportPrinter.append("<tr><span>"+ resAvailability.get(k).getHotelName() +"</span></tr>");
				ReportPrinter.append("<table border=1 style=width:100%>"
						+ "<tr><th>Test Case</th>"
						+ "<th>Test Description</th>"
						+ "<th>Expected Result</th>"
						+ "<th>Actual Result</th>"
						+ "<th>Test Status</th></tr>");
				//System.out.println(page1Req.getCheckin());
				//System.out.println(resAvailability.get(k).getHotelRoom().get(i).getDateTimeFromDate());
				if(resAvailability.get(k).getHotelRoom().get(i).getDateTimeFromDate().contains(page1Req.getCheckin()))
				{
					//System.out.println(resAvailability.get(k).getHotelName());
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify checkin date</td>"
							+	"<td>"+ page1Req.getCheckin() +"</td>"
							+ 	"<td>"+ resAvailability.get(k).getHotelRoom().get(i).getDateTimeFromDate() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ page1Req.getCheckin() +"</td>"
						+ 	"<td>"+ resAvailability.get(k).getHotelRoom().get(i).getDateTimeFromDate() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				ArrayList<String> chldcnt1 = new ArrayList<String>();
				ArrayList<String> chldcnt2 = new ArrayList<String>();
				ArrayList<String> adltcnt1 = new ArrayList<String>();
				ArrayList<String> adltcnt2 = new ArrayList<String>();
				ArrayList<String> city1 = new ArrayList<String>();
				ArrayList<String> city2 = new ArrayList<String>();
				for(int j = 0, x = page1Req.getGuest().size()-1  ; j < page1Req.getGuest().size() ; j++, x--)
				{
					//System.out.println("j : " + j);
					try {
							chldcnt1.add(page1Req.getGuest().get(j).getChildCount());
							chldcnt2.add(resAvailability.get(k).getHotelRoom().get(i).getAvailableRoom().get(x).getChildCount());
							
							adltcnt1.add(page1Req.getGuest().get(j).getAdultCount());
							adltcnt2.add(resAvailability.get(k).getHotelRoom().get(i).getAvailableRoom().get(x).getAdultCount());
							
							city1.add(page1Req.getDestination());
							city2.add(resAvailability.get(k).getLocation());
						
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				if(chldcnt1.containsAll(chldcnt2))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify children count</td>"
							+	"<td>"+ chldcnt1 +"</td>"
							+ 	"<td>"+ chldcnt2 +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify children count</td>"
							+	"<td>"+ chldcnt1 +"</td>"
							+ 	"<td>"+ chldcnt2 +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				if(adltcnt1.containsAll(adltcnt2))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify adult count</td>"
							+	"<td>"+ adltcnt1 +"</td>"
							+ 	"<td>"+ adltcnt2 +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify adult count</td>"
							+	"<td>"+ adltcnt1 +"</td>"
							+ 	"<td>"+ adltcnt2 +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				if(city1.containsAll(city2))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify adult destination</td>"
							+	"<td>"+ city1 +"</td>"
							+ 	"<td>"+ city2 +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify adult destination</td>"
							+	"<td>"+ city1 +"</td>"
							+ 	"<td>"+ city2 +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				ReportPrinter.append("</table>");
				ReportPrinter.append	("<b></b><br>"
                        +	 "<hr size=4>");
			}
		}
	}
	
	public void searchReqAgainstRes(StringBuffer ReportPrinter, hbPage1Req page1Req, resHotelAvailability resAvailability)
	{
		ReportPrinter.append("<tr><span color=red>(4) Search Verification xml request aganst response</span></tr>");
		ReportPrinter.append("<p></p>");
		
		
		int cnt = 1 ;
		for(int i = 0 ; i < resAvailability.getHotelDetails().size() ; i++)
		{
			
			ReportPrinter.append("<tr><span>"+ resAvailability.getHotelDetails().get(i).getHotelName() +"</span></tr>");
			ReportPrinter.append("<table border=1 style=width:100%>"
					+ "<tr><th>Test Case</th>"
					+ "<th>Test Description</th>"
					+ "<th>Expected Result</th>"
					+ "<th>Actual Result</th>"
					+ "<th>Test Status</th></tr>");
			//System.out.println(page1Req.getCheckin());
			//System.out.println(resAvailability.getHotelDetails().get(i).getCheckin());
			if(page1Req.getCheckin().equals(resAvailability.getHotelDetails().get(i).getCheckin()))
			{
				//System.out.println(resAvailability.getHotelDetails().get(i).getHotelName());
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ page1Req.getCheckin() +"</td>"
						+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getCheckin() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ page1Req.getCheckin() +"</td>"
						+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getCheckin() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(page1Req.getCheckout().equals(resAvailability.getHotelDetails().get(0).getCheckout()))
			{
				//System.out.println(page1Req.getCheckout());
				//System.out.println(resAvailability.getHotelDetails().get(i).getCheckout());
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkout date</td>"
						+	"<td>"+ page1Req.getCheckout() +"</td>"
						+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getCheckout() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkout date</td>"
						+	"<td>"+ page1Req.getCheckout() +"</td>"
						+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getCheckout() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			for(int j = 0 ; j < page1Req.getGuest().size() ; j++)
			{

				//System.out.println("j : " + j);
				try {
						if(page1Req.getGuest().get(j).getChildCount().trim().equals(resAvailability.getHotelDetails().get(i).getAvailRoom().get(j).getChildCount().trim()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify child count</td>"
									+	"<td>"+page1Req.getGuest().get(j).getChildCount() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getAvailRoom().get(j).getChildCount().trim() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify child count</td>"
									+	"<td>"+ page1Req.getGuest().get(j).getChildCount() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getAvailRoom().get(j).getChildCount().trim() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
						if(page1Req.getGuest().get(j).getAdultCount().trim().equals(resAvailability.getHotelDetails().get(i).getAvailRoom().get(j).getAdultCount().trim()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify adult count</td>"
									+	"<td>"+page1Req.getGuest().get(j).getAdultCount() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getAvailRoom().get(j).getAdultCount().trim() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify adult count</td>"
									+	"<td>"+ page1Req.getGuest().get(j).getAdultCount() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getAvailRoom().get(j).getAdultCount().trim() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
						if(page1Req.getDestination().equals(resAvailability.getHotelDetails().get(i).getDestinationCode()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify Destination</td>"
									+	"<td>"+page1Req.getDestination() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getDestinationCode() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify Destination</td>"
									+	"<td>"+page1Req.getDestination() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(i).getDestinationCode() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
						
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				
			
			}
			ReportPrinter.append("</table>");
			ReportPrinter.append	("<b></b><br>"
                    +	 "<hr size=4>");
		}
		
		
		
	/*	System.out.println(page1Req.getGuest().size());
		for(int i = 0 ; i < page1Req.getGuest().size() ; i++)
		{
			System.out.println("Hotel count : " + resAvailability.getHotelDetails().size());
			for(int j = 0 ; j < resAvailability.getHotelDetails().size() ; j ++)
			{
				System.out.println("j : " + j);
				try {
						if(page1Req.getGuest().get(i).getChildCount().trim().equals(resAvailability.getHotelDetails().get(j).getAvailRoom().get(i).getChildCount().trim()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getHotelName() +" Verify child count</td>"
									+	"<td>"+page1Req.getGuest().get(i).getChildCount() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getAvailRoom().get(i).getChildCount().trim() +"</td>"
									+	"<td>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getHotelName() +" Verify child count</td>"
									+	"<td>"+ page1Req.getGuest().get(i).getChildCount() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getAvailRoom().get(i).getChildCount().trim() +"</td>"
									+	"<td>Failed</td></tr>");
						}
						cnt++;
						if(page1Req.getGuest().get(i).getAdultCount().trim().equals(resAvailability.getHotelDetails().get(j).getAvailRoom().get(i).getAdultCount().trim()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getHotelName() +" Verify adult count</td>"
									+	"<td>"+page1Req.getGuest().get(i).getAdultCount() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getAvailRoom().get(i).getAdultCount().trim() +"</td>"
									+	"<td>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getHotelName() +" Verify adult count</td>"
									+	"<td>"+ page1Req.getGuest().get(i).getAdultCount() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getAvailRoom().get(i).getAdultCount().trim() +"</td>"
									+	"<td>Failed</td></tr>");
						}
						
						if(page1Req.getDestination().equals(resAvailability.getHotelDetails().get(0).getDestinationCode()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getHotelName() +" Verify Destination</td>"
									+	"<td>"+page1Req.getDestination() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getDestinationCode() +"</td>"
									+	"<td>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getHotelName() +" Verify Destination</td>"
									+	"<td>"+page1Req.getDestination() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getDestinationCode() +"</td>"
									+	"<td>Failed</td></tr>");
						}
						cnt++;
						
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				
			}
		}*/
		
		
	}
}
