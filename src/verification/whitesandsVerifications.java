package verification;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import readAndWrite.readHotelResults;
import readXml.whiteSandsAvailabilityRes;

public class whitesandsVerifications {
	
	private static Map<String, String>  	txtProperty 		= new HashMap<String, String>();
	public void verifyHotelList(StringBuffer ReportPrinter, ArrayList<readHotelResults> hotelList, ArrayList<whiteSandsAvailabilityRes> resAvailability, String defaultCurrency, Map<String, String> currency, String checkin, String nyts)
	{
		Properties properties 	= new Properties();
		try 
		{
			FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitmarkUps.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value 		= properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		
		ReportPrinter.append("<span>(1) Verify results (Hotel List)</span>");
		ReportPrinter.append("<br>");
		int cnt = 1;
		System.out.println(resAvailability.size());
		System.out.println(hotelList.size());
		for(int i = 0 ; i < hotelList.size() ; i++)
		{
			if(hotelList.get(i).getSupplier().equals("whitesands"))
			{
				for(int j = 0 ; j < resAvailability.size() ; j++)
				{
					if(resAvailability.get(j).getHotelName().trim().equalsIgnoreCase(hotelList.get(i).getTitle().trim()))	
					{
						System.out.println("test");
						for(int x = j ; x > 0 ; x++)
						{
							if(hotelList.get(i).getTitle().trim().equalsIgnoreCase(resAvailability.get(x).getHotelName().trim()))
							{
								System.out.println(hotelList.get(i).getTitle());
								ArrayList<whiteSandsAvailabilityRes> ws = new ArrayList<whiteSandsAvailabilityRes>();
								ws.add(resAvailability.get(x));
								System.out.println(ws.size());
							}
							else
							{
								break;
							}
						}
						ReportPrinter.append("<table border=1 style=width:100%>"
								+ "<tr><th>Test Case</th>"
								+ "<th>Test Description</th>"
								+ "<th>Expected Result</th>"
								+ "<th>Actual Result</th>"
								+ "<th>Test Status</th></tr>");
						System.out.println("hote list test -- 1");
						if(hotelList.get(i).getTitle().trim().equalsIgnoreCase(resAvailability.get(j).getHotelName()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ resAvailability.get(j).getHotelName() +"</td>"
									+ 	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ resAvailability.get(j).getHotelName() +"</td>"
									+ 	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
						
						currencyConveter cc = new currencyConveter();
						String[] cuscc = hotelList.get(i).getRoom().get(0).getRate().split(" ");
						String conValue = cc.convert(defaultCurrency, resAvailability.get(j).getPrice(), cuscc[0], currency, "AED");
						addProfitmarkup apm = new addProfitmarkup();
						int apmValue = apm.profitMarkup(conValue, txtProperty.get("whitesands"));
						String apmVal = String.valueOf(apmValue);
						System.out.println(resAvailability.get(j).getPrice());
						System.out.println(apmVal);
						String[] webValue = hotelList.get(i).getTotalCost().split(" ");
						System.out.println(webValue[0]);
						System.out.println(webValue[1]);
						if(apmVal.equals(hotelList.get(i).getTotalCost()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify the price</td>"
									+	"<td>"+ apmVal +"</td>"
									+ 	"<td>"+ hotelList.get(i).getTotalCost() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify the price</td>"
									+	"<td>"+ apmVal +"</td>"
									+ 	"<td>"+ hotelList.get(i).getTotalCost() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
						System.out.println(hotelList.get(i).getAvailability());
						if(hotelList.get(i).getAvailability().equalsIgnoreCase("available_label") && resAvailability.get(j).getAvailability().equalsIgnoreCase("1"))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify the availability</td>"
									+	"<td>"+ resAvailability.get(j).getAvailability() +"</td>"
									+ 	"<td>"+ hotelList.get(i).getAvailability() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify the availability</td>"
									+	"<td>"+ resAvailability.get(j).getAvailability() +"</td>"
									+ 	"<td>"+ hotelList.get(i).getAvailability() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
						String[] fromdate = resAvailability.get(j).getFromDate().split("/");
						String date = fromdate[1].concat("/").concat(fromdate[2]).concat("/").concat(fromdate[0]);
						if(date.trim().equalsIgnoreCase(checkin.trim()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify checkin date</td>"
									+	"<td>"+ checkin +"</td>"
									+ 	"<td>"+ date +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify checkin date</td>"
									+	"<td>"+ checkin +"</td>"
									+ 	"<td>"+ date +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
						if(hotelList.get(i).getRoom().get(0).getRatePlan().equalsIgnoreCase(resAvailability.get(j).getMealname()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify rate plan</td>"
									+	"<td>"+ resAvailability.get(j).getMealname() +"</td>"
									+ 	"<td>"+ hotelList.get(i).getRoom().get(0).getRatePlan() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify rate plan</td>"
									+	"<td>"+ resAvailability.get(j).getMealname() +"</td>"
									+ 	"<td>"+ hotelList.get(i).getRoom().get(0).getRatePlan() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
						ReportPrinter.append("</table>");
						
					}
				}
			}
			
		}
		
	}

}
