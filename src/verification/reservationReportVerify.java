package verification;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import readAndWrite.profitAndLostReport;
import readAndWrite.reservationReport;
import readConfirmation.conhotel;
import readXml.canDeadRes;
import readXml.resHotelAvailability;
import readXml.resavailableRoom;
import reports.customerConfirmationEmail;
import reports.voucherMail;

public class reservationReportVerify {
	
	
	public void inv13customerConfirmationmail(customerConfirmationEmail cusCon, resHotelAvailability res, conhotel hotel, StringBuffer ReportPrinter, Map<String, String> currency, String defaultCurrency, Map<String, String> profitMarkUp)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(13) Customer confirmation report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		String[] 	temp 	= 	null;
		try {
			temp	=	cusCon.getBookingReference().split(":");
			temp	=	temp[1].trim().split(" ");
		} catch (Exception e) {
			// TODO: handle exception
		}
					
		//System.out.println(temp[3]);
		//System.out.println(temp[5]);
		//System.out.println(hotel.getCustomer().getFirstName());
		int cnt = 1;
		try {
			String name1 = hotel.getCustomer().getFirstName().concat(" ").concat(hotel.getCustomer().getLastName());
			String[] name = cusCon.getBookingReference().split(" ");
			String name2 = name[7].concat(" ").concat(name[9]);
			if(name1.contains(name2))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify customer name</td>"
						+	"<td>"+ name1 +"</td>"
						+ 	"<td>"+ name2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify customer name</td>"
						+	"<td>"+ name1 +"</td>"
						+ 	"<td>"+ name2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			String[] bookingNumber = cusCon.getBookingnumber().split(":");
			if(hotel.getReservationNumber().equals(bookingNumber[1].trim()))
			{
				ReportPrinter.append	(	"<tr><td>2</td>"
						+ 	"<td>Verify booking number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ bookingNumber[1].trim() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>2</td>"
						+ 	"<td>Verify booking number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ bookingNumber[1].trim() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		try {
			cusCon.setCheckIn(cusCon.getCheckIn().replace(":", "").trim());
			cusCon.setCheckIn(formatdate2(cusCon.getCheckIn()));
			cusCon.setCheckOut(cusCon.getCheckOut().replace(":", "").trim());
			cusCon.setCheckOut(formatdate2(cusCon.getCheckOut()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
			try {
				if(res.getHotelDetails().get(0).getFromDate().contains(cusCon.getCheckIn()))
				{
					ReportPrinter.append	(	"<tr><td>3</td>"
							+ 	"<td>Verify checkin date</td>"
							+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
							+ 	"<td>"+ cusCon.getCheckIn() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>3</td>"
							+ 	"<td>Verify checkin date</td>"
							+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
							+ 	"<td>"+ cusCon.getCheckIn() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				String mailDate = cusCon.getCheckOut().replace(":", "").trim();
				String[] conDate = res.getHotelDetails().get(0).getToDate().split("T");
				if(conDate[0].equals(mailDate))
				{
					ReportPrinter.append	(	"<tr><td>4</td>"
							+ 	"<td>Verify checkin date</td>"
							+	"<td>"+ conDate[0] +"</td>"
							+ 	"<td>"+ mailDate +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>4</td>"
							+ 	"<td>Verify checkin date</td>"
							+	"<td>"+ conDate[0] +"</td>"
							+ 	"<td>"+ mailDate +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				if(cusCon.getStarCategory().contains(res.getHotelDetails().get(0).getStarLevel()))
				{
					ReportPrinter.append	(	"<tr><td>5</td>"
							+ 	"<td>Verify star category</td>"
							+	"<td>"+ res.getHotelDetails().get(0).getStarLevel() +"</td>"
							+ 	"<td>"+ cusCon.getStarCategory() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>5</td>"
							+ 	"<td>Verify star category</td>"
							+	"<td>"+ res.getHotelDetails().get(0).getStarLevel() +"</td>"
							+ 	"<td>"+ cusCon.getStarCategory() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				if(cusCon.getBookingStatus().toLowerCase().contains(res.getHotelDetails().get(0).getStatus().toLowerCase()))
				{
					ReportPrinter.append	(	"<tr><td>6</td>"
							+ 	"<td>Verify booking status</td>"
							+	"<td>"+ res.getHotelDetails().get(0).getStatus() +"</td>"
							+ 	"<td>"+ cusCon.getBookingStatus() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>6</td>"
							+ 	"<td>Verify booking status</td>"
							+	"<td>"+ res.getHotelDetails().get(0).getStatus() +"</td>"
							+ 	"<td>"+ cusCon.getBookingStatus() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
				//System.out.println(resAvailabilityFinalRes.getHotelDetails().get(i).getRoomType());
		
		for(int i = 0 ; i < res.getHotelDetails().size() ; i++)
		{
			try {
				System.out.println(res.getHotelDetails().get(i).getRoomType());
				System.out.println(cusCon.getRoom().get(i).getRoomType());
				if(cusCon.getRoom().get(i).getRoomType().equals(res.getHotelDetails().get(i).getRoomType()))
				{
					ReportPrinter.append	(	"<tr><td>7</td>"
							+ 	"<td>Verify room type</td>"
							+	"<td>"+ res.getHotelDetails().get(i).getRoomType() +"</td>"
							+ 	"<td>"+ cusCon.getRoom().get(i).getRoomType() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>7</td>"
							+ 	"<td>Verify room type</td>"
							+	"<td>"+ res.getHotelDetails().get(i).getRoomType() +"</td>"
							+ 	"<td>"+ cusCon.getRoom().get(i).getRoomType() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				if(hotel.getRoom().get(i).getBedType().equals(cusCon.getRoom().get(i).getBedType()))
				{
					ReportPrinter.append	(	"<tr><td>8</td>"
							+ 	"<td>Verify bed type</td>"
							+	"<td>"+ hotel.getRoom().get(i).getBedType() +"</td>"
							+ 	"<td>"+ cusCon.getRoom().get(i).getBedType() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>8</td>"
							+ 	"<td>Verify bed type</td>"
							+	"<td>"+ hotel.getRoom().get(i).getBedType() +"</td>"
							+ 	"<td>"+ cusCon.getRoom().get(i).getBedType() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		currencyConveter cc = new currencyConveter();
		addProfitmarkup apm = new addProfitmarkup();
		String 	total = "";
		try {
			total = cc.convert(defaultCurrency, res.getFinalPrice(), cusCon.getCurrencyCode(), currency, res.getFinalcurrency());
			int total1 = apm.profitMarkup(total, profitMarkUp.get("inv13"));
			total = String.valueOf(Integer.parseInt(total) + total1);
			String[] sub = cusCon.getSubTotal().split("\\.");
			String sub1 = sub[0].replace(",", "");
			if(res.getFinalPrice().equals(sub1))
			{
				ReportPrinter.append	(	"<tr><td>9</td>"
						+ 	"<td>Verify final price</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ sub1 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>9</td>"
						+ 	"<td>Verify final price</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ sub1 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");	
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = cusCon.getFirstName().split(":");
			if(temp[1].trim().equals(hotel.getCustomer().getFirstName()))
			{
				ReportPrinter.append	(	"<tr><td>10</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ hotel.getCustomer().getFirstName()+"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>10</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ hotel.getCustomer().getFirstName()+"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = cusCon.getLastName().split(":");
			if(temp[1].trim().equals(hotel.getCustomer().getLastName()))
			{
				ReportPrinter.append	(	"<tr><td>11</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ hotel.getCustomer().getLastName()+"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>11</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ hotel.getCustomer().getLastName()+"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = cusCon.getAddress1().split(":");
			if(temp[1].trim().equals(hotel.getCustomer().getAddress()))
			{
				ReportPrinter.append	(	"<tr><td>12</td>"
						+ 	"<td>Verify customer address</td>"
						+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>12</td>"
						+ 	"<td>Verify customer address</td>"
						+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			temp = cusCon.getCity().split(":");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(temp[1].trim().equals(hotel.getCustomer().getCity()))
			{
				ReportPrinter.append	(	"<tr><td>13</td>"
						+ 	"<td>Verify customer city</td>"
						+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>13</td>"
						+ 	"<td>Verify customer city</td>"
						+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = cusCon.getCountry().split(":");
			if(temp[1].trim().equals(hotel.getCustomer().getCountry()))
			{
				ReportPrinter.append	(	"<tr><td>14</td>"
						+ 	"<td>Verify customer country</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>14</td>"
						+ 	"<td>Verify customer country</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = cusCon.getPhoneNumber().split(":");
			if(temp[1].trim().equals(hotel.getCustomer().getPhoneNumber()))
			{
				ReportPrinter.append	(	"<tr><td>15</td>"
						+ 	"<td>Verify customer phone number</td>"
						+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>15</td>"
						+ 	"<td>Verify customer phone number</td>"
						+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = cusCon.getEmail().split(":");
			if(temp[1].trim().equals(hotel.getCustomer().getEmail()))
			{
				ReportPrinter.append	(	"<tr><td>16</td>"
						+ 	"<td>Verify customer email address</td>"
						+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>16</td>"
						+ 	"<td>Verify customer email address</td>"
						+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		try {
			String temp1 = cusCon.getReferenceNumber().replace(":", "").trim();
			if(temp[1].trim().equals(hotel.getOnline().getReferenceNumber().trim()))
			{
				ReportPrinter.append	(	"<tr><td>17</td>"
						+ 	"<td>Verify booking reference number</td>"
						+	"<td>"+ hotel.getOnline().getReferenceNumber() +"</td>"
						+ 	"<td>"+ temp1 +"</td>"
						+	"<td class='passed'>passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>17</td>"
						+ 	"<td>Verify booking reference number</td>"
						+	"<td>"+ hotel.getOnline().getReferenceNumber() +"</td>"
						+ 	"<td>"+ temp1 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = cusCon.getPaymentId().split(":");
			System.out.println(hotel.getPaymentid());
			System.out.println(temp[1]);
			if(temp[1].trim().equals(hotel.getOnline().getPaymentId()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify payment id</td>"
						+	"<td>"+ hotel.getOnline().getPaymentId() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify payment id</td>"
						+	"<td>"+ hotel.getOnline().getPaymentId() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = cusCon.getMerchantTrackId().split(":");
			System.out.println(hotel.getMerchantTrackId());
			System.out.println(temp[1].trim());
			if(temp[1].trim().equals(hotel.getOnline().getTrackId()))
			{
				ReportPrinter.append	(	"<tr><td>18</td>"
						+ 	"<td>Verify merchant track id</td>"
						+	"<td>"+ hotel.getOnline().getTrackId() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='passed'>passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>18</td>"
						+ 	"<td>Verify merchant track id</td>"
						+	"<td>"+ hotel.getOnline().getTrackId() +"</td>"
						+ 	"<td>"+ temp[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ReportPrinter.append("</table>");
	}
	
	public void inv13CustomerVoucher(voucherMail voucher, resHotelAvailability res, conhotel hotel, StringBuffer ReportPrinter, String defaultCurrency, Map<String, String> currency)
	{

		int cnt = 1;
		
		String[] temp = voucher.getHotelname().split(":");
		System.out.println(temp);
		System.out.println(voucher.getLeadPassenger());
		ReportPrinter.append("<span>(14) customer voucher email verification </span>");
		ReportPrinter.append("<br>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		if(temp[1].trim().equals(res.getHotelInfoname()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ temp[1] +"</td>"
					+ 	"<td>"+ res.getHotelInfoname() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ temp[1] +"</td>"
					+ 	"<td>"+ res.getHotelInfoname() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getCity().split(":");
		if(temp[1].trim().equals(res.getDestinationName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel destination</td>"
					+	"<td>"+ res.getDestinationName() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel destination</td>"
					+	"<td>"+ res.getDestinationName() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getAddress().split(":");
		if(temp[1].equals(hotel.getAddress()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel address</td>"
					+	"<td>"+ hotel.getAddress() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel address</td>"
					+	"<td>"+ hotel.getAddress() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getStatus().split(":");
		if(temp[1].trim().equals(res.getStatus()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reservation status</td>"
					+	"<td>"+ res.getStatus() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reservation status</td>"
					+	"<td>"+ res.getStatus() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getCheckin().split(":");
		String date = formatdate(temp[1]);
		if(date.equals(res.getDateFromDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ res.getDateFromDate() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ res.getDateFromDate() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getCheckOut().split(":");
		date = formatdate(temp[1]);
		if(res.getDateToDate().equals(date))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ res.getDateToDate() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ res.getDateToDate() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		//looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooop
		temp = voucher.getRoomType().split(":");
		if(temp[1].trim().equals(res.getHotelRoom().get(0).getRoom()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getRoom() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getRoom() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getRatePlan().split(":");
		if(temp[1].trim().equals(res.getHotelRoom().get(0).getBoard()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		/*for(int i  = 0 ; i < res.getHotelRoom().size() ; i++)
		{
			System.out.println(voucher.getOccupancy().get(i).getFirstname());
			System.out.println(res.getRoomAvailability().get(i).getName());
		}*/
		if(res.getTotalAmount().equals(voucher.getAmountDueAtBooking()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify total amount</td>"
					+	"<td>"+ res.getTotalAmount() +"</td>"
					+ 	"<td>"+ voucher.getAmountDueAtBooking() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify total amount</td>"
					+	"<td>"+ res.getTotalAmount() +"</td>"
					+ 	"<td>"+ voucher.getAmountDueAtBooking() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		
	
	}
	
	public void hbCustomerVoucher(voucherMail voucher, resHotelAvailability res, conhotel hotel, StringBuffer ReportPrinter)
	{
		
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(15) Customer voucher Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		String passenger = res.getHoldername().concat(" ").concat(res.getHolderlastName());
		int cnt = 1;
		if(voucher.getLeadPassenger().trim().toLowerCase().contains(passenger))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify lead passenger name</td>"
					+	"<td>"+ passenger +"</td>"
					+ 	"<td>"+ voucher.getLeadPassenger() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify lead passenger name</td>"
					+	"<td>"+ passenger +"</td>"
					+ 	"<td>"+ voucher.getLeadPassenger() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		String[] temp = voucher.getHotelname().split(":");
		if(temp[1].trim().equals(res.getHotelInfoname()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ temp[1] +"</td>"
					+ 	"<td>"+ res.getHotelInfoname() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ temp[1] +"</td>"
					+ 	"<td>"+ res.getHotelInfoname() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getCity().split(":");
		if(temp[1].trim().equals(res.getDestinationName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel destination</td>"
					+	"<td>"+ res.getDestinationName() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel destination</td>"
					+	"<td>"+ res.getDestinationName() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getAddress().split(":");
		if(temp[1].equals(hotel.getAddress()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel address</td>"
					+	"<td>"+ hotel.getAddress() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel address</td>"
					+	"<td>"+ hotel.getAddress() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getStatus().split(":");
		if(temp[1].trim().equals(res.getStatus()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reservation status</td>"
					+	"<td>"+ res.getStatus() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reservation status</td>"
					+	"<td>"+ res.getStatus() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getCheckin().split(":");
		String date = formatdate(temp[1]);
		if(date.equals(res.getDateFromDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ res.getDateFromDate() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ res.getDateFromDate() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getCheckOut().split(":");
		date = formatdate(temp[1]);
		if(res.getDateToDate().equals(date))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ res.getDateToDate() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ res.getDateToDate() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		//looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooop
		temp = voucher.getRoomType().split(":");
		if(temp[1].trim().equals(res.getHotelRoom().get(0).getRoom()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getRoom() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getRoom() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		temp = voucher.getRatePlan().split(":");
		if(temp[1].trim().equals(res.getHotelRoom().get(0).getBoard()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
					+ 	"<td>"+ temp[1] +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		/*for(int i  = 0 ; i < res.getHotelRoom().size() ; i++)
		{
			System.out.println(voucher.getOccupancy().get(i).getFirstname());
			System.out.println(res.getRoomAvailability().get(i).getName());
		}*/
		if(res.getTotalAmount().equals(voucher.getAmountDueAtBooking()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify total amount</td>"
					+	"<td>"+ res.getTotalAmount() +"</td>"
					+ 	"<td>"+ voucher.getAmountDueAtBooking() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify total amount</td>"
					+	"<td>"+ res.getTotalAmount() +"</td>"
					+ 	"<td>"+ voucher.getAmountDueAtBooking() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		
	}
	
	public void cchbCustomerConfirmationmail(customerConfirmationEmail cusCon, resHotelAvailability resAvailabilityFinalRes, conhotel hotel, StringBuffer ReportPrinter, String defaultCurrency, Map<String, String> currency)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(15) Reservation Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		String[] 	temp 	= 	cusCon.getBookingReference().split(":");
					temp	=	temp[1].trim().split(" ");
		String reportname =  temp[3].concat(" ").concat(temp[5]);
		String xmlName = resAvailabilityFinalRes.getHoldername().concat(" ").concat(resAvailabilityFinalRes.getHolderlastName());
		int cnt = 1;
		if(reportname.toLowerCase().equals(xmlName.toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer name</td>"
					+	"<td>"+ xmlName +"</td>"
					+ 	"<td>"+ reportname +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer name</td>"
					+	"<td>"+ xmlName +"</td>"
					+ 	"<td>"+ reportname +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(cusCon.getBookingnumber().equals(hotel.getReferenceNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking refernce number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ cusCon.getBookingnumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ cusCon.getBookingnumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(resAvailabilityFinalRes.getDateFromDate().equals(cusCon.getCheckIn()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checking date</td>"
					+	"<td>"+ resAvailabilityFinalRes.getDateFromDate() +"</td>"
					+ 	"<td>"+ cusCon.getCheckIn() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checking date</td>"
					+	"<td>"+ resAvailabilityFinalRes.getDateFromDate() +"</td>"
					+ 	"<td>"+ cusCon.getCheckIn() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCheckout().equals(cusCon.getCheckOut()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ hotel.getCheckout() +"</td>"
					+ 	"<td>"+ cusCon.getCheckOut() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ hotel.getCheckout() +"</td>"
					+ 	"<td>"+ cusCon.getCheckOut() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(resAvailabilityFinalRes.getStatus().equals(cusCon.getBookingStatus()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking status</td>"
					+	"<td>"+ resAvailabilityFinalRes.getStatus() +"</td>"
					+ 	"<td>"+ cusCon.getBookingStatus() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking status</td>"
					+	"<td>"+ resAvailabilityFinalRes.getStatus() +"</td>"
					+ 	"<td>"+ cusCon.getBookingStatus() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		currencyConveter cc = new currencyConveter();
		System.out.println(resAvailabilityFinalRes.getHotelRoom().get(0).getBoard());
		for(int i=0 ; i<resAvailabilityFinalRes.getHotelRoom().size(); i++)
		{
			if(resAvailabilityFinalRes.getHotelRoom().get(i).getBoard().equals(cusCon.getRoom().get(i).getMealPlan()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify meal plan</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getBoard() +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getMealPlan() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify meal plan</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getBoard() +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getMealPlan() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			if(resAvailabilityFinalRes.getHotelRoom().get(i).getRoomType().equals(cusCon.getRoom().get(i).getRoomType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getRoomType() +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getRoomType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getRoomType() +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getRoomType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			String total = cc.convert(defaultCurrency, resAvailabilityFinalRes.getHotelRoom().get(i).getPrice(), cusCon.getRoom().get(i).getCurrencyCode(), currency, resAvailabilityFinalRes.getCurrencyCode());
			if(resAvailabilityFinalRes.getHotelRoom().get(i).getPrice().equals(cusCon.getRoom().get(i).getTotal()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room price</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getTotal() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room price</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getTotal() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		}
		String total = cc.convert(defaultCurrency, resAvailabilityFinalRes.getTotalAmount(), cusCon.getCurrencyCode(), currency, resAvailabilityFinalRes.getCurrencyCode());
		if(cusCon.getSubTotal().equals(resAvailabilityFinalRes.getTotalAmount()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ total +"</td>"
					+ 	"<td>"+ cusCon.getSubTotal() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ total +"</td>"
					+ 	"<td>"+ cusCon.getSubTotal() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(resAvailabilityFinalRes.getHoldername().equals(cusCon.getFirstName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer first name</td>"
					+	"<td>"+ resAvailabilityFinalRes.getHoldername() +"</td>"
					+ 	"<td>"+ cusCon.getFirstName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer first name</td>"
					+	"<td>"+ resAvailabilityFinalRes.getHoldername() +"</td>"
					+ 	"<td>"+ cusCon.getFirstName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(resAvailabilityFinalRes.getHolderlastName().equals(cusCon.getLastName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer last name</td>"
					+	"<td>"+ resAvailabilityFinalRes.getHolderlastName() +"</td>"
					+ 	"<td>"+ cusCon.getLastName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer last name</td>"
					+	"<td>"+ resAvailabilityFinalRes.getHolderlastName() +"</td>"
					+ 	"<td>"+ cusCon.getLastName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCustomer().getAddress().equals(cusCon.getAddress1()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer address</td>"
					+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
					+ 	"<td>"+ cusCon.getAddress1() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer address</td>"
					+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
					+ 	"<td>"+ cusCon.getAddress1() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCustomer().getCity().equals(cusCon.getCity()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer city</td>"
					+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
					+ 	"<td>"+ cusCon.getCity() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer city</td>"
					+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
					+ 	"<td>"+ cusCon.getCity() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCustomer().getCountry().equals(cusCon.getCountry()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer country</td>"
					+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
					+ 	"<td>"+ cusCon.getCountry() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer country</td>"
					+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
					+ 	"<td>"+ cusCon.getCountry() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}	
		if(hotel.getCustomer().getPhoneNumber().equals(cusCon.getPhoneNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer phone number</td>"
					+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
					+ 	"<td>"+ cusCon.getPhoneNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer phone number</td>"
					+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
					+ 	"<td>"+ cusCon.getPhoneNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCustomer().getEmail().equals(cusCon.getEmail()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer email address</td>"
					+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
					+ 	"<td>"+ cusCon.getEmail() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer email address</td>"
					+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
					+ 	"<td>"+ cusCon.getEmail() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		outerloop : for(int i = 0 ; i < hotel.getOccypancy().size() ; i++)
		{
			for(int j = 0 ; j < hotel.getOccypancy().get(i).size() ; j++)
			{
				try {
					System.out.println(hotel.getOccypancy().get(i).get(j).getFirstName());
					System.out.println(cusCon.getOccupancy().get(i).getFirstname());
					if(hotel.getOccypancy().get(i).get(j).getFirstName().equals(cusCon.getOccupancy().get(i).getFirstname()))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify room occupancy first name</td>"
								+	"<td>"+ hotel.getOccypancy().get(i).get(j).getFirstName() +"</td>"
								+ 	"<td>"+ cusCon.getOccupancy().get(i).getFirstname() +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify room occupancy first name</td>"
								+	"<td>"+ hotel.getOccypancy().get(i).get(j).getFirstName() +"</td>"
								+ 	"<td>"+ cusCon.getOccupancy().get(i).getFirstname() +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
					if(hotel.getOccypancy().get(i).get(j).getLastName().equals(cusCon.getOccupancy().get(i).getLastName()))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify room occupancy last name</td>"
								+	"<td>"+ hotel.getOccypancy().get(i).get(j).getLastName() +"</td>"
								+ 	"<td>"+ cusCon.getOccupancy().get(i).getLastName() +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify room occupancy last name</td>"
								+	"<td>"+ hotel.getOccypancy().get(i).get(j).getLastName() +"</td>"
								+ 	"<td>"+ cusCon.getOccupancy().get(i).getLastName() +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
				} catch (Exception e) {
					// TODO: handle exception
					break outerloop;
				}
				
			}
			
		}
		if(hotel.getReferenceNumber().equals(cusCon.getReferenceNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ cusCon.getReferenceNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ cusCon.getReferenceNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getPaymentid().equals(cusCon.getPaymentId()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify payment id</td>"
					+	"<td>"+ hotel.getPaymentid() +"</td>"
					+ 	"<td>"+ cusCon.getPaymentId() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify payment id</td>"
					+	"<td>"+ hotel.getPaymentid() +"</td>"
					+ 	"<td>"+ cusCon.getPaymentId() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getMerchantTrackId().equals(cusCon.getMerchantTrackId()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify merchant track id</td>"
					+	"<td>"+ hotel.getMerchantTrackId() +"</td>"
					+ 	"<td>"+ cusCon.getMerchantTrackId() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify merchant track id</td>"
					+	"<td>"+ hotel.getMerchantTrackId() +"</td>"
					+ 	"<td>"+ cusCon.getMerchantTrackId() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getAmount().equals(cusCon.getAmount()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify amount</td>"
					+	"<td>"+ hotel.getAmount() +"</td>"
					+ 	"<td>"+ cusCon.getAmount() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify amount</td>"
					+	"<td>"+ hotel.getAmount() +"</td>"
					+ 	"<td>"+ cusCon.getAmount() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}

	
	public void hbCustomerConfirmationmail(customerConfirmationEmail cusCon, resHotelAvailability resAvailabilityFinalRes, conhotel hotel, StringBuffer ReportPrinter, Map<String, String> currency, String defaultCurrency)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(14) Reservation Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		String[] 	temp 	= 	cusCon.getBookingReference().split(":");
					temp	=	temp[1].trim().split(" ");
		String reportname =  temp[3].concat(" ").concat(temp[5]);
		String xmlName = resAvailabilityFinalRes.getHoldername().concat(" ").concat(resAvailabilityFinalRes.getHolderlastName());
		int cnt = 1;
		if(reportname.toLowerCase().equals(xmlName.toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer name</td>"
					+	"<td>"+ xmlName +"</td>"
					+ 	"<td>"+ reportname +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer name</td>"
					+	"<td>"+ xmlName +"</td>"
					+ 	"<td>"+ reportname +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(cusCon.getBookingnumber().equals(hotel.getReferenceNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking refernce number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ cusCon.getBookingnumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ cusCon.getBookingnumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(resAvailabilityFinalRes.getDateFromDate().equals(cusCon.getCheckIn()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checking date</td>"
					+	"<td>"+ resAvailabilityFinalRes.getDateFromDate() +"</td>"
					+ 	"<td>"+ cusCon.getCheckIn() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checking date</td>"
					+	"<td>"+ resAvailabilityFinalRes.getDateFromDate() +"</td>"
					+ 	"<td>"+ cusCon.getCheckIn() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCheckout().equals(cusCon.getCheckOut()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ hotel.getCheckout() +"</td>"
					+ 	"<td>"+ cusCon.getCheckOut() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ hotel.getCheckout() +"</td>"
					+ 	"<td>"+ cusCon.getCheckOut() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(resAvailabilityFinalRes.getStatus().equals(cusCon.getBookingStatus()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking status</td>"
					+	"<td>"+ resAvailabilityFinalRes.getStatus() +"</td>"
					+ 	"<td>"+ cusCon.getBookingStatus() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking status</td>"
					+	"<td>"+ resAvailabilityFinalRes.getStatus() +"</td>"
					+ 	"<td>"+ cusCon.getBookingStatus() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		currencyConveter cc = new currencyConveter();
		System.out.println(resAvailabilityFinalRes.getHotelRoom().get(0).getBoard());
		for(int i=0 ; i<resAvailabilityFinalRes.getHotelRoom().size(); i++)
		{
			if(resAvailabilityFinalRes.getHotelRoom().get(i).getBoard().equals(cusCon.getRoom().get(i).getMealPlan()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify meal plan</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getBoard() +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getMealPlan() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify meal plan</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getBoard() +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getMealPlan() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			if(resAvailabilityFinalRes.getHotelRoom().get(i).getRoomType().equals(cusCon.getRoom().get(i).getRoomType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getRoomType() +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getRoomType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ resAvailabilityFinalRes.getHotelRoom().get(i).getRoomType() +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getRoomType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			
			
			String total = cc.convert(defaultCurrency, resAvailabilityFinalRes.getHotelRoom().get(i).getPrice(), cusCon.getCurrencyCode(), currency, resAvailabilityFinalRes.getCurrencyCode());
			if(resAvailabilityFinalRes.getHotelRoom().get(i).getPrice().equals(cusCon.getRoom().get(i).getTotal()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room price</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getTotal() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room price</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ cusCon.getRoom().get(i).getTotal() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		}
		String total = cc.convert(defaultCurrency, resAvailabilityFinalRes.getTotalAmount(), cusCon.getCurrencyCode(), currency, resAvailabilityFinalRes.getCurrencyCode());
		if(cusCon.getSubTotal().equals(resAvailabilityFinalRes.getTotalAmount()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ total +"</td>"
					+ 	"<td>"+ cusCon.getSubTotal() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ total +"</td>"
					+ 	"<td>"+ cusCon.getSubTotal() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(resAvailabilityFinalRes.getHoldername().equals(cusCon.getFirstName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer first name</td>"
					+	"<td>"+ resAvailabilityFinalRes.getHoldername() +"</td>"
					+ 	"<td>"+ cusCon.getFirstName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer first name</td>"
					+	"<td>"+ resAvailabilityFinalRes.getHoldername() +"</td>"
					+ 	"<td>"+ cusCon.getFirstName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(resAvailabilityFinalRes.getHolderlastName().equals(cusCon.getLastName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer last name</td>"
					+	"<td>"+ resAvailabilityFinalRes.getHolderlastName() +"</td>"
					+ 	"<td>"+ cusCon.getLastName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer last name</td>"
					+	"<td>"+ resAvailabilityFinalRes.getHolderlastName() +"</td>"
					+ 	"<td>"+ cusCon.getLastName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCustomer().getAddress().equals(cusCon.getAddress1()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer address</td>"
					+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
					+ 	"<td>"+ cusCon.getAddress1() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer address</td>"
					+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
					+ 	"<td>"+ cusCon.getAddress1() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCustomer().getCity().equals(cusCon.getCity()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer city</td>"
					+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
					+ 	"<td>"+ cusCon.getCity() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer city</td>"
					+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
					+ 	"<td>"+ cusCon.getCity() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCustomer().getCountry().equals(cusCon.getCountry()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer country</td>"
					+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
					+ 	"<td>"+ cusCon.getCountry() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer country</td>"
					+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
					+ 	"<td>"+ cusCon.getCountry() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}	
		if(hotel.getCustomer().getPhoneNumber().equals(cusCon.getPhoneNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer phone number</td>"
					+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
					+ 	"<td>"+ cusCon.getPhoneNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer phone number</td>"
					+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
					+ 	"<td>"+ cusCon.getPhoneNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getCustomer().getEmail().equals(cusCon.getEmail()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer email address</td>"
					+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
					+ 	"<td>"+ cusCon.getEmail() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer email address</td>"
					+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
					+ 	"<td>"+ cusCon.getEmail() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		for(int i = 0 ; i < hotel.getOccypancy().size() ; i++)
		{
			if(hotel.getOccypancy2().get(i).getFirstName().equals(cusCon.getOccupancy().get(i).getFirstname()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room occupancy first name</td>"
						+	"<td>"+ hotel.getOccypancy2().get(i).getFirstName() +"</td>"
						+ 	"<td>"+ cusCon.getOccupancy().get(i).getFirstname() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room occupancy first name</td>"
						+	"<td>"+ hotel.getOccypancy2().get(i).getFirstName() +"</td>"
						+ 	"<td>"+ cusCon.getOccupancy().get(i).getFirstname() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			if(hotel.getOccypancy2().get(i).getLastName().equals(cusCon.getOccupancy().get(i).getLastName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room occupancy last name</td>"
						+	"<td>"+ hotel.getOccypancy2().get(i).getLastName() +"</td>"
						+ 	"<td>"+ cusCon.getOccupancy().get(i).getLastName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room occupancy last name</td>"
						+	"<td>"+ hotel.getOccypancy2().get(i).getLastName() +"</td>"
						+ 	"<td>"+ cusCon.getOccupancy().get(i).getLastName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		}
		if(hotel.getReferenceNumber().equals(cusCon.getReferenceNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ cusCon.getReferenceNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ cusCon.getReferenceNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getPaymentid().equals(cusCon.getPaymentId()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify payment id</td>"
					+	"<td>"+ hotel.getPaymentid() +"</td>"
					+ 	"<td>"+ cusCon.getPaymentId() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify payment id</td>"
					+	"<td>"+ hotel.getPaymentid() +"</td>"
					+ 	"<td>"+ cusCon.getPaymentId() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getMerchantTrackId().equals(cusCon.getMerchantTrackId()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify merchant track id</td>"
					+	"<td>"+ hotel.getMerchantTrackId() +"</td>"
					+ 	"<td>"+ cusCon.getMerchantTrackId() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify merchant track id</td>"
					+	"<td>"+ hotel.getMerchantTrackId() +"</td>"
					+ 	"<td>"+ cusCon.getMerchantTrackId() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getAmount().equals(cusCon.getAmount()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify amount</td>"
					+	"<td>"+ hotel.getAmount() +"</td>"
					+ 	"<td>"+ cusCon.getAmount() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify amount</td>"
					+	"<td>"+ hotel.getAmount() +"</td>"
					+ 	"<td>"+ cusCon.getAmount() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
	}
	
	public void inv13profitAndLossVerify(resHotelAvailability res, reservationReport report, conhotel hotel, StringBuffer ReportPrinter, String supplier, String channel, String[] searchDetails, Map<String, String> currency, String defaultCurrency)
	{


		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>Reservation Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		int cnt = 1 ;
		if(hotel.getReservationNumber().trim().contains(report.getBookingNumber().trim()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ report.getBookingNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ report.getBookingNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String today = dateFormat.format(date);
		
		if(report.getBookingDate().equals(today))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ report.getBookingDate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ report.getBookingDate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		//System.out.println(report.getSupplierName().trim().toLowerCase());
		//System.out.println(supplier.trim().toLowerCase());
		if(report.getSupplierName().trim().toLowerCase().contains(supplier.trim().toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify supplier</td>"
					+	"<td>"+ supplier +"</td>"
					+ 	"<td>"+ report.getSupplierName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify supplier</td>"
					+	"<td>"+ supplier +"</td>"
					+ 	"<td>"+ report.getSupplierName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		try {
			if(res.getHotelInfoname().equals(report.getHotelName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getStatus().trim().toLowerCase().equals(report.getBookingType().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking status</td>"
						+	"<td>"+ res.getStatus() +"</td>"
						+ 	"<td>"+ report.getBookingType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking status</td>"
						+	"<td>"+ res.getStatus() +"</td>"
						+ 	"<td>"+ report.getBookingType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		if(channel.trim().toLowerCase().equals(report.getChannel().trim().toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking channel</td>"
					+	"<td>"+ channel +"</td>"
					+ 	"<td>"+ report.getChannel() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking channel</td>"
					+	"<td>"+ channel +"</td>"
					+ 	"<td>"+ report.getChannel() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		try {
			if(res.getHoldername().trim().toLowerCase().equals(report.getFirtName().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ res.getHoldername() +"</td>"
						+ 	"<td>"+ report.getFirtName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ res.getHoldername() +"</td>"
						+ 	"<td>"+ report.getFirtName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHolderlastName().trim().toLowerCase().equals(report.getLastName().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ res.getHolderlastName() +"</td>"
						+ 	"<td>"+ report.getLastName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ res.getHolderlastName() +"</td>"
						+ 	"<td>"+ report.getLastName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		String checkinDate = formatdate(report.getCheckIn());
		try {
			if(res.getDateFromDate().equals(checkinDate))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checking date</td>"
						+	"<td>"+ res.getDateFromDate() +"</td>"
						+ 	"<td>"+ report.getCheckIn() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checking date</td>"
						+	"<td>"+ res.getDateFromDate() +"</td>"
						+ 	"<td>"+ report.getCheckIn() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		System.out.println(report.getRoomType());
		try {
			if(res.getHotelRoom().get(0).getRoom().equals(report.getRoomType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getRoom() +"</td>"
						+ 	"<td>"+ report.getRoomType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getRoom() +"</td>"
						+ 	"<td>"+ report.getRoomType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			if(res.getHotelRoom().get(0).getBoard().equals(report.getBedType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
						+ 	"<td>"+ report.getBedType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify bed type</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
						+ 	"<td>"+ report.getBedType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHotelRoom().get(0).getBoardType().equals(report.getRatePlan()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify rate plan</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoardType() +"</td>"
						+ 	"<td>"+ report.getRatePlan() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify rate plan</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoardType() +"</td>"
						+ 	"<td>"+ report.getRatePlan() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		if(hotel.getPaymentid().equals(report.getTransId()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify trans id</td>"
					+	"<td>"+ hotel.getPaymentid() +"</td>"
					+ 	"<td>"+ report.getTransId() +"</td>"
					+	"<td class='passsed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify trans id</td>"
					+	"<td>"+ hotel.getPaymentid() +"</td>"
					+ 	"<td>"+ report.getTransId() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(hotel.getReferenceNumber().equals(report.getAppCode()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ report.getAppCode() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ report.getAppCode() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		//currency convertion required
		currencyConveter cc = new currencyConveter();
		String total = cc.convert(defaultCurrency, res.getTotalAmount(), report.getCurrency(), currency, res.getCurrencyCode());
		try {
			if(res.getTotalAmount().equals(report.getTotalCost()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify get total</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ report.getTotalCost() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify get total</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ report.getTotalCost() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		if(searchDetails[3].equals(report.getNumOfNyts().trim()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights</td>"
					+	"<td>"+ searchDetails[3] +"</td>"
					+ 	"<td>"+ report.getNumOfNyts().trim() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights</td>"
					+	"<td>"+ searchDetails[3] +"</td>"
					+ 	"<td>"+ report.getNumOfNyts().trim() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		//System.out.println(searchDetails[4]);
		//System.out.println(report.getNumOfRooms());
		if(searchDetails[4].equals(report.getNumOfRooms().trim()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ searchDetails[4] +"</td>"
					+ 	"<td>"+ report.getNumOfRooms().trim() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ searchDetails[4] +"</td>"
					+ 	"<td>"+ report.getNumOfRooms().trim() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		ReportPrinter.append("</table>");
		
	
	
	}
	
	public void profitAndLossVerify(resHotelAvailability res, profitAndLostReport report, conhotel hotel, StringBuffer ReportPrinter, String supplier, String channel, String[] searchDetails, String defaultCurrency, Map<String, String> currency, Map<String, String> profitMarkUp)
	{

		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(11) Profit and loss Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		int cnt = 1 ;
		try {
			if(report.getBookingNumber().trim().equals(hotel.getReservationNumber().trim()))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify reservation number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ report.getBookingNumber() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify reservation number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ report.getBookingNumber() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(report.getCombination().trim().equals("Hotel"))
			{
				ReportPrinter.append	(	"<tr><td>2</td>"
						+ 	"<td>Verify combination</td>"
						+	"<td>Hotel</td>"
						+ 	"<td>"+ report.getCombination() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>2</td>"
						+ 	"<td>Verify combination</td>"
						+	"<td>Hotel</td>"
						+ 	"<td>"+ report.getCombination() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		String temp = "";
		if(searchDetails[8].trim().equals("web"))
		{
			temp = "Website";
		}
		else if(searchDetails[8].trim().equals("cc"))
		{
			temp = "Call Center";
		}
		System.out.println(temp);
		try {
			if(report.getBookingChannel().trim().equals(temp))
			{
				ReportPrinter.append	(	"<tr><td>3</td>"
						+ 	"<td>Verify booking channel</td>"
						+	"<td>"+ temp +"</td>"
						+ 	"<td>"+ report.getBookingChannel() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>3</td>"
						+ 	"<td>Verify booking channel</td>"
						+	"<td>"+ temp +"</td>"
						+ 	"<td>"+ report.getBookingChannel() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		/*try {
			System.out.println(report.getNoOfGuests());
			System.out.println(hotel.getOccypancy().get(0).size());
			if(report.getNoOfGuests().trim().equals(String.valueOf(hotel.getOccypancy().size())))
			{
				ReportPrinter.append	(	"<tr><td>4</td>"
						+ 	"<td>Verify number of guests</td>"
						+	"<td>"+ hotel.getOccypancy().size() +"</td>"
						+ 	"<td>"+ report.getNoOfGuests() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>4</td>"
						+ 	"<td>Verify number of guests</td>"
						+	"<td>"+ hotel.getOccypancy().size() +"</td>"
						+ 	"<td>"+ report.getNoOfGuests() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}*/
		
		String[] date = null;
		String condate = "";
		try {
			date = report.getChechIn().trim().split("/");
			String[] date2 = res.getHotelDetails().get(0).getFromDate().split("T");
			condate = formatdate3(date2[0]);
			if(date[0].trim().equals(condate.trim()))
			{
				ReportPrinter.append	(	"<tr><td>4</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ date2[0] +"</td>"
						+ 	"<td>"+ date[0] +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>4</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ date2[0] +"</td>"
						+ 	"<td>"+ date[0] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			String[] date2 = res.getHotelDetails().get(0).getToDate().split("T");
			if(date[1].trim().equals(date2[0].trim()))
			{
				ReportPrinter.append	(	"<tr><td>5</td>"
						+ 	"<td>Verify checkout date</td>"
						+	"<td>"+ date2[0] +"</td>"
						+ 	"<td>"+ date[1] +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>5</td>"
						+ 	"<td>Verify checkout date</td>"
						+	"<td>"+ date2[0] +"</td>"
						+ 	"<td>"+ date[1] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(searchDetails[3].equals(report.getNumOfNyts().trim()))
			{
				ReportPrinter.append	(	"<tr><td>6</td>"
						+ 	"<td>Verify number of nights</td>"
						+	"<td>"+ searchDetails[3] +"</td>"
						+ 	"<td>"+ report.getNumOfNyts() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>6</td>"
						+ 	"<td>Verify number of nights</td>"
						+	"<td>"+ searchDetails[3] +"</td>"
						+ 	"<td>"+ report.getNumOfNyts() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		currencyConveter cc = new currencyConveter();
		String cost = "";
		try {
			cost = cc.convert(defaultCurrency, hotel.getPay().getTotal(), report.getBaseCurrency().trim(), currency, hotel.getCurrencyCode().trim());
			String[] value =  report.getGrossBookingValue().split("\\.");
			String value2 = value[0].trim().replace(",", "");
			String value3 = String.valueOf(Integer.parseInt(cost) + Integer.parseInt(profitMarkUp.get("creditCard")));
			if(value3.equals(value2))
			{
				ReportPrinter.append	(	"<tr><td>7</td>"
						+ 	"<td>Verify number of nights</td>"
						+	"<td>"+ value3 +"</td>"
						+ 	"<td>"+ value2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>7</td>"
						+ 	"<td>Verify number of nights</td>"
						+	"<td>"+ value3 +"</td>"
						+ 	"<td>"+ value2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
		
	
	}
	
	public void inv13Verify(resHotelAvailability res, reservationReport report, conhotel hotel, StringBuffer ReportPrinter, String supplier, String channel, String[] searchDetails, String defaultCurrency, Map<String, String> currency)
	{

		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(9) Reservation Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		int cnt = 1 ;
		if(hotel.getReservationNumber().contains(report.getBookingNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ report.getBookingNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ report.getBookingNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String today = dateFormat.format(date);
		
		if(report.getBookingDate().equals(today))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ report.getBookingDate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ report.getBookingDate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(report.getSupplierName().trim().toLowerCase().contains(supplier.trim().toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify supplier</td>"
					+	"<td>"+ supplier +"</td>"
					+ 	"<td>"+ report.getSupplierName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify supplier</td>"
					+	"<td>"+ supplier +"</td>"
					+ 	"<td>"+ report.getSupplierName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		try {
			System.out.println(res.getHotelInfoname());
			System.out.println(res.getHotelName());
			if(res.getHotelInfoname().equals(report.getHotelName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getStatus().trim().toLowerCase().equals(report.getBookingType().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking status</td>"
						+	"<td>"+ res.getStatus() +"</td>"
						+ 	"<td>"+ report.getBookingType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking status</td>"
						+	"<td>"+ res.getStatus() +"</td>"
						+ 	"<td>"+ report.getBookingType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		
		if(channel.trim().toLowerCase().equals(report.getChannel().trim().toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking channel</td>"
					+	"<td>"+ channel +"</td>"
					+ 	"<td>"+ report.getChannel() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking channel</td>"
					+	"<td>"+ channel +"</td>"
					+ 	"<td>"+ report.getChannel() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		try {
			if(res.getHoldername().trim().toLowerCase().equals(report.getFirtName().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ res.getHoldername() +"</td>"
						+ 	"<td>"+ report.getFirtName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ res.getHoldername() +"</td>"
						+ 	"<td>"+ report.getFirtName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHolderlastName().trim().toLowerCase().equals(report.getLastName().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ res.getHolderlastName() +"</td>"
						+ 	"<td>"+ report.getLastName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ res.getHolderlastName() +"</td>"
						+ 	"<td>"+ report.getLastName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		String checkinDate = formatdate(report.getCheckIn());
		try {
			if(res.getDateFromDate().equals(checkinDate))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checking date</td>"
						+	"<td>"+ res.getDateFromDate() +"</td>"
						+ 	"<td>"+ report.getCheckIn() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checking date</td>"
						+	"<td>"+ res.getDateFromDate() +"</td>"
						+ 	"<td>"+ report.getCheckIn() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//System.out.println(res.getHotelDetails().get(0).getRoomType());
		//System.out.println(report.getRoomType());
		try {
			if(res.getHotelDetails().get(0).getRoomType().equals(report.getRoomType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getRoomType() +"</td>"
						+ 	"<td>"+ report.getRoomType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getRoomType() +"</td>"
						+ 	"<td>"+ report.getRoomType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHotelRoom().get(0).getBoard().equals(report.getBedType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
						+ 	"<td>"+ report.getBedType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify bed type</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
						+ 	"<td>"+ report.getBedType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			//System.out.println(res.getHotelRoom().get(0).getBoardType());
			//System.out.println(report.getRatePlan());
			if(res.getHotelRoom().get(0).getBoardType().equals(report.getRatePlan()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify rate plan</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoardType() +"</td>"
						+ 	"<td>"+ report.getRatePlan() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify rate plan</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoardType() +"</td>"
						+ 	"<td>"+ report.getRatePlan() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		if(hotel.getPaymentid().equals(report.getTransId()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify trans id</td>"
					+	"<td>"+ hotel.getPaymentid() +"</td>"
					+ 	"<td>"+ report.getTransId() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify trans id</td>"
					+	"<td>"+ hotel.getPaymentid() +"</td>"
					+ 	"<td>"+ report.getTransId() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(hotel.getReferenceNumber().equals(report.getHotelConfirmationNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ report.getHotelConfirmationNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReferenceNumber() +"</td>"
					+ 	"<td>"+ report.getHotelConfirmationNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		//currency convertion required
		currencyConveter cc = new currencyConveter();
		System.out.println(defaultCurrency);
		System.out.println(res.getTotalAmount());
		System.out.println(report.getCurrency());
		System.out.println(res.getCurrencyCode());
		try {
			String total = cc.convert(defaultCurrency, res.getTotalAmount(), report.getCurrency(), currency, res.getCurrencyCode());
			if(total.equals(report.getTotalCost()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify get total</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ report.getTotalCost() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify get total</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ report.getTotalCost() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		if(searchDetails[3].equals(report.getNumOfNyts().trim()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights</td>"
					+	"<td>"+ searchDetails[3] +"</td>"
					+ 	"<td>"+ report.getNumOfNyts().trim() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights</td>"
					+	"<td>"+ searchDetails[3] +"</td>"
					+ 	"<td>"+ report.getNumOfNyts().trim() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		//System.out.println(searchDetails[4]);
		//System.out.println(report.getNumOfRooms());
		if(searchDetails[4].equals(report.getNumOfRooms().trim()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ searchDetails[4] +"</td>"
					+ 	"<td>"+ report.getNumOfRooms().trim() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ searchDetails[4] +"</td>"
					+ 	"<td>"+ report.getNumOfRooms().trim() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		ReportPrinter.append("</table>");
		
	
	}
	
	public void verifyResReportAfterCancellation(StringBuffer ReportPrinter, String resReport2)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(18) Reservation report after cancellation</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
		
		if(resReport2.equals("Total Records:0"))
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Check for reservation report after the cancellation</td>"
					+	"<td>Total Records:0</td>"
					+ 	"<td>"+ resReport2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Check for reservation report after the cancellation</td>"
					+	"<td>Total Records:0</td>"
					+ 	"<td>"+ resReport2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		ReportPrinter.append("</table>");
		
	}
	
	public void verify(resHotelAvailability res, reservationReport report, conhotel hotel, StringBuffer ReportPrinter, String supplier, String channel, String[] searchDetails, Map<String, String> currency, String defaultCurrency, Map<String, String> profitMarkUp)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(9)Reservation Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		int cnt = 1 ;
		try {
			if(hotel.getReservationNumber().contains(report.getBookingNumber()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ report.getBookingNumber() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ report.getBookingNumber() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String today = dateFormat.format(date);
		
		try {
			if(report.getBookingDate().equals(today))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking date</td>"
						+	"<td>"+ today +"</td>"
						+ 	"<td>"+ report.getBookingDate() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking date</td>"
						+	"<td>"+ today +"</td>"
						+ 	"<td>"+ report.getBookingDate() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(report.getSupplierName().trim().toLowerCase().contains(supplier.trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify supplier</td>"
						+	"<td>"+ supplier +"</td>"
						+ 	"<td>"+ report.getSupplierName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify supplier</td>"
						+	"<td>"+ supplier +"</td>"
						+ 	"<td>"+ report.getSupplierName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHotelDetails().get(0).getHotelName().equals(report.getHotelName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getHotelName() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			String status = "";
			for(int i = 0 ; i < res.getHotelDetails().size() ; i++)
			{
				if(res.getHotelDetails().get(i).getStatus().equals("Request"))
				{
					status = "Request";
				}
			}
			System.out.println(report.getBookingType());
			if(status.trim().toLowerCase().equals(report.getBookingType().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking status</td>"
						+	"<td>"+ status +"</td>"
						+ 	"<td>"+ report.getBookingType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking status</td>"
						+	"<td>"+ status +"</td>"
						+ 	"<td>"+ report.getBookingType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		try {
			if(channel.trim().toLowerCase().equals(report.getChannel().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking channel</td>"
						+	"<td>"+ channel +"</td>"
						+ 	"<td>"+ report.getChannel() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking channel</td>"
						+	"<td>"+ channel +"</td>"
						+ 	"<td>"+ report.getChannel() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHotelDetails().get(0).getFirstName().trim().toLowerCase().equals(report.getFirtName().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFirstName() +"</td>"
						+ 	"<td>"+ report.getFirtName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer first name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFirstName() +"</td>"
						+ 	"<td>"+ report.getFirtName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHotelDetails().get(0).getLastName().trim().toLowerCase().equals(report.getLastName().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getLastName() +"</td>"
						+ 	"<td>"+ report.getLastName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer last name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getLastName() +"</td>"
						+ 	"<td>"+ report.getLastName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		System.out.println(report.getCheckIn());
		String checkinDate = "";
		try {
			checkinDate = formatdate(report.getCheckIn());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			System.out.println(res.getHotelDetails().get(0).getFromDate());
			System.out.println(checkinDate);
			if(res.getHotelDetails().get(0).getFromDate().contains(checkinDate))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checking date</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
						+ 	"<td>"+ report.getCheckIn() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checking date</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
						+ 	"<td>"+ report.getCheckIn() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		 
		try {
			System.out.println(res.getHotelDetails().size());
			System.out.println(res.getHotelDetails().get(0).getRoomType());
			System.out.println(report.getRoomType());
			if(res.getHotelDetails().get(0).getRoomType().equals(report.getRoomType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getRoomType() +"</td>"
						+ 	"<td>"+ report.getRoomType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room type</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getRoomType() +"</td>"
						+ 	"<td>"+ report.getRoomType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
			try {
				if(res.getHotelRoom().get(0).getBoard().equals(report.getBedType()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify bed type</td>"
							+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
							+ 	"<td>"+ report.getBedType() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify bed type</td>"
							+	"<td>"+ res.getHotelRoom().get(0).getBoard() +"</td>"
							+ 	"<td>"+ report.getBedType() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
			} catch (Exception e) {
				// TODO: handle exception
			}
		
		try {
			System.out.println(report.getRatePlan());
			if(res.getHotelRoom().get(0).getBoardType().equals(report.getRatePlan()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify rate plan</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoardType() +"</td>"
						+ 	"<td>"+ report.getRatePlan() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify rate plan</td>"
						+	"<td>"+ res.getHotelRoom().get(0).getBoardType() +"</td>"
						+ 	"<td>"+ report.getRatePlan() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			System.out.println(hotel.getOnline().getPaymentId());
			if(hotel.getPaymentid().equals("") || report.getTransId().equals(""))
			{
				
			}
			else
			{
				if(hotel.getPaymentid().equals(report.getTransId()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify trans id</td>"
							+	"<td>"+ hotel.getPaymentid() +"</td>"
							+ 	"<td>"+ report.getTransId() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify trans id</td>"
							+	"<td>"+ hotel.getPaymentid() +"</td>"
							+ 	"<td>"+ report.getTransId() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			}
			
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}	
		
		try {
			if(hotel.getReferenceNumber().equals("") || report.getHotelConfirmationNumber().equals(""))
			{
				
			}
			else
			{
				if(hotel.getReferenceNumber().equals(report.getHotelConfirmationNumber()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify reference number</td>"
							+	"<td>"+ hotel.getReferenceNumber() +"</td>"
							+ 	"<td>"+ report.getHotelConfirmationNumber() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify reference number</td>"
							+	"<td>"+ hotel.getReferenceNumber() +"</td>"
							+ 	"<td>"+ report.getHotelConfirmationNumber() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}	
		
		//currency convertion required
		currencyConveter cc = new currencyConveter();
		try {
			String total = cc.convert(defaultCurrency, res.getTotalAmount(), report.getCurrency(), currency, res.getCurrencyCode());
			if(total.equals(report.getTotalCost()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify get total</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ report.getTotalCost() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify get total</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ report.getTotalCost() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(searchDetails[3].equals(report.getNumOfNyts().trim()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify number of nights</td>"
						+	"<td>"+ searchDetails[3] +"</td>"
						+ 	"<td>"+ report.getNumOfNyts().trim() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify number of nights</td>"
						+	"<td>"+ searchDetails[3] +"</td>"
						+ 	"<td>"+ report.getNumOfNyts().trim() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		//System.out.println(searchDetails[4]);
		//System.out.println(report.getNumOfRooms());
		try {
			if(searchDetails[4].equals(report.getNumOfRooms().trim()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify number of rooms</td>"
						+	"<td>"+ searchDetails[4] +"</td>"
						+ 	"<td>"+ report.getNumOfRooms().trim() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify number of rooms</td>"
						+	"<td>"+ searchDetails[4] +"</td>"
						+ 	"<td>"+ report.getNumOfRooms().trim() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
		
	}
	
	public String formatdate(String date)
	{
		System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyy-MM-dd");
		String x = sdf.format(d);
		return x;
	}

	public String formatdate2(String date)
	{
		System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyy-MM-dd");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
	
	public String formatdate3(String date)
	{
		System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyy-MM-dd");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
}
