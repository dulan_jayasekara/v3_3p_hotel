package verification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.lang.model.type.ErrorType;

import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;

import excelReader.ExcelReader;

import readAndWrite.hotel;
import readAndWrite.readHotelResults;
import readXml.hotelAvailability;
import readXml.resHotelAvailability;

public class errorVerification {

	private static Map<String, String>  	txtProperty 		= new HashMap<String, String>();
	
	public void selectHotelError(String error, String errorType, resHotelAvailability resAvailability, resHotelAvailability resRoomAvailability, String hotelName, String defaultCurrency, Map<String, String> currency, StringBuffer ReportPrinter, ArrayList<readHotelResults> hotelList) throws FileNotFoundException
	{
		System.out.println(error);
		ExcelReader 							newReader 			= 	new ExcelReader();
		FileInputStream 						stream 				=	new FileInputStream(new File("excel/searchDetails.xls"));
		ArrayList<Map<Integer, String>> 		ContentList 		= 	new ArrayList<Map<Integer,String>>();
												ContentList 		= 	newReader.contentReading(stream);
		Map<Integer, String>  					content 			= 	new HashMap<Integer, String>();
												content   			= 	ContentList.get(0);
		Iterator<Map.Entry<Integer, String>>   	mapiterator 		= 	content.entrySet().iterator();
		Map.Entry<Integer, String> 				Entry 				= 	(Map.Entry<Integer, String>)mapiterator.next();
		String 									Content 			= 	Entry.getValue();
		String[] 								searchDetails 		= 	Content.split(",");
		int 									roomCount 			= 	Integer.parseInt(searchDetails[4]);
		String									pmConValue			= 	"";
		String									pmOriginalConValue 	= 	"";
		String									newValue2			=	"";
		currencyConveter 						cc 					= 	new currencyConveter();
		addProfitmarkup 						apm 				= 	new addProfitmarkup();
		if(error.contains("The rates for this hotel have changed"))
		{
			String[] 							newValue 			= 	error.split(":");
												newValue			=	newValue[1].split("Do");
												newValue			= 	newValue[0].split(" ");
			String								currencyCode		=	newValue[1].trim();
												newValue2			=  	newValue[2].replace(".", "/");
												newValue			= 	newValue2.split("/");
												newValue2			= 	newValue[0].concat(".").concat(newValue[1]);
			System.out.println(newValue2);
			System.out.println(currencyCode);
			
			System.out.println(resAvailability.getHotelDetails().get(2).getHotelName());
			String originalValue = null;
			ArrayList<Float> minval = new ArrayList<Float>(); 
			float temp = 0 ;
			int hotelListIndex = 0 ;
			for(int i = 0 ; i < hotelList.size() ; i++)
			{
				if(hotelList.get(i).getTitle().equals(hotelName))
				{
					System.out.println(hotelList.get(i).getTitle());
					hotelListIndex = i;
				}
			}
			for(int i = 0 ; i < resAvailability.getHotelDetails().size(); i++)
			{
				if(resAvailability.getHotelDetails().get(i).getHotelName().equals(hotelName))
				{
					for(int j = 0 ; j < resAvailability.getHotelDetails().get(i).getRhRoom().size() ; j++)
					{
						minval.add(Float.parseFloat(resAvailability.getHotelDetails().get(i).getRhRoom().get(j).getPrice()));
					}
					
					for(int j = 0 ; j < minval.size() ; j++)
					{
						if(temp < minval.get(j))
						{
							temp = minval.get(j);
						}
					}
					System.out.println(temp);
					//temp = temp*roomCount;
					//System.out.println(temp);
					System.out.println(resRoomAvailability.getTotalAmount());
					originalValue = resRoomAvailability.getTotalAmount();
					System.out.println(resAvailability.getHotelDetails().get(i).getRhRoom().size());
					System.out.println(originalValue);
					System.out.println(resRoomAvailability.getCurrencyCode());
					
					String conValue = cc.convert(defaultCurrency, String.valueOf(temp), currencyCode, currency, resRoomAvailability.getCurrencyCode());
					Properties properties 	= new Properties();
					try 
					{
						//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
						FileReader reader 		=	new FileReader(new File("/rezsystem/workspace/hotelXmlValidation/profitMarkUps.properties"));
						
						properties.load(reader);
					} 
					catch (IOException e)
					{
						 System.out.println(e.toString());
					}
					for (String key : properties.stringPropertyNames()) 
					{
					    String value 			= 	properties.getProperty(key);
					    txtProperty.put(key, value);
					}
					pmConValue 			= 	String.valueOf(apm.profitMarkup(conValue, txtProperty.get("hb")));
					System.out.println(pmConValue);
					String originalConValue  	= 	cc.convert(defaultCurrency, resRoomAvailability.getTotalAmount(), currencyCode, currency, resRoomAvailability.getCurrencyCode());
					pmOriginalConValue 	= 	String.valueOf(apm.profitMarkup(originalConValue, txtProperty.get("hb")));
					break;
				}
			}
			ReportPrinter.append("<span>Results page rates changed error</span>");
			ReportPrinter.append("<table border=1 style=width:800px>"
					+ "<tr><th>Test Case</th>"
					+ "<th>Error type</th>"
					+ "<th>Error</th></tr>");
			
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>"+ errorType +"</td>"
					+	"<td>"+ error +"</td></tr>");
			ReportPrinter.append("</table>");
			
			ReportPrinter.append("<table border=1 style=width:800px>"
					+ "<tr><th>Test Case</th>"
					+ "<th>Test Description</th>"
					+ "<th>Original Result</th>"
					+ "<th>Changed Result</th>"
					+ "<th>Test Status</th></tr>");
			
			if(pmOriginalConValue.equals(pmConValue))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify changed cost xml against xml</td>"
						+	"<td>"+ pmOriginalConValue +"</td>"
						+ 	"<td>"+ pmConValue +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify changed cost xml against xml</td>"
						+	"<td>"+ pmOriginalConValue +"</td>"
						+ 	"<td>"+ pmConValue +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			String[] cost = hotelList.get(hotelListIndex).getTotalCost().split(" ");
			
			if(newValue2.equals(pmConValue))
			{
				ReportPrinter.append	(	"<tr><td>2</td>"
						+ 	"<td>Verify changed cost web against web</td>"
						+	"<td>"+ cost[1] +"</td>"
						+ 	"<td>"+ newValue2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>2</td>"
						+ 	"<td>Verify changed cost web against web</td>"
						+	"<td>"+ cost[1] +"</td>"
						+ 	"<td>"+ newValue2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			ReportPrinter.append("</table>");
		}
		else
		{
			
			ReportPrinter.append("<table border=1 style=width:800px>"
					+ "<tr><th>Test Case</th>"
					+ "<th>Test Description</th>"
					+ "<th>Expected Result</th>"
					+ "<th>Actual Result</th>"
					+ "<th>Test Status</th></tr>");
			System.out.println(":p");
			
			if(pmOriginalConValue.equals(pmConValue))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify changed cost xml against xml</td>"
						+	"<td>"+ pmOriginalConValue +"</td>"
						+ 	"<td>"+ pmConValue +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
				ReportPrinter.append("</table>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify changed cost xml against xml</td>"
						+	"<td>"+ pmOriginalConValue +"</td>"
						+ 	"<td>"+ pmConValue +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
				ReportPrinter.append("</table>");
				
				if(error.contains("The rates for this hotel have changed"))
				{
					ReportPrinter.append("<table border=1 style=width:800px>"
							+ "<tr><th>Error</th>"
							+ "<th>Changed value web</th>"
							+ "<th>Changed value xml</th></tr>");
					
					ReportPrinter.append	(	"<tr><td>1</td>"
							+ 	"<td>The rates for this hotel have changed</td>"
							+	"<td>"+ pmOriginalConValue +"</td>"
							+	"<td>"+ newValue2 +"</td></tr>");
					ReportPrinter.append("</table>");
					
				}
			}
		}
		
	}
	
	public void paymentPageError(StringBuffer ReportPrinter, hotelAvailability preReqAvailability, resHotelAvailability resRoomAvailability2, resHotelAvailability resRoomAvailability, resHotelAvailability resRoomAvailability3, resHotelAvailability resRoomAvailability4, String availability, String error, hotel hotel)
	{
		System.out.println(error);
		System.out.println(availability);
		System.out.println(resRoomAvailability.getTotalAmount());
		System.out.println(resRoomAvailability2.getTotalAmount());
		System.out.println(hotel.getPgType());
		System.out.println(hotel.getPgError());
		if(error != null && availability != null)
		{
			if(availability.contains("Availablity / Rates Changed"))
			{
				System.out.println(resRoomAvailability.getTotalAmount());
				System.out.println(resRoomAvailability2.getTotalAmount());
				System.out.println("error available");
			}
		}
		if(hotel.getPgType() != null && hotel.getPgType() !="")
		{
			ReportPrinter.append("<span>Payment gateway error</span>");
			
			ReportPrinter.append("<table border=1 style=width:800px>"
				+ "<tr><th>Error type</th>"
				+ "<th>Error</th></tr>");
			
			ReportPrinter.append	(	"<tr><td>"+ hotel.getPgType() +"</td>"
					+ 	"<td>"+ hotel.getPgError() +"</td></tr>");
			ReportPrinter.append("</table>");
			
			System.out.println(preReqAvailability.getDateFromDate());
			System.out.println(preReqAvailability.getHolderName());
		}
		
	}
}
