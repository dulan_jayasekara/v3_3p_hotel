package verification;

import java.io.Console;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import readAndWrite.hotel;
import readAndWrite.readHotelResults;
import readXml.hbPage1Req;
import readXml.resHotelAvailability;

public class inv27Verification {
	
	public void paymentPageAgainstRoomAvalabilityRes(StringBuffer ReportPrinter, hotel hotel, resHotelAvailability res, Map<String, String> pm, String hotelAddress)
	{
ReportPrinter.append("<span>(3) Room availability response against payment page</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		int cnt = 1;
		
		if(hotel.getHotelName().equals(res.getItemName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel name</td>"
					+	"<td>"+ res.getItemName() +"</td>"
					+ 	"<td>"+ hotel.getHotelName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel name</td>"
					+	"<td>"+ res.getItemName() +"</td>"
					+ 	"<td>"+ hotel.getHotelName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(hotel.getAddress().equals(hotelAddress))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel address</td>"
					+	"<td>"+ hotelAddress +"</td>"
					+ 	"<td>"+ hotel.getAddress() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel address</td>"
					+	"<td>"+ hotelAddress +"</td>"
					+ 	"<td>"+ hotel.getAddress() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		String conDate = formatDate(hotel.getCheckin());
		if(conDate.equals(res.getHotelRoom().get(0).getDateTimeFromDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getDateTimeFromDate() +"</td>"
					+ 	"<td>"+ conDate +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getDateTimeFromDate() +"</td>"
					+ 	"<td>"+ conDate +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		conDate = formatDate(hotel.getCheckOut());
		if(conDate.equals(res.getHotelRoom().get(0).getDateTimeToDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getDateTimeToDate() +"</td>"
					+ 	"<td>"+ conDate +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getDateTimeToDate() +"</td>"
					+ 	"<td>"+ conDate +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(hotel.getNights().equals(res.getNights()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights</td>"
					+	"<td>"+ res.getNights() +"</td>"
					+ 	"<td>"+ hotel.getNights() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights</td>"
					+	"<td>"+ res.getNights() +"</td>"
					+ 	"<td>"+ hotel.getNights() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(hotel.getRooms().equals(String.valueOf(res.getHotelRoom().size())))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ res.getHotelRoom().size() +"</td>"
					+ 	"<td>"+ hotel.getRooms() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ res.getHotelRoom().size() +"</td>"
					+ 	"<td>"+ hotel.getRooms() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		ArrayList<cancellationPolicy> cpList = new ArrayList<cancellationPolicy>();
		for(int i = 0 ; i < hotel.getCanPolicy().size() ; i++)
		{	
			cancellationPolicy cp = new cancellationPolicy();
			try {
				String[] 	temp = hotel.getCanPolicy().get(i).split("between");
							temp = temp[1].split("and");
				cp.setFromdate(temp[0].trim());
							temp = hotel.getCanPolicy().get(i).split("and");
							temp = temp[1].split("you");
				cp.setTodate(temp[0].trim());
				try {
								temp = hotel.getCanPolicy().get(i).split("charged.");
								temp = temp[1].split("of");
								temp = temp[0].trim().split(" ");
					cp.setAmoubt(temp[1].trim());
				} catch (Exception e) {
					// TODO: handle exception
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		cpList.add(cp);			
			
		}
		System.out.println("-----------------------------------------------------------------");
		ArrayList<String> fromdateList = new ArrayList<String>();
		for(int i = 0 ; i < cpList.size() ; i++)
		{
			fromdateList.add(formatDate(cpList.get(i).getFromdate()));
			System.out.println(formatDate(cpList.get(i).getFromdate()));
		}
		for(int i = 0 ; i < res.getCancel().size() ; i++)
		{
			System.out.println(res.getCancel().get(i).getFromdate());
		}
		
		System.out.println(hotel.getRooms());
		
		
	}

	public String formatDate(String date)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyy-MM-dd");
		String newDateString = sdf.format(d);
		return newDateString;
	}
	
	public void roomAvailabilityResAgainstReq(StringBuffer ReportPrinter, hbPage1Req req, resHotelAvailability res)
	{
		ReportPrinter.append("<span>(2) Room availability Response against Request</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		int cnt = 1;
		if(req.getHotelId().equals(res.getItemCode()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel code</td>"
					+	"<td>"+ res.getItemCode() +"</td>"
					+ 	"<td>"+ req.getHotelId() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel code</td>"
					+	"<td>"+ res.getItemCode() +"</td>"
					+ 	"<td>"+ req.getHotelId() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(req.getHotelName().equals(res.getItemName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel name</td>"
					+	"<td>"+ res.getItemName() +"</td>"
					+ 	"<td>"+ req.getHotelName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Hotel name</td>"
					+	"<td>"+ res.getItemName() +"</td>"
					+ 	"<td>"+ req.getHotelName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(req.getCheckin().equals(res.getHotelRoom().get(0).getDateTimeFromDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getDateTimeFromDate() +"</td>"
					+ 	"<td>"+ req.getCheckin() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getDateTimeFromDate() +"</td>"
					+ 	"<td>"+ req.getCheckin() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(req.getDuration().equals(res.getHotelRoom().get(0).getNights()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights staying</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getNights() +"</td>"
					+ 	"<td>"+ req.getDuration() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights staying</td>"
					+	"<td>"+ res.getHotelRoom().get(0).getNights() +"</td>"
					+ 	"<td>"+ req.getDuration() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(req.getRoom().size() == res.getHotelRoom().size())
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ res.getHotelRoom().size() +"</td>"
					+ 	"<td>"+ req.getRoom().size() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ res.getHotelRoom().size() +"</td>"
					+ 	"<td>"+ req.getRoom().size() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		ReportPrinter.append("</table>");
		System.out.println(req.getRoom().size());
		System.out.println(res.getHotelRoom().size());
	}
	
	public void hotelList(StringBuffer ReportPrinter, ArrayList<readHotelResults> hotelList, ArrayList<resHotelAvailability> resAvailability, String defaultCurrency, Map<String, String> currency)
	{
		ReportPrinter.append("<span>(1) Pre reservation req against res</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		ArrayList<String> hotelListCost = new ArrayList<String>();
		addProfitmarkup apm = new addProfitmarkup();
		String[] cusCurrency = hotelList.get(0).getTotalCost().split(" ");
		String conValue = "";
		for(int i = 0 ; i < hotelList.size() ; i++)
		{
			hotelListCost.add(hotelList.get(i).getTotalCost().replace("USD", "").trim());
		}
		currencyConveter cc = new currencyConveter();
		ArrayList<String> xmlCost = new ArrayList<String>();
		for(int i = 0 ; i < resAvailability.size() ; i++)
		{	 	
				conValue = cc.convert(defaultCurrency, resAvailability.get(i).getRc().get(0).getPrice(), cusCurrency[0], currency, resAvailability.get(i).getRc().get(0).getCurrency());
				conValue = String.valueOf(Integer.parseInt(conValue) + apm.profitMarkup(conValue, "10%"));
				xmlCost.add(conValue);
		}
		System.out.println(hotelListCost);
		System.out.println(xmlCost);
		
		ReportPrinter.append("</table>");
	}
	
}
