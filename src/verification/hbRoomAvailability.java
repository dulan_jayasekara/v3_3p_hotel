package verification;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import readAndWrite.hotel;
import readAndWrite.readHotelResults;
import readXml.hbPage1Req;
import readXml.resHotelAvailability;
import readXml.resavailableRoom;

public class hbRoomAvailability {
	private static Map<String, String>  txtProperty = new HashMap<String, String>();
	
	public void checkWhetherResultsAvailable()
	{
		
	}
	
	public void inv13AvailabilityReqAgainstWeb(StringBuffer ReportPrinter, hbPage1Req roomAvailabilityReq, ArrayList<readHotelResults> hotelList, String hotelName, String[] searchDetails)
	{
		int cnt = 1;
		
		ReportPrinter.append("<span>(2) Room availability Verification INV13 xml req against web</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		for(int i = 0 ; i < hotelList.size() ; i ++)
		{	
			if(hotelName.equals(hotelList.get(i).getTitle()))
			{
				if(hotelName.equals(hotelList.get(i).getTitle()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify hotel name</td>"
							+	"<td>"+ hotelName +"</td>"
							+ 	"<td>"+ hotelList.get(i).getTitle() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify hotel name</td>"
							+	"<td>"+ hotelName +"</td>"
							+ 	"<td>"+ hotelList.get(i).getTitle() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				//System.out.println(hotelList.get(i).getRoom().size());
				//System.out.println(roomAvailabilityReq.getGuest().size());
				ArrayList<String> adltCnt1 = new ArrayList<String>();
				ArrayList<String> adltCnt2 = new ArrayList<String>();
				ArrayList<String> chldCount1 = new ArrayList<String>();
				ArrayList<String> chldCount2 = new ArrayList<String>();
				String[] adlt = searchDetails[5].split("/");
				String[] chldrn = searchDetails[6].split("/");
				for(int j = 0 ; j < Integer.parseInt(searchDetails[4]) ; j++)
				{
					String[] roomInfo = hotelList.get(i).getRoom().get(j).getRoomInfo().split(" ");
					String adultCount = roomInfo[4];
					String childCount = roomInfo[6];
					try {
						
						
						adltCnt1.add(adlt[j]);
						adltCnt2.add(roomAvailabilityReq.getGuest().get(j).getAdultCount());
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						
						chldCount1.add(chldrn[j]);
						chldCount2.add(roomAvailabilityReq.getGuest().get(j).getChildCount());
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
				
				if(adltCnt1.containsAll(adltCnt2))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify adult count</td>"
							+	"<td>"+ adltCnt1 +"</td>"
							+ 	"<td>"+ adltCnt2 +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify adult count</td>"
							+	"<td>"+ adltCnt1 +"</td>"
							+ 	"<td>"+ adltCnt2 +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
				if(chldCount1.containsAll(chldCount2))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify child count</td>"
							+	"<td>"+ chldCount1 +"</td>"
							+ 	"<td>"+ chldCount2 +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify child count</td>"
							+	"<td>"+ chldCount1 +"</td>"
							+ 	"<td>"+ chldCount2 +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				cnt++;
			}
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}
	
	public void availablityReqAgainstWeb(StringBuffer ReportPrinter, hbPage1Req roomAvailabilityReq, ArrayList<readHotelResults> hotelList, String hotelName)
	{		
		int cnt = 1 ;
		
		
		ReportPrinter.append("<span>(2) Room availability Verification hb xml req against web</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		for(int i = 0 ; i < hotelList.size() ; i ++)
		{
			if(hotelName.equals(hotelList.get(i).getTitle()))
			{
				if(hotelName.equals(hotelList.get(i).getTitle()))
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify hotel name</td>"
							+	"<td>"+ hotelName +"</td>"
							+ 	"<td>"+ hotelList.get(i).getTitle() +"</td>"
							+	"<td class='passed'>Passed</td></tr>");
				}
				else
				{
					ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
							+ 	"<td>Verify hotel name</td>"
							+	"<td>"+ hotelName +"</td>"
							+ 	"<td>"+ hotelList.get(i).getTitle() +"</td>"
							+	"<td class='Failed'>Failed</td></tr>");
				}
				for(int j = 0 ; j < Integer.parseInt(roomAvailabilityReq.getRoomCount()) ; j++)
				{
					String[] roomInfo = hotelList.get(i).getRoom().get(j).getRoomInfo().split(" ");
					System.out.println(hotelList.get(i).getRoom().get(j).getRoomInfo());
					String adultCount = "";
					try {
						 adultCount = roomInfo[4];
					} catch (Exception e) {
						// TODO: handle exception
					}
					String childCount = "";
					try {
						childCount = roomInfo[6];
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						if(adultCount.equals(roomAvailabilityReq.getGuest().get(j).getAdultCount()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify adult count</td>"
									+	"<td>"+ adultCount +"</td>"
									+ 	"<td>"+ roomAvailabilityReq.getGuest().get(j).getAdultCount() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify adult count</td>"
									+	"<td>"+ adultCount +"</td>"
									+ 	"<td>"+ roomAvailabilityReq.getGuest().get(j).getAdultCount() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						if(childCount.equals(roomAvailabilityReq.getGuest().get(j).getChildCount()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify child count</td>"
									+	"<td>"+ childCount +"</td>"
									+ 	"<td>"+ roomAvailabilityReq.getGuest().get(j).getChildCount() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify child count</td>"
									+	"<td>"+ childCount +"</td>"
									+ 	"<td>"+ roomAvailabilityReq.getGuest().get(j).getChildCount() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
			}
			ReportPrinter.append("</table>");
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}
	
	public void inv13AvailabilityReqAgainstRes(StringBuffer ReportPrinter, hbPage1Req roomAvailabilityReq, resHotelAvailability roomAvailabilityRes, String addToCartError, String errorType, String defaultCurrency, Map<String, String> currency)
	{

		//System.out.println(errorType);
		//System.out.println(addToCartError);
		if(addToCartError.equals("*Sorry! This product is no longer available. Please choose another."))
		{
			ReportPrinter.append("<span>Errors and warnings</span>");
			
			ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Error Type</th>"
				+ "<th>Error</th></tr>");
			
			String[] newCost = addToCartError.split(":");
			//System.out.println(newCost[1]);
			ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
					+ 	"<td>"+ addToCartError +"</td></tr>");
			ReportPrinter.append("</table>");
		}
		else
		{
			int cnt = 1;
			Properties properties = new Properties();
			try 
			{
				//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
				FileReader reader =new FileReader(new File("D://workSpace//aug7//hotelXmlValidation//profitMarkUps.properties"));
				
				properties.load(reader);
			} 
			catch (IOException e)
			{
				// System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value = properties.getProperty(key);
			    txtProperty.put(key, value);
			}
				
			if(addToCartError.equals("") || addToCartError.equals(null) || addToCartError.isEmpty() || addToCartError.equals(" "))
			{
				ReportPrinter.append("<span>(2.1)Errors and warnings</span>");
				
				ReportPrinter.append("<table border=1 style=width:100%>"
					+ "<tr><th>Error Type</th>"
					+ "<th>Error</th></tr>");
				
				String[] newCost = addToCartError.split(":");
				try {
					//System.out.println(newCost[1]);
				} catch (Exception e) {
					// TODO: handle exception
				}
				ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
						+ 	"<td>"+ addToCartError +"</td></tr>");
				ReportPrinter.append("</table>");
			}
			else
			{
				try {
					String[] newCost = addToCartError.split(":");
					//System.out.println(newCost[1]);
					String[] newCost1 = newCost[1].split(" ");
					//System.out.println("------------------------------------------------------");
					//System.out.println(newCost1[1].trim());
					//System.out.println(newCost1[2].trim());
					String currencyCode = newCost1[1].trim();
					String[] newCost2 = newCost1[2].split("\\.");
					//System.out.println("------------------------------------------------------");
					//System.out.println(newCost2[0]);
					//System.out.println(newCost2[1]);
					String cost = newCost2[0].concat(".").concat(newCost2[1]);
					//System.out.println(cost);
					//System.out.println(roomAvailabilityRes.getTotalAmount());
					//System.out.println(roomAvailabilityRes.getCurrencyCode());
					
					ReportPrinter.append("<span>Errors and warnings</span>");
					
					ReportPrinter.append("<table border=1 style=width:100%>"
						+ "<tr><th>Error Type</th>"
						+ "<th>Error</th></tr>");
					ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
							+ 	"<td>"+ addToCartError +"</td></tr>");
					ReportPrinter.append("</table>");
					
					currencyConveter cc = new currencyConveter();
					String cCost = cc.convert(defaultCurrency, roomAvailabilityRes.getTotalAmount(), currencyCode, currency, roomAvailabilityRes.getCurrencyCode());
					addProfitmarkup apm = new addProfitmarkup();
					int pmAddedCost = apm.profitMarkup(cCost, txtProperty.get("hb"));
					//System.out.println(cost);
					//System.out.println(pmAddedCost);
					ReportPrinter.append("<span>Add to cart error</span>");
					
					ReportPrinter.append("<table border=1 style=width:100%>"
							+ "<tr><th>Test Case</th>"
							+ "<th>Test Description</th>"
							+ "<th>Expected Result</th>"
							+ "<th>Actual Result</th>"
							+ "<th>Test Status</th></tr>");
					if(cost.equals(String.valueOf(pmAddedCost)))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify changed cost</td>"
								+	"<td>"+ pmAddedCost +"</td>"
								+ 	"<td>"+ cost +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify changed cost</td>"
								+	"<td>"+ pmAddedCost +"</td>"
								+ 	"<td>"+ cost +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
					ReportPrinter.append("</table>");
				} catch (Exception e) {
					// TODO: handle exception
				}

				
			}
		
			ReportPrinter.append("<span>Room availability Verification inv13 xml requset against response</span>");
		
			ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
			
			//System.out.println(roomAvailabilityReq.getCheckin());
			//System.out.println(roomAvailabilityRes.getSingleRoom().getDateTimeFromDate());
			if(roomAvailabilityRes.getSingleRoom().getDateTimeFromDate().contains(roomAvailabilityReq.getCheckin()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ roomAvailabilityReq.getCheckin() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getDateFromDate() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ roomAvailabilityReq.getCheckin() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getDateFromDate() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			if(roomAvailabilityReq.getGuest().size() == roomAvailabilityRes.getRoomAvailability().size())
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Room count</td>"
						+	"<td>"+ roomAvailabilityReq.getGuest().size() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getRoomAvailability().size() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Room count</td>"
						+	"<td>"+ roomAvailabilityReq.getGuest().size() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getRoomAvailability().size() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			for(int i = 0, k = roomAvailabilityReq.getGuest().size()-1 ; i < roomAvailabilityReq.getGuest().size() ; i++, k--)
			{
				//System.out.println(roomAvailabilityReq.getGuest().get(i).getAdultCount());
				//System.out.println(roomAvailabilityRes.getRoomAvailability().size());
				try {
					if(roomAvailabilityReq.getGuest().get(i).getAdultCount().equals(roomAvailabilityRes.getRoomAvailability().get(k).getAdultCount()))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify adult count</td>"
								+	"<td>"+ roomAvailabilityReq.getGuest().get(i).getAdultCount() +"</td>"
								+ 	"<td>"+ roomAvailabilityRes.getRoomAvailability().get(k).getAdultCount() +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify adult count</td>"
								+	"<td>"+ roomAvailabilityReq.getGuest().get(i).getAdultCount() +"</td>"
								+ 	"<td>"+ roomAvailabilityRes.getRoomAvailability().get(k).getAdultCount() +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
					cnt++;
					if(roomAvailabilityReq.getGuest().get(i).getChildCount().equals(roomAvailabilityRes.getRoomAvailability().get(k).getChildCount()))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify Child count</td>"
								+	"<td>"+ roomAvailabilityReq.getGuest().get(i).getChildCount() +"</td>"
								+ 	"<td>"+ roomAvailabilityRes.getRoomAvailability().get(k).getChildCount() +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify Child count</td>"
								+	"<td>"+ roomAvailabilityReq.getGuest().get(i).getChildCount() +"</td>"
								+ 	"<td>"+ roomAvailabilityRes.getRoomAvailability().get(k).getChildCount() +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
					cnt++;
					//System.out.println(roomAvailabilityReq.getGuest().get(i).getCus().size());
					for(int j = 0, x = roomAvailabilityReq.getGuest().get(i).getCus().size()-1 ; j < roomAvailabilityReq.getGuest().get(i).getCus().size() ; j++, x--)
					{
						if(roomAvailabilityReq.getGuest().get(i).getCus().get(j).getAge().equals(roomAvailabilityRes.getRoomAvailability().get(i).getGuest().get(x).getCustomerAge()))
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify age</td>"
									+	"<td>"+ roomAvailabilityReq.getGuest().get(i).getCus().get(j).getAge() +"</td>"
									+ 	"<td>"+ roomAvailabilityRes.getRoomAvailability().get(i).getGuest().get(x).getCustomerAge() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
									+ 	"<td>Verify age</td>"
									+	"<td>"+ roomAvailabilityReq.getGuest().get(i).getCus().get(j).getAge() +"</td>"
									+ 	"<td>"+ roomAvailabilityRes.getRoomAvailability().get(i).getGuest().get(x).getCustomerAge() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						cnt++;
					}
				
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			
			ReportPrinter.append("</table>");
			ReportPrinter.append	("<b></b><br>"
                    +	 "<hr size=4>");

		}
			
	}
	
	public void availablityReqAgaistRes(StringBuffer ReportPrinter, hbPage1Req roomAvailabilityReq, resHotelAvailability roomAvailabilityRes, String addToCartError, String errorType, String defaultCurrency, Map<String, String> currency)
	{
		//System.out.println(errorType);
		//System.out.println(addToCartError);
		if(addToCartError.equals("*Sorry! This product is no longer available. Please choose another."))
		{
			ReportPrinter.append("<span>Errors and warnings</span>");
			
			ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Error Type</th>"
				+ "<th>Error</th></tr>");
			
			String[] newCost = addToCartError.split(":");
			//System.out.println(newCost[1]);
			ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
					+ 	"<td>"+ addToCartError +"</td></tr>");
			ReportPrinter.append("</table>");
		}
		else
		{
			int cnt = 1;
			Properties properties = new Properties();
			try 
			{
				//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
				FileReader reader =new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitMarkUps.properties"));
				
				properties.load(reader);
			} 
			catch (IOException e)
			{
				// System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value = properties.getProperty(key);
			    txtProperty.put(key, value);
			}
				
			System.out.println(addToCartError);
			if(!addToCartError.equals("") || !addToCartError.equals(null) || !addToCartError.isEmpty())
			{
				try {
					ReportPrinter.append("<span>(3) Errors and warnings</span>");
					
					ReportPrinter.append("<table border=1 style=width:100%>"
						+ "<tr><th>Error Type</th>"
						+ "<th>Error</th></tr>");
					
					String[] newCost = addToCartError.split(":");
					//System.out.println(newCost[1]);
					ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
							+ 	"<td>"+ addToCartError +"</td></tr>");
					ReportPrinter.append("</table>");
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			else
			{
				try {
					String[] newCost = addToCartError.split(":");
					//System.out.println(newCost[0]);
					String[] newCost1 = newCost[1].split(" ");
					//System.out.println("------------------------------------------------------");
					//System.out.println(newCost1[1].trim());
					//System.out.println(newCost1[2].trim());
					String currencyCode = newCost1[1].trim();
					String[] newCost2 = newCost1[2].split("\\.");
					//System.out.println("------------------------------------------------------");
					//System.out.println(newCost2[0]);
					//System.out.println(newCost2[1]);
					String cost = newCost2[0].concat(".").concat(newCost2[1]);
					//System.out.println(cost);
					//System.out.println(roomAvailabilityRes.getTotalAmount());
					//System.out.println(roomAvailabilityRes.getCurrencyCode());
					
					currencyConveter cc = new currencyConveter();
					String cCost = cc.convert(defaultCurrency, roomAvailabilityRes.getTotalAmount(), currencyCode, currency, roomAvailabilityRes.getCurrencyCode());
					addProfitmarkup apm = new addProfitmarkup();
					int pmAddedCost = apm.profitMarkup(cCost, txtProperty.get("hb"));
					//System.out.println(cost);
					//System.out.println(pmAddedCost);
					
					ReportPrinter.append("<span>Errors and warnings</span>");
					
					ReportPrinter.append("<table border=1 style=width:100%>"
						+ "<tr><th>Error Type</th>"
						+ "<th>Error</th></tr>");
					ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
							+ 	"<td>"+ addToCartError +"</td></tr>");
					ReportPrinter.append("</table>");
					
					ReportPrinter.append("<span>Add to cart error</span>");
					
					ReportPrinter.append("<table border=1 style=width:100%>"
							+ "<tr><th>Test Case</th>"
							+ "<th>Test Description</th>"
							+ "<th>Expected Result</th>"
							+ "<th>Actual Result</th>"
							+ "<th>Test Status</th></tr>");
					if(cost.equals(String.valueOf(pmAddedCost)))
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify changed cost</td>"
								+	"<td>"+ pmAddedCost +"</td>"
								+ 	"<td>"+ cost +"</td>"
								+	"<td class='passed'>Passed</td></tr>");
					}
					else
					{
						ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
								+ 	"<td>Verify changed cost</td>"
								+	"<td>"+ pmAddedCost +"</td>"
								+ 	"<td>"+ cost +"</td>"
								+	"<td class='Failed'>Failed</td></tr>");
					}
					ReportPrinter.append("</table>");
				} catch (Exception e) {
					// TODO: handle exception
				}

				
			}
		
			ReportPrinter.append("<span>(3.1)Room availability Verification hb xml requset against response</span>");
		
			ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
			
			if(roomAvailabilityReq.getCheckin().equals(roomAvailabilityRes.getDateFromDate()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ roomAvailabilityReq.getCheckin() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getDateFromDate() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ roomAvailabilityReq.getCheckin() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getDateFromDate() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			if(roomAvailabilityReq.getCheckout().equals(roomAvailabilityRes.getDateToDate()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkout date</td>"
						+	"<td>"+ roomAvailabilityReq.getCheckout() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getDateToDate() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkout date</td>"
						+	"<td>"+ roomAvailabilityReq.getCheckout() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getDateToDate() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			if(roomAvailabilityReq.getDestination().equals(roomAvailabilityRes.getDestinationCode()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Destination</td>"
						+	"<td>"+ roomAvailabilityReq.getDestination() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getDestinationCode() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Destination</td>"
						+	"<td>"+ roomAvailabilityReq.getDestination() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getDestinationCode() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			if(roomAvailabilityReq.getRoomCount().equals(String.valueOf(roomAvailabilityRes.getHotelRoom().size())))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify Room count</td>"
						+	"<td>"+ roomAvailabilityReq.getRoomCount() +"</td>"
						+ 	"<td>"+ roomAvailabilityRes.getHotelRoom().size() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			
			ArrayList<String> adltCnt1 	= 	new ArrayList<String>();
			ArrayList<String> adltCnt2 	= 	new ArrayList<String>();
			ArrayList<String> chldCnt1 	= 	new ArrayList<String>();
			ArrayList<String> chldCnt2 	= 	new ArrayList<String>();
			ArrayList<String> age1		=	new ArrayList<String>();
			ArrayList<String> age2		= 	new ArrayList<String>();
			for(int i = 0 ; i < roomAvailabilityReq.getGuest().size() ; i++)
			{
				adltCnt1.add(roomAvailabilityReq.getGuest().get(i).getAdultCount());
				adltCnt2.add(roomAvailabilityRes.getRoomAvailability().get(i).getAdultCount());
				
				chldCnt1.add(roomAvailabilityReq.getGuest().get(i).getChildCount());
				chldCnt2.add(roomAvailabilityRes.getRoomAvailability().get(i).getChildCount());
				
				for(int j = 0 ; j < roomAvailabilityReq.getGuest().get(i).getCus().size() ; j++)
				{
					age1.add(roomAvailabilityReq.getGuest().get(i).getCus().get(j).getAge());
					age2.add(roomAvailabilityRes.getRoomAvailability().get(i).getGuest().get(j).getCustomerAge());
				}
			}
			
			if(adltCnt1.equals(adltCnt2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify adult count</td>"
						+	"<td>"+ adltCnt1 +"</td>"
						+ 	"<td>"+ adltCnt2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify adult count</td>"
						+	"<td>"+ adltCnt1 +"</td>"
						+ 	"<td>"+ adltCnt2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(chldCnt1.equals(chldCnt2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify child count</td>"
						+	"<td>"+ chldCnt1 +"</td>"
						+ 	"<td>"+ chldCnt2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify child count</td>"
						+	"<td>"+ chldCnt1 +"</td>"
						+ 	"<td>"+ chldCnt2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			
			if(age1.equals(age2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify child age</td>"
						+	"<td>"+ age1 +"</td>"
						+ 	"<td>"+ age2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify child age</td>"
						+	"<td>"+ age1 +"</td>"
						+ 	"<td>"+ age2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
			System.out.println(adltCnt1);
			System.out.println(adltCnt2);
			ReportPrinter.append("</table>");
			ReportPrinter.append	("<b></b><br>"
                    +	 "<hr size=4>");

		}
			}
	
	public void inv13AvailablityResAgainstWeb(hotel hotel, StringBuffer ReportPrinter, resHotelAvailability resRoomAvailability, Map<String, String> currency, String defaultCurrency)
	{

		Properties properties = new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitMarkUps.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			// System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		int cnt = 1;
		ReportPrinter.append("<span>(5)Room availability Verification hb payment against response</span>");
	
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		if(hotel.getHotelName().equals(resRoomAvailability.getHotelName()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ hotel.getHotelName() +"</td>"
					+ 	"<td>"+ resRoomAvailability.getHotelName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ hotel.getHotelName() +"</td>"
					+ 	"<td>"+ resRoomAvailability.getHotelName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		String checkin = dateFormat(hotel.getCheckin());
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		Date d = null;
		String newDateString = null;
		try {
			d = sdf.parse(hotel.getCheckin());
			sdf.applyPattern("yyyy-MM-dd");
			newDateString = sdf.format(d);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(newDateString);
		//System.out.println(resRoomAvailability.getSingleRoom().getDateTimeFromDate());
		if(resRoomAvailability.getSingleRoom().getDateTimeFromDate().contains(newDateString))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ newDateString +"</td>"
					+ 	"<td>"+ resRoomAvailability.getSingleRoom().getDateTimeFromDate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ newDateString +"</td>"
					+ 	"<td>"+ resRoomAvailability.getSingleRoom().getDateTimeFromDate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		int roomCount = 0 ;
		/*for(int i = 0 ; i < resRoomAvailability.getRoomAvailability().size() ; i++)
		{
			roomCount = roomCount + Integer.parseInt(resRoomAvailability.getRoomAvailability().get(i).getRoomCount());
		}*/
		//System.out.println(hotel.getRoom().size());
		//System.out.println(resRoomAvailability.getRoomAvailability().size());
		try {
			if(hotel.getRooms().equals(String.valueOf(resRoomAvailability.getRoomAvailability().size())))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room count</td>"
						+	"<td>"+ hotel.getRooms() +"</td>"
						+ 	"<td>"+ resRoomAvailability.getRoomAvailability().size() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room count</td>"
						+	"<td>"+ hotel.getRooms() +"</td>"
						+ 	"<td>"+ resRoomAvailability.getRoomAvailability().size() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		//System.out.println(resRoomAvailability.getMinPrice());
		currencyConveter cc = new currencyConveter();
		String cValue = cc.convert(defaultCurrency, resRoomAvailability.getMinPrice(), hotel.getCurrencyCode(), currency, resRoomAvailability.getCurrencyCode());
		addProfitmarkup addpm = new addProfitmarkup();
		int conValue = addpm.profitMarkup(cValue, txtProperty.get("hb"));
		conValue = Integer.parseInt(cValue) + conValue;
		int value = Integer.parseInt(hotel.getPay().getSubTotal());
		/*if(conValue + 2 >= value && value >= conValue - 2)
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ value +"</td>"
					+ 	"<td>"+ conValue +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ value +"</td>"
					+ 	"<td>"+ conValue +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;*/
		ArrayList<String> adlt1 = new ArrayList<String>();
		ArrayList<String> adlt2 = new ArrayList<String>();
		ArrayList<String> chld1 = new ArrayList<String>();
		ArrayList<String> chld2 = new ArrayList<String>();
		ArrayList<String> room1 = new ArrayList<String>();
		ArrayList<String> room2 = new ArrayList<String>();
		ArrayList<String> cost1 = new ArrayList<String>();
		ArrayList<String> cost2 = new ArrayList<String>();
		ArrayList<String> board1 = new ArrayList<String>();
		ArrayList<String> board2 = new ArrayList<String>();
		
		System.out.println();
		for(int i = 0 ; i < hotel.getRoom().size() ; i++)
		{
			try {
				adlt1.add(resRoomAvailability.getRoomAvailability().get(i).getAdultCount());
				adlt2.add(hotel.getRoom().get(i).getAdultCount());
				
				chld1.add(resRoomAvailability.getRoomAvailability().get(i).getChildCount());
				chld2.add(hotel.getRoom().get(i).getChildCount());
				
				room1.add(resRoomAvailability.getSingleRoom().getRoomName());
				room2.add(hotel.getRoom().get(i).getRoomType());
				
				cValue = cc.convert(defaultCurrency, resRoomAvailability.getRoomAvailability().get(i).getPrice(), hotel.getCurrencyCode(), currency, resRoomAvailability.getCurrencyCode());
				int pmValue = addpm.profitMarkup(cValue, txtProperty.get("hb"));
				pmValue = pmValue + Integer.parseInt(cValue);
				value = Integer.parseInt(hotel.getRoom().get(i).getTotalRate());
				cost2.add(String.valueOf(pmValue));
				cost1.add(String.valueOf(value));
				
				try {
					board1.add(resRoomAvailability.getHotelRoom().get(i).getBoard());
					board2.add(hotel.getRoom().get(i).getBoardType());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		if(adlt1.containsAll(adlt2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adlt1 +"</td>"
					+ 	"<td>"+ adlt2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
				+ 	"<td>Verify adult count</td>"
				+	"<td>"+ adlt1 +"</td>"
				+ 	"<td>"+ adlt2 +"</td>"
				+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(chld1.containsAll(chld2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chld1 +"</td>"
					+ 	"<td>"+ chld2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chld1 +"</td>"
					+ 	"<td>"+ chld2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(room1.containsAll(room2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ room1 +"</td>"
					+ 	"<td>"+ room2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ room1 +"</td>"
					+ 	"<td>"+ room2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(cost1.containsAll(cost2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify cost per room</td>"
					+	"<td>"+ cost1 +"</td>"
					+ 	"<td>"+ cost2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify cost per room</td>"
					+	"<td>"+ cost1 +"</td>"
					+ 	"<td>"+ cost2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(board1.containsAll(board2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify board type</td>"
					+	"<td>"+ board1 +"</td>"
					+ 	"<td>"+ board2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify board type</td>"
					+	"<td>"+ board1 +"</td>"
					+ 	"<td>"+ board2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	
	}
	
	public void availablityResAgainstWeb(hotel hotel, StringBuffer ReportPrinter, resHotelAvailability resRoomAvailability, Map<String, String> currency, String defaultCurrency)
	{
		Properties properties = new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("D://workSpace//hotelXmlValidation//profitMarkUps.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 //System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		int cnt = 1;
		ReportPrinter.append("<span>(6) Room availability Verification hb payment against response</span>");
	
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		if(hotel.getHotelName().equals(resRoomAvailability.getHotelInfoname()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ resRoomAvailability.getHotelInfoname() +"</td>"
					+ 	"<td>"+ hotel.getHotelName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ resRoomAvailability.getHotelInfoname() +"</td>"
					+ 	"<td>"+ hotel.getHotelName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		String checkin = dateFormat(hotel.getCheckin());
		if(checkin.equals(resRoomAvailability.getDateFromDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ resRoomAvailability.getDateFromDate() +"</td>"
					+ 	"<td>"+ checkin +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ resRoomAvailability.getDateFromDate() +"</td>"
					+ 	"<td>"+ checkin +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		String checkout = dateFormat(hotel.getCheckOut());
		if(checkout.equals(resRoomAvailability.getDateToDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ resRoomAvailability.getDateToDate() +"</td>"
					+ 	"<td>"+ checkout +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ resRoomAvailability.getDateToDate() +"</td>"
					+ 	"<td>"+ checkout +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		cnt++;
		/*for(int i = 0 ; i < resRoomAvailability.getHotelRoom().size() ; i++)
		{
			System.out.println(hotel.getCancellationDeadLine());
			String date = dateFormat2(hotel.getCancellationDeadLine());
			System.out.println(date);
			if(date.equals(resRoomAvailability.getHotelRoom().get(i).getDateTimeFromDate()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify cancellation deadline</td>"
						+	"<td>"+ date +"</td>"
						+ 	"<td>"+ resRoomAvailability.getHotelRoom().get(i).getDateTimeFromDate() +"</td>"
						+	"<td>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify cancellation deadline</td>"
						+	"<td>"+ date +"</td>"
						+ 	"<td>"+ resRoomAvailability.getHotelRoom().get(i).getDateTimeFromDate() +"</td>"
						+	"<td>Failed</td></tr>");
			}
			cnt++;
		}*/
		int roomCount = 0 ;
		for(int i = 0 ; i < resRoomAvailability.getRoomAvailability().size() ; i++)
		{
			roomCount = roomCount + Integer.parseInt(resRoomAvailability.getRoomAvailability().get(i).getRoomCount());
		}
		//System.out.println(hotel.getRooms());
		//System.out.println(hotel.getRoom().size());
		if(hotel.getRoom().size() == roomCount)
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room count</td>"
					+	"<td>"+ hotel.getRoom().size() +"</td>"
					+ 	"<td>"+ roomCount +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room count</td>"
					+	"<td>"+ hotel.getRoom().size() +"</td>"
					+ 	"<td>"+ roomCount +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		currencyConveter cc = new currencyConveter();
		String cValue = cc.convert(defaultCurrency, resRoomAvailability.getTotalAmount(), hotel.getCurrencyCode(), currency, resRoomAvailability.getCurrencyCode());
		System.out.println(cValue);
		addProfitmarkup addpm = new addProfitmarkup();
		int conValue = addpm.profitMarkup(cValue, txtProperty.get("hb"));
		conValue = conValue + Integer.parseInt(cValue);
		System.out.println(hotel.getPay().getSubTotal());
		int value = Integer.parseInt(hotel.getPay().getSubTotal());
		if(conValue + 2 >= value && value >= conValue - 2)
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ conValue  +"</td>"
					+ 	"<td>"+ value +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ conValue +"</td>"
					+ 	"<td>"+ value +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		for(int i = 0 ; i < hotel.getRoom().size() ; i++)
		{
			System.out.println("hotel -- " + hotel.getRoom().get(i).getAdultCount());
			System.out.println("hotel -- " + hotel.getRoom().get(i).getChildCount());
			System.out.println("res -- " + resRoomAvailability.getRoomAvailability().get(i).getAdultCount());
			System.out.println("res -- " + resRoomAvailability.getRoomAvailability().get(i).getChildCount());
		}
		
		ArrayList<String> adltCnt1 = new ArrayList<String>();
		ArrayList<String> adltCnt2 = new ArrayList<String>();
		ArrayList<String> chldCnt1 = new ArrayList<String>();
		ArrayList<String> chldCnt2 = new ArrayList<String>();
		ArrayList<String> rmType1 = new ArrayList<String>();
		ArrayList<String> rmType2 = new ArrayList<String>();
		ArrayList<String> cpr1 = new ArrayList<String>();
		ArrayList<String> cpr2 = new ArrayList<String>();
		ArrayList<String> bt1 = new ArrayList<String>();
		ArrayList<String> bt2 = new ArrayList<String>();
		for(int i = 0 ; i < hotel.getRoom().size() ; i++)
		{
			adltCnt2.add(hotel.getRoom().get(i).getAdultCount());
			adltCnt1.add(resRoomAvailability.getRoomAvailability().get(i).getAdultCount());
			
			chldCnt2.add(hotel.getRoom().get(i).getChildCount());
			chldCnt1.add(resRoomAvailability.getRoomAvailability().get(i).getChildCount());
			
			rmType2.add(hotel.getRoom().get(i).getRoomType());
			rmType1.add(resRoomAvailability.getHotelRoom().get(i).getRoom());
			
			cValue = cc.convert(defaultCurrency, resRoomAvailability.getHotelRoom().get(i).getPrice(), hotel.getCurrencyCode(), currency, resRoomAvailability.getCurrencyCode());
			int pmValue = addpm.profitMarkup(cValue, txtProperty.get("hb"));
			pmValue = pmValue + Integer.parseInt(cValue);
			value = Integer.parseInt(hotel.getRoom().get(i).getTotalRate());
			
			cpr1.add(String.valueOf(pmValue));
			cpr2.add(String.valueOf(value));
		
			bt2.add(hotel.getRoom().get(i).getBoardType());
			bt1.add(resRoomAvailability.getHotelRoom().get(i).getBoard());
		}
		
		if(adltCnt1.containsAll(adltCnt2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adltCnt1  +"</td>"
					+ 	"<td>"+ adltCnt2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adltCnt1  +"</td>"
					+ 	"<td>"+ adltCnt2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(chldCnt1.containsAll(chldCnt2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chldCnt1  +"</td>"
					+ 	"<td>"+ chldCnt2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
				+ 	"<td>Verify child count</td>"
				+	"<td>"+ chldCnt1  +"</td>"
				+ 	"<td>"+ chldCnt2 +"</td>"
				+	"<td class='Failed'>Failed</td></tr>");
			
		}
		cnt++;
		
		if(rmType1.containsAll(rmType2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ rmType1  +"</td>"
					+ 	"<td>"+ rmType2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ rmType1  +"</td>"
					+ 	"<td>"+ rmType2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(cpr1.containsAll(cpr2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify cost per room</td>"
					+	"<td>"+ cpr1  +"</td>"
					+ 	"<td>"+ cpr2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify cost per room</td>"
					+	"<td>"+ cpr1  +"</td>"
					+ 	"<td>"+ cpr2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(bt1.containsAll(bt2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify board type</td>"
					+	"<td>"+ bt1  +"</td>"
					+ 	"<td>"+ bt2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify board type</td>"
					+	"<td>"+ bt1  +"</td>"
					+ 	"<td>"+ bt2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}
	
	public String dateFormat(String date)
	{
		//System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(sdf.format(d));
		sdf.applyPattern("yyyyMMdd");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
	public String dateFormat2(String date)
	{
		//System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyyMMdd");
		//System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
}
