package verification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import excelReader.ExcelReader;

import readAndWrite.readHotelResults;
import readAndWrite.readSearchDetails;
import readXml.resHotelAvailability;

public class hbPage1Res {
	private static Map<String, String>  txtProperty = new HashMap<String, String>();
	
	public void inv13VerifyAvailabilityResponse(ArrayList<readHotelResults> hotelList, ArrayList<resHotelAvailability> resAvailability, StringBuffer ReportPrinter, String defaultCurrency, Map<String, String> currency, int inv13HotelCount, resHotelAvailability hbresAvailability) throws FileNotFoundException
	{


		ExcelReader newReader = new ExcelReader();
		readSearchDetails	searchgRead = new readSearchDetails();
		FileInputStream stream =new FileInputStream(new File("excel/searchDetails.xls"));
		
		ArrayList<Map<Integer, String>> ContentList = new ArrayList<Map<Integer,String>>();
		ContentList = newReader.contentReading(stream);
		
		Map<Integer, String>  content = new HashMap<Integer, String>();
		content   = ContentList.get(0);
		
		Iterator<Map.Entry<Integer, String>>   mapiterator = content.entrySet().iterator();
		
		
			Map.Entry<Integer, String> Entry = (Map.Entry<Integer, String>)mapiterator.next();
			String Content = Entry.getValue();
			String[] searchDetails = Content.split(",");
		
		Properties properties 		= new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader 		=	new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitMarkUps.properties"));
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 //System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value 	= properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		//System.out.println(hotelList.size());
		ReportPrinter.append("<span>(4) Verify results</span>");
		ReportPrinter.append("<br>");
		ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
		if(hotelList.size() == inv13HotelCount)
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify number of hotels</td>"
					+	"<td>"+ inv13HotelCount +"</td>"
					+ 	"<td>"+ hotelList.size() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ inv13HotelCount +"</td>"
					+ 	"<td>"+ hotelList.size() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		System.out.println(hotelList.size());
		//for(int i = 0 ; i < hotelList.size() ; i++)
		for(int j = 0 ; j < resAvailability.size() ; j++)
		{
			
			int count = 1;
			for(int i = 0 ; i < hotelList.size() ; i++)
			{
				if(hotelList.get(i).getSupplier().trim().equalsIgnoreCase("INV13"))
				{
					if(hotelList.get(i).getTitle().trim().equals(resAvailability.get(j).getHotelName().trim()))
					{
						ReportPrinter.append("<span>"+ hotelList.get(i).getTitle() +"</span>");
						
						ReportPrinter.append("<table border=1 style=width:100%>"
								+ "<tr><th>Test Case</th>"
								+ "<th>Test Description</th>"
								+ "<th>Expected Result</th>"
								+ "<th>Actual Result</th>"
								+ "<th>Test Status</th></tr>");
						
						
						if(hotelList.get(i).getTitle().trim().equalsIgnoreCase(resAvailability.get(j).getHotelName()))
						{
							if(hotelList.get(i).getTitle().trim().equalsIgnoreCase(resAvailability.get(j).getHotelName()))
							{
								System.out.println(hotelList.get(i).getTitle());
								System.out.println(resAvailability.get(j).getHotelName());
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify hotel name</td>"
										+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
										+ 	"<td>"+ resAvailability.get(j).getHotelName() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify hotel name</td>"
										+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
										+ 	"<td>"+ resAvailability.get(j).getHotelName() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							ArrayList<String> roomName1 	=	 	new ArrayList<String>();
							ArrayList<String> price1 		= 		new ArrayList<String>();
							ArrayList<String> ratePlan1 	= 		new ArrayList<String>();
							ArrayList<String> roomName2 	=		new ArrayList<String>();
							ArrayList<String> price2 		= 		new ArrayList<String>();
							ArrayList<String> ratePlan2 	= 		new ArrayList<String>();
							//System.out.println(resAvailability.get(j).getHotelName());
							//System.out.println(hotelList.get(i).getTitle());
							//System.out.println(resAvailability.get(j).getHotelRoom().size());
							//System.out.println(hotelList.get(i).getRoom().size());
							for(int k = 0 ; k < resAvailability.get(j).getHotelRoom().size() ; k++)
							{
								try {
									//System.out.println(hotelList.get(i).getRoom().get(k).getRoomType());
									//System.out.println(resAvailability.get(j).getHotelRoom().get(k).getRoomName());
									if(roomName1.contains(hotelList.get(i).getRoom().get(k).getRoomType()))
									{
										
									}
									else
									{
										roomName1.add(hotelList.get(i).getRoom().get(k).getRoomType());
									}
									if(roomName2.contains(resAvailability.get(j).getHotelRoom().get(k).getRoomName()))
									{
										
									}
									else
									{
										roomName2.add(resAvailability.get(j).getHotelRoom().get(k).getRoomName());
									}
									
								} catch (Exception e) {
									// TODO: handle exception
								}
								
								resAvailability.get(j).getCurrencyCode();
								String[] supCC = hotelList.get(i).getTotalCost().split(" ");
								currencyConveter converter =  new currencyConveter();
								String value = resAvailability.get(j).getHotelRoom().get(k).getAvailableRoom().get(0).getPrice();
								addProfitmarkup pm	=  new addProfitmarkup();
								String convertedValue	= converter.convert(defaultCurrency, value, supCC[0].trim(), currency, resAvailability.get(0).getCurrencyCode());
								int pmAddedPrice	=	pm.profitMarkup(convertedValue, txtProperty.get("inv13"));
								pmAddedPrice  =  Integer.parseInt(convertedValue) + pmAddedPrice;
								String[] webValue = null;
								String wValue = "";
								try {
									webValue = hotelList.get(i).getRoom().get(k).getRate().split(" ");
									wValue = webValue[1];
									int iwebValue	= Integer.parseInt(wValue) ;
									if(price1.contains(String.valueOf(iwebValue)))
									{
										
									}
									else
									{
										price1.add(String.valueOf(iwebValue));
									}
									if(price2.contains(String.valueOf(pmAddedPrice)))
									{
										
									}
									else
									{
										price2.add(String.valueOf(pmAddedPrice));
									}
									
								
								} catch (Exception e) {
									// TODO: handle exception
								}
								count++;
								try {
									//System.out.println(hotelList.get(i).getRoom().size());
									//System.out.println(resAvailability.get(j).getHotelRoom().get(k).getAvailableRoom().size());
									if(ratePlan1.contains(hotelList.get(i).getRoom().get(k).getRatePlan()))
									{
									
									}
									else
									{
										ratePlan1.add(hotelList.get(i).getRoom().get(k).getRatePlan());
									}
									ratePlan1.add(hotelList.get(i).getRoom().get(k).getRatePlan());
									for(int a = 0 ; a < resAvailability.get(j).getHotelRoom().get(k).getAvailableRoom().size() ; a++)
									{
										if(ratePlan2.contains(resAvailability.get(j).getHotelRoom().get(k).getAvailableRoom().get(a).getBoardName()))
										{
											
										}
										else
										{
											ratePlan2.add(resAvailability.get(j).getHotelRoom().get(k).getAvailableRoom().get(a).getBoardName());
										}
										
									}
									count++;
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
							ArrayList<String> temp1 = new ArrayList<String>();
							for(int a = 0 ; a < roomName1.size() ; a++)
							{
								if(temp1.contains(roomName1.get(a).toLowerCase()))
								{
									
								}
								else
								{
									temp1.add(roomName1.get(a).toLowerCase());
								}
								
							}
							ArrayList<String> temp2 = new ArrayList<String>();
							System.out.println(roomName2.size());
							for(int a = 0 ; a < roomName2.size() ; a++)
							{
								if(temp2.contains(roomName2.get(a).toLowerCase()))
								{
									
								}
								else
								{
									temp2.add(roomName2.get(a).toLowerCase());
								}
								
							}
							roomName1.clear();
							roomName2.clear();
							roomName1 = temp1;
							roomName2 = temp2;
							if(roomName1.containsAll(roomName2))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ roomName1 +"</td>"
										+ 	"<td>"+ roomName2 +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ roomName1 +"</td>"
										+ 	"<td>"+ roomName2 +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							temp1.clear();
							for(int a = 0 ; a < price1.size() ; a++)
							{
								if(temp1.contains(price1.get(a).toLowerCase()))
								{
									
								}
								else
								{
									temp1.add(price1.get(a).toLowerCase());
								}
								
							}
							temp2.clear();
							for(int a = 0 ; a < price2.size() ; a++)
							{
								if(temp2.contains(price2.get(a).toLowerCase()))
								{
									
								}
								else
								{
									temp2.add(price2.get(a).toLowerCase());
								}
								
							}
							price1.clear();
							price2.clear();
							price1 = temp1;
							price2 = temp2;
							if(price1.containsAll(price2))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room price</td>"
										+	"<td>"+ price1 +"</td>"
										+ 	"<td>"+ price2 +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room price</td>"
										+	"<td>"+ price1 +"</td>"
										+ 	"<td>"+ price2 +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							
							temp1.clear();
							temp2.clear();
							for(int a = 0 ; a < ratePlan1.size() ; a++)
							{
								if(temp1.contains(ratePlan1.get(a).toLowerCase()))
								{
									
								}
								else
								{
									temp1.add(ratePlan1.get(a).toLowerCase());
								}
							}
							temp2.clear();
							for(int a = 0 ; a < ratePlan2.size() ; a++)
							{
								if(temp2.contains(ratePlan2.get(a).toLowerCase()))
								{
									
								}
								else
								{
									temp2.add(ratePlan2.get(a).toLowerCase());
								}
							}
							ratePlan1.clear();
							ratePlan2.clear();
							ratePlan1 = temp1;
							ratePlan2 = temp2;
							if(ratePlan1.containsAll(ratePlan2))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify rate plan</td>"
										+	"<td>"+ ratePlan1 +"</td>"
										+ 	"<td>"+ ratePlan2 +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify rate plan</td>"
										+	"<td>"+ ratePlan1 +"</td>"
										+ 	"<td>"+ ratePlan2 +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
						/*	System.out.println(roomName1);
							System.out.println(roomName2);
							System.out.println(price1);
							System.out.println(price2);
							System.out.println(ratePlan1);
							System.out.println(ratePlan2);*/
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ resAvailability.get(j).getHotelName() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						count++;
						//break;
					}
				}
				else if(hotelList.get(i).getSupplier().trim().equalsIgnoreCase("hotelbeds_v2"))
				{
					if(hotelList.get(i).getTitle().trim().equalsIgnoreCase(hbresAvailability.getHotelDetails().get(j).getHotelName().trim()))
					{
						
						ReportPrinter.append("<span>"+ hotelList.get(i).getTitle() +"</span>");
						
						ReportPrinter.append("<table border=1 style=width:100%>"
								+ "<tr><th>Test Case</th>"
								+ "<th>Test Description</th>"
								+ "<th>Expected Result</th>"
								+ "<th>Actual Result</th>"
								+ "<th>Test Status</th></tr>");
						if(hotelList.get(i).getTitle().trim().equalsIgnoreCase(hbresAvailability.getHotelDetails().get(j).getHotelName().trim()))
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getHotelName() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getHotelName() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						count++;
						//System.out.println(resAvailability.getHotelDetails().get(j).getRhRoom().size());
						//System.out.println(hotelList.get(i).getRoom().size());
						for(int k = 0 ; k < hotelList.get(i).getRoom().size() ; k++)
						{
							if(hotelList.get(i).getRoom().get(k).getRoomType().trim().equalsIgnoreCase(hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom().trim()))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRoomType() +"</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRoomType() +"</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							//String value	=	converter.convert(defaultCode, availability.getTotalAmount(), hotel.getCurrencyCode(), currency, availability.getCurrencyCode());
							hbresAvailability.getCurrencyCode();
							String[] supCC = hotelList.get(i).getTotalCost().split(" ");
							String[] webValue	= hotelList.get(i).getRoom().get(k).getRate().split(" ");
							//System.out.println(hotelList.get(i).getRoom().get(k).getRate());
							System.out.println(webValue[0]);
							currencyConveter converter =  new currencyConveter();
							String value = hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getPrice();
							//System.out.println(resAvailability.getHotelDetails().get(j).getCurrencyCode());
							addProfitmarkup pm	=  new addProfitmarkup();
							//System.out.println(resAvailability.getHotelRoom().get(j).getPrice());
							String convertedValue	= converter.convert(defaultCurrency, value, supCC[0].trim(), currency, hbresAvailability.getHotelDetails().get(j).getCurrencyCode());
							int pmAddedPrice	=	pm.profitMarkup(convertedValue, txtProperty.get("hb"));
							int iwebValue	= Integer.parseInt(webValue[0]);
							if(pmAddedPrice + 2 >= iwebValue && iwebValue >= pmAddedPrice-2)
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify price per room</td>"
										+	"<td>"+ iwebValue +"</td>"
										+ 	"<td>"+ pmAddedPrice +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify price per room</td>"
										+	"<td>"+ iwebValue +"</td>"
										+ 	"<td>"+ pmAddedPrice +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							if(hotelList.get(i).getRoom().get(k).getRatePlan().trim().equalsIgnoreCase(hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard().trim()))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify rate plan</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRatePlan() +"</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify rate plan</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRatePlan() +"</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							System.out.println(hotelList.get(i).getAvailability().trim());
							System.out.println(hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus());
							if(hotelList.get(i).getAvailability().trim().equalsIgnoreCase("available") && hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus().trim().equalsIgnoreCase("N"))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify availability</td>"
										+	"<td>N</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify availability</td>"
										+	"<td>N</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							if(hotelList.get(i).getSupplier().trim().equalsIgnoreCase("hotelbeds_v2"))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify supplier</td>"
										+	"<td>hotelbeds_v2</td>"
										+ 	"<td>"+ hotelList.get(i).getSupplier() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify supplier</td>"
										+	"<td>hotelbeds_v2</td>"
										+ 	"<td>"+ hotelList.get(i).getSupplier() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							
						}
						
						//break;
					}
				}
				
			}
			
		}
		ReportPrinter.append("</table>");
	
	
	}
	
	public void inv13VerifyAvailabilityResponseWeb(ArrayList<readHotelResults> hotelList, ArrayList<resHotelAvailability> resAvailability, StringBuffer ReportPrinter, String defaultCurrency, Map<String, String> currency, int inv13HotelCount, resHotelAvailability hbresAvailability) throws FileNotFoundException
	{

		ExcelReader newReader = new ExcelReader();
		readSearchDetails	searchgRead = new readSearchDetails();
		FileInputStream stream =new FileInputStream(new File("excel/searchDetails.xls"));
		
		ArrayList<Map<Integer, String>> ContentList = new ArrayList<Map<Integer,String>>();
		ContentList = newReader.contentReading(stream);
		
		Map<Integer, String>  content = new HashMap<Integer, String>();
		content   = ContentList.get(0);
		
		Iterator<Map.Entry<Integer, String>>   mapiterator = content.entrySet().iterator();
		
		
			Map.Entry<Integer, String> Entry = (Map.Entry<Integer, String>)mapiterator.next();
			String Content = Entry.getValue();
			String[] searchDetails = Content.split(",");
		
		Properties properties 		= new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader 		=	new FileReader(new File("D://workSpace//hotelXmlValidation//profitMarkUps.properties"));
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 //System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value 	= properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		//System.out.println(hotelList.size());
		ReportPrinter.append("<span>(5) Verify results</span>");
		ReportPrinter.append("<br>");
		ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
		if(hotelList.size() == inv13HotelCount)
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ inv13HotelCount +"</td>"
					+ 	"<td>"+ hotelList.size() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ inv13HotelCount +"</td>"
					+ 	"<td>"+ hotelList.size() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		System.out.println(hotelList.size());
		for(int i = 0 ; i < hotelList.size() ; i++)
		{
			String webHotelName = hotelList.get(i).getTitle();
			System.out.println(webHotelName);
			System.out.println(hotelList.get(i).getSupplier());
			int count = 1;
			//System.out.println(resAvailability.size());
			for(int j = 0 ; j < resAvailability.size() ; j++)
			{
				//System.out.println(webHotelName);
				//System.out.println(resAvailability.get(j).getHotelName());
				//System.out.println(hotelList.get(i).getSupplier());
				if(hotelList.get(i).getSupplier().trim().equalsIgnoreCase("INV13"))
				{
					if(webHotelName.trim().equals(resAvailability.get(j).getHotelName().trim()))
					{
						ReportPrinter.append("<span>"+ webHotelName +"</span>");
						
						ReportPrinter.append("<table border=1 style=width:100%>"
								+ "<tr><th>Test Case</th>"
								+ "<th>Test Description</th>"
								+ "<th>Expected Result</th>"
								+ "<th>Actual Result</th>"
								+ "<th>Test Status</th></tr>");
						if(hotelList.get(i).getTitle().trim().equalsIgnoreCase(resAvailability.get(j).getHotelName()))
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ resAvailability.get(j).getHotelName() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ resAvailability.get(j).getHotelName() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						count++;
						//System.out.println(hotelList.get(i).getRoom().size());
						//System.out.println(searchDetails[4]);
						for(int k = 0 ; k < Integer.parseInt(searchDetails[4]) ; k++)
						{
							//System.out.println(hotelList.get(i).getRoom().size());
							//System.out.println(resAvailability.get(j).getHotelRoom().size());
						    //System.out.println(resAvailability.get(j).getHotelRoom().get(k).getRoomName());
						    //System.out.println(hotelList.get(i).getRoom().size());
						   	//System.out.println(hotelList.get(i).getRoom().get(k).getRoomType());
						   	System.out.println(hotelList.get(i).getRoom().get(k).getRoomType());
						   	//System.out.println(hotelList.get(j).getRoom().get(k).getRoomType());
						   	System.out.println(resAvailability.get(j).getHotelRoom().get(k).getRoomName());
							if(hotelList.get(i).getRoom().get(k).getRoomType().trim().equalsIgnoreCase(resAvailability.get(j).getHotelRoom().get(k).getRoomName()))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRoomType() +"</td>"
										+ 	"<td>"+ resAvailability.get(j).getHotelRoom().get(k).getRoomName() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRoomType() +"</td>"
										+ 	"<td>"+ resAvailability.get(j).getHotelRoom().get(k).getRoomName() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
	//String value	=	converter.convert(defaultCode, availability.getTotalAmount(), hotel.getCurrencyCode(), currency, availability.getCurrencyCode());
							resAvailability.get(j).getCurrencyCode();
							//System.out.println(hotelList.get(i).getTotalCost());
							String[] supCC = hotelList.get(i).getTotalCost().split(" ");
							//System.out.println(hotelList.get(i).getTitle());
							//System.out.println(hotelList.get(i).getRoom().get(k).getRate());
							currencyConveter converter =  new currencyConveter();
							//System.out.println(j);
							//System.out.println(k);
							//System.out.println(i);
							//System.out.println(resAvailability.get(j).getHotelRoom().get(0).getAvailableRoom().get(0).getPrice());
							String value = resAvailability.get(j).getHotelRoom().get(k).getAvailableRoom().get(0).getPrice();
							//System.out.println(resAvailability.get(j).getCurrencyCode());
							addProfitmarkup pm	=  new addProfitmarkup();
							//System.out.println(resAvailability.get(0).getCurrencyCode());
							String convertedValue	= converter.convert(defaultCurrency, value, supCC[0].trim(), currency, resAvailability.get(0).getCurrencyCode());
							//System.out.println(txtProperty.get("inv13"));
							int pmAddedPrice	=	pm.profitMarkup(convertedValue, txtProperty.get("inv13"));
							//System.out.println(hotelList.get(i).getRoom().size());
							try {
								//System.out.println(hotelList.get(i).getRoom().get(0).getRate());
								//System.out.println(hotelList.get(i).getRoom().get(1).getRate());
								//System.out.println(hotelList.get(i).getRoom().get(2).getRate());
								//System.out.println(hotelList.get(i).getRoom().get(3).getRate());
							} catch (Exception e) {
								// TODO: handle exception
							}
							String[] webValue = null;
							String wValue = "";
							/*try {
								webValue	= hotelList.get(i).getRoom().get(k).getRate().split(" ");
								//System.out.println(webValue[1]);
								wValue = webValue[1];
							} catch (Exception e) {
								// TODO: handle exception
							}*/
							try {
								webValue = hotelList.get(i).getRoom().get(k).getRate().split(" ");
								wValue = webValue[1];
							} catch (Exception e) {
								// TODO: handle exception
							}
							System.out.println(wValue);
							int iwebValue	= Integer.parseInt(wValue) ;
							if(pmAddedPrice + 2 >= iwebValue && iwebValue >= pmAddedPrice-2)
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify price per room</td>"
										+	"<td>"+ iwebValue +"</td>"
										+ 	"<td>"+ pmAddedPrice +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify price per room</td>"
										+	"<td>"+ iwebValue +"</td>"
										+ 	"<td>"+ pmAddedPrice +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							//System.out.println(hotelList.get(i).getRoom().get(k).getRatePlan());
							//System.out.println(resAvailability.get(j).getHotelRoom().get(k).getBoardType());
							try {
								if(hotelList.get(i).getRoom().get(k).getRatePlan().trim().equalsIgnoreCase(resAvailability.get(j).getHotelRoom().get(k).getBoardType()))
								{
									ReportPrinter.append	(	"<tr><td>"+count+"</td>"
											+ 	"<td>Verify rate plan</td>"
											+	"<td>"+ hotelList.get(i).getRoom().get(k).getRatePlan() +"</td>"
											+ 	"<td>"+ resAvailability.get(j).getHotelRoom().get(k).getBoardType() +"</td>"
											+	"<td class='passed'>Passed</td></tr>");
								}
								else
								{
									ReportPrinter.append	(	"<tr><td>"+count+"</td>"
											+ 	"<td>Verify rate plan</td>"
											+	"<td>"+ hotelList.get(i).getRoom().get(k).getRatePlan() +"</td>"
											+ 	"<td>"+ resAvailability.get(j).getHotelRoom().get(k).getBoardType() +"</td>"
											+	"<td class='Failed'>Failed</td></tr>");
								}
								count++;
							} catch (Exception e) {
								// TODO: handle exception
							}
							
							/*System.out.println(hotelList.get(i).getAvailability());
							System.out.println(resAvailability.get(j).getHotelRoom().get(k).getAvailability());
							if(hotelList.get(i).getAvailability().trim().equals("available_label") && resAvailability.get(j).getHotelRoom().get(k).getAvailability().equals("true"))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify availability</td>"
										+	"<td>true</td>"
										+ 	"<td>"+ resAvailability.get(j).getHotelRoom().get(k).getAvailability() +"</td>"
										+	"<td>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify availability</td>"
										+	"<td>true</td>"
										+ 	"<td>"+ resAvailability.get(j).getHotelRoom().get(k).getAvailability() +"</td>"
										+	"<td>Failed</td></tr>");
							}
							count++;*/
						}
						
						break;
					}
				}
				else if(hotelList.get(i).getSupplier().trim().equalsIgnoreCase("hotelbeds_v2"))
				{
					if(webHotelName.trim().equalsIgnoreCase(hbresAvailability.getHotelDetails().get(j).getHotelName().trim()))
					{
						
						ReportPrinter.append("<span>"+ webHotelName +"</span>");
						
						ReportPrinter.append("<table border=1 style=width:100%>"
								+ "<tr><th>Test Case</th>"
								+ "<th>Test Description</th>"
								+ "<th>Expected Result</th>"
								+ "<th>Actual Result</th>"
								+ "<th>Test Status</th></tr>");
						if(hotelList.get(i).getTitle().trim().equalsIgnoreCase(hbresAvailability.getHotelDetails().get(j).getHotelName().trim()))
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getHotelName() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getHotelName() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						count++;
						//System.out.println(resAvailability.getHotelDetails().get(j).getRhRoom().size());
						//System.out.println(hotelList.get(i).getRoom().size());
						for(int k = 0 ; k < hotelList.get(i).getRoom().size() ; k++)
						{
							if(hotelList.get(i).getRoom().get(k).getRoomType().trim().equalsIgnoreCase(hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom().trim()))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRoomType() +"</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRoomType() +"</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
	//String value	=	converter.convert(defaultCode, availability.getTotalAmount(), hotel.getCurrencyCode(), currency, availability.getCurrencyCode());
							hbresAvailability.getCurrencyCode();
							String[] supCC = hotelList.get(i).getTotalCost().split(" ");
							String[] webValue	= hotelList.get(i).getRoom().get(k).getRate().split(" ");
							//System.out.println(hotelList.get(i).getRoom().get(k).getRate());
							System.out.println(webValue[0]);
							currencyConveter converter =  new currencyConveter();
							String value = hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getPrice();
							//System.out.println(resAvailability.getHotelDetails().get(j).getCurrencyCode());
							addProfitmarkup pm	=  new addProfitmarkup();
							//System.out.println(resAvailability.getHotelRoom().get(j).getPrice());
							String convertedValue	= converter.convert(defaultCurrency, value, supCC[0].trim(), currency, hbresAvailability.getHotelDetails().get(j).getCurrencyCode());
							int pmAddedPrice	=	pm.profitMarkup(convertedValue, txtProperty.get("hb"));
							int iwebValue	= Integer.parseInt(webValue[0]);
							if(pmAddedPrice + 2 >= iwebValue && iwebValue >= pmAddedPrice-2)
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify price per room</td>"
										+	"<td>"+ iwebValue +"</td>"
										+ 	"<td>"+ pmAddedPrice +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify price per room</td>"
										+	"<td>"+ iwebValue +"</td>"
										+ 	"<td>"+ pmAddedPrice +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							if(hotelList.get(i).getRoom().get(k).getRatePlan().trim().equalsIgnoreCase(hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard().trim()))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify rate plan</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRatePlan() +"</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify rate plan</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRatePlan() +"</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							System.out.println(hotelList.get(i).getAvailability().trim());
							System.out.println(hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus());
							if(hotelList.get(i).getAvailability().trim().equalsIgnoreCase("available") && hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus().trim().equalsIgnoreCase("N"))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify availability</td>"
										+	"<td>N</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify availability</td>"
										+	"<td>N</td>"
										+ 	"<td>"+ hbresAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							if(hotelList.get(i).getSupplier().trim().equalsIgnoreCase("hotelbeds_v2"))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify supplier</td>"
										+	"<td>hotelbeds_v2</td>"
										+ 	"<td>"+ hotelList.get(i).getSupplier() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify supplier</td>"
										+	"<td>hotelbeds_v2</td>"
										+ 	"<td>"+ hotelList.get(i).getSupplier() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							
						}
						
						//break;
					}
				}
				
			}
			
		}
		ReportPrinter.append("</table>");
	
	}
	
	public void verify(ArrayList<readHotelResults> hotelList, resHotelAvailability resAvailability, StringBuffer ReportPrinter, String defaultCurrency, Map<String, String> currency, String hbhotelCount)
	{
		Properties properties = new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitMarkUps.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 //System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(5) Verify page1 response against results</span>");
		ReportPrinter.append("<br>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Test Description</th>"
				+ "<th>Expected Result</th>"
				+ "<th>Actual Result</th>"
				+ "<th>Test Status</th></tr>");
		if(hbhotelCount.equals(String.valueOf(hotelList.size())))
		{
			ReportPrinter.append	(	"<tr><td></td>"
					+ 	"<td>Verify hotel count</td>"
					+	"<td>"+ hbhotelCount +"</td>"
					+ 	"<td>"+ hotelList.size() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td></td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ hbhotelCount +"</td>"
					+ 	"<td>"+ hotelList.size() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		for(int i = 0 ; i < hotelList.size() ; i++)
		{ 
			
			String webHotelName = hotelList.get(i).getTitle();
			//System.out.println(webHotelName);
			int count = 1;
			//System.out.println(resAvailability.getHotelDetails().size());
			ReportPrinter.append("<span>"+ webHotelName +"</span>");
			
			
			for(int j = 0 ; j < resAvailability.getHotelDetails().size() ; j++)
			{
				
				try {
					//System.out.println(resAvailability.getHotelDetails().get(j).getHotelName().trim());
					if(webHotelName.trim().equals(resAvailability.getHotelDetails().get(j).getHotelName().trim()))
					{
						ReportPrinter.append("<span>"+ webHotelName +"</span>");
						
						ReportPrinter.append("<table border=1 style=width:100%>"
								+ "<tr><th>Test Case</th>"
								+ "<th>Test Description</th>"
								+ "<th>Expected Result</th>"
								+ "<th>Actual Result</th>"
								+ "<th>Test Status</th></tr>");
						if(hotelList.get(i).getTitle().trim().equals(resAvailability.getHotelDetails().get(j).getHotelName().trim()))
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getHotelName() +"</td>"
									+	"<td class='passed'>Passed</td></tr>");
						}
						else
						{
							ReportPrinter.append	(	"<tr><td>"+count+"</td>"
									+ 	"<td>Verify hotel name</td>"
									+	"<td>"+ hotelList.get(i).getTitle() +"</td>"
									+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getHotelName() +"</td>"
									+	"<td class='Failed'>Failed</td></tr>");
						}
						count++;
						//System.out.println(resAvailability.getHotelDetails().get(j).getRhRoom().size());
						//System.out.println(hotelList.get(i).getRoom().size());
						for(int k = 0 ; k < resAvailability.getHotelDetails().get(i).getRhRoom().size() ; k++)
						{
							if(hotelList.get(i).getRoom().get(k).getRoomType().trim().equals(resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom().trim()))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRoomType() +"</td>"
										+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify room type</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRoomType() +"</td>"
										+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getRoom() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
	//String value	=	converter.convert(defaultCode, availability.getTotalAmount(), hotel.getCurrencyCode(), currency, availability.getCurrencyCode());
							resAvailability.getCurrencyCode();
							String[] supCC = hotelList.get(i).getTotalCost().split(" ");
							String[] webValue	= hotelList.get(i).getRoom().get(k).getRate().split(" ");
							//System.out.println(webValue[1]);
							currencyConveter converter =  new currencyConveter();
							String value = resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getPrice();
							//System.out.println(resAvailability.getHotelDetails().get(j).getCurrencyCode());
							addProfitmarkup pm	=  new addProfitmarkup();
							//System.out.println(resAvailability.getHotelRoom().get(j).getPrice());
							String convertedValue	= converter.convert(defaultCurrency, value, supCC[0].trim(), currency, resAvailability.getHotelDetails().get(j).getCurrencyCode());
							//System.out.println(convertedValue);
							//System.out.println(txtProperty.get("hb"));
							int pmAddedPrice	=	pm.profitMarkup(convertedValue, txtProperty.get("hb"));
							int iwebValue	= 		Integer.parseInt(webValue[1]) ;
							if(pmAddedPrice + 2 >= iwebValue && iwebValue >= pmAddedPrice-2)
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify price per room</td>"
										+	"<td>"+ iwebValue +"</td>"
										+ 	"<td>"+ pmAddedPrice +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify price per room</td>"
										+	"<td>"+ iwebValue +"</td>"
										+ 	"<td>"+ pmAddedPrice +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							if(hotelList.get(i).getRoom().get(k).getRatePlan().trim().equals(resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard().trim()))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify rate plan</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRatePlan() +"</td>"
										+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify rate plan</td>"
										+	"<td>"+ hotelList.get(i).getRoom().get(k).getRatePlan() +"</td>"
										+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getBoard() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							//System.out.println(hotelList.get(i).getAvailability().trim());
							if(hotelList.get(i).getAvailability().trim().equals("available_label") && resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus().trim().equals("N"))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify availability</td>"
										+	"<td>N</td>"
										+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify availability</td>"
										+	"<td>N</td>"
										+ 	"<td>"+ resAvailability.getHotelDetails().get(j).getRhRoom().get(k).getStatus() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							if(hotelList.get(i).getSupplier().trim().equals("hotelbeds_v2"))
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify supplier</td>"
										+	"<td>hotelbeds_v2</td>"
										+ 	"<td>"+ hotelList.get(i).getSupplier() +"</td>"
										+	"<td class='passed'>Passed</td></tr>");
							}
							else
							{
								ReportPrinter.append	(	"<tr><td>"+count+"</td>"
										+ 	"<td>Verify supplier</td>"
										+	"<td>hotelbeds_v2</td>"
										+ 	"<td>"+ hotelList.get(i).getSupplier() +"</td>"
										+	"<td class='Failed'>Failed</td></tr>");
							}
							count++;
							
						}
						
						//break;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			ReportPrinter.append("</table>");
		}
	}
}









