package verification;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import currencyConvertion.addBookingFee;
import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import readAndWrite.thirdPartySupplierReport;
import readConfirmation.conhotel;
import readXml.resHotelAvailability;

public class thirdpartySupReportVerification {
	
	public void inv13Verify(resHotelAvailability res, thirdPartySupplierReport tpsr, conhotel hotel, StringBuffer ReportPrinter, String sup, String portal, String[] searchDetails, String defaultCurrency)
	{

		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(12) 3rd party supplier payable report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		if(hotel.getReservationNumber().trim().equals(tpsr.getBookingnumber().trim()))
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify Booking number</td>"
					+	"<td>"+ hotel.getReservationNumber().trim() +"</td>"
					+ 	"<td>"+ tpsr.getBookingnumber().trim() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify Booking number</td>"
					+	"<td>"+ hotel.getReservationNumber().trim() +"</td>"
					+ 	"<td>"+ tpsr.getBookingnumber().trim() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String today = dateFormat.format(date);
		
		if(today.equals(tpsr.getBookingdate()))
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify Booking date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ tpsr.getBookingdate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify Booking date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ tpsr.getBookingdate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}

		if(tpsr.getProductType().trim().equals("Hotels"))
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify Product Type</td>"
					+	"<td>Hotels</td>"
					+ 	"<td>"+ tpsr.getProductType() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify Product Type</td>"
					+	"<td>Hotels</td>"
					+ 	"<td>"+ tpsr.getProductType() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		try {
			if(res.getHotelInfoname().equals(tpsr.getProductName()))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Product Name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ tpsr.getProductName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Product Name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ tpsr.getProductName() +"</td>"
						+	"<td class='Failed'>Passed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		String date2 = formatdate(tpsr.getServiceElementDate());
		try {
			if(res.getDateFromDate().equals(date2))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Checkin date</td>"
						+	"<td>"+ res.getDateFromDate() +"</td>"
						+ 	"<td>"+ date2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Checkin date</td>"
						+	"<td>"+ res.getDateFromDate() +"</td>"
						+ 	"<td>"+ date2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(tpsr.getSupplierName().toLowerCase().trim().equals("hotelbed"))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Supplier Name</td>"
						+	"<td>"+ res.getSupplierName() +"</td>"
						+ 	"<td>"+ tpsr.getSupplierName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Supplier Name</td>"
						+	"<td>"+ res.getSupplierName() +"</td>"
						+ 	"<td>"+ tpsr.getSupplierName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}	
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		try {
			if(tpsr.getBookingStatus().equals("Normal"))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Booking Status</td>"
						+	"<td>Normal</td>"
						+ 	"<td>"+ tpsr.getBookingStatus() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Booking Status</td>"
						+	"<td>"+ res.getStatus() +"</td>"
						+ 	"<td>"+ tpsr.getBookingStatus() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		String[] temp = null;
		String name = "";
		try {
			temp = res.getRoomAvailability().get(0).getGuest().get(0).getName().split(" ");
			name = temp[1].concat(" ").concat(res.getRoomAvailability().get(0).getGuest().get(0).getName());
			if(name.trim().equals(tpsr.getGuestName()))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Guest Name</td>"
						+	"<td>"+ name +"</td>"
						+ 	"<td>"+ tpsr.getGuestName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Guest Name</td>"
						+	"<td>"+ name +"</td>"
						+ 	"<td>"+ tpsr.getGuestName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			temp = tpsr.getPortalCurrency().trim().split(" ");
			if(temp[2].trim().contains(defaultCurrency))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Portal Currency</td>"
						+	"<td>"+ defaultCurrency +"</td>"
						+ 	"<td>"+ temp[2] +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Portal Currency</td>"
						+	"<td>"+ defaultCurrency +"</td>"
						+ 	"<td>"+ temp[2] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getTotalAmount().equals(tpsr.getPayableAmount()))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Total Amount</td>"
						+	"<td>"+ res.getTotalAmount() +"</td>"
						+ 	"<td>"+ tpsr.getPayableAmount() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify Total Amount</td>"
						+	"<td>"+ res.getTotalAmount() +"</td>"
						+ 	"<td>"+ tpsr.getPayableAmount() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//System.out.println(res.getTotalAmount());
		//System.out.println(tpsr.getPayableAmount());
		
	
	}
	
	public void verifyAfterCancellation(StringBuffer ReportPrinter, String availability)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(17) Third party supplier payable Report Verification after cancellation</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		if(availability.equals("not found"))
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify third party supplier payable report record availablity</td>"
					+	"<td>not found</td>"
					+ 	"<td>"+ availability +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify third party supplier payable report record availablity</td>"
					+	"<td>not found</td>"
					+ 	"<td>"+ availability +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		ReportPrinter.append("</table>");
	}

	public void verify(resHotelAvailability res, thirdPartySupplierReport tpsr, conhotel hotel, StringBuffer ReportPrinter, String sup, String portal, String[] searchDetails, String defaultCurrency, Map<String, String> currency, Map<String, String> bookingFee, Map<String, String> profitMarkUp)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(12) Third party supplier payable Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		if(hotel.getReservationNumber().trim().equals(tpsr.getBookingnumber().trim()))
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify Booking number</td>"
					+	"<td>"+ hotel.getReservationNumber().trim() +"</td>"
					+ 	"<td>"+ tpsr.getBookingnumber().trim() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>Verify Booking number</td>"
					+	"<td>"+ hotel.getReservationNumber().trim() +"</td>"
					+ 	"<td>"+ tpsr.getBookingnumber().trim() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String today = dateFormat.format(date);
		
		if(today.equals(tpsr.getBookingdate()))
		{
			ReportPrinter.append	(	"<tr><td>2</td>"
					+ 	"<td>Verify Booking date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ tpsr.getBookingdate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>2</td>"
					+ 	"<td>Verify Booking date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ tpsr.getBookingdate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}

		if(tpsr.getProductType().trim().equals("Hotels"))
		{
			ReportPrinter.append	(	"<tr><td>3</td>"
					+ 	"<td>Verify Product Type</td>"
					+	"<td>Hotels</td>"
					+ 	"<td>"+ tpsr.getProductType() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>3</td>"
					+ 	"<td>Verify Product Type</td>"
					+	"<td>Hotels</td>"
					+ 	"<td>"+ tpsr.getProductType() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(res.getHotelDetails().get(0).getHotelName().equals(tpsr.getProductName()))
		{
			ReportPrinter.append	(	"<tr><td>4</td>"
					+ 	"<td>Verify Product Name</td>"
					+	"<td>"+ res.getHotelDetails().get(0).getHotelName() +"</td>"
					+ 	"<td>"+ tpsr.getProductName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>4</td>"
					+ 	"<td>Verify Product Name</td>"
					+	"<td>"+ res.getHotelDetails().get(0).getHotelName() +"</td>"
					+ 	"<td>"+ tpsr.getProductName() +"</td>"
					+	"<td class='Failed'>Passed</td></tr>");
		}
		String date2 = formatdate(tpsr.getServiceElementDate());
		System.out.println(res.getHotelDetails().get(0).getFromDate());
		String[] date3 = res.getHotelDetails().get(0).getFromDate().split("T");
		String date4 = date3[0].replace("-", "");
		System.out.println(date2);
		if(date4.equals(date2))
		{
			ReportPrinter.append	(	"<tr><td>5</td>"
					+ 	"<td>Verify Checkin date</td>"
					+	"<td>"+ date4 +"</td>"
					+ 	"<td>"+ date2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>5</td>"
					+ 	"<td>Verify Checkin date</td>"
					+	"<td>"+ date4 +"</td>"
					+ 	"<td>"+ date2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		System.out.println(sup);
		System.out.println(tpsr.getSupplierName());
		if(tpsr.getSupplierName().trim().toLowerCase().trim().equals(sup.trim().toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>6</td>"
					+ 	"<td>Verify Supplier Name</td>"
					+	"<td>"+ sup +"</td>"
					+ 	"<td>"+ tpsr.getSupplierName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>6</td>"
					+ 	"<td>Verify Supplier Name</td>"
					+	"<td>"+ sup +"</td>"
					+ 	"<td>"+ tpsr.getSupplierName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		if(tpsr.getBookingStatus().equals("Normal"))
		{
			ReportPrinter.append	(	"<tr><td>7</td>"
					+ 	"<td>Verify Booking Status</td>"
					+	"<td>Normal</td>"
					+ 	"<td>"+ tpsr.getBookingStatus() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>7</td>"
					+ 	"<td>Verify Booking Status</td>"
					+	"<td>"+ res.getHotelDetails().get(0).getStatus() +"</td>"
					+ 	"<td>"+ tpsr.getBookingStatus() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		try {
			String name = res.getHotelDetails().get(0).getFirstName().concat(" ").concat(res.getHotelDetails().get(0).getLastName());
			System.out.println(tpsr.getGuestName());
			if(name.trim().equals(tpsr.getGuestName()))
			{
				ReportPrinter.append	(	"<tr><td>8</td>"
						+ 	"<td>Verify Guest Name</td>"
						+	"<td>"+ name +"</td>"
						+ 	"<td>"+ tpsr.getGuestName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>8</td>"
						+ 	"<td>Verify Guest Name</td>"
						+	"<td>"+ name +"</td>"
						+ 	"<td>"+ tpsr.getGuestName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			
			String[] temp = tpsr.getPortalCurrency().trim().split(" ");
			/*if(temp[2].trim().contains(defaultCurrency))
			{
				ReportPrinter.append	(	"<tr><td>9</td>"
						+ 	"<td>Verify Portal Currency</td>"
						+	"<td>"+ name +"</td>"
						+ 	"<td>"+ tpsr.getGuestName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>9</td>"
						+ 	"<td>Verify Portal Currency</td>"
						+	"<td>"+ name +"</td>"
						+ 	"<td>"+ tpsr.getGuestName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}*/
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		try {
			currencyConveter 	cc 	= 	new currencyConveter();
			addProfitmarkup 	apm =	new addProfitmarkup();
			addBookingFee 		abf = 	new addBookingFee();
			System.out.println(res.getTotalAmount());
			System.out.println(tpsr.getPortalCurrency());
			String[] supCurrency = tpsr.getPortalCurrency().split(" ");
			supCurrency = supCurrency[4].split("\\(");
			supCurrency = supCurrency[1].split("\\)");
			String total = cc.convert(defaultCurrency, res.getFinalPrice(), supCurrency[0], currency, res.getFinalcurrency());
			//int pmTotal = apm.profitMarkup(res.getTotalAmount(), profitMarkUp.get("hb"));
			//int bfTotal = abf.bookingFee(res.getTotalAmount(), bookingFee.get("hotelbeds"));
			//total = String.valueOf(Integer.parseInt(total) + pmTotal + bfTotal);
			System.out.println(total);
			System.out.println(tpsr.getPayableAmount());
			String[] total2 = tpsr.getPayableAmount().split("\\.");
			String total3 = total2[0].replace(",", "");
			if(total.equals(total3))
			{
				ReportPrinter.append	(	"<tr><td>9</td>"
						+ 	"<td>Verify Total Amount</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ total3 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>9</td>"
						+ 	"<td>Verify Total Amount</td>"
						+	"<td>"+ total +"</td>"
						+ 	"<td>"+ total3 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
		
		//System.out.println(res.getTotalAmount());
		//System.out.println(tpsr.getPayableAmount());
		
	}
	
	public String formatdate(String date)
	{
		System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyyMMdd");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
}
