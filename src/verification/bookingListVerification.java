package verification;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.openqa.jetty.http.SSORealm;

import currencyConvertion.addBookingFee;
import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import readAndWrite.bookingListReport;
import readAndWrite.reservationReport;
import readConfirmation.conhotel;
import readXml.canDeadRes;
import readXml.resHotelAvailability;

public class bookingListVerification {

	private static Map<String, String>  	txtProperty 		= new HashMap<String, String>();
	private static Map<String, String>  	bookingFee 			= new HashMap<String, String>();
	
	public void inv13Verify(resHotelAvailability res, bookingListReport report, conhotel hotel, StringBuffer ReportPrinter, String supplier, String channel, String[] searchDetails)
	{

		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(10) Reservation Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		int cnt = 1;
		if(hotel.getReservationNumber().trim().equals(report.getResNumber().trim()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReservationNumber() +"</td>"
					+ 	"<td>"+ report.getResNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference number</td>"
					+	"<td>"+ hotel.getReservationNumber() +"</td>"
					+ 	"<td>"+ report.getResNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String today = dateFormat.format(date);
		
		if(today.equals(report.getResDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ report.getResDate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ report.getResDate() +"</td>"
					+	"<td class='Failed'>failed</td></tr>");
		}
		
		Calendar cal = Calendar.getInstance();
    	cal.getTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    	String time = sdf.format(cal.getTime());
    	if(time.equals(report.getResTime()))
    	{
    		ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference time</td>"
					+	"<td>"+ time +"</td>"
					+ 	"<td>"+ report.getResTime() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
    	}
    	else
    	{
    		ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reference time</td>"
					+	"<td>"+ time +"</td>"
					+ 	"<td>"+ report.getResTime() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
    	}
    	String name = res.getHoldername().concat(" ").concat(res.getHolderlastName());
    	System.out.println(res.getHoldername());
    	System.out.println(res.getHolderlastName());
    	if(name.toLowerCase().trim().equals(report.getCusName().toLowerCase().trim()))
    	{
    		ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer name</td>"
					+	"<td>"+ name +"</td>"
					+ 	"<td>"+ report.getCusName() +"</td>"
					+	"<td class='passed'>passed</td></tr>");
    	}
    	else
    	{
    		ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer name</td>"
					+	"<td>"+ name +"</td>"
					+ 	"<td>"+ report.getCusName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
    	}
    	try {
    		String name2 = res.getRoomAvailability().get(0).getGuest().get(0).getName().concat(" ").concat(res.getRoomAvailability().get(0).getGuest().get(0).getLastName());
    		
    		if(name2.trim().equals(report.getLeadName().trim()))
    		{
    			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
    					+ 	"<td>Verify lead guest name</td>"
    					+	"<td>"+ name2 +"</td>"
    					+ 	"<td>"+ report.getLeadName() +"</td>"
    					+	"<td class='passed'>Passed</td></tr>");
    		}
    		else
    		{
    			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
    					+ 	"<td>Verify lead guest name</td>"
    					+	"<td>"+ name2 +"</td>"
    					+ 	"<td>"+ report.getLeadName() +"</td>"
    					+	"<td class='Failed'>Faileded</td></tr>");
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}
    	try {
    		String conDate = formatdate(report.getFirstElement());
    		if(res.getDateFromDate().equals(conDate))
    		{
    			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
    					+ 	"<td>Verify checkin date</td>"
    					+	"<td>"+ res.getDateFromDate() +"</td>"
    					+ 	"<td>"+ report.getFirstElement() +"</td>"
    					+	"<td class='passed'>Passed</td></tr>");
    		}
    		else
    		{
    			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
    					+ 	"<td>Verify checkin date</td>"
    					+	"<td>"+ res.getDateFromDate() +"</td>"
    					+ 	"<td>"+ report.getFirstElement() +"</td>"
    					+	"<td class='Failed'>Failed</td></tr>");
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}
    	try {
    		String conDate = formatdate(report.getFirstElement());
    		if(res.getDateFromDate().equals(conDate))
    		{
    			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
    					+ 	"<td>Verify checkin date</td>"
    					+	"<td>"+ res.getDateFromDate() +"</td>"
    					+ 	"<td>"+ report.getFirstElement() +"</td>"
    					+	"<td class='passed'>Passed</td></tr>");
    		}
    		else
    		{
    			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
    					+ 	"<td>Verify checkin date</td>"
    					+	"<td>"+ res.getDateFromDate() +"</td>"
    					+ 	"<td>"+ report.getFirstElement() +"</td>"
    					+	"<td class='Failed'>Failed</td></tr>");
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			String conDate = formatdate(report.getDeadLine());
			if(res.getDateToDate().equals(conDate))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify cancellation deadline</td>"
						+	"<td>"+ res.getDateToDate() +"</td>"
						+ 	"<td>"+ report.getDeadLine() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify cancellation deadline</td>"
						+	"<td>"+ res.getDateToDate() +"</td>"
						+ 	"<td>"+ report.getDeadLine() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			if(res.getDestinationName().equals(report.getCity()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify city</td>"
						+	"<td>"+ res.getDestinationName() +"</td>"
						+ 	"<td>"+ report.getCity() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify city</td>"
						+	"<td>"+ res.getDestinationName() +"</td>"
						+ 	"<td>"+ report.getCity() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println(hotel.getCustomer().getCity());
		System.out.println(report.getBc().getCity());
		if(hotel.getCustomer().getCity().equals(report.getBc().getCity()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer city</td>"
					+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
					+ 	"<td>"+ report.getBc().getCity() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer city</td>"
					+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
					+ 	"<td>"+ report.getBc().getCity() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		try {
			if(res.getStatus().trim().toLowerCase().contains(report.getStatus().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify status</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ report.getCountry().trim() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify status</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ report.getCountry().trim() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		if(report.getBc().getCountry().contains(hotel.getCustomer().getCountry()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer country</td>"
					+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
					+ 	"<td>"+ report.getBc().getCountry() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify customer country</td>"
					+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
					+ 	"<td>"+ report.getBc().getCountry() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		try {
			if(res.getSupplierName().contains(report.getBc().getSupplier()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify supplier</td>"
						+	"<td>"+ res.getSupplierName() +"</td>"
						+ 	"<td>"+ report.getBc().getSupplier() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify supplier</td>"
						+	"<td>"+ res.getSupplierName() +"</td>"
						+ 	"<td>"+ report.getBc().getSupplier() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			if(res.getHotelInfoname().trim().equals(report.getHotelName().trim()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			String conDate = formatdate(report.getCheckin());
			if(res.getDateFromDate().equals(conDate))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ res.getDateFromDate() +"</td>"
						+ 	"<td>"+ report.getCheckin() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ res.getDateFromDate() +"</td>"
						+ 	"<td>"+ report.getCheckin() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getRoomAvailability().get(0).getRoomCount().equals(report.getNumOfRooms()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room count</td>"
						+	"<td>"+ res.getRoomAvailability().get(0).getRoomCount() +"</td>"
						+ 	"<td>"+ report.getNumOfRooms() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify room count</td>"
						+	"<td>"+ res.getRoomAvailability().get(0).getRoomCount() +"</td>"
						+ 	"<td>"+ report.getNumOfRooms() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getStatus().trim().toLowerCase().contains(report.getStatus().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify status</td>"
						+	"<td>"+ res.getStatus() +"</td>"
						+ 	"<td>"+ report.getStatus() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify status</td>"
						+	"<td>"+ res.getStatus() +"</td>"
						+ 	"<td>"+ report.getStatus() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// convert--------------------------------------------------------------------------------------------------------
		try {
			if(res.getTotalAmount().equals(report.getSectorBookingValue()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify sector booking value</td>"
						+	"<td>"+ res.getTotalAmount() +"</td>"
						+ 	"<td>"+ report.getSectorBookingValue() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify sector booking value</td>"
						+	"<td>"+ res.getTotalAmount() +"</td>"
						+ 	"<td>"+ report.getSectorBookingValue() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		if(hotel.getReservationNumber().trim().equals(report.getBc().getReservationNumber().trim()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card reservation number</td>"
					+	"<td>"+ hotel.getReservationNumber() +"</td>"
					+ 	"<td>"+ report.getBc().getReservationNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card reservation number</td>"
					+	"<td>"+ hotel.getReservationNumber() +"</td>"
					+ 	"<td>"+ report.getBc().getReservationNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		date = new Date();
		today = dateFormat.format(date);
		if(today.equals(report.getBc().getReservationDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card reservation date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ report.getBc().getReservationDate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card reservation date</td>"
					+	"<td>"+ today +"</td>"
					+ 	"<td>"+ report.getBc().getReservationDate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		String[] date2 =  report.getBc().getFirstElement().split("/");
		String day2 = date2[2].trim().concat(date2[1].trim().concat(date2[0].trim()));
		try {
			if(res.getDateFromDate().equals(day2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card date of the first element used</td>"
						+	"<td>"+ res.getCheckinDate() +"</td>"
						+ 	"<td>"+ report.getBc().getFirstElement() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card date of the first element used</td>"
						+	"<td>"+ res.getCheckinDate() +"</td>"
						+ 	"<td>"+ report.getBc().getFirstElement() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		
		date2 	=  	report.getBc().getCancellationDeadline().split("/");
		day2 	= 	date2[2].trim().concat(date2[1].trim().concat(date2[0].trim()));
		try {
			if(res.getDateToDate().equals(day2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card cancellation deadline</td>"
						+	"<td>"+ res.getDateToDate() +"</td>"
						+ 	"<td>"+ report.getBc().getCancellationDeadline() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card cancellation deadline</td>"
						+	"<td>"+ res.getDateToDate() +"</td>"
						+ 	"<td>"+ report.getBc().getCancellationDeadline() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getTotalAmount().equals(report.getBc().getTotalBookingvalue()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card cancellation deadline</td>"
						+	"<td>"+ res.getTotalAmount() +"</td>"
						+ 	"<td>"+ report.getBc().getTotalBookingvalue() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card subtotal</td>"
						+	"<td>"+ res.getTotalAmount() +"</td>"
						+ 	"<td>"+ report.getBc().getTotalBookingvalue() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		if(hotel.getPay().getTotalTaxesAndOtherCharges().equals(report.getBc().getTaxAndOther()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card tax and other charges</td>"
					+	"<td>"+ hotel.getPay().getTotalBookingValue() +"</td>"
					+ 	"<td>"+ report.getBc().getTaxAndOther() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card tax and other charges</td>"
					+	"<td>"+ hotel.getPay().getTotalBookingValue() +"</td>"
					+ 	"<td>"+ report.getBc().getTaxAndOther() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		// credit card verification
		
		if(name.toLowerCase().equals(report.getBc().getFirstName().toLowerCase()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer first name</td>"
					+	"<td>"+ name +"</td>"
					+ 	"<td>"+ report.getBc().getFirstName() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer first name</td>"
					+	"<td>"+ name +"</td>"
					+ 	"<td>"+ report.getBc().getFirstName() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(hotel.getCustomer().getAddress().equals(report.getBc().getAddress()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer address</td>"
					+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
					+ 	"<td>"+ report.getBc().getAddress() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer address</td>"
					+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
					+ 	"<td>"+ report.getBc().getAddress() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(hotel.getCustomer().getEmail().equals(report.getBc().getEmail()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer email address</td>"
					+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
					+ 	"<td>"+ report.getBc().getEmail() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer email address</td>"
					+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
					+ 	"<td>"+ report.getBc().getEmail() +"</td>"
					+	"<td class='Failed'>Passed</td></tr>");
		}
		
		if(hotel.getCustomer().getCity().equals(report.getBc().getCity()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer city</td>"
					+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
					+ 	"<td>"+ report.getBc().getCity() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer city</td>"
					+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
					+ 	"<td>"+ report.getBc().getCity() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(hotel.getCustomer().getCountry().equals(report.getBc().getCountry()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer country</td>"
					+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
					+ 	"<td>"+ report.getBc().getCountry() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer country</td>"
					+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
					+ 	"<td>"+ report.getBc().getCountry() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(hotel.getCustomer().getPhoneNumber().equals(report.getBc().getPhoneNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer phone number</td>"
					+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
					+ 	"<td>"+ report.getBc().getPhoneNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card customer phone number</td>"
					+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
					+ 	"<td>"+ report.getBc().getPhoneNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		try {
			if(res.getHotelInfoname().equals(report.getBc().getHotelName()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getBc().getHotelName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card hotel name</td>"
						+	"<td>"+ res.getHotelInfoname() +"</td>"
						+ 	"<td>"+ report.getBc().getHotelName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		if(hotel.getAddress().equals(report.getBc().getHotelAddress()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card hotel address</td>"
					+	"<td>"+ hotel.getAddress() +"</td>"
					+ 	"<td>"+ report.getBc().getHotelAddress() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card hotel address</td>"
					+	"<td>"+ hotel.getAddress() +"</td>"
					+ 	"<td>"+ report.getBc().getHotelAddress() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		date2 	=  	report.getBc().getCheckin().split("/");
		day2 	= 	date2[2].trim().concat(date2[1].trim().concat(date2[0].trim()));
		try {
			if(res.getDateFromDate().equals(day2))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card hotel checkin date</td>"
						+	"<td>"+ res.getCheckinDate() +"</td>"
						+ 	"<td>"+ report.getBc().getCheckin() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card hotel checkin date</td>"
						+	"<td>"+ res.getCheckinDate() +"</td>"
						+ 	"<td>"+ report.getBc().getCheckin() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		day2 = formatdate2(report.getBc().getCheckOut());
		if(hotel.getCheckout().equals(day2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card hotel checkout date</td>"
					+	"<td>"+ hotel.getCheckout() +"</td>"
					+ 	"<td>"+ report.getBc().getCheckOut() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card hotel checkout date</td>"
					+	"<td>"+ hotel.getCheckout() +"</td>"
					+ 	"<td>"+ report.getBc().getCheckOut() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(searchDetails[4].equals(report.getBc().getNoOfRooms()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card hotel number of rooms</td>"
					+	"<td>"+ searchDetails[4] +"</td>"
					+ 	"<td>"+ report.getBc().getNoOfRooms() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card hotel number of rooms</td>"
					+	"<td>"+ searchDetails[4] +"</td>"
					+ 	"<td>"+ report.getBc().getNoOfRooms() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		if(searchDetails[3].equals(report.getBc().getNumOfNyts()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card hotel number of nights</td>"
					+	"<td>"+ searchDetails[3] +"</td>"
					+ 	"<td>"+ report.getBc().getNumOfNyts() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking card hotel number of nights</td>"
					+	"<td>"+ searchDetails[3] +"</td>"
					+ 	"<td>"+ report.getBc().getNumOfNyts() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		//convert-------------------------------------------------------------------------------------------------------
		try {
			if(res.getTotalAmount().equals(report.getBc().getTotalBookingvalue()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card hotel total booking value</td>"
						+	"<td>"+ res.getTotalAmount() +"</td>"
						+ 	"<td>"+ report.getBc().getTotalBookingvalue() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card hotel total booking value</td>"
						+	"<td>"+ res.getTotalAmount() +"</td>"
						+ 	"<td>"+ report.getBc().getTotalBookingvalue() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// rest of the hotel payment details
		
		for(int i = 0 ; i < res.getHotelRoom().size() ; i ++)
		{
			if(res.getHotelRoom().get(i).getRoom().trim().equals(report.getBc().getRoom().get(i).getRoomType().trim()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card room type</td>"
						+	"<td>"+ res.getHotelRoom().get(i).getRoomType() +"</td>"
						+ 	"<td>"+ report.getBc().getRoom().get(i).getRoomType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card room type</td>"
						+	"<td>"+ res.getHotelRoom().get(i).getRoomType() +"</td>"
						+ 	"<td>"+ report.getBc().getRoom().get(i).getRoomType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			
			if(hotel.getRoom().get(i).getBedType().equals(report.getBc().getRoom().get(i).getBedType()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card bed type</td>"
						+	"<td>"+ hotel.getRoom().get(i).getBedType() +"</td>"
						+ 	"<td>"+ report.getBc().getRoom().get(i).getBedType() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card bed type</td>"
						+	"<td>"+ hotel.getRoom().get(i).getBedType() +"</td>"
						+ 	"<td>"+ report.getBc().getRoom().get(i).getBedType() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			
			if(res.getHotelRoom().get(i).getBoard().equals(report.getBc().getRoom().get(i).getRatePlan()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card rate plan</td>"
						+	"<td>"+ res.getHotelRoom().get(i).getBoard() +"</td>"
						+ 	"<td>"+ report.getBc().getRoom().get(i).getRatePlan() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card rate plan</td>"
						+	"<td>"+ res.getHotelRoom().get(i).getBoard() +"</td>"
						+ 	"<td>"+ report.getBc().getRoom().get(i).getRatePlan() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			System.out.println(res.getHotelRoom().get(i).getPrice());
			System.out.println(report.getBc().getRoom().get(i).getTotalRate());
			
			if(res.getHotelRoom().get(i).getPrice().equals(report.getBc().getRoom().get(i).getTotalRate()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card customer name</td>"
						+	"<td>"+ res.getHotelRoom().get(i).getPrice() +"</td>"
						+ 	"<td>"+ report.getBc().getRoom().get(i).getTotalRate() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify booking card customer name</td>"
						+	"<td>"+ res.getHotelRoom().get(i).getPrice() +"</td>"
						+ 	"<td>"+ report.getBc().getRoom().get(i).getTotalRate() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
		}
		ReportPrinter.append("</table>");
	
	}
	
	public void verify(resHotelAvailability res, bookingListReport report, conhotel hotel, StringBuffer ReportPrinter, String supplier, String channel, String[] searchDetails, Map<String, String> currency, String defaultCurrency, canDeadRes pen, Map<String, String> profitMarkUp) throws ParseException
	{
		 Properties properties 	= new Properties();
			try 
			{
				//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
				//FileReader reader 	=new FileReader(new File("D://workSpace//hotelXmlValidation//logginInfo.properties"));
				FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitMarkUps.properties"));
				
				properties.load(reader);
			} 
			catch (IOException e)
			{
				 System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value 		= properties.getProperty(key);
			    txtProperty.put(key, value);
			}
			
			try 
			{
				//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
				//FileReader reader 	=new FileReader(new File("D://workSpace//hotelXmlValidation//logginInfo.properties"));
				FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//bookingFee.properties"));
				
				properties.load(reader);
			} 
			catch (IOException e)
			{
				 System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value 		= properties.getProperty(key);
			    bookingFee.put(key, value);
			}
		
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(10) Booking list Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		int cnt = 1;
		try {
			if(hotel.getReservationNumber().trim().equals(report.getResNumber().trim()))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify reference number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ report.getResNumber() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>Verify reference number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ report.getResNumber() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String today = dateFormat.format(date);
		
		try {
			if(today.equals(report.getResDate()))
			{
				ReportPrinter.append	(	"<tr><td>2</td>"
						+ 	"<td>Verify reference date</td>"
						+	"<td>"+ today +"</td>"
						+ 	"<td>"+ report.getResDate() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>2</td>"
						+ 	"<td>Verify reference date</td>"
						+	"<td>"+ today +"</td>"
						+ 	"<td>"+ report.getResDate() +"</td>"
						+	"<td class='Failed'>failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
    	try {
    		if(res.getReferenceTime().equals(report.getResTime()))
        	{
        		ReportPrinter.append	(	"<tr><td>3</td>"
    					+ 	"<td>Verify reference time</td>"
    					+	"<td>"+ res.getReferenceTime() +"</td>"
    					+ 	"<td>"+ report.getResTime() +"</td>"
    					+	"<td class='passed'>Passed</td></tr>");
        	}
        	else
        	{
        		ReportPrinter.append	(	"<tr><td>3</td>"
    					+ 	"<td>Verify reference time</td>"
    					+	"<td>"+ res.getReferenceTime() +"</td>"
    					+ 	"<td>"+ report.getResTime() +"</td>"
    					+	"<td class='Failed'>Failed</td></tr>");
        	}
    		cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
    	String name = "";
    	/*try {
    		name = res.getHoldername().concat(" ").concat(res.getHolderlastName());
    		System.out.println(name);
        	if(name.toLowerCase().trim().equals(report.getCusName().toLowerCase().trim()))
        	{
        		ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
    					+ 	"<td>Verify customer name</td>"
    					+	"<td>"+ name +"</td>"
    					+ 	"<td>"+ report.getCusName() +"</td>"
    					+	"<td class='passed'>passed</td></tr>");
        	}
        	else
        	{
        		ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
    					+ 	"<td>Verify customer name</td>"
    					+	"<td>"+ name +"</td>"
    					+ 	"<td>"+ report.getCusName() +"</td>"
    					+	"<td class='Failed'>Failed</td></tr>");
        	}
        	cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}*/
    	
    	try {
        	String name2 = res.getHotelDetails().get(0).getFirstName().concat(" ").concat(res.getHotelDetails().get(0).getLastName());
    		String[] leadName = report.getLeadName().split(" ");
    		String name1 = leadName[2].concat(" ").concat(leadName[3]);
    		System.out.println(name1);
    		System.out.println(name2);
    		if(name2.trim().equals(name1))
    		{
    			ReportPrinter.append	(	"<tr><td>4</td>"
    					+ 	"<td>Verify lead guest name</td>"
    					+	"<td>"+ name2 +"</td>"
    					+ 	"<td>"+ name1 +"</td>"
    					+	"<td class='passed'>Passed</td></tr>");
    		}
    		else
    		{
    			ReportPrinter.append	(	"<tr><td>4</td>"
    					+ 	"<td>Verify lead guest name</td>"
    					+	"<td>"+ name2 +"</td>"
    					+ 	"<td>"+ name1 +"</td>"
    					+	"<td class='Failed'>Faileded</td></tr>");
    		}
    		cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
		String conDate = formatdate3(report.getFirstElement());
		try {
			if(res.getHotelDetails().get(0).getFromDate().contains(conDate))
			{
				ReportPrinter.append	(	"<tr><td>5</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
						+ 	"<td>"+ conDate +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>5</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
						+ 	"<td>"+ conDate +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		conDate = formatdate3(report.getDeadLine());
		String conDate2 = formatdate5(pen.getCheckin());
	
		int canDead = 0;
		if(pen.getPenalty().get(0).getTimeunit().equals("Hour"))
		{
			canDead = Integer.parseInt(pen.getPenalty().get(0).getMultiplier()) / 24;
		}
		
		String changedDate = changeDate(String.valueOf(canDead), conDate2);
		try {
			System.out.println(changedDate);
			System.out.println(conDate);
			if(changedDate.equals(conDate))
			{
				ReportPrinter.append	(	"<tr><td>6</td>"
						+ 	"<td>Verify cancellation deadline</td>"
						+	"<td>"+ changedDate +"</td>"
						+ 	"<td>"+ conDate +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>6</td>"
						+ 	"<td>Verify cancellation deadline</td>"
						+	"<td>"+ changedDate +"</td>"
						+ 	"<td>"+ conDate +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHotelDetails().get(0).getAddress().contains(report.getCity()))
			{
				ReportPrinter.append	(	"<tr><td>7</td>"
						+ 	"<td>Verify city</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getAddress() +"</td>"
						+ 	"<td>"+ report.getCity() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>7</td>"
						+ 	"<td>Verify city</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getAddress() +"</td>"
						+ 	"<td>"+ report.getCity() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(hotel.getCustomer().getCity().equals(report.getBc().getCity()))
			{
				ReportPrinter.append	(	"<tr><td>8</td>"
						+ 	"<td>Verify customer's contry</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ report.getBc().getCountry() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>8</td>"
						+ 	"<td>Verify country</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ report.getBc().getCountry() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			ArrayList<String> status = new ArrayList<String>();
			for(int i = 0 ; i < res.getHotelDetails().size() ; i ++)
			{
				status.add(res.getHotelDetails().get(0).getStatus());
			}
			String status2 = "";
			if(status.contains("Request"))
			{
				status2 = "Request";
			}
			else
			{
				status2 = "Confirm";
			}
			System.out.println(res.getHotelDetails().get(0).getStatus());
			System.out.println(report.getStatus());
			if(status2.trim().toLowerCase().contains(report.getStatus().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>9</td>"
						+ 	"<td>Verify status</td>"
						+	"<td>"+ status2 +"</td>"
						+ 	"<td>"+ report.getStatus() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>9</td>"
						+ 	"<td>Verify status</td>"
						+	"<td>"+ status2 +"</td>"
						+ 	"<td>"+ report.getStatus() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		/*try {
			if(report.getBc().getCountry().contains(hotel.getCustomer().getCountry()))
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer country</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ report.getBc().getCountry() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
						+ 	"<td>Verify customer country</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ report.getBc().getCountry() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}*/
		
		try {
			if(report.getBc().getSupplier().equals("INV13"))
			{
				ReportPrinter.append	(	"<tr><td>10</td>"
						+ 	"<td>Verify supplier</td>"
						+	"<td> INV13 </td>"
						+ 	"<td>"+ report.getBc().getSupplier() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>10</td>"
						+ 	"<td>Verify supplier</td>"
						+	"<td> INV13 </td>"
						+ 	"<td>"+ report.getBc().getSupplier() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHotelDetails().get(0).getHotelName().equals(report.getHotelName().trim()))
			{
				ReportPrinter.append	(	"<tr><td>11</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getHotelName() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>11</td>"
						+ 	"<td>Verify hotel name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getHotelName() +"</td>"
						+ 	"<td>"+ report.getHotelName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			conDate = formatdate3(report.getCheckin());
			System.out.println(res.getHotelDetails().get(0).getFromDate());
			System.out.println(conDate);
			if(res.getHotelDetails().get(0).getFromDate().contains(conDate))
			{
				ReportPrinter.append	(	"<tr><td>12</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
						+ 	"<td>"+ conDate +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>12</td>"
						+ 	"<td>Verify checkin date</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
						+ 	"<td>"+ conDate +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		try {
			System.out.println(res.getRoomAvailability().size());
			if(String.valueOf(res.getHotelDetails().size()).equals(report.getNumOfRooms()))
			{
				ReportPrinter.append	(	"<tr><td>13</td>"
						+ 	"<td>Verify room count</td>"
						+	"<td>"+ res.getHotelDetails().size() +"</td>"
						+ 	"<td>"+ report.getNumOfRooms() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>13</td>"
						+ 	"<td>Verify room count</td>"
						+	"<td>"+ res.getHotelDetails().size() +"</td>"
						+ 	"<td>"+ report.getNumOfRooms() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		// convert--------------------------------------------------------------------------------------------------------
		currencyConveter 	cc 		= 	new currencyConveter();
		addProfitmarkup 	apm 	= 	new addProfitmarkup();
		addBookingFee 		abf 	= 	new addBookingFee();
		try {
			String total = cc.convert(defaultCurrency, res.getFinalPrice(), report.getCurrencyCode(), currency, res.getFinalcurrency());
			String pm = String.valueOf(apm.profitMarkup(total, txtProperty.get("hb")));
			String bf = String.valueOf(abf.bookingFee(total, bookingFee.get("hotelbeds")));
			String total2 = String.valueOf(Integer.parseInt(total) + Integer.parseInt(pm) + Integer.parseInt(bf));
			String[] sbv = report.getSectorBookingValue().trim().split("\\.");
			sbv = sbv[0].split(",");
			String sbv2 = sbv[0].concat(sbv[1]);
			if(total2.equals(sbv2))
			{
				ReportPrinter.append	(	"<tr><td>14</td>"
						+ 	"<td>Verify sector booking value</td>"
						+	"<td>"+ total2 +"</td>"
						+ 	"<td>"+ sbv2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>14</td>"
						+ 	"<td>Verify sector booking value</td>"
						+	"<td>"+ total2 +"</td>"
						+ 	"<td>"+ sbv2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(hotel.getReservationNumber().trim().equals(report.getBc().getReservationNumber().trim()))
			{
				ReportPrinter.append	(	"<tr><td>15</td>"
						+ 	"<td>Verify booking card reservation number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ report.getBc().getReservationNumber() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>15</td>"
						+ 	"<td>Verify booking card reservation number</td>"
						+	"<td>"+ hotel.getReservationNumber() +"</td>"
						+ 	"<td>"+ report.getBc().getReservationNumber() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		date = new Date();
		today = dateFormat.format(date);
		
		try {
			if(today.equals(report.getBc().getReservationDate()))
			{
				ReportPrinter.append	(	"<tr><td>16</td>"
						+ 	"<td>Verify booking card reservation date</td>"
						+	"<td>"+ today +"</td>"
						+ 	"<td>"+ report.getBc().getReservationDate() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>16</td>"
						+ 	"<td>Verify booking card reservation date</td>"
						+	"<td>"+ today +"</td>"
						+ 	"<td>"+ report.getBc().getReservationDate() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		String[] date2 = null;
		String day2 = "";
		try {
			date2 =  report.getBc().getFirstElement().split("/");
			day2 = date2[2].trim().concat(date2[1].trim().concat(date2[0].trim()));
			day2 = formatdate4(day2);
			System.out.println(res.getHotelDetails().get(0).getFromDate());
			System.out.println(day2);
			if(res.getHotelDetails().get(0).getFromDate().contains(day2))
			{
				ReportPrinter.append	(	"<tr><td>17</td>"
						+ 	"<td>Verify booking card date of the first element used</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
						+ 	"<td>"+ day2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>17</td>"
						+ 	"<td>Verify booking card date of the first element used</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getFromDate() +"</td>"
						+ 	"<td>"+ day2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			date2 	=  	report.getBc().getCancellationDeadline().split("/");
			day2 	= 	date2[2].trim().concat("-").concat(date2[1].trim().concat("-").concat(date2[0].trim()));
			if(changedDate.equals(day2))
			{
				ReportPrinter.append	(	"<tr><td>18</td>"
						+ 	"<td>Verify booking card cancellation deadline</td>"
						+	"<td>"+ changedDate +"</td>"
						+ 	"<td>"+ day2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>18</td>"
						+ 	"<td>Verify booking card cancellation deadline</td>"
						+	"<td>"+ changedDate +"</td>"
						+ 	"<td>"+ day2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			String total = cc.convert(defaultCurrency, res.getFinalPrice(), report.getCurrencyCode(), currency, res.getFinalcurrency());
			String pm = String.valueOf(apm.profitMarkup(total, txtProperty.get("hb")));
			String bf = String.valueOf(abf.bookingFee(total, bookingFee.get("hotelbeds")));
			String total2 = String.valueOf(Integer.parseInt(total) + Integer.parseInt(pm) + Integer.parseInt(bf));
			String[] sbv = report.getBc().getTotalBookingvalue().trim().split("\\.");
			sbv = sbv[0].split(",");
			String sbv2 = sbv[0].concat(sbv[1]);
			if(total2.equals(sbv2))
			{
				ReportPrinter.append	(	"<tr><td>19</td>"
						+ 	"<td>Verify booking card subtotal</td>"
						+	"<td>"+ total2 +"</td>"
						+ 	"<td>"+ sbv2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>19</td>"
						+ 	"<td>Verify booking card subtotal</td>"
						+	"<td>"+ total2 +"</td>"
						+ 	"<td>"+ sbv2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			int tax = 0;
			for(int i = 0 ; i < res.getHotelDetails().size() ; i++)
			{
				tax = tax + Integer.parseInt(res.getHotelDetails().get(i).getTotalTax());
			}
			/*String conTax = cc.convert(defaultCurrency, String.valueOf(tax), hotel.getCurrencyCode(), currency, res.getHotelDetails().get(0).getCurrencyCode());
			System.out.println(conTax);*/
			tax = apm.profitMarkup(res.getFinalPrice(), txtProperty.get("creditCard"));
			System.out.println(tax);
			String[] tax2 = report.getBc().getCreditCardFee().split("\\.");
			if(String.valueOf(tax).equals(tax2[0]))
			{
				ReportPrinter.append	(	"<tr><td>20</td>"
						+ 	"<td>Verify booking card tax and other charges</td>"
						+	"<td>"+ tax +"</td>"
						+ 	"<td>"+ tax2[0] +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>20</td>"
						+ 	"<td>Verify booking card tax and other charges</td>"
						+	"<td>"+ tax +"</td>"
						+ 	"<td>"+ tax2[0] +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// credit card verification
		try {
			String name2 = hotel.getCustomer().getFirstName().trim().concat(" ").concat(hotel.getCustomer().getLastName().trim());
			if(name2.equals(report.getBc().getFirstName().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>21</td>"
						+ 	"<td>Verify booking card customer name</td>"
						+	"<td>"+ name2 +"</td>"
						+ 	"<td>"+ report.getBc().getFirstName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>21</td>"
						+ 	"<td>Verify booking card customer name</td>"
						+	"<td>"+ name2 +"</td>"
						+ 	"<td>"+ report.getBc().getFirstName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(hotel.getCustomer().getAddress().equals(report.getBc().getAddress()))
			{
				ReportPrinter.append	(	"<tr><td>22</td>"
						+ 	"<td>Verify booking card customer address</td>"
						+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
						+ 	"<td>"+ report.getBc().getAddress() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>22</td>"
						+ 	"<td>Verify booking card customer address</td>"
						+	"<td>"+ hotel.getCustomer().getAddress() +"</td>"
						+ 	"<td>"+ report.getBc().getAddress() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(hotel.getCustomer().getEmail().equals(report.getBc().getEmail()))
			{
				ReportPrinter.append	(	"<tr><td>23</td>"
						+ 	"<td>Verify booking card customer email address</td>"
						+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
						+ 	"<td>"+ report.getBc().getEmail() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>23</td>"
						+ 	"<td>Verify booking card customer email address</td>"
						+	"<td>"+ hotel.getCustomer().getEmail() +"</td>"
						+ 	"<td>"+ report.getBc().getEmail() +"</td>"
						+	"<td class='Failed'>Passed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(hotel.getCustomer().getCity().equals(report.getBc().getCity()))
			{
				ReportPrinter.append	(	"<tr><td>24</td>"
						+ 	"<td>Verify booking card customer city</td>"
						+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
						+ 	"<td>"+ report.getBc().getCity() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>24</td>"
						+ 	"<td>Verify booking card customer city</td>"
						+	"<td>"+ hotel.getCustomer().getCity() +"</td>"
						+ 	"<td>"+ report.getBc().getCity() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(hotel.getCustomer().getCountry().equals(report.getBc().getCountry()))
			{
				ReportPrinter.append	(	"<tr><td>25</td>"
						+ 	"<td>Verify booking card customer country</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ report.getBc().getCountry() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>25</td>"
						+ 	"<td>Verify booking card customer country</td>"
						+	"<td>"+ hotel.getCustomer().getCountry() +"</td>"
						+ 	"<td>"+ report.getBc().getCountry() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(hotel.getCustomer().getPhoneNumber().equals(report.getBc().getPhoneNumber()))
			{
				ReportPrinter.append	(	"<tr><td>26</td>"
						+ 	"<td>Verify booking card customer phone number</td>"
						+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
						+ 	"<td>"+ report.getBc().getPhoneNumber() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>26</td>"
						+ 	"<td>Verify booking card customer phone number</td>"
						+	"<td>"+ hotel.getCustomer().getPhoneNumber() +"</td>"
						+ 	"<td>"+ report.getBc().getPhoneNumber() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(res.getHotelDetails().get(0).getHotelName().equals(report.getBc().getHotelName()))
			{
				ReportPrinter.append	(	"<tr><td>27</td>"
						+ 	"<td>Verify booking card hotel name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getHotelName() +"</td>"
						+ 	"<td>"+ report.getBc().getHotelName() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>27</td>"
						+ 	"<td>Verify booking card hotel name</td>"
						+	"<td>"+ res.getHotelDetails().get(0).getHotelName() +"</td>"
						+ 	"<td>"+ report.getBc().getHotelName() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(hotel.getAddress().toLowerCase().equals(report.getBc().getHotelAddress().trim().toLowerCase()))
			{
				ReportPrinter.append	(	"<tr><td>28</td>"
						+ 	"<td>Verify booking card hotel address</td>"
						+	"<td>"+ hotel.getAddress() +"</td>"
						+ 	"<td>"+ report.getBc().getHotelAddress() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>28</td>"
						+ 	"<td>Verify booking card hotel address</td>"
						+	"<td>"+ hotel.getAddress() +"</td>"
						+ 	"<td>"+ report.getBc().getHotelAddress() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			//date2 	=  	report.getBc().getCheckin().split("/");
			//day2 	= 	date2[2].trim().concat("-").concat(date2[1].trim().concat("-").concat(date2[0].trim()));
			day2 = formatdate2(report.getBc().getCheckin());
			if(hotel.getCheckIn().equals(day2))
			{
				ReportPrinter.append	(	"<tr><td>29</td>"
						+ 	"<td>Verify booking card hotel checkin date</td>"
						+	"<td>"+ hotel.getCheckIn() +"</td>"
						+ 	"<td>"+ day2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>29</td>"
						+ 	"<td>Verify booking card hotel checkin date</td>"
						+	"<td>"+ hotel.getCheckIn() +"</td>"
						+ 	"<td>"+ day2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			day2 = formatdate2(report.getBc().getCheckOut());
			if(hotel.getCheckout().equals(day2))
			{
				ReportPrinter.append	(	"<tr><td>30</td>"
						+ 	"<td>Verify booking card hotel checkout date</td>"
						+	"<td>"+ hotel.getCheckout() +"</td>"
						+ 	"<td>"+ day2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>30</td>"
						+ 	"<td>Verify booking card hotel checkout date</td>"
						+	"<td>"+ hotel.getCheckout() +"</td>"
						+ 	"<td>"+ day2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(searchDetails[4].equals(report.getBc().getNoOfRooms()))
			{
				ReportPrinter.append	(	"<tr><td>31</td>"
						+ 	"<td>Verify booking card hotel number of rooms</td>"
						+	"<td>"+ searchDetails[4] +"</td>"
						+ 	"<td>"+ report.getBc().getNoOfRooms() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>31</td>"
						+ 	"<td>Verify booking card hotel number of rooms</td>"
						+	"<td>"+ searchDetails[4] +"</td>"
						+ 	"<td>"+ report.getBc().getNoOfRooms() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(searchDetails[3].equals(report.getBc().getNumOfNyts()))
			{
				ReportPrinter.append	(	"<tr><td>32</td>"
						+ 	"<td>Verify booking card hotel number of nights</td>"
						+	"<td>"+ searchDetails[3] +"</td>"
						+ 	"<td>"+ report.getBc().getNumOfNyts() +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>32</td>"
						+ 	"<td>Verify booking card hotel number of nights</td>"
						+	"<td>"+ searchDetails[3] +"</td>"
						+ 	"<td>"+ report.getBc().getNumOfNyts() +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			
			String total = cc.convert(defaultCurrency, res.getFinalPrice(), report.getCurrencyCode(), currency, res.getFinalcurrency());
			String pm = String.valueOf(apm.profitMarkup(total, txtProperty.get("hb")));
			String bf = String.valueOf(abf.bookingFee(total, bookingFee.get("hotelbeds")));
			String total2 = String.valueOf(Integer.parseInt(total) + Integer.parseInt(pm) + Integer.parseInt(bf));	
			String[] sbv = report.getBc().getTotalBookingvalue().trim().split("\\.");
			sbv = sbv[0].split(",");
			String sbv2 = sbv[0].concat(sbv[1]);
			if(total2.equals(sbv2))
			{
				ReportPrinter.append	(	"<tr><td>33</td>"
						+ 	"<td>Verify booking card hotel total booking value</td>"
						+	"<td>"+ total2 +"</td>"
						+ 	"<td>"+ sbv2 +"</td>"
						+	"<td class='passed'>Passed</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>33</td>"
						+ 	"<td>Verify booking card hotel total booking value</td>"
						+	"<td>"+ total2 +"</td>"
						+ 	"<td>"+ sbv2 +"</td>"
						+	"<td class='Failed'>Failed</td></tr>");
			}
			cnt++;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		// rest of the hotel payment details
		ArrayList<String> roomtype1 	= new ArrayList<String>();
		ArrayList<String> roomtype2 	= new ArrayList<String>();
		ArrayList<String> bedtype1 		= new ArrayList<String>();
		ArrayList<String> bedtype2 		= new ArrayList<String>();
		ArrayList<String> ratePlan1 	= new ArrayList<String>();
		ArrayList<String> ratePlan2 	= new ArrayList<String>();
		ArrayList<String> cpr1 			= new ArrayList<String>();
		ArrayList<String> cpr2 			= new ArrayList<String>();
		for(int i = 0 ; i < res.getHotelDetails().size() ; i ++)
		{
			try {
				roomtype1.add(res.getHotelDetails().get(i).getRoomType().trim());
				roomtype2.add(report.getBc().getRoom().get(i).getRoomType().trim());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				bedtype1.add(hotel.getRoom().get(i).getBedType().trim());
				bedtype2.add(report.getBc().getRoom().get(i).getBedType().trim());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				ratePlan1.add(hotel.getRoom().get(i).getBoardType().trim());
				ratePlan2.add(report.getBc().getRoom().get(i).getRatePlan().trim());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				System.out.println(res.getHotelDetails().get(i).getPrice());
				System.out.println(report.getBc().getRoom().get(i).getTotalRate());
				
				String total = cc.convert(defaultCurrency, res.getHotelDetails().get(i).getPrice(), report.getCurrencyCode(), currency, res.getHotelDetails().get(i).getCurrencyCode());
				String pm = String.valueOf(apm.profitMarkup(total, txtProperty.get("hb")));
				String bf = String.valueOf(abf.bookingFee(total, bookingFee.get("hotelbeds")));
				String total2 = String.valueOf(Integer.parseInt(total) + Integer.parseInt(pm) + Integer.parseInt(bf));	
				
				cpr1.add(total2.trim());
				String[] value = report.getBc().getRoom().get(i).getTotalRate().trim().split("\\."); 
				cpr2.add(value[0]);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		cnt = 34;
		if(roomtype1.containsAll(roomtype2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ roomtype1 +"</td>"
					+ 	"<td>"+ roomtype2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ roomtype1 +"</td>"
					+ 	"<td>"+ roomtype2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(bedtype1.contains(bedtype2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify bed type</td>"
					+	"<td>"+ bedtype1 +"</td>"
					+ 	"<td>"+ bedtype2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify bed type</td>"
					+	"<td>"+ bedtype1 +"</td>"
					+ 	"<td>"+ bedtype2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(ratePlan1.containsAll(ratePlan2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify rate plan</td>"
					+	"<td>"+ ratePlan1 +"</td>"
					+ 	"<td>"+ ratePlan2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify rate plan</td>"
					+	"<td>"+ ratePlan1 +"</td>"
					+ 	"<td>"+ ratePlan2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(cpr1.containsAll(cpr2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify cost per romm</td>"
					+	"<td>"+ cpr1 +"</td>"
					+ 	"<td>"+ cpr2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify cost per room</td>"
					+	"<td>"+ cpr1 +"</td>"
					+ 	"<td>"+ cpr2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}
	
	public String formatdate(String date)
	{
		System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyyMMdd");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
	
	public String formatdate2(String date)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("dd MMM yyyy");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
	
	public String formatdate5(String date)
	{
		System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyy-MM-dd");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
	
	public String formatdate3(String date)
	{
		System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyy-MM-dd");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
	
	public String formatdate4(String date)
	{
		System.out.println(date);
		//c.setTime(sdf.parse(searchDetails[2]));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("yyyy-MM-dd");
		System.out.println(sdf.format(d));
		String x = sdf.format(d);
		return x;
	}
	
	public String changeDate(String date, String conDate2) 
	{
		System.out.println(conDate2);
		int i = Integer.parseInt(date) + 3;
		System.out.println(i);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(conDate2));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		c.add(Calendar.DATE, -i);  // number of days to add
		date = sdf.format(c.getTime());
		System.out.println(date);
		return date;
	}
}
