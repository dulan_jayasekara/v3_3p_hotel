package verification;

import java.util.ArrayList;

import readXml.hbPage1Req;
import readXml.hotelAvailability;
import readXml.resHotelAvailability;

public class hbPreReservation {

	public void inv13VerifyPreReservation(hbPage1Req preReqAvailability, resHotelAvailability preResAvailability, StringBuffer ReportPrinter)
	{

		int cnt = 1 ;
		ReportPrinter.append("<span>(6) Pre reservation req against res</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		System.out.println(preReqAvailability.getCheckin());
		System.out.println(preResAvailability.getSingleRoom().getDateTimeFromDate());
		if(preResAvailability.getSingleRoom().getDateTimeFromDate().contains(preReqAvailability.getCheckin()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkin date</td>"
					+	"<td>"+ preReqAvailability.getCheckin() +"</td>"
					+ 	"<td>"+ preResAvailability.getSingleRoom().getDateTimeFromDate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkin date</td>"
					+	"<td>"+ preReqAvailability.getCheckin() +"</td>"
					+ 	"<td>"+ preResAvailability.getSingleRoom().getDateTimeFromDate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		System.out.println(preReqAvailability.getGuest().size());
		ArrayList<String> adlt1 = new ArrayList<String>();
		ArrayList<String> adlt2 = new ArrayList<String>();
		ArrayList<String> chld1 = new ArrayList<String>();
		ArrayList<String> chld2 = new ArrayList<String>();
		ArrayList<String> gst1 = new ArrayList<String>();
		ArrayList<String> gst2 = new ArrayList<String>();
		for(int i = 0, k = preReqAvailability.getGuest().size()-1 ; i < preReqAvailability.getGuest().size() ; i++, k--)
		{
			try {
				adlt1.add(preReqAvailability.getGuest().get(i).getAdultCount());
				adlt2.add(preResAvailability.getRoomAvailability().get(k).getAdultCount());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				chld1.add(preReqAvailability.getGuest().get(i).getChildCount());
				chld2.add(preResAvailability.getRoomAvailability().get(k).getChildCount());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			//if(preReqAvailability.getRoomAvailability().get(i).getRoomType().equals(preResAvailability.getHotelAvailability().get(i).getHotelRoom()))
			
			for(int j = 0 ; j < preReqAvailability.getGuest().get(i).getCus().size() ; j++)
			{
				try {
					gst1.add(preReqAvailability.getGuest().get(i).getCus().get(j).getAge());
					gst2.add(preResAvailability.getRoomAvailability().get(k).getGuest().get(j).getCustomerAge());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		}
		
		if(adlt1.containsAll(adlt2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adlt1 +"</td>"
					+ 	"<td>"+ adlt2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adlt1 +"</td>"
					+ 	"<td>"+ adlt2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(chld1.containsAll(chld2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chld1 +"</td>"
					+ 	"<td>"+ chld2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chld1 +"</td>"
					+ 	"<td>"+ chld2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(gst1.containsAll(gst2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify guest age</td>"
					+	"<td>"+ gst1 +"</td>"
					+ 	"<td>"+ gst2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify guest age</td>"
					+	"<td>"+ gst1 +"</td>"
					+ 	"<td>"+ gst2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		//System.out.println(preReqAvailability.getRoomAvailability().get(0).getRoomCode());
		try {
			System.out.println(preResAvailability.getHotelDetails().get(0).getAvailRoom().get(0).getRoom().get(0).getRoomType());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		ReportPrinter.append("</table>");
	
	}
	
	public void verifyPreReservation(hotelAvailability preReqAvailability, resHotelAvailability preResAvailability, StringBuffer ReportPrinter)
	{
		int cnt = 1 ;
		ReportPrinter.append("<span>(8) Pre reservation req against res</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		if(preReqAvailability.getDateFromDate().equals(preResAvailability.getDateFromDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkin date</td>"
					+	"<td>"+ preReqAvailability.getDateFromDate() +"</td>"
					+ 	"<td>"+ preResAvailability.getDateFromDate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Checkin date</td>"
					+	"<td>"+ preReqAvailability.getDateFromDate() +"</td>"
					+ 	"<td>"+ preResAvailability.getDateFromDate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		if(preReqAvailability.getDateTodate().equals(preResAvailability.getDateToDate()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ preReqAvailability.getDateTodate() +"</td>"
					+ 	"<td>"+ preResAvailability.getDateToDate() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkout date</td>"
					+	"<td>"+ preReqAvailability.getDateTodate() +"</td>"
					+ 	"<td>"+ preResAvailability.getDateToDate() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(preReqAvailability.getDestination().equals(preResAvailability.getDestinationCode()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Destination</td>"
					+	"<td>"+ preReqAvailability.getDestination() +"</td>"
					+ 	"<td>"+ preResAvailability.getDestinationCode() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Destination</td>"
					+	"<td>"+ preReqAvailability.getDestination() +"</td>"
					+ 	"<td>"+ preResAvailability.getDestinationCode() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		ArrayList<String> adltCnt1 	= new ArrayList<String>();
		ArrayList<String> adltCnt2 	= new ArrayList<String>();
		ArrayList<String> chldCnt1 	= new ArrayList<String>();
		ArrayList<String> chldCnt2 	= new ArrayList<String>();
		ArrayList<String> rmType1 	= new ArrayList<String>();
		ArrayList<String> rmType2	= new ArrayList<String>();
		ArrayList<String> gstType1 	= new ArrayList<String>();
		ArrayList<String> gstType2 	= new ArrayList<String>();
		ArrayList<String> gstAge1 	= new ArrayList<String>();
		ArrayList<String> gstAge2 	= new ArrayList<String>();
		
		for(int i = 0 ; i < preReqAvailability.getRoomAvailability().size() ; i++)
		{
			adltCnt1.add(preReqAvailability.getRoomAvailability().get(i).getAdultCount());
			adltCnt2.add(preResAvailability.getRoomAvailability().get(i).getAdultCount());
			
			chldCnt1.add(preReqAvailability.getRoomAvailability().get(i).getChildCount());
			chldCnt2.add(preResAvailability.getRoomAvailability().get(i).getChildCount());
			
			rmType1.add(preReqAvailability.getRoomAvailability().get(i).getRoomCode());
			rmType2.add(preResAvailability.getHotelRoom().get(i).getRoomCode());

			//if(preReqAvailability.getRoomAvailability().get(i).getRoomType().equals(preResAvailability.getHotelAvailability().get(i).getHotelRoom()))
			for(int j = 0 ; j < preReqAvailability.getRoomAvailability().get(i).getGuestList().size() ; j++)
			{
				gstType1.add(preReqAvailability.getRoomAvailability().get(i).getGuestList().get(j).getCustomerType());
				gstType2.add(preResAvailability.getRoomAvailability().get(i).getGuest().get(j).getCustomerType());
			
				gstAge1.add(preReqAvailability.getRoomAvailability().get(i).getGuestList().get(j).getCustomerAge());
				gstAge2.add(preResAvailability.getRoomAvailability().get(i).getGuest().get(j).getCustomerAge());
			}
		}
		
		if(adltCnt1.containsAll(adltCnt2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adltCnt1 +"</td>"
					+ 	"<td>"+ adltCnt2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify adult count</td>"
					+	"<td>"+ adltCnt1 +"</td>"
					+ 	"<td>"+ adltCnt2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(chldCnt1.containsAll(chldCnt2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chldCnt1 +"</td>"
					+ 	"<td>"+ chldCnt2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child count</td>"
					+	"<td>"+ chldCnt1 +"</td>"
					+ 	"<td>"+ chldCnt2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(rmType1.containsAll(rmType2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ rmType1 +"</td>"
					+ 	"<td>"+ rmType2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify room type</td>"
					+	"<td>"+ rmType1 +"</td>"
					+ 	"<td>"+ rmType2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");	
		}
		cnt++;

		if(gstAge1.containsAll(gstAge2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child age</td>"
					+	"<td>"+ gstAge1 +"</td>"
					+ 	"<td>"+ gstAge2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify child age</td>"
					+	"<td>"+ gstAge1 +"</td>"
					+ 	"<td>"+ gstAge2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		if(gstType1.containsAll(gstType2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify guest type</td>"
					+	"<td>"+ gstType1 +"</td>"
					+ 	"<td>"+ gstType2 +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify guest type</td>"
					+	"<td>"+ gstType1 +"</td>"
					+ 	"<td>"+ gstType2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		System.out.println(preReqAvailability.getRoomAvailability().get(0).getRoomCode());
		try {
			System.out.println(preResAvailability.getHotelDetails().get(0).getAvailRoom().get(0).getRoom().get(0).getRoomType());
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println(preResAvailability.getHotelRoom().get(0).getRoomCode());
		
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
	}
}
