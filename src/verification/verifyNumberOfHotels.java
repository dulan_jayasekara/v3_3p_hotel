package verification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import readAndWrite.readHotelResults;
import readXml.resHotelAvailability;

public class verifyNumberOfHotels {

	public void verify(int resultsWeb, int resultsXml, StringBuffer ReportPrinter)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(1.1) Cancellation Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Number of results in the results page</th>"
			+ "<th>Number of results in the availability response xml</th></tr>");
		
		ReportPrinter.append	(	"<tr><td>1</td>"
				+ 	"<td>"+ resultsWeb +"</td>"
				+	"<td>"+ resultsXml +"</td></tr>");
		ReportPrinter.append("</table>");
	}
	
	public void searchingHotel(resHotelAvailability resAvailability, String hotel2, StringBuffer ReportPrinter, ArrayList<readHotelResults> hotelList)
	{
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(1.2) Check whehter searching hotel is available in availability response xml</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Hotel name</th>"
			+ "<th>Xml availability</th></tr>");
		System.out.println(resAvailability.getHotelDetails().size());
		int cnt = 0;
		for(int i = 0 ; i < resAvailability.getHotelDetails().size(); i++)
		{
			if(resAvailability.getHotelDetails().get(i).getHotelName().equals(hotel2.trim()))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>"+ hotel2 +"</td>"
						+	"<td class='passed'>available</td></tr>");
				cnt++;
			}
		}
		if(cnt == 0)
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
				+ 	"<td>"+ hotel2 +"</td>"
				+	"<td class='Failed'>not available</td></tr>");
		}
		ReportPrinter.append("</table>");
		
		ReportPrinter.append("<span>(1.3) Check whehter searching hotel is available in results page</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Hotel name</th>"
			+ "<th>Results page availability</th></tr>");
		cnt = 0 ;
		for(int i = 0 ; i < hotelList.size(); i++)
		{
			if(hotelList.get(i).getTitle().trim().equals(hotel2.trim()))
			{
				ReportPrinter.append	(	"<tr><td>1</td>"
						+ 	"<td>"+ hotel2 +"</td>"
						+	"<td class='passed'>available</td></tr>");
				cnt++;
			}
		}
		
		if(cnt == 0)
		{
			ReportPrinter.append	(	"<tr><td>1</td>"
					+ 	"<td>"+ hotel2 +"</td>"
					+	"<td class='Failed'>not available</td></tr>");
		}
		ReportPrinter.append("</table>");
	}
	
	public void rateChange(String error, resHotelAvailability resRoomAvailability, resHotelAvailability resAvailability, String hotel2, StringBuffer ReportPrinter)
	{	
		Map			<String, String>  	roomRates 		= new HashMap<String, String>();
		Map			<String, String>  	roomAvailRates 	= new HashMap<String, String>();
		
		for(int i = 0 ; i < resAvailability.getHotelDetails().size() ; i++)
		{
			if(hotel2.trim().equals(resAvailability.getHotelDetails().get(i).getHotelName().trim()))
			{
				for(int j = 0 ; j < resAvailability.getHotelDetails().get(i).getRhRoom().size() ; j++)
				{
					if(roomRates.containsKey(resAvailability.getHotelDetails().get(i).getRhRoom().get(j).getRoom()))
					{
						
					}
					else
					{
						roomRates.put(resAvailability.getHotelDetails().get(i).getRhRoom().get(j).getRoom(), resAvailability.getHotelDetails().get(i).getRhRoom().get(j).getPrice());
					}
					
				}
			}
		}
		for(int i = 0 ; i < resRoomAvailability.getHotelRoom().size() ; i++)
		{
			roomAvailRates.put(resRoomAvailability.getHotelRoom().get(i).getRoom(), resRoomAvailability.getHotelRoom().get(i).getPrice());
		}
		ArrayList<String> roomTypes = new ArrayList<String>();
		Iterator roomAvailIterator 	= roomAvailRates.keySet().iterator();
		Iterator roomIterator 		= roomRates.keySet().iterator();
		while(roomAvailIterator.hasNext())
		{
			roomTypes.add(roomAvailIterator.next().toString());
		}
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(1.4) Check whehter rates got changed or not</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
				+ "<tr><th>Test Case</th>"
				+ "<th>Room type (Availabilty Respone)</th>"
				+ "<th>Room price (Availability Response)</th>"
				+ "<th>Room Price (Room Availabilty Response)</th></tr>");
		for(int i = 0 ; i < roomTypes.size() ; i++)
		{
			if(roomAvailRates.get(roomTypes.get(i)).equals(roomRates.get(roomTypes.get(i))))
			{
				ReportPrinter.append	(	"<tr><td>"+ (i+1) +"</td>"
						+ 	"<td>"+ roomTypes.get(i) +"</td>"
						+	"<td class='passed'>"+ roomRates.get(roomTypes.get(i)) +"</td>"
						+   "<td class='passed'>"+ roomAvailRates.get(roomTypes.get(i)) +"</td></tr>");
			}
			else
			{
				ReportPrinter.append	(	"<tr><td>"+ (i+1) +"</td>"
						+ 	"<td>"+ roomTypes.get(i) +"</td>"
						+	"<td class='Failed'>"+ roomRates.get(roomTypes.get(i)) +"</td>"
						+   "<td class='Failed'>"+ roomAvailRates.get(roomTypes.get(i)) +"</td></tr>");
			}
			
		}
		
		ReportPrinter.append("</table>");
		
		
	}
}
