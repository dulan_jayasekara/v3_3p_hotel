package verification;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import currencyConvertion.addBookingFee;
import currencyConvertion.addProfitmarkup;
import currencyConvertion.currencyConveter;
import readAndWrite.cancellationDetails;
import readAndWrite.cancellationReport;
import readConfirmation.conhotel;
import readXml.resHotelAvailability;

public class cancellationVerify {

	private static Map<String, String>  	pmTxt 		= new HashMap<String, String>();
	private static Map<String, String>  	bfTxt 		= new HashMap<String, String>();
	public void hb(StringBuffer ReportPrinter, cancellationDetails cd, resHotelAvailability canResponse, conhotel conHotel, String channel, String defaultCurrency, Map<String, String> currency, cancellationReport cr)
	{
		Properties properties 	= new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			//FileReader reader 	=new FileReader(new File("D://workSpace//hotelXmlValidation//logginInfo.properties"));
			FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//logginInfo.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value 		= properties.getProperty(key);
		    pmTxt.put(key, value);
		}
		
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			//FileReader reader 	=new FileReader(new File("D://workSpace//hotelXmlValidation//logginInfo.properties"));
			FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//logginInfo.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value 		= properties.getProperty(key);
		    bfTxt.put(key, value);
		}
		
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span>(14 / 16) Cancellation Report Verification</span>");
		
		ReportPrinter.append("<table border=1 style=width:100%>"
			+ "<tr><th>Test Case</th>"
			+ "<th>Test Description</th>"
			+ "<th>Expected Result</th>"
			+ "<th>Actual Result</th>"
			+ "<th>Test Status</th></tr>");
		
		int cnt =1;
		System.out.println("Reservation # " + cd.getBookingNumber()+ " has been Successfully cancelled !");
		if(canResponse.getStatus().equals("CANCELLED") || cd.getMsg().equals("Reservation # " + cd.getBookingNumber()+ " has been Successfully cancelled !"))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reservation number</td>"
					+	"<td>"+ canResponse.getStatus() +"</td>"
					+ 	"<td>"+ cd.getMsg() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reservation number</td>"
					+	"<td>"+ canResponse.getStatus() +"</td>"
					+ 	"<td>"+ cd.getMsg() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cd.setBookingNumber(cd.getBookingNumber().replace(":", "").trim());
		if(cd.getBookingNumber().equalsIgnoreCase(conHotel.getReservationNumber()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reservation number</td>"
					+	"<td>"+ conHotel.getReservationNumber() +"</td>"
					+ 	"<td>"+ cd.getBookingNumber() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify reservation number</td>"
					+	"<td>"+ conHotel.getReservationNumber() +"</td>"
					+ 	"<td>"+ cd.getBookingNumber() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		cd.setBookingChanel(cd.getBookingChanel().replace(":", "").trim());
		if(channel.equalsIgnoreCase(cd.getBookingChanel()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking channel</td>"
					+	"<td>"+ channel +"</td>"
					+ 	"<td>"+ cd.getBookingChanel() +"</td>"
					+	"<td class='passed'>Passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking channel</td>"
					+	"<td>"+ channel +"</td>"
					+ 	"<td>"+ cd.getBookingChanel() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		String date = new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime());
		String date2 = cd.getBookingDate().replace(":", "").trim();
		if(date.equalsIgnoreCase(date2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking date</td>"
					+	"<td>"+ date +"</td>"
					+ 	"<td>"+ date2 +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify booking date</td>"
					+	"<td>"+ date +"</td>"
					+ 	"<td>"+ date2 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		String[] hotel = conHotel.getHotelName().split("View");
		
		if(cd.getHotelName().trim().equalsIgnoreCase(hotel[0].trim()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ hotel[0].trim() +"</td>"
					+ 	"<td>"+ cd.getHotelName().trim() +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel name</td>"
					+	"<td>"+ hotel[0].trim() +"</td>"
					+ 	"<td>"+ cd.getHotelName().trim() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		String address = cd.getAddress().replace(":", "").trim();
		if(address.equalsIgnoreCase(conHotel.getAddress()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel address</td>"
					+	"<td>"+ conHotel.getAddress() +"</td>"
					+ 	"<td>"+ address +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify hotel address</td>"
					+	"<td>"+ conHotel.getAddress() +"</td>"
					+ 	"<td>"+ address +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		date = cd.getCheckin().replace(":", "").trim();
		date = date.replace("-", " ");
		if(date.equalsIgnoreCase(conHotel.getCheckIn()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ conHotel.getCheckIn() +"</td>"
					+ 	"<td>"+ date +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ conHotel.getCheckIn() +"</td>"
					+ 	"<td>"+ date +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		date = cd.getCheckout().replace(":", "").trim();
		date = date.replace("-", " ");
		if(date.equalsIgnoreCase(conHotel.getCheckout()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ conHotel.getCheckout() +"</td>"
					+ 	"<td>"+ date +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify checkin date</td>"
					+	"<td>"+ conHotel.getCheckout() +"</td>"
					+ 	"<td>"+ date +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		
		String nyts = cd.getNytsNum().replace(":", "").trim();
		if(nyts.equalsIgnoreCase(conHotel.getNights()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights</td>"
					+	"<td>"+ conHotel.getNights() +"</td>"
					+ 	"<td>"+ nyts +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of nights</td>"
					+	"<td>"+ conHotel.getNights() +"</td>"
					+ 	"<td>"+ nyts +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		String rooms = cd.getRoomsNum().replace(":", "").trim();
		if(rooms.equalsIgnoreCase(conHotel.getRooms()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ conHotel.getRooms() +"</td>"
					+ 	"<td>"+ rooms +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify number of rooms</td>"
					+	"<td>"+ conHotel.getRooms() +"</td>"
					+ 	"<td>"+ rooms +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		
		currencyConveter 	cc 	= 	new currencyConveter();
		addProfitmarkup	 	pm 	= 	new addProfitmarkup();
		addBookingFee 		bf 	= 	new addBookingFee();
		String total = cd.getTotalHotelCancellationCharge().replace(":", "").trim();
		/*System.out.println(canResponse.getCurrencyCode());
		System.out.println(conHotel.getCurrencyCode());
		System.out.println(canResponse.getTotalAmount());
		System.out.println(cr.getTotalCancellationFee());
		String conValue = cc.convert(defaultCurrency, canResponse.getTotalAmount(), conHotel.getCurrencyCode(), currency, canResponse.getCurrencyCode());
		conValue = String.valueOf(pm.profitMarkup(conValue, pmTxt.get("hotelbeds")));
		String value = String.valueOf(Integer.parseInt(canResponse.getTotalAmount()) + Integer.parseInt(conValue));
		conValue = String.valueOf(bf.bookingFee(conValue, bfTxt.get("hotelbeds")));
		value = String.valueOf(Integer.parseInt(value) + Integer.parseInt(conValue));
		System.out.println(value);*/
		
		if(canResponse.getTotalAmount().contains(cr.getTotalCancellationFee()))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify Cancellation Fees</td>"
					+	"<td>"+ canResponse.getTotalAmount() +"</td>"
					+ 	"<td>"+ cr.getTotalCancellationFee() +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify sub total</td>"
					+	"<td>"+ canResponse.getTotalAmount() +"</td>"
					+ 	"<td>"+ cr.getTotalCancellationFee() +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		ArrayList< String> names1 = new ArrayList<String>();
		try {
			for(int i = 0 ; i > -1 ; i++)
			{
				String name = cd.getRoomOccupancy().get(i).getTitle().trim().concat(" ").concat(cd.getRoomOccupancy().get(i).getFirstName().trim()).concat(" ").concat(cd.getRoomOccupancy().get(i).getLastName().trim());
				names1.add(name);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		ArrayList<String> names2 = new ArrayList<String>();
		for(int i = 0 ; i < conHotel.getOccypancy().size() ; i++)
		{
			for(int j = 0 ; j < conHotel.getOccypancy().get(i).size() ; j++)
			{
				try {
					String name = conHotel.getOccypancy().get(i).get(j).getTitle().concat(" ").concat(conHotel.getOccypancy().get(i).get(j).getFirstName()).concat(" ").concat(conHotel.getOccypancy().get(i).get(j).getLastName());
					if(names2.contains(name))
					{
						
					}
					else
					{
						names2.add(name);
					}
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				
			}
		}
		if(names1.containsAll(names2))
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify occupancy</td>"
					+	"<td>"+ names2 +"</td>"
					+ 	"<td>"+ names1 +"</td>"
					+	"<td class='passed'>passed</td></tr>");
		}
		else
		{
			ReportPrinter.append	(	"<tr><td>"+ cnt +"</td>"
					+ 	"<td>Verify occupancy</td>"
					+	"<td>"+ names2 +"</td>"
					+ 	"<td>"+ names1 +"</td>"
					+	"<td class='Failed'>Failed</td></tr>");
		}
		cnt++;
		ReportPrinter.append("</table>");
		ReportPrinter.append	("<b></b><br>"
                +	 "<hr size=4>");
		
		System.out.println(cd.getRoomOccupancy().get(0).getFirstName());
		System.out.println(conHotel.getRoom());
		
	}
}
