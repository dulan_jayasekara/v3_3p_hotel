package readAndWrite;

import java.awt.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.zip.Adler32;

import javax.lang.model.type.ErrorType;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import excelReader.ExcelReader;

public class paymentReadAndWrite {

		public hotel paymentReadWeb(WebDriver driver, StringBuffer ReportPrinter) throws FileNotFoundException
		{
			WebDriverWait 	wait 	= new WebDriverWait(driver, 3);
			hotel 			hotel 	= new hotel();
			room			room	= new room();
			payments		pay		= new payments();
			
			try {
				hotel.setHotelName				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[1]/div[1]/b")).getText());
				String hotelHelp 			=	driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[1]/div[1]")).getText();
				String[] addressHelp 		= 	hotelHelp.split(hotel.getHotelName());
				addressHelp					= 	addressHelp[1].split("Status");
				hotel.setAddress				(addressHelp[0].trim());
				hotel.setCheckin				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div/div[1]/span[2]")).getText());
				hotel.setCheckOut				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div/div[2]/span[2]")).getText());
				addressHelp					= 	addressHelp[1].split("Remove This Item");
				addressHelp					= 	addressHelp[0].split("-");
				hotel.setBookingStatus			(addressHelp[1]);
				hotel.setCancellationDeadLine	(driver.findElement(By.xpath(".//*[@id='tconditions']/div[2]/div[2]/table/tbody/tr/td[2]")).getText());
				hotel.setCurrencyCode			(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[3]/table/tbody/tr[1]/td[5]")).getText());
				ArrayList<room> 				roomList = new ArrayList<room>();
				for(int i=1 ; i>0 ; i++)
				{ 
					try {
						driver.switchTo().defaultContent();
						//roomnumber_Central Park_1
						//System.out.println("roomnumber_"+ hotel.getHotelName() +"_1");
						room.setRoom						(driver.findElement(By.id		("roomnumber_"+ hotel.getHotelName() +"_"+ i +"")).getText());
						room.setRoomType					(driver.findElement(By.id		("roomtype_"+ hotel.getHotelName() +"_"+ i +"")).getText());
						room.setBoardType					(driver.findElement(By.id		("roomboardtype_"+ hotel.getHotelName() +"_"+ i +"")).getText());
						room.setBedType						(driver.findElement(By.id		("bedtype_"+ hotel.getHotelName() +"_"+ i +"")).getText());
						room.setTotalRate					(driver.findElement(By.id	("sellrate_"+ hotel.getHotelName() +"_"+ i +"")).getText());
						String[] count						= driver.findElement(By.id	("roomnumber_"+ hotel.getHotelName() +"_"+ i +"")).getText().split("s");
						String[] adultCount					= count[1].split(" ");
						room.setAdultCount					(adultCount[0]);
						System.out.println(room.getAdultCount());
						String[] childCount					= count[1].split("n");
						childCount							= childCount[1].split(" ");
						room.setChildCount					(childCount[0]);
						System.out.println(room.getChildCount());
						roomList.add(room);
					} catch (Exception e) {
						break;
					}
				}
				hotel.setRoom(roomList);
				pay.setSubTotal								(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[3]/div[2]/div[4]")).getText());
				pay.setTotalTaxAndServiceCharges			(driver.findElement(By.id("totaltax_hotel")).getText());
				try {
					pay.setAmountbeingProcessedNow				(driver.findElement(By.id("totpayNowamt")).getText());
				} catch (Exception e) {
					// TODO: handle exception
				}
				pay.setAmountdueatCheckInUtilization		(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[3]/div[2]/div[8]/div[5]")).getText());
				hotel.setPay(pay);
				
				finalPayment finalPay						= new finalPayment();
				finalPay.setPacckageCurrency				(driver.findElement(By.xpath(".//*[@id='masterCardPaymentRow']/td[2]/div[1]/table/tbody/tr[3]/td/span[2]")).getText());
				finalPay.setTotalGrossPackageBookingValue	(driver.findElement(By.xpath(".//*[@id='masterCardPaymentRow']/td[2]/div[2]/table/tbody/tr[1]/td[2]")).getText());
				finalPay.setTotalTaxesandFees				(driver.findElement(By.xpath(".//*[@id='totaltaxpgcurrid']")).getText());
				finalPay.setTotalPackageBookingValue		(driver.findElement(By.xpath(".//*[@id='totalchargeamtpgcurrid']")).getText());
				finalPay.setAmountbeingProcessedNow			(driver.findElement(By.xpath(".//*[@id='totalpaynowamountpgcurrid']")).getText());
				finalPay.setAmountDueatCheckInUtilization	(driver.findElement(By.xpath(".//*[@id='totalamountatutilizationpgcurrid']")).getText());
				hotel.setFinalPay(finalPay);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			ExcelReader 							newReader 		= 	new ExcelReader();
			FileInputStream 						stream 			=	new FileInputStream(new File("excel/customer.xls"));
			ArrayList<Map<Integer, String>> 		ContentList 	= 	new ArrayList<Map<Integer,String>>();
													ContentList 	= 	newReader.contentReading(stream);
			Map<Integer, String>  					content 		= 	new HashMap<Integer, String>();
													content   		= 	ContentList.get(0);
			Iterator<Map.Entry<Integer, String>>   	mapiterator 	= 	content.entrySet().iterator();
			Map.Entry<Integer, String> 				Entry 			= 	(Map.Entry<Integer, String>)mapiterator.next();
			String 									Content 		= 	Entry.getValue();
			String[] 								customerDetails = 	Content.split(",");
			
			new Select(driver.findElement(By.id("cusTitle"))).selectByVisibleText(customerDetails[0]);
			driver.findElement(By.id("cusFName")).sendKeys(customerDetails[1]);
			String[] 								tele			= 	customerDetails[3].split("/");
			driver.findElement(By.id("cusareacodetext")).sendKeys(tele[0]);
			driver.findElement(By.id("cusPhonetext")).sendKeys(tele[1]);
			driver.findElement(By.id("cusEmail")).sendKeys(customerDetails[4]);
			driver.findElement(By.id("cusConfEmail")).sendKeys(customerDetails[4]);
			driver.findElement(By.id("cusAdd1")).sendKeys(customerDetails[5]);
			new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(customerDetails[6]);
			driver.findElement(By.id("cusCity")).sendKeys(customerDetails[7]);
			driver.findElement(By.id("cusLName")).sendKeys(customerDetails[2]);
			driver.findElement(By.id("cusZip")).sendKeys(customerDetails[8]);
			
													stream 			=	new FileInputStream(new File("excel/searchDetails.xls"));
													ContentList 	= 	newReader.contentReading(stream);
													content   		= 	ContentList.get(0);
													mapiterator 	= 	content.entrySet().iterator();
													Entry 			= 	(Map.Entry<Integer, String>)mapiterator.next();
													Content 		= 	Entry.getValue();
			String[] 								roomOccupancy 	= 	Content.split(",");
			
			System.out.println(roomOccupancy[5]);
			String[] 								adultCount		= 	roomOccupancy[5].split("/");
			String[] 								childCount		= 	roomOccupancy[6].split("/");
				
													stream 			=	new FileInputStream(new File("excel/occupancy.xls"));
													ContentList 	= 	newReader.contentReading(stream);
													content   		= 	ContentList.get(0);
													mapiterator 	= 	content.entrySet().iterator();
			
													newReader 		= 	new ExcelReader();
			readSearchDetails						searchgRead 	= 	new readSearchDetails();
													stream 			=	new FileInputStream(new File("excel/searchDetails.xls"));
													ContentList 	= 	new ArrayList<Map<Integer,String>>();
													ContentList 	= 	newReader.contentReading(stream);
													content 		= 	new HashMap<Integer, String>();
													content   		= 	ContentList.get(0);
													mapiterator 	= 	content.entrySet().iterator();
													Entry 			= 	(Map.Entry<Integer, String>)mapiterator.next();
													Content 		= 	Entry.getValue();
			String[] 								searchDetails 	= 	Content.split(",");
			
			ArrayList<WebElement> 					adltFname 		= 	new ArrayList<WebElement>();
			ArrayList<ArrayList<WebElement>> 		adltFnameList 	= 	new ArrayList<ArrayList<WebElement>>();
			int roomCnt = Integer.parseInt(searchDetails[4]);
			String[] adltPerRoom = searchDetails[5].split("/");
			String[] chldPerRoom = searchDetails[6].split("/");
			
			for(int j = 0 ; j < roomCnt ; j++)
			{
				for(int i = 0 ; i < Integer.parseInt(adltPerRoom[j]) ; i++)
				{
					try {
						adltFname.addAll(driver.findElements(By.id("firstName_adult~" + i)));
						if(adltFname.get(i).equals(null) || adltFname.isEmpty())
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
					adltFnameList.add(adltFname);	
				}
			}
			
			ArrayList<WebElement> adltLname = new ArrayList<WebElement>();
			ArrayList<ArrayList<WebElement>> adltLnameList = new ArrayList<ArrayList<WebElement>>();
			
			for(int j = 0 ; j < roomCnt ; j++)
			{
				for(int i = 0 ; i < Integer.parseInt(adltPerRoom[j]) ; i++)
				{
					try {
						adltLname.addAll(driver.findElements(By.id("lastName_adult~" + i)));
						if(adltLname.get(i).equals(null) || adltLname.isEmpty())
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
				adltLnameList.add(adltLname);
				}
			}
				
			
			
			newReader = new ExcelReader();
			stream =new FileInputStream(new File("excel/occupancy.xls"));
			ContentList = new ArrayList<Map<Integer,String>>();
			ContentList = newReader.contentReading(stream);
			content = new HashMap<Integer, String>();
			content   = ContentList.get(0);
			mapiterator = content.entrySet().iterator();
			System.out.println(adltFname.size());
			WebDriverWait 	wait3 	= new WebDriverWait(driver, 3);
			int cntHelp = 0;
			for(int i = 0 ; i < adltFnameList.size() ; i++)
			{		
				for(int j = 0 ; j < adltFname.size() ; j++)
				{
					try {
						System.out.println("firstName_adult~" + j);
						//adltFname.get(cntHelp).findElement(By.id("firstName_adult~" + i)).sendKeys("");
						if(adltFname.get(j).findElement(By.id("firstName_adult~" + i)).getAttribute("value").equals(""))
						{
							//adltFnameList.get(cntHelp).get(j).findElement(By.id("firstName_adult~" + i)).sendKeys("");
							Entry = (Map.Entry<Integer, String>)mapiterator.next();
							Content = Entry.getValue();
							String[] occupancy = Content.split(",");
							wait3.until(ExpectedConditions.presenceOfElementLocated(By.id("firstName_adult~" + i)));
							adltFname.get(j).findElement(By.id("firstName_adult~" + i)).sendKeys(occupancy[0]);
							adltLname.get(j).findElement(By.id("lastName_adult~" + i)).sendKeys(occupancy[1]);
							//adltFnameList.get(cntHelp).get(j).findElement(By.id("firstName_adult~" + i)).sendKeys(occupancy[0]);
							//adltLnameList.get(cntHelp).get(j).findElement(By.id("lastName_adult~" + i)).sendKeys(occupancy[1]);
						}
						else
						{
							break;
						}
						
						cntHelp++;
					} catch (Exception e) {
						// TODO: handle exception
						//break;
						//cntHelp++;
					}
				}	
			}
			
			stream =new FileInputStream(new File("excel/occupancy.xls"));
			ContentList = newReader.contentReading(stream);
			content   = ContentList.get(0);
			mapiterator = content.entrySet().iterator();
			//driver.findElement(By.id("pay_offline")).click();	
			ArrayList<WebElement> childFname = new ArrayList<WebElement>();
			ArrayList<ArrayList<WebElement>> 		childFnameList 	= 	new ArrayList<ArrayList<WebElement>>();
			for(int j = 0 ; j < roomCnt ; j++)
			{
				for(int i = 0 ; i < Integer.parseInt(chldPerRoom[j]) ; i++)
				{
					try {
						childFname.addAll(driver.findElements(By.id("firstName_~child" + i)));
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}	
					childFnameList.add(childFname);
				}
			}
			ArrayList<WebElement> childLname = new ArrayList<WebElement>();
			ArrayList<ArrayList<WebElement>> 		childLnameList 	= 	new ArrayList<ArrayList<WebElement>>();

			for(int j = 0 ; j < roomCnt ; j++)
			{
				for(int i = 0 ; i < Integer.parseInt(chldPerRoom[j]) ; i++)
				{
					try {
						childLname.addAll(driver.findElements(By.id("lastname~child" + i)));
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
					childLnameList.add(childLname);
				}
			}
		
			newReader = new ExcelReader();
			stream =new FileInputStream(new File("excel/occupancy.xls"));
			ContentList = new ArrayList<Map<Integer,String>>();
			ContentList = newReader.contentReading(stream);
			content = new HashMap<Integer, String>();
			content   = ContentList.get(0);
			mapiterator = content.entrySet().iterator();
			cntHelp = 0 ;
			System.out.println(childFname.size());
			for(int i = 0 ; i < childLnameList.size() ; i++)
			{		
				for(int j = 0 ; j < childFname.size() ; j++)
				{
					try {
						if(childFname.get(j).findElement(By.id("firstName_~child" + i)).getAttribute("value").equals(""))
						{
							//firstName_~child1
							wait3.until(ExpectedConditions.presenceOfElementLocated(By.id("firstName_~child" + i)));
							childFname.get(j).findElement(By.id("firstName_~child" + i)).sendKeys("");
							Entry = (Map.Entry<Integer, String>)mapiterator.next();
							Content = Entry.getValue();
							String[] occupancy = Content.split(",");
							childFname.get(j).findElement(By.id("firstName_~child" + i)).sendKeys(occupancy[5]);
							childLname.get(j).findElement(By.id("lastname~child" + i)).sendKeys(occupancy[6]);
						}
						else
						{
							break;
						}
						
						cntHelp++;
					} catch (Exception e) {
						// TODO: handle exception
						//break;
						//cntHelp++;
					}	
					
				}	
			}
			
			driver.findElement(By.id("confreadPolicy")).click();
			driver.findElement(By.id("confTnC")).click();
			try {
				driver.findElement(By.xpath(".//*[@id='confirm-booking']/div[3]/a/div")).click();
			} catch (Exception e) {
				// TODO: handle exception
				try {
					driver.findElement(By.className("proceed_btn m_proceed_btn")).click();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
			try {
				String date = formatDate(searchDetails[2]);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("status_H_15813-1@@"+ date +"@@hotelbeds_v2")));
				hotel.setAvailability			(driver.findElement(By.id("status_H_15813-1@@"+ date +"@@hotelbeds_v2")).getText());
				hotel.setAvailabilityError		(driver.findElement(By.id("message_H_15900-3@@"+ date +"@@hotelbeds_v2")).getText());
				
				ReportPrinter.append("<span>Payment confirmation Error</span>");
				
				ReportPrinter.append("<table border=1 style=width:800px>"
					+ "<tr><th>Error</th>"
					+ "<th>Error type</th></tr>");
				
				ReportPrinter.append	(	"<tr><td>"+ hotel.getAvailability() +"</td>"
						+ 	"<td>"+ hotel.getAvailabilityError() +"</td></tr>");
				ReportPrinter.append("</table>");
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println("1" + hotel.getAvailability());
			System.out.println("2" + hotel.getAvailabilityError());
			try {
				driver.findElement(By.className("proceed_btn")).click();
			} catch (Exception e) {
				// TODO: handle exception
				try {
					driver.findElement(By.xpath(".//*[@id='divConfirm']/a/div")).click();
				} catch (Exception e2) {
					// TODO: handle exception
					try {
						driver.findElement(By.xpath(".//*[@id='returnToPayment']/div")).click();
					} catch (Exception e3) {
						// TODO: handle exception
					}
				}
			}
			
			System.out.println("payment gateway test 1");
			try {
				stream =new FileInputStream(new File("excel/paymentDetails.xls"));
				System.out.println("payment gateway test 2");
				ContentList = newReader.contentReading(stream);
				content   = ContentList.get(0);
				mapiterator = content.entrySet().iterator();		
				Entry = (Map.Entry<Integer, String>)mapiterator.next();
				Content = Entry.getValue();
				String[] payment = Content.split(",");	
				String[] cardNumber  = payment[0].split("/");
				System.out.println("payment gateway test 3");
				driver.switchTo().frame("paygatewayFrame");
				System.out.println("payment gateway test 4");
				WebDriverWait 	wait2 	= new WebDriverWait(driver, 30);
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart2")));
				System.out.println("payment gateway test 5");
				((JavascriptExecutor)driver).executeScript("$('#cardnumberpart1').val('4111');");
				((JavascriptExecutor)driver).executeScript("$('#cardnumberpart2').val('1111');");
				((JavascriptExecutor)driver).executeScript("$('#cardnumberpart3').val('1111');");
				((JavascriptExecutor)driver).executeScript("$('#cardnumberpart4').val('1111');");
				System.out.println("payment gateway test 6");
				//((JavascriptExecutor)driver).executeScript("$('#ho_departure').val('08/10/2014');");

				String[] expDate = payment[1].split("/");
				new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(expDate[0]);
				new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(expDate[1]);
				driver.findElement(By.id("cardholdername")).sendKeys(payment[2]);
				((JavascriptExecutor)driver).executeScript("$('#cv2').val(123);");
				//driver.findElement(By.id("cv2")).sendKeys(payment[3]);
				driver.findElement(By.className("proceed_btn")).click();
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				driver.switchTo().defaultContent();
				System.out.println("test bill -- 1");
				try {
					hotel.setErroType(driver.findElement(By.id("ui-dialog-title-dialog-alert-message-WJ_18")).getText());
					hotel.setError(driver.findElement(By.id("dialog-alert-message-WJ_18")).getText());
				} catch (Exception e) {
					// TODO: handle exception
				}
				System.out.println("test bill -- 2");
				try {
					hotel.setErroType(driver.findElement(By.id("ui-dialog-title-dialog-alert-message-WJ_18")).getText());
					hotel.setError(driver.findElement(By.id("dialog-alert-message-WJ_18")).getText());
				} catch (Exception e) {
					// TODO: handle exception
				}
				System.out.println("test bill -- 3");
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.className("proceed_btn")));
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("test bill -- 4");
					try {
						wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='tripsummary']/div[9]/a/div")));
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
				System.out.println("test bill -- 5");
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[7]/div[3]/div/button")));
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialog-alert-message-WJ_18")));
					hotel.setErroType( driver.findElement(By.id("ui-dialog-title-dialog-alert-message-WJ_18")).getText());
					hotel.setError(driver.findElement(By.id("dialog-alert-message-WJ_18")).getText());
					driver.findElement(By.xpath("html/body/div[7]/div[3]/div/button")).click();
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("test bill -- 6");
					try {
						System.out.println("test bill -- 6.1");
						driver.findElement(By.xpath("html/body/div[7]/div[3]/div")).click();
						System.out.println("test bill -- 6.2");
					} catch (Exception e2) {
						// TODO: handle exception
						System.out.println("test bill -- 7");
						try {
							driver.findElement(By.className("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only")).click();
						} catch (Exception e3) {
							// TODO: handle exception
						}
					}
				}
				System.out.println("test bill -- 8");
				try {
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ui-dialog-title-dialog-error-message-WJ_0")));
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialog-error-message-WJ_0")));
					hotel.setErroType(driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_0")).getText());
					hotel.setError(driver.findElement(By.id("dialog-error-message-WJ_0")).getText());
					driver.findElement(By.xpath("html/body/div[19]/div[11]/div/button")).click();
				} catch (Exception e) {
					// TODO: handle exception
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return hotel;
		}
	
		public hotel paymentRead(WebDriver driver) throws FileNotFoundException
		{
			hotel 		hotel 	= new hotel();
			room		room	= new room();
			payments	pay		= new payments();
			try {
				
				//driver.switchTo().frame("live_message_frame");
				hotel.setHotelName				(driver.findElement(By.id("hotel_name")).getText());
				hotel.setAddress				(driver.findElement(By.id("hotel_address")).getText());
				hotel.setCheckin				(driver.findElement(By.id("checkin_h")).getText());
				hotel.setCheckOut				(driver.findElement(By.id("checkout_h")).getText());
				hotel.setBookingStatus			(driver.findElement(By.id("booktype_h")).getText());
				try {
					hotel.setCancellationDeadLine	(driver.findElement(By.id("duedate_h")).getText());
				} catch (Exception e) {
					// TODO: handle exception
				}
				hotel.setNights					(driver.findElement(By.id("nights_h")).getText());
				hotel.setRooms					(driver.findElement(By.id("rooms_h")).getText());
				
				ArrayList<String> canList = new ArrayList<String>();
				for(int i = 1 ; i > 0 ; i++)
				{
					try {
						canList.add(driver.findElement(By.xpath(".//*[@id='cxlpolicy_h_0']/ul/li["+ i +"]")).getText());
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
				}
				hotel.setCanPolicy(canList);
				
				try {
					String currencyCode 			= 	(driver.findElement(By.id("pkg_depositcur")).getText());
					System.out.println(currencyCode);
					String[] cc						= 	currencyCode.split("\\(");
					String[] cc2					= 	cc[1].split("\\)");
					System.out.println(cc2[0]);
					hotel.setCurrencyCode			(cc2[0]);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				ArrayList<room> 				roomList = new ArrayList<room>();
				for(int i=1 ; i>0 ; i++)
				{
					try {
						room.setRoom			(driver.findElement(By.id("roomdata_" + i)).getText());
						room.setRoomType		(driver.findElement(By.id("roomtype_" + i)).getText());
						room.setBoardType		(driver.findElement(By.id("boardtype_" + i)).getText());
						room.setBedType			(driver.findElement(By.id("bedtype_" + i)).getText());
						room.setTotalRate		(driver.findElement(By.id("rate_" + i)).getText());
						String[] count			= driver.findElement(By.id("roomdata_" + i)).getText().split(":");
						System.out.println(driver.findElement(By.id("roomdata_" + i)).getText());
						System.out.println(count[1]);
						String[] adultCount		= count[1].split(" ");
						System.out.println(adultCount[1]);
						room.setAdultCount		(adultCount[1]);
						String[] childCount		= count[2].trim().split("\\)");
						System.out.println(childCount[0]);
						room.setChildCount		(childCount[0]);
						roomList.add(room);
					} catch (Exception e) {
						break;
					}
				}
				hotel.setRoom(roomList);
				
				pay.setSubTotal							(driver.findElement(By.id("subtotal_h")).getText());
				System.out.println(pay.getSubTotal());
				pay.setTotalHotelBookingValue			(driver.findElement(By.id("totalbookval_h")).getText());
				pay.setTotalTaxAndServiceCharges		(driver.findElement(By.id("tax_h")).getText());
				try {
					pay.setTotalGrossPackageBookingValue	(driver.findElement(By.xpath(".//*[@id='pkg_totalrate']/span")).getText());
				} catch (Exception e) {
					// TODO: handle exception
				}
				pay.setTotalTaxesAndOtherCharges		(driver.findElement(By.id("tottaxamountdivid")).getText());
				pay.setTotalBookingValue				(driver.findElement(By.id("totchargeamt")).getText());
				pay.setAmountbeingProcessedNow			(driver.findElement(By.id("payNwpaymentsection")).getText());
				try {
					pay.setAmountdueatCheckInUtilization	(driver.findElement(By.xpath(".//*[@id='total_transfer_info_table']/tbody/tr[7]/td[2]")).getText());

				} catch (Exception e) {
					// TODO: handle exception
				}
				hotel.setPay(pay);
				finalPayment finalPay			= new finalPayment();
				try {
					finalPay.setTheTotalPackageBookingValue		(driver.findElement(By.id("totchgpgamt")).getText());
					finalPay.setTotalCurrency					(driver.findElement(By.id("totchgpgcurrency")).getText());
					finalPay.setPacckageCurrency				(driver.findElement(By.id("pkg_depositcur")).getText());
					finalPay.setTotalGrossPackageBookingValue	(driver.findElement(By.id("pkg_depositval")).getText());
					finalPay.setTotalTaxesandFees				(driver.findElement(By.id("totaltaxpgcurrid")).getText());
					finalPay.setTotalPackageBookingValue		(driver.findElement(By.id("totalchargeamtpgcurrid")).getText());
					finalPay.setAmountbeingProcessedNow			(driver.findElement(By.id("totalpaynowamountpgcurrid")).getText());
					finalPay.setAmountDueatCheckInUtilization	(driver.findElement(By.id("totalamountatutilizationpgcurrid")).getText());
					hotel.setFinalPay(finalPay);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			
			ExcelReader newReader = new ExcelReader();
			FileInputStream stream =new FileInputStream(new File("excel/customer.xls"));
			
			ArrayList<Map<Integer, String>> ContentList = new ArrayList<Map<Integer,String>>();
			ContentList = newReader.contentReading(stream);
			
			Map<Integer, String>  content = new HashMap<Integer, String>();
			content   = ContentList.get(0);
			
			Iterator<Map.Entry<Integer, String>>   mapiterator = content.entrySet().iterator();
			
			
				Map.Entry<Integer, String> Entry = (Map.Entry<Integer, String>)mapiterator.next();
				String Content = Entry.getValue();
				String[] customerDetails = Content.split(",");
			
			new Select(driver.findElement(By.id("cusTitle"))).selectByVisibleText(customerDetails[0]);
			driver.findElement(By.id("cusFName")).sendKeys(customerDetails[1]);
			String[] tele	= customerDetails[3].split("/");
			driver.findElement(By.id("cusareacodetext")).sendKeys(tele[0]);
			driver.findElement(By.id("cusPhonetext")).sendKeys(tele[1]);
			driver.findElement(By.id("cusEmail")).sendKeys(customerDetails[4]);
			driver.findElement(By.id("cusAdd1")).sendKeys(customerDetails[5]);
			new Select(driver.findElement(By.id("cusCountry"))).selectByVisibleText(customerDetails[6]);
			driver.findElement(By.id("cusCity")).sendKeys(customerDetails[7]);
			driver.findElement(By.id("City_lkup")).click();
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("top_li")));
			driver.findElement(By.className("top_li")).click();
			driver.findElement(By.id("cusLName")).sendKeys(customerDetails[2]);
			driver.findElement(By.id("cusZip")).sendKeys(customerDetails[8]);
			
			
			
			stream 			=new FileInputStream(new File("excel/searchDetails.xls"));
			ContentList 	= newReader.contentReading(stream);
			content   		= ContentList.get(0);
			mapiterator 	= content.entrySet().iterator();
			Entry 			= (Map.Entry<Integer, String>)mapiterator.next();
			Content 		= Entry.getValue();
			String[] roomOccupancy = Content.split(",");
			
			System.out.println(roomOccupancy[5]);
			String[] roomCount	= roomOccupancy[5].split("/");
			String[] roomCount2 = roomOccupancy[6].split("/");
			
			stream 			=new FileInputStream(new File("excel/occupancy.xls"));
			ContentList 	= newReader.contentReading(stream);
			content   		= ContentList.get(0);
			mapiterator 	= content.entrySet().iterator();
			for(int i = 0 ; i < roomCount.length ; i++)
			{
				for(int j = 0 ; j < Integer.parseInt(roomCount[i]) ; j++)
				{
					try {
						Entry = (Map.Entry<Integer, String>)mapiterator.next();
						Content = Entry.getValue();
						String[] occupancy = Content.split(",");
						driver.findElement(By.id("adult_fname_0_"+i+"_" + j)).sendKeys(occupancy[0]);
						driver.findElement(By.id("adult_lname_0_"+i+"_" + j)).sendKeys(occupancy[1]);
						if(occupancy[2].equals("yes"))
							driver.findElement(By.id("adult_smoking_y_0_"+i+"_" + j)).click();
						else
							driver.findElement(By.id("adult_smoking_n_0_"+i+"_" + j)).click();
						if(occupancy[3].equals("yes"))
							driver.findElement(By.id("adult_handycap_0_"+i+"_" + j)).click();
						if(occupancy[4].equals("yes"))
							driver.findElement(By.id("adult_weelchair_0_"+i+"_" + j)).click();
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
			}
			
			stream =new FileInputStream(new File("excel/occupancy.xls"));
			ContentList = newReader.contentReading(stream);
			content   = ContentList.get(0);
			mapiterator = content.entrySet().iterator();
			//-------------------------------------------offline payment---------------------------------------------------
			//driver.findElement(By.id("pay_offline")).click();	
			for(int i = 0 ; i < roomCount2.length ; i++)
			{
				for(int j = 0 ; j < Integer.parseInt(roomCount2[j]) ; j++)
				{
					System.out.println("child_fname_0_"+ i +"_" + j);
					try {
						Entry = (Map.Entry<Integer, String>)mapiterator.next();
						Content = Entry.getValue();
						String[] occupancy = Content.split(",");
						System.out.println("j :" + j + " i :" + i );
						driver.findElement(By.id("child_fname_0_"+ i +"_" + j)).sendKeys(occupancy[5]);
						driver.findElement(By.id("child_lname_0_"+ i +"_" + j)).sendKeys(occupancy[6]);
						if(occupancy[8].equals("yes"))
							driver.findElement(By.id("child_handycap_0_"+i+"_" + j)).click();
						if(occupancy[9].equals("yes"))
							driver.findElement(By.id("child_weelchair_0_"+i+"_" + j)).click();
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
				
			}
			WebDriverWait wait2 = new WebDriverWait(driver, 30);
			driver.findElement(By.id("cancellation")).click();
			driver.findElement(By.id("continue_submit")).click();
			
			String paymentError = "";
			String paymentErrorType = "";
			String day = "";
			String[] date = hotel.getCheckin().split(" ");
			day = date[2].concat("-").concat(date[1]).concat("-").concat(date[0]);
			
			try {
				System.out.println("confimation test -- 3");
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.className("proceed_btn")));
				System.out.println("confimation test -- 3.1");
				driver.findElement(By.className("proceed_btn")).click();
				System.out.println("confimation test -- 3.2");
			} catch (Exception e) {
				// TODO: handle exception
				try {
					System.out.println("confimation test -- 3.3");
					wait2.until(ExpectedConditions.presenceOfElementLocated(By.className("proceed_btn")));
					driver.findElement(By.className("proceed_btn")).click();
					System.out.println("confimation test -- 3.4");
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
			try {
				System.out.println("confimation test -- 1");
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("message_H_175391-57@@"+ day +"@@hotelbeds_v2")));
				hotel.setPaymentError(driver.findElement(By.id("message_H_175391-57@@"+ day +"@@hotelbeds_v2")).getText());
				System.out.println(hotel.getPaymentError());
				try {
					System.out.println("confimation test -- 2");
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='returnToPayment']/div")));
					driver.findElement(By.xpath(".//*[@id='returnToPayment']/div")).click();
				} catch (Exception e) {
					// TODO: handle exception
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				if(hotel.getPaymentError().equals(""))
				{
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("message_H_38699-25@@" + day + "@@hotelbeds_v2")));
					hotel.setPaymentError(driver.findElement(By.id("message_H_38699-25@@" + day + "@@hotelbeds_v2")).getText());
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				if(hotel.getPaymentError().equals(""))
				{
					//hotel.setPaymentErrorType = driver.findElement(By.id("")).getText();
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialog-error-message-WJ_23")));
					hotel.setPaymentError(driver.findElement(By.id("dialog-error-message-WJ_23")).getText());
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				System.out.println("confimation test -- 4");
				stream =new FileInputStream(new File("excel/paymentDetails.xls"));
				ContentList = newReader.contentReading(stream);
				content   = ContentList.get(0);
				mapiterator = content.entrySet().iterator();		
				Entry = (Map.Entry<Integer, String>)mapiterator.next();
				Content = Entry.getValue();
				String[] payment = Content.split(",");	
				String[] cardNumber  = payment[0].split("/");
				driver.switchTo().frame("paygatewayFrame");
				System.out.println("confimation test -- 5");
				//WebDriverWait wait2 = new WebDriverWait(driver, 30);
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart1")));
				wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("cardnumberpart2")));
				((JavascriptExecutor)driver).executeScript("$('#cardnumberpart1').val('4111');");
				((JavascriptExecutor)driver).executeScript("$('#cardnumberpart2').val('1111');");
				((JavascriptExecutor)driver).executeScript("$('#cardnumberpart3').val('1111');");
				((JavascriptExecutor)driver).executeScript("$('#cardnumberpart4').val('1111');");
				//((JavascriptExecutor)driver).executeScript("$('#ho_departure').val('08/10/2014');");

				String[] expDate = payment[1].split("/");
				new Select(driver.findElement(By.id("cardexpmonth"))).selectByVisibleText(expDate[0]);
				new Select(driver.findElement(By.id("cardexpyear"))).selectByVisibleText(expDate[1]);
				driver.findElement(By.id("cardholdername")).sendKeys(payment[2]);
				((JavascriptExecutor)driver).executeScript("$('#cv2').val(123);");
				//driver.findElement(By.id("cv2")).sendKeys(payment[3]);
				driver.findElement(By.className("proceed_btn")).click();
				driver.switchTo().defaultContent();
				driver.switchTo().frame("live_message_frame");
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("p_hotel_bookings_1")));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			
			return hotel;
			
		}
		
		public String formatDate(String date)
		{
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date d = null;
			try {
				d = sdf.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sdf.applyPattern("yyyy-MMM-dd");
			String newDateString = sdf.format(d);
			return newDateString;
		}
		
}
