package readAndWrite;

import java.util.ArrayList;

public class bookingcard {
	
	String reservationNumber ="";
	String currencyCode ="";
	String hCurrencyCode =""; 
	String origin =""; 
	String destination =""; 
	String productsBooked =""; 
	String bookingStatus =""; 
	String reservationDate =""; 
	String firstElement =""; 
	String cancellationDeadline =""; 
	String subTotal =""; 
	String bookingFee =""; 
	String taxAndOther =""; 
	String creditCardFee =""; 
	String totalBookingvalue =""; 
	String payableUpfront =""; 
	String payableCheckout =""; 
	String utilization =""; 
	String amountPaid =""; 
	String firstName =""; 
	String address =""; 
	String email =""; 
	String city =""; 
	String country =""; 
	String emergencyContact =""; 
	String phoneNumber =""; 
	String hotelName =""; 
	String hotelAddress =""; 
	String checkin =""; 
	String checkOut =""; 
	String supplier =""; 
	String hotelBookingStatus =""; 
	String numOfNyts =""; 
	String noOfRooms =""; 
	String hotelContractType =""; 
	String hsubTotal =""; 
	String hTax =""; 
	String htotalBookingValue =""; 
	String hUpfront =""; 
	String hAmountpaid =""; 
	String hDueAtCheckin ="";

	ArrayList<bookingReportRoom> room = new ArrayList<bookingReportRoom>();
	
	
	public String gethCurrencyCode() {
		return hCurrencyCode;
	}

	public void sethCurrencyCode(String hCurrencyCode) {
		this.hCurrencyCode = hCurrencyCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public ArrayList<bookingReportRoom> getRoom() {
		return room;
	}

	public void setRoom(ArrayList<bookingReportRoom> room) {
		this.room = room;
	}

	public String getReservationNumber() {
		return reservationNumber;
	}

	public void setReservationNumber(String reservationNumber) {
		this.reservationNumber = reservationNumber;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getProductsBooked() {
		return productsBooked;
	}

	public void setProductsBooked(String productsBooked) {
		this.productsBooked = productsBooked;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(String reservationDate) {
		this.reservationDate = reservationDate;
	}

	public String getFirstElement() {
		return firstElement;
	}

	public void setFirstElement(String firstElement) {
		this.firstElement = firstElement;
	}

	public String getCancellationDeadline() {
		return cancellationDeadline;
	}

	public void setCancellationDeadline(String cancellationDeadline) {
		this.cancellationDeadline = cancellationDeadline;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public String getBookingFee() {
		return bookingFee;
	}

	public void setBookingFee(String bookingFee) {
		this.bookingFee = bookingFee;
	}

	public String getTaxAndOther() {
		return taxAndOther;
	}

	public void setTaxAndOther(String taxAndOther) {
		this.taxAndOther = taxAndOther;
	}

	public String getCreditCardFee() {
		return creditCardFee;
	}

	public void setCreditCardFee(String creditCardFee) {
		this.creditCardFee = creditCardFee;
	}

	public String getTotalBookingvalue() {
		return totalBookingvalue;
	}

	public void setTotalBookingvalue(String totalBookingvalue) {
		this.totalBookingvalue = totalBookingvalue;
	}

	public String getPayableUpfront() {
		return payableUpfront;
	}

	public void setPayableUpfront(String payableUpfront) {
		this.payableUpfront = payableUpfront;
	}

	public String getPayableCheckout() {
		return payableCheckout;
	}

	public void setPayableCheckout(String payableCheckout) {
		this.payableCheckout = payableCheckout;
	}

	public String getUtilization() {
		return utilization;
	}

	public void setUtilization(String utilization) {
		this.utilization = utilization;
	}

	public String getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(String amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelAddress() {
		return hotelAddress;
	}

	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getHotelBookingStatus() {
		return hotelBookingStatus;
	}

	public void setHotelBookingStatus(String hotelBookingStatus) {
		this.hotelBookingStatus = hotelBookingStatus;
	}

	public String getNumOfNyts() {
		return numOfNyts;
	}

	public void setNumOfNyts(String numOfNyts) {
		this.numOfNyts = numOfNyts;
	}

	public String getNoOfRooms() {
		return noOfRooms;
	}

	public void setNoOfRooms(String noOfRooms) {
		this.noOfRooms = noOfRooms;
	}

	public String getHotelContractType() {
		return hotelContractType;
	}

	public void setHotelContractType(String hotelContractType) {
		this.hotelContractType = hotelContractType;
	}

	public String getHsubTotal() {
		return hsubTotal;
	}

	public void setHsubTotal(String hsubTotal) {
		this.hsubTotal = hsubTotal;
	}

	public String gethTax() {
		return hTax;
	}

	public void sethTax(String hTax) {
		this.hTax = hTax;
	}

	public String getHtotalBookingValue() {
		return htotalBookingValue;
	}

	public void setHtotalBookingValue(String htotalBookingValue) {
		this.htotalBookingValue = htotalBookingValue;
	}

	public String gethUpfront() {
		return hUpfront;
	}

	public void sethUpfront(String hUpfront) {
		this.hUpfront = hUpfront;
	}

	public String gethAmountpaid() {
		return hAmountpaid;
	}

	public void sethAmountpaid(String hAmountpaid) {
		this.hAmountpaid = hAmountpaid;
	}

	public String gethDueAtCheckin() {
		return hDueAtCheckin;
	}

	public void sethDueAtCheckin(String hDueAtCheckin) {
		this.hDueAtCheckin = hDueAtCheckin;
	}
	
	

}
