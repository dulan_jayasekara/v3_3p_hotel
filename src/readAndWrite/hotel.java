package readAndWrite;

import java.util.ArrayList;

public class hotel {

	String notification =""; 
	String notificationError =""; 
	String pgType =""; 
	String pgError ="";
	String hotelName =""; 
	String availabilityError =""; 
	String availability =""; 
	String address =""; 
	String bookingStatus =""; 
	String cancellationDeadLine =""; 
	String checkin =""; 
	String checkOut =""; 
	String nights =""; 
	String rooms =""; 
	String currencyCode ="";
	String error = "";
	String erroType = "";
	String paymentError = "";
	
	
	payments pay 					= new payments();
	ArrayList<room>	room			= new ArrayList<room>();
	finalPayment finalPay			= new finalPayment();
	ArrayList<String> canPolicy 	= new ArrayList<String>();
	
	
	public ArrayList<String> getCanPolicy() {
		return canPolicy;
	}

	public void setCanPolicy(ArrayList<String> canPolicy) {
		this.canPolicy = canPolicy;
	}

	public String getPaymentError() {
		return paymentError;
	}

	public void setPaymentError(String paymentError) {
		this.paymentError = paymentError;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErroType() {
		return erroType;
	}

	public void setErroType(String erroType) {
		this.erroType = erroType;
	}

	/**
	 * @return the notification
	 */
	public String getNotification() {
		return notification;
	}

	/**
	 * @param notification the notification to set
	 */
	public void setNotification(String notification) {
		this.notification = notification;
	}

	/**
	 * @return the notificationError
	 */
	public String getNotificationError() {
		return notificationError;
	}

	/**
	 * @param notificationError the notificationError to set
	 */
	public void setNotificationError(String notificationError) {
		this.notificationError = notificationError;
	}

	/**
	 * @return the pgType
	 */
	public String getPgType() {
		return pgType;
	}

	/**
	 * @param pgType the pgType to set
	 */
	public void setPgType(String pgType) {
		this.pgType = pgType;
	}

	/**
	 * @return the pgError
	 */
	public String getPgError() {
		return pgError;
	}

	/**
	 * @param pgError the pgError to set
	 */
	public void setPgError(String pgError) {
		this.pgError = pgError;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @return the availabilityError
	 */
	public String getAvailabilityError() {
		return availabilityError;
	}

	/**
	 * @param availabilityError the availabilityError to set
	 */
	public void setAvailabilityError(String availabilityError) {
		this.availabilityError = availabilityError;
	}

	/**
	 * @return the availability
	 */
	public String getAvailability() {
		return availability;
	}

	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(String availability) {
		this.availability = availability;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public finalPayment getFinalPay() {
		return finalPay;
	}

	public void setFinalPay(finalPayment finalPay) {
		this.finalPay = finalPay;
	}

	public payments getPay() {
		return pay;
	}

	public void setPay(payments pay) {
		this.pay = pay;
	}

	public ArrayList<room> getRoom() {
		return room;
	}

	public void setRoom(ArrayList<room> room) {
		this.room = room;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getCancellationDeadLine() {
		return cancellationDeadLine;
	}

	public void setCancellationDeadLine(String cancellationDeadLine) {
		this.cancellationDeadLine = cancellationDeadLine;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getNights() {
		return nights;
	}

	public void setNights(String nights) {
		this.nights = nights;
	}

	public String getRooms() {
		return rooms;
	}

	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	
	
}
