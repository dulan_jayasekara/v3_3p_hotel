package readAndWrite;

public class readRoomResults {

	String roomInfo =""; 
	String roomType =""; 
	String bed =""; 
	String ratePlan =""; 
	String rate ="";

	/**
	 * @return the roomInfo
	 */
	public String getRoomInfo() {
		return roomInfo;
	}

	/**
	 * @param roomInfo the roomInfo to set
	 */
	public void setRoomInfo(String roomInfo) {
		this.roomInfo = roomInfo;
	}

	/**
	 * @return the roomType
	 */
	public String getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return the bed
	 */
	public String getBed() {
		return bed;
	}

	/**
	 * @param bed the bed to set
	 */
	public void setBed(String bed) {
		this.bed = bed;
	}

	/**
	 * @return the ratePlan
	 */
	public String getRatePlan() {
		return ratePlan;
	}

	/**
	 * @param ratePlan the ratePlan to set
	 */
	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}

	/**
	 * @return the rate
	 */
	public String getRate() {
		return rate;
	}

	/**
	 * @param rate the rate to set
	 */
	public void setRate(String rate) {
		this.rate = rate;
	}
	
}
