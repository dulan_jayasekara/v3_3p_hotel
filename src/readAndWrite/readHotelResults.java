package readAndWrite;

import java.util.ArrayList;

public class readHotelResults {

	String title =""; 
	String address =""; 
	String availability =""; 
	String totalCost =""; 
	String supplier ="";
	ArrayList<readRoomResults> room		=  new ArrayList<readRoomResults>();
	ArrayList<readHotelResults> hotel	= new ArrayList<readHotelResults>();
	/**
	 * @return the title
	 */
	
	public String getTitle() {
		return title;
	}
	/**
	 * @return the supplier
	 */
	public String getSupplier() {
		return supplier;
	}
	/**
	 * @param supplier the supplier to set
	 */
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the availability
	 */
	public String getAvailability() {
		return availability;
	}
	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	/**
	 * @return the totalCost
	 */
	public String getTotalCost() {
		return totalCost;
	}
	/**
	 * @param totalCost the totalCost to set
	 */
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	/**
	 * @return the room
	 */
	public ArrayList<readRoomResults> getRoom() {
		return room;
	}
	/**
	 * @param room the room to set
	 */
	public void setRoom(ArrayList<readRoomResults> room) {
		this.room = room;
	}
	/**
	 * @return the hotel
	 */
	public ArrayList<readHotelResults> getHotel() {
		return hotel;
	}
	/**
	 * @param hotel the hotel to set
	 */
	public void setHotel(ArrayList<readHotelResults> hotel) {
		this.hotel = hotel;
	}
	
	
}
