package readAndWrite;

import java.util.ArrayList;

public class cancellationDetails {

	String bookingNumber = ""; 
	String bookingDate = ""; 
	String toName = ""; 
	String bookingChanel = ""; 
	String subTotal = ""; 
	String totalPayable = ""; 
	String hotelName = ""; 
	String address = ""; 
	String checkin = ""; 
	String checkout = ""; 
	String adults = ""; 
	String city = ""; 
	String nytsNum = ""; 
	String roomsNum = ""; 
	String children = ""; 
	String hotelCancellationCharge = ""; 
	String customerCancellationCharge = ""; 
	String hotelCancellationNumber = ""; 
	String currency = ""; 
	String totalHotelCancellationCharge = ""; 
	String totalCustomerCancellationCharge = ""; 
	String additionalCancellationCharge = ""; 
	String gstCharge = ""; 
	String totalCharge = "";
	String msgType = "";
	String msg = "";
	String lastname = "";
	
	
	ArrayList<cancellationRoomOccupancy> roomOccupancy = new ArrayList<cancellationRoomOccupancy>();
	ArrayList<cancelltionRateDetails> 	rateDetails = new ArrayList<cancelltionRateDetails>();
	
	
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public ArrayList<cancellationRoomOccupancy> getRoomOccupancy() {
		return roomOccupancy;
	}
	public void setRoomOccupancy(ArrayList<cancellationRoomOccupancy> roomOccupancy) {
		this.roomOccupancy = roomOccupancy;
	}
	public ArrayList<cancelltionRateDetails> getRateDetails() {
		return rateDetails;
	}
	public void setRateDetails(ArrayList<cancelltionRateDetails> rateDetails) {
		this.rateDetails = rateDetails;
	}
	public String getBookingNumber() {
		return bookingNumber;
	}
	public void setBookingNumber(String bookingNumber) {
		this.bookingNumber = bookingNumber;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getToName() {
		return toName;
	}
	public void setToName(String toName) {
		this.toName = toName;
	}
	
	public String getBookingChanel() {
		return bookingChanel;
	}
	public void setBookingChanel(String bookingChanel) {
		this.bookingChanel = bookingChanel;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getTotalPayable() {
		return totalPayable;
	}
	public void setTotalPayable(String totalPayable) {
		this.totalPayable = totalPayable;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getNytsNum() {
		return nytsNum;
	}
	public void setNytsNum(String nytsNum) {
		this.nytsNum = nytsNum;
	}
	public String getRoomsNum() {
		return roomsNum;
	}
	public void setRoomsNum(String roomsNum) {
		this.roomsNum = roomsNum;
	}
	public String getChildren() {
		return children;
	}
	public void setChildren(String children) {
		this.children = children;
	}
	public String getHotelCancellationCharge() {
		return hotelCancellationCharge;
	}
	public void setHotelCancellationCharge(String hotelCancellationCharge) {
		this.hotelCancellationCharge = hotelCancellationCharge;
	}
	public String getCustomerCancellationCharge() {
		return customerCancellationCharge;
	}
	public void setCustomerCancellationCharge(String customerCancellationCharge) {
		this.customerCancellationCharge = customerCancellationCharge;
	}
	public String getHotelCancellationNumber() {
		return hotelCancellationNumber;
	}
	public void setHotelCancellationNumber(String hotelCancellationNumber) {
		this.hotelCancellationNumber = hotelCancellationNumber;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTotalHotelCancellationCharge() {
		return totalHotelCancellationCharge;
	}
	public void setTotalHotelCancellationCharge(String totalHotelCancellationCharge) {
		this.totalHotelCancellationCharge = totalHotelCancellationCharge;
	}
	public String getTotalCustomerCancellationCharge() {
		return totalCustomerCancellationCharge;
	}
	public void setTotalCustomerCancellationCharge(
			String totalCustomerCancellationCharge) {
		this.totalCustomerCancellationCharge = totalCustomerCancellationCharge;
	}
	public String getAdditionalCancellationCharge() {
		return additionalCancellationCharge;
	}
	public void setAdditionalCancellationCharge(String additionalCancellationCharge) {
		this.additionalCancellationCharge = additionalCancellationCharge;
	}
	public String getGstCharge() {
		return gstCharge;
	}
	public void setGstCharge(String gstCharge) {
		this.gstCharge = gstCharge;
	}
	public String getTotalCharge() {
		return totalCharge;
	}
	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}
	
	
	
	
}
