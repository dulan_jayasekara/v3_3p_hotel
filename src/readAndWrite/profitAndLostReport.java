package readAndWrite;

public class profitAndLostReport {

	String bookingNumber; 
	String bookingDate; 
	String consultantName; 
	String combination; 
	String commissionTemplate; 
	String contractType; 
	String bookingType; 
	String agentName; 
	String bookingChannel; 
	String poslocation; 
	String state; 
	String noOfGuests; 
	String chechIn; 
	String numOfNyts; 
	String baseCurrency; 
	String grossBookingValue; 
	String agentCommission; 
	String netBookingValues; 
	String costOfSales; 
	String profit; 
	String profitPercentage; 
	String invoiceTotal; 
	String invoicrpaid; 
	String invoiceBalanceDue;
	public String getBookingNumber() {
		return bookingNumber;
	}
	public void setBookingNumber(String bookingNumber) {
		this.bookingNumber = bookingNumber;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getConsultantName() {
		return consultantName;
	}
	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}
	public String getCombination() {
		return combination;
	}
	public void setCombination(String combination) {
		this.combination = combination;
	}
	public String getCommissionTemplate() {
		return commissionTemplate;
	}
	public void setCommissionTemplate(String commissionTemplate) {
		this.commissionTemplate = commissionTemplate;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getBookingType() {
		return bookingType;
	}
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getBookingChannel() {
		return bookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}
	public String getPoslocation() {
		return poslocation;
	}
	public void setPoslocation(String poslocation) {
		this.poslocation = poslocation;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getNoOfGuests() {
		return noOfGuests;
	}
	public void setNoOfGuests(String noOfGuests) {
		this.noOfGuests = noOfGuests;
	}
	public String getChechIn() {
		return chechIn;
	}
	public void setChechIn(String chechIn) {
		this.chechIn = chechIn;
	}
	public String getNumOfNyts() {
		return numOfNyts;
	}
	public void setNumOfNyts(String numOfNyts) {
		this.numOfNyts = numOfNyts;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public String getGrossBookingValue() {
		return grossBookingValue;
	}
	public void setGrossBookingValue(String grossBookingValue) {
		this.grossBookingValue = grossBookingValue;
	}
	public String getAgentCommission() {
		return agentCommission;
	}
	public void setAgentCommission(String agentCommission) {
		this.agentCommission = agentCommission;
	}
	public String getNetBookingValues() {
		return netBookingValues;
	}
	public void setNetBookingValues(String netBookingValues) {
		this.netBookingValues = netBookingValues;
	}
	public String getCostOfSales() {
		return costOfSales;
	}
	public void setCostOfSales(String costOfSales) {
		this.costOfSales = costOfSales;
	}
	public String getProfit() {
		return profit;
	}
	public void setProfit(String profit) {
		this.profit = profit;
	}
	public String getProfitPercentage() {
		return profitPercentage;
	}
	public void setProfitPercentage(String profitPercentage) {
		this.profitPercentage = profitPercentage;
	}
	public String getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(String invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public String getInvoicrpaid() {
		return invoicrpaid;
	}
	public void setInvoicrpaid(String invoicrpaid) {
		this.invoicrpaid = invoicrpaid;
	}
	public String getInvoiceBalanceDue() {
		return invoiceBalanceDue;
	}
	public void setInvoiceBalanceDue(String invoiceBalanceDue) {
		this.invoiceBalanceDue = invoiceBalanceDue;
	}
	
	
	
}
