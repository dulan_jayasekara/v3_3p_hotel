package readAndWrite;

import java.util.ArrayList;

public class searchRoomDetails {

	String room =""; 
	String adultCount =""; 
	String childCount =""; 
	String age ="";
	
	

	/**
	 * @return the room
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * @param room the room to set
	 */
	public void setRoom(String room) {
		this.room = room;
	}

	/**
	 * @return the adultCount
	 */
	public String getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount the adultCount to set
	 */
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}

	/**
	 * @return the childCount
	 */
	public String getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount the childCount to set
	 */
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}

	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}

	/**
	 * @return the age
	 */
	
	
	
}
