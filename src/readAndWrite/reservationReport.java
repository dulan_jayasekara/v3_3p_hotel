package readAndWrite;

public class reservationReport {

	String bookingNumber =""; 
	String appCode =""; 
	String bookingDate =""; 
	String hotelConfirmationNumber =""; 
	String transId =""; 
	String supplierName =""; 
	String hotelName =""; 
	String bookingType =""; 
	String channel =""; 
	String firtName =""; 
	String lastName =""; 
	String customerType =""; 
	String checkIn =""; 
	String checkOut =""; 
	String roomType =""; 
	String bedType =""; 
	String ratePlan =""; 
	String hotelContactNumber =""; 
	String currency =""; 
	String totalCost =""; 
	String bookingFee ="";  
	String orderValue =""; 
	String numOfRooms =""; 
	String numOfNyts =""; 
	String totalCostFinal =""; 
	String orderValueFinal =""; 
	String pageTotal =""; 
	String grandTotal ="";

	public String getBookingNumber() {
		return bookingNumber;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getTransId() {
		return transId;
	}

	public String getHotelConfirmationNumber() {
		return hotelConfirmationNumber;
	}

	public void setHotelConfirmationNumber(String hotelConfirmationNumber) {
		this.hotelConfirmationNumber = hotelConfirmationNumber;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public void setBookingNumber(String bookingNumber) {
		this.bookingNumber = bookingNumber;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	

	public String getFirtName() {
		return firtName;
	}

	public void setFirtName(String firtName) {
		this.firtName = firtName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getBedType() {
		return bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	public String getRatePlan() {
		return ratePlan;
	}

	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}

	public String getHotelContactNumber() {
		return hotelContactNumber;
	}

	public void setHotelContactNumber(String hotelContactNumber) {
		this.hotelContactNumber = hotelContactNumber;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}

	public String getBookingFee() {
		return bookingFee;
	}

	public void setBookingFee(String bookingFee) {
		this.bookingFee = bookingFee;
	}

	public String getOrderValue() {
		return orderValue;
	}

	public void setOrderValue(String orderValue) {
		this.orderValue = orderValue;
	}

	public String getNumOfRooms() {
		return numOfRooms;
	}

	public void setNumOfRooms(String numOfRooms) {
		this.numOfRooms = numOfRooms;
	}

	public String getNumOfNyts() {
		return numOfNyts;
	}

	public void setNumOfNyts(String numOfNyts) {
		this.numOfNyts = numOfNyts;
	}

	public String getTotalCostFinal() {
		return totalCostFinal;
	}

	public void setTotalCostFinal(String totalCostFinal) {
		this.totalCostFinal = totalCostFinal;
	}

	public String getOrderValueFinal() {
		return orderValueFinal;
	}

	public void setOrderValueFinal(String orderValueFinal) {
		this.orderValueFinal = orderValueFinal;
	}

	public String getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(String pageTotal) {
		this.pageTotal = pageTotal;
	}

	public String getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}

	
}
