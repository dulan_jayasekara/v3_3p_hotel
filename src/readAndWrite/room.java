package readAndWrite;

public class room {

	String room =""; 
	String roomType =""; 
	String boardType =""; 
	String bedType =""; 
	String totalRate =""; 
	String adultCount =""; 
	String childCount ="";

	
	/**
	 * @return the adultCount
	 */
	public String getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount the adultCount to set
	 */
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}

	/**
	 * @return the childCount
	 */
	public String getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount the childCount to set
	 */
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getBoardType() {
		return boardType;
	}

	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}

	public String getBedType() {
		return bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	public String getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(String totalRate) {
		this.totalRate = totalRate;
	}
	
}
