package readAndWrite;

public class thirdPartySupplierReport {

	String bookingnumber = ""; 
	String document = ""; 
	String bookingdate = ""; 
	String bookingChannel = ""; 
	String productType = ""; 
	String productName = ""; 
	String serviceElementDate = ""; 
	String supplierName = ""; 
	String bookingStatus = ""; 
	String guestName = ""; 
	String portalCurrency = ""; 
	String payableAmount = ""; 
	String totalPaid = ""; 
	String balanceDue = ""; 
	String supCurrency = ""; 
	String fpayableAmount = ""; 
	String ftotalpaid = ""; 
	String fblanceDue = "";

	public String getBookingnumber() {
		return bookingnumber;
	}

	public void setBookingnumber(String bookingnumber) {
		this.bookingnumber = bookingnumber;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getBookingdate() {
		return bookingdate;
	}

	public void setBookingdate(String bookingdate) {
		this.bookingdate = bookingdate;
	}

	public String getBookingChannel() {
		return bookingChannel;
	}

	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getServiceElementDate() {
		return serviceElementDate;
	}

	public void setServiceElementDate(String serviceElementDate) {
		this.serviceElementDate = serviceElementDate;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getPortalCurrency() {
		return portalCurrency;
	}

	public void setPortalCurrency(String portalCurrency) {
		this.portalCurrency = portalCurrency;
	}

	public String getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}

	public String getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(String totalPaid) {
		this.totalPaid = totalPaid;
	}

	public String getBalanceDue() {
		return balanceDue;
	}

	public void setBalanceDue(String balanceDue) {
		this.balanceDue = balanceDue;
	}

	public String getSupCurrency() {
		return supCurrency;
	}

	public void setSupCurrency(String supCurrency) {
		this.supCurrency = supCurrency;
	}

	public String getFpayableAmount() {
		return fpayableAmount;
	}

	public void setFpayableAmount(String fpayableAmount) {
		this.fpayableAmount = fpayableAmount;
	}

	public String getFtotalpaid() {
		return ftotalpaid;
	}

	public void setFtotalpaid(String ftotalpaid) {
		this.ftotalpaid = ftotalpaid;
	}

	public String getFblanceDue() {
		return fblanceDue;
	}

	public void setFblanceDue(String fblanceDue) {
		this.fblanceDue = fblanceDue;
	}
	
	
}
