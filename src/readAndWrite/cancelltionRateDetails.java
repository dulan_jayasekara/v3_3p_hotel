package readAndWrite;

public class cancelltionRateDetails {

	
	String roomType = ""; 
	String bedType = ""; 
	String boardType = ""; 
	String adults = ""; 
	String children = ""; 
	String roomRate = ""; 
	String supRate = ""; 
	String total = "";
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getBedType() {
		return bedType;
	}
	public void setBedType(String bedType) {
		this.bedType = bedType;
	}
	public String getBoardType() {
		return boardType;
	}
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChildren() {
		return children;
	}
	public void setChildren(String children) {
		this.children = children;
	}
	public String getRoomRate() {
		return roomRate;
	}
	public void setRoomRate(String roomRate) {
		this.roomRate = roomRate;
	}
	public String getSupRate() {
		return supRate;
	}
	public void setSupRate(String supRate) {
		this.supRate = supRate;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
	
	
}
