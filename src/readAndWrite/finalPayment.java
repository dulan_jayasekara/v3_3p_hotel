package readAndWrite;

public class finalPayment {

	String totalCurrency =""; 
	String pacckageCurrency =""; 
	String theTotalPackageBookingValue =""; 
	String totalGrossPackageBookingValue =""; 
	String totalTaxesandFees =""; 
	String totalPackageBookingValue =""; 
	String amountbeingProcessedNow =""; 
	String amountDueatCheckInUtilization ="";

	public String getTotalCurrency() {
		return totalCurrency;
	}

	public void setTotalCurrency(String totalCurrency) {
		this.totalCurrency = totalCurrency;
	}

	public String getPacckageCurrency() {
		return pacckageCurrency;
	}

	public void setPacckageCurrency(String pacckageCurrency) {
		this.pacckageCurrency = pacckageCurrency;
	}

	public String getTheTotalPackageBookingValue() {
		return theTotalPackageBookingValue;
	}

	public void setTheTotalPackageBookingValue(String theTotalPackageBookingValue) {
		this.theTotalPackageBookingValue = theTotalPackageBookingValue;
	}

	public String getTotalGrossPackageBookingValue() {
		return totalGrossPackageBookingValue;
	}

	public void setTotalGrossPackageBookingValue(
			String totalGrossPackageBookingValue) {
		this.totalGrossPackageBookingValue = totalGrossPackageBookingValue;
	}

	public String getTotalTaxesandFees() {
		return totalTaxesandFees;
	}

	public void setTotalTaxesandFees(String totalTaxesandFees) {
		this.totalTaxesandFees = totalTaxesandFees;
	}

	public String getTotalPackageBookingValue() {
		return totalPackageBookingValue;
	}

	public void setTotalPackageBookingValue(String totalPackageBookingValue) {
		this.totalPackageBookingValue = totalPackageBookingValue;
	}

	public String getAmountbeingProcessedNow() {
		return amountbeingProcessedNow;
	}

	public void setAmountbeingProcessedNow(String amountbeingProcessedNow) {
		this.amountbeingProcessedNow = amountbeingProcessedNow;
	}

	public String getAmountDueatCheckInUtilization() {
		return amountDueatCheckInUtilization;
	}

	public void setAmountDueatCheckInUtilization(
			String amountDueatCheckInUtilization) {
		this.amountDueatCheckInUtilization = amountDueatCheckInUtilization;
	}
	
	
}
