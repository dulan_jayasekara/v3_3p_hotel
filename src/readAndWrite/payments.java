package readAndWrite;

public class payments {

	String subTotal =""; 
	String totalTaxAndServiceCharges =""; 
	String totalHotelBookingValue =""; 
	String totalGrossPackageBookingValue =""; 
	String totalTaxesAndOtherCharges =""; 
	String totalBookingValue =""; 
	String amountbeingProcessedNow =""; 
	String amountdueatCheckInUtilization ="";

	
	
	public String getTotalGrossPackageBookingValue() {
		return totalGrossPackageBookingValue;
	}

	public void setTotalGrossPackageBookingValue(
			String totalGrossPackageBookingValue) {
		this.totalGrossPackageBookingValue = totalGrossPackageBookingValue;
	}

	public String getTotalTaxesAndOtherCharges() {
		return totalTaxesAndOtherCharges;
	}

	public void setTotalTaxesAndOtherCharges(String totalTaxesAndOtherCharges) {
		this.totalTaxesAndOtherCharges = totalTaxesAndOtherCharges;
	}

	public String getTotalBookingValue() {
		return totalBookingValue;
	}

	public void setTotalBookingValue(String totalBookingValue) {
		this.totalBookingValue = totalBookingValue;
	}

	public String getAmountbeingProcessedNow() {
		return amountbeingProcessedNow;
	}

	public void setAmountbeingProcessedNow(String amountbeingProcessedNow) {
		this.amountbeingProcessedNow = amountbeingProcessedNow;
	}

	public String getAmountdueatCheckInUtilization() {
		return amountdueatCheckInUtilization;
	}

	public void setAmountdueatCheckInUtilization(
			String amountdueatCheckInUtilization) {
		this.amountdueatCheckInUtilization = amountdueatCheckInUtilization;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public String getTotalTaxAndServiceCharges() {
		return totalTaxAndServiceCharges;
	}

	public void setTotalTaxAndServiceCharges(String totalTaxAndServiceCharges) {
		this.totalTaxAndServiceCharges = totalTaxAndServiceCharges;
	}

	public String getTotalHotelBookingValue() {
		return totalHotelBookingValue;
	}

	public void setTotalHotelBookingValue(String totalHotelBookingValue) {
		this.totalHotelBookingValue = totalHotelBookingValue;
	}
	
	
}
