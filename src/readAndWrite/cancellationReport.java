package readAndWrite;

public class cancellationReport {

	String canNumber = ""; 
	String canDate = ""; 
	String canNotes = ""; 
	String bookingnum = ""; 
	String documentNum = ""; 
	String cancelby = ""; 
	String cancellationreason = ""; 
	String hotelname = ""; 
	String suppliername = ""; 
	String suppliernumber = ""; 
	String numOfnyts = ""; 
	String numOfRooms = ""; 
	String cuslastName = ""; 
	String cusFirstName = ""; 
	String checkin = ""; 
	String checkout = ""; 
	String bookingDate = ""; 
	String roomType = ""; 
	String bedType = ""; 
	String ratePlan = ""; 
	String sellingCurrency = ""; 
	String totalCost = ""; 
	String totalCancellationFee = ""; 
	String additionalFee = ""; 
	String totalCostUsd = "";
	String totalCancelationFeeUsd = "";
	public String getCanNumber() {
		return canNumber;
	}
	public void setCanNumber(String canNumber) {
		this.canNumber = canNumber;
	}
	public String getCanDate() {
		return canDate;
	}
	public void setCanDate(String canDate) {
		this.canDate = canDate;
	}
	public String getCanNotes() {
		return canNotes;
	}
	public void setCanNotes(String canNotes) {
		this.canNotes = canNotes;
	}
	public String getBookingnum() {
		return bookingnum;
	}
	public void setBookingnum(String bookingnum) {
		this.bookingnum = bookingnum;
	}
	public String getDocumentNum() {
		return documentNum;
	}
	public void setDocumentNum(String documentNum) {
		this.documentNum = documentNum;
	}
	public String getCancelby() {
		return cancelby;
	}
	public void setCancelby(String cancelby) {
		this.cancelby = cancelby;
	}
	public String getCancellationreason() {
		return cancellationreason;
	}
	public void setCancellationreason(String cancellationreason) {
		this.cancellationreason = cancellationreason;
	}
	public String getHotelname() {
		return hotelname;
	}
	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}
	public String getSuppliername() {
		return suppliername;
	}
	public void setSuppliername(String suppliername) {
		this.suppliername = suppliername;
	}
	public String getSuppliernumber() {
		return suppliernumber;
	}
	public void setSuppliernumber(String suppliernumber) {
		this.suppliernumber = suppliernumber;
	}
	public String getNumOfnyts() {
		return numOfnyts;
	}
	public void setNumOfnyts(String numOfnyts) {
		this.numOfnyts = numOfnyts;
	}
	public String getNumOfRooms() {
		return numOfRooms;
	}
	public void setNumOfRooms(String numOfRooms) {
		this.numOfRooms = numOfRooms;
	}
	public String getCuslastName() {
		return cuslastName;
	}
	public void setCuslastName(String cuslastName) {
		this.cuslastName = cuslastName;
	}
	public String getCusFirstName() {
		return cusFirstName;
	}
	public void setCusFirstName(String cusFirstName) {
		this.cusFirstName = cusFirstName;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getBedType() {
		return bedType;
	}
	public void setBedType(String bedType) {
		this.bedType = bedType;
	}
	public String getRatePlan() {
		return ratePlan;
	}
	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}
	public String getSellingCurrency() {
		return sellingCurrency;
	}
	public void setSellingCurrency(String sellingCurrency) {
		this.sellingCurrency = sellingCurrency;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getTotalCancellationFee() {
		return totalCancellationFee;
	}
	public void setTotalCancellationFee(String totalCancellationFee) {
		this.totalCancellationFee = totalCancellationFee;
	}
	public String getAdditionalFee() {
		return additionalFee;
	}
	public void setAdditionalFee(String additionalFee) {
		this.additionalFee = additionalFee;
	}
	public String getTotalCostUsd() {
		return totalCostUsd;
	}
	public void setTotalCostUsd(String totalCostUsd) {
		this.totalCostUsd = totalCostUsd;
	}
	public String getTotalCancelationFeeUsd() {
		return totalCancelationFeeUsd;
	}
	public void setTotalCancelationFeeUsd(String totalCancelationFeeUsd) {
		this.totalCancelationFeeUsd = totalCancelationFeeUsd;
	}
	
	
}
