package readAndWrite;

public class readSearchDetails {

	String country =""; 
	String destination =""; 
	String checkIn =""; 
	String checkout =""; 
	String nights =""; 
	String roomCount ="";
	searchRoomDetails room = new searchRoomDetails();
	
	/**
	 * @return the roomCount
	 */
	public String getRoomCount() {
		return roomCount;
	}
	/**
	 * @param roomCount the roomCount to set
	 */
	public void setRoomCount(String roomCount) {
		this.roomCount = roomCount;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	/**
	 * @return the checkIn
	 */
	public String getCheckIn() {
		return checkIn;
	}
	/**
	 * @param checkIn the checkIn to set
	 */
	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}
	/**
	 * @return the checkout
	 */
	public String getCheckout() {
		return checkout;
	}
	/**
	 * @param checkout the checkout to set
	 */
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	/**
	 * @return the nights
	 */
	public String getNights() {
		return nights;
	}
	/**
	 * @param nights the nights to set
	 */
	public void setNights(String nights) {
		this.nights = nights;
	}
	/**
	 * @return the room
	 */
	public searchRoomDetails getRoom() {
		return room;
	}
	/**
	 * @param room the room to set
	 */
	public void setRoom(searchRoomDetails room) {
		this.room = room;
	}
	
	
}
