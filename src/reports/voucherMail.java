package reports;

import java.util.ArrayList;

public class voucherMail {

	String leadPassenger = ""; 
	String voucherNumber = ""; 
	String hotelname = ""; 
	String city = ""; 
	String tel = ""; 
	String checkin = ""; 
	String roomType = ""; 
	String ratePlan = ""; 
	String numOfAdults = ""; 
	String starCategory = ""; 
	String address = ""; 
	String status = ""; 
	String checkOut = ""; 
	String bedType = ""; 
	String numOfNyts = ""; 
	String numOfRooms = ""; 
	String numofChildren = ""; 
	String tele = ""; 
	String fax = ""; 
	String email = ""; 
	String website = ""; 
	String amountDueAtBooking = ""; 
	String utilization = "";
	ArrayList<roomOccypancy> 	occupancy = new ArrayList<roomOccypancy>();
	
	
	
	
	
	public String getAmountDueAtBooking() {
		return amountDueAtBooking;
	}

	public void setAmountDueAtBooking(String amountDueAtBooking) {
		this.amountDueAtBooking = amountDueAtBooking;
	}

	public String getUtilization() {
		return utilization;
	}

	public void setUtilization(String utilization) {
		this.utilization = utilization;
	}

	public ArrayList<roomOccypancy> getOccupancy() {
		return occupancy;
	}

	public void setOccupancy(ArrayList<roomOccypancy> occupancy) {
		this.occupancy = occupancy;
	}

	public String getTele() {
		return tele;
	}

	public void setTele(String tele) {
		this.tele = tele;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getLeadPassenger() {
		return leadPassenger;
	}

	public void setLeadPassenger(String leadPassenger) {
		this.leadPassenger = leadPassenger;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getHotelname() {
		return hotelname;
	}

	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getRatePlan() {
		return ratePlan;
	}

	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}

	public String getNumOfAdults() {
		return numOfAdults;
	}

	public void setNumOfAdults(String numOfAdults) {
		this.numOfAdults = numOfAdults;
	}

	public String getStarCategory() {
		return starCategory;
	}

	public void setStarCategory(String starCategory) {
		this.starCategory = starCategory;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getBedType() {
		return bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	public String getNumOfNyts() {
		return numOfNyts;
	}

	public void setNumOfNyts(String numOfNyts) {
		this.numOfNyts = numOfNyts;
	}

	public String getNumOfRooms() {
		return numOfRooms;
	}

	public void setNumOfRooms(String numOfRooms) {
		this.numOfRooms = numOfRooms;
	}

	public String getNumofChildren() {
		return numofChildren;
	}

	public void setNumofChildren(String numofChildren) {
		this.numofChildren = numofChildren;
	}
	
	
}
