package reports;

import java.util.ArrayList;

public class customerConfirmationEmail {
	
	String telNumber = ""; 
	String fax = ""; 
	String email = ""; 
	String webSite = ""; 
	String bookingReference = ""; 
	String bookingnumber = ""; 
	String checkIn = ""; 
	String checkOut = ""; 
	String starCategory = ""; 
	String bookingStatus = ""; 
	String currencyCode = ""; 
	String taxes = ""; 
	String subTotal = ""; 
	String total = ""; 
	String payableNow = ""; 
	String toBePaid = ""; 
	String firstName = ""; 
	String lastName = ""; 
	String address1 = ""; 
	String address2 = ""; 
	String city = ""; 
	String country = ""; 
	String phoneNumber = ""; 
	String emergency = ""; 
	String cusemail = ""; 
	String referenceNumber = ""; 
	String merchantTrackId = ""; 
	String paymentId = ""; 
	String amount = ""; 
	String hotelName = ""; 
	String portal = ""; 
	String hotelTele = ""; 
	String hotelEmail = ""; 
	String flag = "";

	ArrayList<roomOccypancy> 	occupancy 	= new ArrayList<roomOccypancy>();
	ArrayList<room> 			room		= new ArrayList<room>();
	
	
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getHotelTele() {
		return hotelTele;
	}

	public void setHotelTele(String hotelTele) {
		this.hotelTele = hotelTele;
	}

	public String getHotelEmail() {
		return hotelEmail;
	}

	public void setHotelEmail(String hotelEmail) {
		this.hotelEmail = hotelEmail;
	}

	public ArrayList<roomOccypancy> getOccupancy() {
		return occupancy;
	}

	public void setOccupancy(ArrayList<roomOccypancy> occupancy) {
		this.occupancy = occupancy;
	}

	public ArrayList<room> getRoom() {
		return room;
	}

	public void setRoom(ArrayList<room> room) {
		this.room = room;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebSite() {
		return webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	public String getBookingReference() {
		return bookingReference;
	}

	public void setBookingReference(String bookingReference) {
		this.bookingReference = bookingReference;
	}

	public String getBookingnumber() {
		return bookingnumber;
	}

	public void setBookingnumber(String bookingnumber) {
		this.bookingnumber = bookingnumber;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getStarCategory() {
		return starCategory;
	}

	public void setStarCategory(String starCategory) {
		this.starCategory = starCategory;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getTaxes() {
		return taxes;
	}

	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getPayableNow() {
		return payableNow;
	}

	public void setPayableNow(String payableNow) {
		this.payableNow = payableNow;
	}

	public String getToBePaid() {
		return toBePaid;
	}

	public void setToBePaid(String toBePaid) {
		this.toBePaid = toBePaid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmergency() {
		return emergency;
	}

	public void setEmergency(String emergency) {
		this.emergency = emergency;
	}

	public String getCusemail() {
		return cusemail;
	}

	public void setCusemail(String cusemail) {
		this.cusemail = cusemail;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getMerchantTrackId() {
		return merchantTrackId;
	}

	public void setMerchantTrackId(String merchantTrackId) {
		this.merchantTrackId = merchantTrackId;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	} 

	
}
