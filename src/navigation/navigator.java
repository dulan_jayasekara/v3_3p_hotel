package navigation;

import initializeDriver.initialize;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.xml.xpath.XPath;

import org.apache.bcel.generic.GOTO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import readAndWrite.bookingReportRoom;
import readAndWrite.bookingcard;
import readAndWrite.cancellationDetails;
import readAndWrite.cancellationReport;
import readAndWrite.cancellationRoomOccupancy;
import readAndWrite.cancelltionRateDetails;
import readAndWrite.profitAndLostReport;
import readAndWrite.readHotelResults;
import readAndWrite.readRoomResults;
import readAndWrite.readSearchDetails;
import readAndWrite.searchRoomDetails;
import readAndWrite.thirdPartySupplierReport;
import readConfirmation.conhotel;
import readXml.resHotelAvailability;
import reports.customerConfirmationEmail;
import reports.room;
import reports.roomOccypancy;
import reports.voucherMail;
import excelReader.ExcelReader;






public class navigator {
	private static Map<String, String>  txtProperty = new HashMap<String, String>();
	public Map<String, String> getCurrency(WebDriver driver, String currencyUrl)
	{
		driver.get(currencyUrl);
		Map<String, String> currency	= new HashMap<String, String>();
		int currencyIterator = 2;
		for(int i = 1 ; i > 0 ; i++)
		{
			try {
				String 	currencyCode= driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/table/tbody/tr["+currencyIterator+"]/td[3]")).getText();
				//System.out.println(currencyCode);
				String	rate		= driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/table/tbody/tr["+currencyIterator+"]/td[5]")).getText();
				//System.out.println(rate);
				currency.put(currencyCode, rate);
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			currencyIterator++;
		}
		return currency;
	}

	public String getDefaultCurrency(WebDriver driver)
	{
		String 					code 					= driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/table/tbody/tr[2]/td[2]")).getText().trim();
		return 					code;
	}
	
	public readSearchDetails navigateToSearchWeb(WebDriver driver, String searchUrl, StringBuffer ReportPrinter, String[] searchDetails) throws InterruptedException, FileNotFoundException
	{
		System.out.println("------------------------------------------------------------------------------------------");
		for(int i = 0 ; i < searchDetails.length ; i++)
		{
			System.out.println(searchDetails[i]);
		}
		driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
		driver.get(searchUrl);

		readSearchDetails		searchgRead 			= new readSearchDetails();
		
		
			try {
				
				//driver.switchTo().frame("live_message_frame");
				driver.switchTo().frame("bec_container_frame");
			} catch (Exception e) {
				// TODO: handle exception
			}
		
		WebDriverWait wait	 			= new WebDriverWait(driver, 10);
		System.out.println("test -- 1.1");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hotels")));	
		driver.findElement(By.id("hotels")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("norooms_H")));
		System.out.println("test -- 1.2");
		//1112|London||54||United Kingdom|
		driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
		((JavascriptExecutor)driver).executeScript("document.getElementById('hid_H_Loc').setAttribute('value','"+ searchDetails[1] +"');");
		System.out.println("test -- 1.3");
		new Select(driver.findElement(By.id("norooms_H"))).selectByVisibleText(searchDetails[4]);
		searchgRead.setRoomCount(searchDetails[4]);
		System.out.println("test -- 1.4");
		//((JavascriptExecutor)driver).executeScript("document.getElementById('ho_departure').setAttribute('value','08/10/2014');");
		//((JavascriptExecutor)driver).executeScript("$('#ho_departure').val('08/10/2014');");
		String[] 				adultCount 				=	searchDetails[5].split("/"); 
		int 					adultExcelIterator		= 1 ;
		String 					adltcnt 				= adultCount[0];
		searchRoomDetails 		roomDetails 			= new searchRoomDetails();
		System.out.println("test -- 1.4.1");
		try {
			for(int i = 0 ; i < adultCount.length ; i++)
			{
				new Select(driver.findElement(By.id("R"+ adultExcelIterator +"occAdults_H"))).selectByVisibleText(adultCount[i]);
				if(i != 0)
				{
					adltcnt = adltcnt.concat(",").concat(adultCount[i]);
				}
				adultExcelIterator++ ;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("test -- 1.5");
		roomDetails.setAdultCount(adltcnt);
		int 					childExcelIterator	 	= 1;
		String[] 				childCount				= searchDetails[6].split("/"); 
		String 					chldCnt 				= childCount[0];
		String 					chldAge 				= "";
		int 					ageExcelIterator 		= 0;
		try {
			for(int i = 0 ; i < childCount.length ; i++)
			{
				new Select(driver.findElement(By.id("R"+ childExcelIterator +"occChildren_H"))).selectByVisibleText(childCount[i]);
				if(i != 0)
				{
					chldCnt = chldCnt.concat(",").concat(childCount[i]);
				}
				childExcelIterator++;
				
				//int 		ageExcelIterator = 0;
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("test -- 1.6");
		String[] 				age						= searchDetails[7].split("/");
		int 					temp 					= 0;
		try {
			int			 		cnt 					= 0;
			for(int j =1 ; j > 0 ; j++)
			{
				if(childCount[temp].equals(null) || childCount[temp].equals(""))
				{
					break;
				}
				
				for(int k =1 ; k > 0 ; k++)
				{
					try {
						//System.out.println(j);
						//System.out.println(k);
						new Select(driver.findElement(By.id("H_R"+j+"childage_" + k))).selectByVisibleText(age[ageExcelIterator]);
						chldAge 						= chldAge.concat(",").concat(age[ageExcelIterator]);
						//System.out.println(chldAge);
						ageExcelIterator++;
						
					} catch (Exception e) {
						break;
					}
				}
				cnt++;
				temp++;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("test -- 1.7");
		roomDetails.setAge(chldAge);
		roomDetails.setChildCount(chldCnt);
		searchgRead.setRoom(roomDetails);
		//((JavascriptExecutor)driver).executeScript("$('#ho_arrival').val('07/27/2014');");
		
		((JavascriptExecutor)driver).executeScript("$('#ho_departure').val('"+ searchDetails[2] +"');");
		//new Select(driver.findElement(By.id("H_nights"))).selectByVisibleText(searchDetails[3]);
		searchgRead.setCheckIn(searchDetails[2]);
		SimpleDateFormat 		sdf 					= new SimpleDateFormat("MM/dd/yyyy");
		Calendar 				c 						= Calendar.getInstance();
		String 					dt 						= null;
		System.out.println("test -- 1.8");
		try {
			c.setTime(sdf.parse(searchDetails[2]));
			//System.out.println(sdf.format(c.getTime()));
			c.add(Calendar.DATE, Integer.parseInt(searchDetails[3]));
								dt 						= sdf.format(c.getTime());
			//System.out.println(dt);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("test -- 1.9");
		//System.out.println(dt);
		Thread.sleep(1000);
		System.out.println("test -- 1.10");
		((JavascriptExecutor)driver).executeScript("$('#ho_arrival').val('"+ dt +"');");
		searchgRead.setCheckout(dt);
		String 					location				= searchDetails[1].replace("|", "/");
		String[] 				loc						= location.split("/");
		driver.findElement(By.id("H_Loc")).sendKeys(loc[1].trim());
		searchgRead.setDestination(loc[1].trim());
		new Select(driver.findElement(By.id("H_Country"))).selectByVisibleText(searchDetails[0]);
		searchgRead.setCountry(searchDetails[0]);
		System.out.println("test -- 1.11");
		Select 					select 					= new Select(driver.findElement(By.id("H_Country")));
		WebElement 				tmp 					= select.getFirstSelectedOption();
		searchgRead.setCountry		(tmp.getText());
		System.out.println("test -- 1.11.1");
		
		//WebDriverWait wait	 			= new WebDriverWait(driver, 30);
		driver.findElement(By.id("search_btns_h")).click();
		System.out.println("test -- 1.11.2");
		long start = System.currentTimeMillis();
		System.out.println("test -- 1.11.3");
		/*try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("sort_heading_WJ_4")));
			System.out.println("test -- 1.11.4");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("test -- 1.11.5");
		}*/
		System.out.println("test -- 1.12");
		long finish = System.currentTimeMillis();
		long totalTime = finish - start; 
		//System.out.println(totalTime/1000);
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span><font color=green>Payment page loading time "+ totalTime/1000 +" seconds</font></span>");
		ReportPrinter.append("<br>");
		System.out.println("test -- 1.13");
		return searchgRead;
	}
	
	public readSearchDetails navigateToSearch(WebDriver driver, String searchUrl, String[] searchDetails) throws FileNotFoundException, InterruptedException
	{
		driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
		driver.get(searchUrl);
		//System.out.println("search test -- 1");
		readSearchDetails		searchgRead 			= new readSearchDetails();
		
		//driver.get("http://rezdemo2.rezgateway.com/rezpackage/operations/reservation/CallCenterWarSearchCriteria.do?module=operations");
		//driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
			try {
				//System.out.println("search test -- 2");
				driver.switchTo().frame("live_message_frame");
				driver.switchTo().frame("bec_container_frame");
			} catch (Exception e) {
				// TODO: handle exception
			}
		
		driver.findElement(By.id("hotels")).click();
		//System.out.println("search test -- 3");
		//1112|London||54||United Kingdom|
		//driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
		((JavascriptExecutor)driver).executeScript("document.getElementById('hid_H_Loc').setAttribute('value','"+ searchDetails[1] +"');");
		new Select(driver.findElement(By.id("norooms_H"))).selectByVisibleText(searchDetails[4]);
		searchgRead.setRoomCount(searchDetails[4]);
		//((JavascriptExecutor)driver).executeScript("document.getElementById('ho_departure').setAttribute('value','08/10/2014');");
		//((JavascriptExecutor)driver).executeScript("$('#ho_departure').val('08/10/2014');");
		//System.out.println("search test -- 4");
		String[] 				adultCount 				= searchDetails[5].split("/"); 
		int 					adultExcelIterator		= 1 ;
		String 					adltcnt 				= adultCount[0];
		searchRoomDetails 		roomDetails 			= new searchRoomDetails();
		try {
			for(int i = 0 ; i < adultCount.length ; i++)
			{
				new Select(driver.findElement(By.id("R"+ adultExcelIterator +"occAdults_H"))).selectByVisibleText(adultCount[i]);
				if(i != 0)
				{
					adltcnt = adltcnt.concat(",").concat(adultCount[i]);
				}
				adultExcelIterator++ ;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		//System.out.println("search test -- 5");
		roomDetails.setAdultCount(adltcnt);
		int 					childExcelIterator	 	= 1;
		String[] 				childCount				= searchDetails[6].split("/"); 
		String 					chldCnt 				= childCount[0];
		String 					chldAge 				= "";
		int 		ageExcelIterator = 0;
		try {
			//System.out.println("search test -- 6");
			for(int i = 0 ; i < childCount.length ; i++)
			{
				new Select(driver.findElement(By.id("R"+ childExcelIterator +"occChildren_H"))).selectByVisibleText(childCount[i]);
				if(i != 0)
				{
					chldCnt = chldCnt.concat(",").concat(childCount[i]);
				}
				childExcelIterator++;
				
				//int 		ageExcelIterator = 0;
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		String[] 	age		= searchDetails[7].split("/");
		int temp = 0;
		try {
			//System.out.println("search test -- 7");
			int cnt = 0;
			for(int j =1 ; j > 0 ; j++)
			{
				if(childCount[temp].equals(null) || childCount[temp].equals(""))
				{
					break;
				}
				
				for(int k =1 ; k > 0 ; k++)
				{
					try {
						//System.out.println(j);
						//System.out.println(k);
						new Select(driver.findElement(By.id("H_R"+j+"childage_" + k))).selectByVisibleText(age[ageExcelIterator]);
						chldAge = chldAge.concat(",").concat(age[ageExcelIterator]);
						//System.out.println(chldAge);
						ageExcelIterator++;
						
					} catch (Exception e) {
						break;
					}
				}
				cnt++;
				temp++;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		//System.out.println("search test -- 8");
		roomDetails.setAge(chldAge);
		roomDetails.setChildCount(chldCnt);
		searchgRead.setRoom(roomDetails);
		//((JavascriptExecutor)driver).executeScript("$('#ho_arrival').val('07/27/2014');");
		//System.out.println("search test -- 9");
		((JavascriptExecutor)driver).executeScript("$('#ho_departure').val('"+ searchDetails[2] +"');");
		//new Select(driver.findElement(By.id("H_nights"))).selectByVisibleText(searchDetails[3]);
		searchgRead.setCheckIn(searchDetails[2]);
		SimpleDateFormat 		sdf 					= new SimpleDateFormat("MM/dd/yyyy");
		Calendar 				c 						= Calendar.getInstance();
		String 					dt 						= null;
		try {
			c.setTime(sdf.parse(searchDetails[2]));
			//System.out.println(sdf.format(c.getTime()));
			c.add(Calendar.DATE, Integer.parseInt(searchDetails[3]));
								dt 						= sdf.format(c.getTime());
			//System.out.println(dt);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//System.out.println("search test -- 10");
		//System.out.println(dt);
		Thread.sleep(1000);
		((JavascriptExecutor)driver).executeScript("$('#ho_arrival').val('"+ dt +"');");
		searchgRead.setCheckout(dt);
		String 					location				= searchDetails[1].replace("|", "/");
		String[] 				loc						= location.split("/");
		driver.findElement(By.id("H_Loc")).sendKeys(loc[1].trim());
		searchgRead.setDestination(loc[1].trim());
		new Select(driver.findElement(By.id("H_Country"))).selectByVisibleText(searchDetails[0]);
		searchgRead.setCountry(searchDetails[0]);
		
		Select 					select 					= new Select(driver.findElement(By.id("H_Country")));
		WebElement 				tmp 					= select.getFirstSelectedOption();
		searchgRead.setCountry		(tmp.getText());
		
		
		
		driver.findElement(By.id("search_btns_h")).click();
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.id("load_h")))
		//System.out.println("A");
		
		return searchgRead;
	}
	
	public ArrayList<readHotelResults> readResultsWeb(WebDriver driver, String room)
	{
		System.out.println("test -- 0.11");
		WebDriverWait 			wait 					= new WebDriverWait(driver, 30);
		int 					roomCount				= Integer.parseInt(room);
		driver.switchTo().defaultContent();
		//driver.switchTo().frame("live_message_frame");
		String 					hotelCountHelp 			= null;;
		try {
			System.out.println("test -- 0.12");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hotel_id_1")));
			System.out.println("test -- 0.121");
								hotelCountHelp			= driver.findElement(By.xpath(".//*[@id='pagination_WJ_2']/div")).getText().trim();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("no results found");
		}
		//System.out.println(hotelCountHelp);
		String[] 				pageCountHelp			= hotelCountHelp.split(" ");
		int 					pageCount 				= 0;
		int 					hotelCount				= Integer.parseInt(pageCountHelp[3]);
		if(hotelCount%10 == 0)
		{
			pageCount = hotelCount/10;
		}
		else
		{
			pageCount = hotelCount/10;
			pageCount = pageCount + 1;
		}
		//System.out.println(pageCount);
		int 					resultsIterator 		= 1;
		String 					value;
		String 					style;
		ArrayList<ArrayList<readHotelResults>> hotelListBig	= new ArrayList<ArrayList<readHotelResults>>();
		ArrayList<readHotelResults> hotelList			= new ArrayList<readHotelResults>();
		int 					cnt 					= 1;
		//outerLoop : for(int pageIterator = 1; pageIterator <= pageCount ; pageIterator++)
		outerLoop : for(int pageIterator = 1; pageIterator <= 1 ; pageIterator++)
		{
			try{
				System.out.println("test -- 0.13");
				//((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+pageIterator+"},'','WJ_2'))");
			}
			catch(Exception e)
			{
				
			}
			try {
				System.out.println("test -- 0.14");
				List<WebElement> moreInfo 				= new ArrayList<WebElement>();
				//moreInfo.addAll(driver.findElements(By.className("load_modal_window")));
				String 			mInfo 					= null;
				innerLoop : for(int i = 1 ; i <= 10 ; i++)
				{
					try {
						System.out.println("test -- 0.15");
						
						driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div["+ i +"]/div[1]/div[5]/div[3]/div[2]/div/span[3]")).getText();
						driver.findElement(By.xpath(".//*[@id='hcancellation-policy']")).getText();

						;
					} catch (Exception e) {
						
						readHotelResults 	hotelResults = new readHotelResults();
						try {
							driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div["+ i +"]/div[1]/div[4]/div[2]/div/img"));
							hotelResults.setAvailability("available");
						} catch (Exception e2) {
							// TODO: handle exception
						}
						// TODO: handle exception
						
						hotelResults.setTitle							(driver.findElement(By.id("hotel_id_"+ cnt +"")).getText());
						System.out.println(hotelResults.getTitle());
						hotelResults.setAddress							(driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div["+ i +"]/div[1]/div[5]/div[3]/div[1]/div")).getText());
						hotelResults.setTotalCost						(driver.findElement(By.xpath(".//*[@id='ratebox_"+ cnt +"']")).getText());
								mInfo 					= driver.findElement(By.id("hotelmoreinfo_"+ cnt +"")).getAttribute("href");
						String[] supplier 				= mInfo.split(",");
								supplier 				= supplier[3].split("'");
						hotelResults.setSupplier						(supplier[1]);
						ArrayList<readRoomResults>  roomList	= new ArrayList<readRoomResults>();
						try {
							System.out.println("test -- 0.16");
							//driver.findElement(By.xpath(".//*[@id='show_rooms_d_"+ cnt +"']")).click();
						} catch (Exception e2) {
							// TODO: handle exception
						}
						int flag = 1;
						for(int j = 4 ; j > 0 ; j++)
						{
							readRoomResults readRoom = new readRoomResults();
							try {
								System.out.println("test -- 0.17");
								readRoom.setRoomType						(driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div["+ i +"]/div["+ j +"]/div[2]")).getText());
								readRoom.setRoomInfo						(driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div["+ i +"]/div["+ j +"]/div[1]")).getText());
								if(readRoom.getRoomInfo().isEmpty() || readRoom.getRoomInfo().equals("") || readRoom.getRoomInfo().equals(" ") || readRoom.getRoomInfo().equals(null))
								{
									readRoom.setRoomInfo(roomList.get(roomList.size()-1).getRoomInfo());
								}
								//System.out.println(readRoom.getRoomInfo());
								readRoom.setRatePlan						(driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div["+ i +"]/div["+ j +"]/div[3]")).getText());
								//hotelprice_1_1
								readRoom.setRate							(driver.findElement(By.id("hotelprice_"+ i +"_"+ flag +"")).getText());
								//System.out.println(readRoom.getRate());
								roomList.add(readRoom);
								j++;
								flag++;
								
							} catch (Exception e2) {
								// TODO: handle exception
								/*try {
									int jHelp = j - 3;
									readRoom.setRoomType					(driver.findElement(By.xpath(".//*[@id='hid_room_"+ cnt +"_"+ jHelp +"']/div[2]")).getText());
									System.out.println(readRoom.getRoomType());
									readRoom.setRoomInfo					(driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div[1]/div["+ --j +"]/div[1]")).getText());
									readRoom.setRatePlan					(driver.findElement(By.xpath(".//*[@id='hid_room_"+ cnt +"_"+ jHelp +"']/div[3]")).getText());
									readRoom.setRate						(driver.findElement(By.xpath(".//*[@id='hid_room_"+ cnt +"_"+ jHelp +"']/div[4]")).getText());
									roomList.add(readRoom);
								} catch (Exception e3) {
									break;
								}*/
								break;
							}
							
							
						}
						hotelResults.setRoom(roomList);
						hotelList.add(hotelResults);
					}	
					
					cnt++;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
		}
		return hotelList;
	}
	
	public ArrayList<readHotelResults> readResults(WebDriver driver, String room, String supplier)
	{ 	 
		System.out.println("resuls test -- 1");
		int roomCount	= Integer.parseInt(room);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		System.out.println("resuls test -- 2");
		String hotelCountHelp = null;
		WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		try {
			System.out.println("resuls test -- 3");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='pagination_WJ_2']/div[1]")));
			System.out.println("resuls test -- 4");
			hotelCountHelp	= driver.findElement(By.xpath(".//*[@id='pagination_WJ_2']/div[1]")).getText().trim();
			System.out.println("resuls test -- 5");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("no results found");
		}
		String[] pageCountHelp		= hotelCountHelp.split(" ");
		int pageCount = 0;
		int hotelCount				= Integer.parseInt(pageCountHelp[6]);
		if(hotelCount%10 == 0)
		{
			pageCount = hotelCount/10;
		}
		else
		{
			pageCount = hotelCount/10;
			pageCount = pageCount + 1;
		}
		int resultsIterator = 1;
		String value;
		String style;
		ArrayList<ArrayList<readHotelResults>> hotelListBig	= new ArrayList<ArrayList<readHotelResults>>();
		ArrayList<readHotelResults> hotelList	= new ArrayList<readHotelResults>();
		outerLoop : for(int pageIterator = 1; pageIterator <= pageCount ; pageIterator++)
		{
			try{
				((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+pageIterator+"},'','WJ_2'))");
			}
			catch(Exception e)
			{
				break;
			}
			
			try 
			{
				
				for(int i = 1 ; i <= 10 ; i++)
				{
					//System.out.println(resultsIterator);
					
					value = driver.findElement(By.id("hotel_id_" + resultsIterator)).getAttribute("value");
					style = driver.findElement(By.id("title_" + value)).getAttribute("style");
					label :
					{
					if(style.contains("color") && style.contains("red"))
					{
						String supX	= driver.findElement(By.id("moreinfo_" + value)).getAttribute("href");
						String[] suppX = supX.split(",");
						String[] supp2X = suppX[3].split("'");
						//System.out.println(supp2X[1]);
						//System.out.println(supplier);
						if(supp2X[1].equalsIgnoreCase(supplier))
						{
							readHotelResults 	hotelResults = new readHotelResults();
							if(driver.findElement(By.id("title_" + value)).getText().equals("") || driver.findElement(By.id("title_" + value)).getText().equals(null))
							{
								//System.out.println(driver.findElement(By.id("title_" + value)).getText());
								break label;
							}
							hotelResults.setTitle			(driver.findElement(By.id("title_" + value)).getText());
							System.out.println(hotelResults.getTitle());
							hotelResults.setAddress			(driver.findElement(By.id("address_" + value)).getText());
							hotelResults.setAvailability	(driver.findElement(By.id("availability_" + value)).getAttribute("class"));
							hotelResults.setTotalCost		(driver.findElement(By.id("ratebox_" + value)).getText());
							String sup	= driver.findElement(By.id("moreinfo_" + value)).getAttribute("href");
							String[] supp = sup.split(",");
							String[] supp2 = supp[3].split("'");
							hotelResults.setSupplier(supp2[1]);
							int breaker		=	0;
							ArrayList<readRoomResults>  roomList	= new ArrayList<readRoomResults>();
							try {
								//show all the rooms
								//driver.findElement(By.id("showroom_" + value)).click();
							} catch (Exception e) {
								// TODO: handle exception
							}
							for(int j = 0 ; j > -1 ; j++)
							{
								readRoomResults roomResults				= new readRoomResults();
								label2 : {
								try {
									roomResults.setRoomInfo		(driver.findElement(By.id("roominfo_" + value +"_" + j)).getText());
									if(roomResults.getRoomInfo().equals(null) || roomResults.getRoomInfo().equals(""))
									{
										break label2;
									}
									roomResults.setRoomType		(driver.findElement(By.id("room_"+ value +"_" + j)).getText());
									//System.out.println(roomResults.getRoomType());
									roomResults.setBed			(driver.findElement(By.id("bed_"+ value + "_" + j)).getText());
									//System.out.println(roomResults.getBed());
									roomResults.setRatePlan		(driver.findElement(By.id("rateplan_" + value +"_" + j)).getText());
									//System.out.println(roomResults.getRatePlan());
									int jj = j;
									jj++;
									roomResults.setRate			(driver.findElement(By.id("HT_rate_" +value+ "_" + jj)).getText());
									//System.out.println(roomResults.getRate());
									roomList.add(roomResults);
									
									breaker++;
									//if(breaker == roomCount)
									//	break;
								} catch (Exception e) {
									// TODO: handle exception
									break;
								}
								}
							
								
							}
							hotelResults.setRoom(roomList);
							//System.out.println(hotelResults.getRoom().size());
							hotelList.add(hotelResults);
						}
			
					}
					
					//hotelListBig.add(hotelList);
					resultsIterator++;
					}
				}
			}
			catch(Exception e)
			{
				
			}
			
			
		}
		return hotelList;
	}
	
	public ArrayList<String> navigateToResultsWeb(WebDriver driver, StringBuffer ReportPrinter, String hotel, String sup)
	{
		WebDriverWait wait = new WebDriverWait(driver, 20);
		Properties properties = new Properties();
		ArrayList<String> tracerAndHotel = new ArrayList<String>();
		String hName = "";
		//Properties properties1 = new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("D://workSpace//hotelXmlValidation//hotelAndSupplier.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 //System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		
		String hotelCountHelp = null;;
		try {
			hotelCountHelp	= driver.findElement(By.xpath(".//*[@id='pagination_WJ_2']/div")).getText().trim();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("no results found");
		}
		//System.out.println(hotelCountHelp);
		String[] pageCountHelp		= hotelCountHelp.split(" ");
		int pageCount = 0;
		int hotelCount				= Integer.parseInt(pageCountHelp[3]);
		if(hotelCount%10 == 0)
		{
			pageCount = hotelCount/10;
		}
		else
		{
			pageCount = hotelCount/10;
			pageCount = pageCount + 1;
		}
		//System.out.println(pageCount);
		String 	mInfo 		= null;
		String 	hotelName	= null;
		String 	tracer 		= null;
		String 	tracer2     = "";
		String 	errorType 	= "";
		String 	error 		= "";
		long	start		= 0 ; 
		
		outerLoop : for(int pageIterator = 1; pageIterator <= pageCount ; pageIterator++)
		{
			try {
				((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+pageIterator+"},'','WJ_2'))");
			} catch (Exception e) {
				// TODO: handle exception
			}
			for(int j = 1 ; j > 0 ; j++)
			{
				try {
					mInfo = driver.findElement(By.id("hotelmoreinfo_"+ j +"")).getAttribute("href");
					String[] id = mInfo.split(",");
					//System.out.println(id[1]);
					id			= id[1].trim().split("'");
					//System.out.println(id[1]);
					
					tracer = driver.findElement(By.id("hotelridetracer_" + id[1])).getAttribute("value");
					if(tracer.equals("") || tracer.equals(" ") || tracer.equals(null) || tracer.isEmpty())
					{
						
					}
					else
					{
						tracer2 = tracer;
						break outerLoop;
					}
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				
			}
			
		}
		outerLoop : for(int pageIterator = 1; pageIterator <= pageCount ; pageIterator++)
		{
			try{
				((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+pageIterator+"},'','WJ_2'))");
			}
			catch(Exception e)
			{
				
			}
			try {
				List<WebElement> moreInfo = new ArrayList<WebElement>();
				moreInfo.addAll(driver.findElements(By.className("load_modal_window")));
				//System.out.println(moreInfo.size());
				for(int i = 1 ; i <= 10 ; i++)
				{
					hotelName = driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div["+ i +"]/div[1]/div[5]/div[2]/div[1]")).getText();
					//System.out.println(txtProperty.get("hotel"));
					if(hotel.trim().equals(hotelName.trim()))
					{
						/*for(int j = 0 ; j < moreInfo.size() ; j++)
						{
							mInfo = moreInfo.get(j).getAttribute("href");
							String[] id = mInfo.split(",");
							//System.out.println(id[1]);
							id			= id[1].split("'");
							//System.out.println(id[1]);
							tracer = driver.findElement(By.id("hotelridetracer_" + id[1])).getAttribute("value");
							if(tracer.equals("") || tracer.equals(" ") || tracer.equals(null) || tracer.isEmpty())
							{
								
							}
							else
							{
								tracer2 = tracer;
							}
						}*/
						
						//System.out.println(tracer2);
						start = System.currentTimeMillis();
						driver.findElement(By.xpath(".//*[@id='hotel_dv_WJ_1']/div["+ i +"]/div[1]/div[6]/div[5]/a")).click();
						try {
							wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='cus_dlg_WJ_33']/div[1]")));
							driver.findElement(By.xpath(".//*[@id='cus_dlg_WJ_33']/div[1]")).click();
						} catch (Exception e) {
							// TODO: handle exception	
							try {
								driver.findElement(By.xpath(".//*[@id='cus_dlg_WJ_34']/div[1]")).click();;
							} catch (Exception ee) {
								// TODO: handle exception
							}
						}
						break outerLoop;
					}
					
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		try {
			if(error.equals(null) || error.equals("") || error.equals(" ") || error.isEmpty())
			{
				error 		= 	driver.findElement(By.id("dialog-error-message-WJ_20")).getText();
				errorType	=	driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_20")).getText();
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			//System.out.println("test");
			if(error.equals(null) || error.equals("") || error.equals(" ") || error.isEmpty())
			{
				error = driver.findElement(By.xpath(".//*[@id='dialog-error-message-WJ_20']")).getText();
				errorType = driver.findElement(By.xpath(".//*[@id='ui-dialog-title-dialog-error-message-WJ_20']")).getText();
				System.out.println(error);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			driver.findElement(By.className("close pop-up-close-button")).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {

			Actions action = new Actions(driver);
			action.sendKeys(Keys.ESCAPE).perform();  
		} catch (Exception e) {
			// TODO: handle exception
			}
		try {
			if(error.equals(null) || error.equals("") || error.equals(" ") || error.isEmpty())
			{
				errorType 	=	driver.findElement(By.id("ui-dialog-title-dialog-warning-message-WJ_21")).getText();
				error		= 	driver.findElement(By.id("dialog-warning-message-WJ_21")).getText();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("screenShots/"+System.currentTimeMillis()/1000+".jpg"));
			}
			
			//File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			//FileUtils.copyFile(screenshot, new File("/rezsystem/workspace/hotelXmlValidation/screenShots"));
			
			
			driver.findElement(By.className("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only")).click();
			
		} catch (Exception e) {
			// TODO: handle exception
			try {
				driver.findElement(By.xpath("html/body/div[16]/div[3]/div/button[1]")).click();
				long finish = System.currentTimeMillis();
				long time = finish-start;
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span><font color=green>Add to cart loading time "+ time/1000 +" seconds</font></span>");
				ReportPrinter.append("<br>");
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		try {
			if(error.equals(null) || error.equals("") || error.equals(" ") || error.isEmpty())
			{
				errorType	=	driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_20")).getText();
				error		=	driver.findElement(By.id("dialog-error-message-WJ_20")).getText();
				TakesScreenshot screen = (TakesScreenshot)driver;
				FileUtils.copyFile(screen.getScreenshotAs(OutputType.FILE),new File("screenShots/"+System.currentTimeMillis()/1000+".jpg"));
				driver.findElement(By.className("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only")).click();
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			try {
				driver.findElement(By.xpath("html/body/div[16]/div[11]/div/button")).click();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		if(errorType.isEmpty() || errorType.equals(null))
		{
			ReportPrinter.append("<span>Hotel availability Error</span>");
			
			ReportPrinter.append("<table border=1 style=width:800px>"
				+ "<tr><th>Error</th>"
				+ "<th>Error type</th></tr>");
			
			ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
					+ 	"<td>"+ error +"</td></tr>");
			ReportPrinter.append("</table>");
		}
		
		long start2 = System.currentTimeMillis();
		try {
			driver.findElement(By.xpath(".//*[@id='b1-items']/a[2]/div")).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='content-booking']/div[1]/div")));
		} catch (Exception e) {
			// TODO: handle exception
		} 
		
		long finish = System.currentTimeMillis();
		long totalTime = finish - start2; 
		ReportPrinter.append("<br>");
		ReportPrinter.append("<span><font color=green>Results page loading time "+ totalTime/1000 +" seconds</font></span>");
		ReportPrinter.append("<br>");
		tracerAndHotel.add(hotelName);
		tracerAndHotel.add(tracer2);
		tracerAndHotel.add(errorType);
		tracerAndHotel.add(error);
		
		return tracerAndHotel;
	}
	
	public voucherMail customerVoucherMail(WebDriver driver, String string)
	{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.id("emaillayoutbasedon_customer2")).click();
		voucherMail vm = new voucherMail();
		//driver.switchTo().defaultContent();
		driver.switchTo().frame("Myemailformat");
		vm.setLeadPassenger				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText());
		vm.setVoucherNumber				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[8]")).getText());
		String[] temp			=		driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[2]/div")).getText().split(":");
		String[] temp2			=		temp[1].trim().split("Fax");
		vm.setTele						(temp2[0]);
		temp2					=		temp[2].trim().split("Email");
		vm.setFax						(temp2[0]);
		temp2					=   	temp[3].trim().split("Website");
		vm.setEmail						(temp2[0]);
		vm.setWebsite					(temp[5].replace("/", ""));
		int i = 0;
		for(i = 1 ; i > 0 ; i++)
		{
			String title = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td")).getText();
			if(title.equals("Accomodation"))
			{
				break;
			}
		}
		
		vm.setHotelname					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+1) +"]/td[2]")).getText());
		vm.setAddress					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+1) +"]/td[8]")).getText());
		vm.setCity						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+2) +"]/td[2]")).getText());
		vm.setStatus					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+2) +"]/td[8]")).getText());
		vm.setTel						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+3) +"]/td[2]")).getText());
		vm.setCheckOut					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+3) +"]/td[8]")).getText());
		vm.setBedType					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+5) +"]/td[8]")).getText());
		vm.setRoomType					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+6) +"]/td[2]")).getText());
		vm.setNumOfNyts					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+6) +"]/td[8]")).getText());
		vm.setRatePlan					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+7) +"]/td[2]")).getText());
		vm.setNumOfRooms				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+7) +"]/td[8]")).getText());
		vm.setNumOfAdults				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+8) +"]/td[2]")).getText());
		vm.setNumofChildren				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+8) +"]/td[8]")).getText());
		vm.setStarCategory				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+9) +"]/td[2]")).getText());
		ArrayList<roomOccypancy> 	roomList	= new ArrayList<roomOccypancy>();
		for(i = (i+13) ; i > 0 ; i++ )
		{
			try {
				roomOccypancy	ro	 = 	new 	 roomOccypancy();
				ro.setRoomNumber			(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td[1]")).getText());
				ro.setFullName				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td[2]")).getText());
				ro.setType					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td[3]")).getText());
				ro.setChildAge				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td[4]")).getText());
				roomList.add(ro);
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			
		}
		vm.setOccupancy(roomList);
		
		for(i = 1 ; i > 0 ; i++)
		{
			String pay = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td")).getText();
			if(pay.equals("Payment"))
			{
				break;
			}
		}
		vm.setAmountDueAtBooking		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+1) +"]/td[2]")).getText());
		vm.setUtilization				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+1) +"]/td[2]")).getText());
		
		return vm;
	}
	
	public reports.voucherMail voucherMail(WebDriver driver, conhotel hotel)
	{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.id("emaillayoutbasedon_customer2")).click();
		driver.switchTo().frame("Myemailformat");
		voucherMail voucher = new voucherMail();
		String[] details = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[2]")).getText().split(":");
		String[] details2  	=		(details[1].trim().split("Fax"));
		voucher.setTel		(details2[0]);
		details2 = details[2].trim().split("Email");
		voucher.setFax(details2[0]);
		details2 = details[3].trim().split("Website");
		voucher.setEmail	(details2[0]);
		String url = details[4].concat(details[5]);
		voucher.setWebsite	(url);
		
		voucher.setLeadPassenger		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText());
		System.out.println(voucher.getLeadPassenger());
		voucher.setVoucherNumber		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td[8]")).getText());
		
		for(int i = 2 ; i > 0 ; i++)
		{
			String temp = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td")).getText();
			if(temp.equals("Accomodation"))
			{
				voucher.setHotelname		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+1) +"]/td[2]")).getText());
				voucher.setAddress			(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+1) +"]/td[8]")).getText());
				voucher.setCity				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+2) +"]/td[2]")).getText());
				voucher.setStatus			(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+2) +"]/td[8]")).getText());
				voucher.setTele				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+3) +"]/td[2]")).getText());
				voucher.setCheckOut			(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+3) +"]/td[8]")).getText());
				voucher.setCheckin			(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+4) +"]/td[2]")).getText());
				voucher.setBedType			(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+6) +"]/td[8]")).getText());
				voucher.setRoomType			(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+7) +"]/td[2]")).getText());
				voucher.setNumOfNyts		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+7) +"]/td[8]")).getText());
				voucher.setRatePlan			(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+8) +"]/td[2]")).getText());
				voucher.setNumOfRooms		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+8) +"]/td[8]")).getText());
				voucher.setNumOfAdults		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+9) +"]/td[2]")).getText());
				voucher.setNumOfRooms		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+9) +"]/td[8]")).getText());
				voucher.setStarCategory		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+10) +"]/td[2]")).getText());
				ArrayList<roomOccypancy> occ = new ArrayList<roomOccypancy>();
				for(int j = (i+13) ; j > i ; j++)
				{
					try {
						roomOccypancy occypancy = new roomOccypancy();
						occypancy.setRoomNumber		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (j) +"]/td/table/tbody/tr[2]/td[1]")).getText());
						occypancy.setFirstname		(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (j) +"]/td/table/tbody/tr[2]/td[2]")).getText());
						occ.add(occypancy);
						if(occypancy.getRoomNumber().equals("") || occypancy.getRoomNumber().equals(null))
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
					
				}
				voucher.setOccupancy(occ);
				break;
			}
		}
		
		for(int i = 2 ; i > 0 ; i++)
		{
			if(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td")).getText().equals("Payment"))
			{
				voucher.setAmountDueAtBooking	(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (i+1) +"]/td[2]")).getText());
				break;
			}
		}
		
		return voucher;
	}
	
	public customerConfirmationEmail customerConfirmationMail(WebDriver driver, String url, conhotel conHotel)
	{
		driver.get(url);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.id("productType_hotels")).click();
		driver.findElement(By.id("searchby_hotels")).click();
		driver.findElement(By.id("searchby_reservationno")).click();
		System.out.println("1");
		driver.findElement(By.id("searchby_hotels")).click();
		//click
		System.out.println("2");
		try {
			//currentFieldFocus('searchby_label');checkRadioAction(this);
			((JavascriptExecutor)driver).executeScript("showSearchParam('reservationnoRow');");
			System.out.println("---");
			//showSearchParam("reservationnoRow");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("searchby_reservationno")));
		driver.findElement(By.id("searchby_reservationno")).click();
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(conHotel.getReservationNumber());
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rowstyle0")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		String id = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]")).getText();
		String[] temp = id.split("-");
		//H9231W020914 
		System.out.println(temp[0].trim());
		System.out.println(conHotel.getReservationNumber());
		customerConfirmationEmail cusCon = new customerConfirmationEmail();
		if(temp[0].trim().equals(conHotel.getReservationNumber().trim()))
		{
			driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]/a/font")).click();
			driver.findElement(By.id("emaillayoutbasedon_customer1")).click();
			
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.switchTo().frame("Myemailformat");
			/*try {
				String temp2 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td")).getText();
				System.out.println(temp2);
				cusCon.setFlag(temp2);
			} catch (Exception ee) {
				// TODO: handle exception
*/				

				try {

					// TODO: handle exception
					temp 	= driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]")).getText().split(":");
					String[] 	temp2 	= temp[1].trim().split("Fax");
					try {
						cusCon.setTelNumber						(temp2[0]);
						temp2 = temp[2].trim().split("Email");
						cusCon.setFax							(temp2[0]);
						temp2 = temp[3].trim().split("Website");
						cusCon.setEmail							(temp2[0]);
						temp = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[1]/td[6]")).getText().split("Web Site : ");
						//System.out.println(temp[0]);
						cusCon.setEmail							(temp[1]);
						cusCon.setBookingReference				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[3]/td")).getText());
						cusCon.setBookingnumber					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr[4]/td")).getText());
						int cnt = 0;
						for(int i = 1 ; i > 0 ; i++ )
						{
							String temp1 = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ i +"]/td[1]")).getText();
							if(temp1.trim().equals("Check In"))
							{
								cnt = i;
								break;
							}
						}
						cusCon.setCheckIn						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ cnt +"]/td[2]")).getText());
						cusCon.setCheckOut						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (cnt+1) +"]/td[2]")).getText());
						cusCon.setBookingStatus					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (cnt+2) +"]/td[2]")).getText());
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					int count = 1;
					System.out.println("cuscon test -- 1");
					for(count = 1 ; count > 0 ; count++)
					{
						try {
							String row = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[1]")).getText();
							if(row.equals("Traveller Detail(s)"))
							{
								break;
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					System.out.println("cuscon test -- 2");
				count = count - 2;
				//System.out.println(count);
				cusCon.setCurrencyCode					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[7]/table/tbody/tr[1]/td[2]")).getText());
				cusCon.setSubTotal						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[7]/table/tbody/tr[2]/td[2]")).getText());
				cusCon.setTaxes							(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[7]/table/tbody/tr[3]/td[2]")).getText());
				cusCon.setTotal							(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[7]/table/tbody/tr[5]/td[2]")).getText());
				cusCon.setPayableNow					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[7]/table/tbody/tr[6]/td[2]")).getText());
				cusCon.setToBePaid						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[7]/table/tbody/tr[7]/td[2]")).getText());
				for(count = 1 ; count > 0 ; count++)
				{
					try {
						String row = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[1]")).getText();
						if(row.equals("Traveller Detail(s)"))
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				System.out.println("cuscon test -- 3");
				cusCon.setFirstName						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+2) +"]/td[2]")).getText());
				cusCon.setLastName						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+3) +"]/td[2]")).getText());
				cusCon.setAddress1						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+4) +"]/td[2]")).getText());
				cusCon.setAddress2						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+5) +"]/td[2]")).getText());
				cusCon.setCity							(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+6) +"]/td[2]")).getText());
				cusCon.setCountry						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+2) +"]/td[8]")).getText());
				cusCon.setPhoneNumber					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+3) +"]/td[8]")).getText());
				cusCon.setEmergency						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+4) +"]/td[8]")).getText());
				cusCon.setEmail							(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count+5) +"]/td[8]")).getText());
				for(count = 1 ; count < 100 ; count++)
				{
					//System.out.println(count);
					try {
						String row = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[1]")).getText();
						if(row.equals("Payment Details"))
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				System.out.println("cuscon test -- 4");
				cusCon.setReferenceNumber				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 1) +"]/td[2]")).getText());
				cusCon.setMerchantTrackId				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td[2]")).getText());
				cusCon.setPaymentId						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 1) +"]/td[8]")).getText());
				cusCon.setAmount						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td[8]")).getText());
				for(count = 1 ; count > 0 ; count++)
				{
				try {
						String row = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[1]")).getText();
						if(row.equals("Hotel Contact Details"))
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				System.out.println("cuscon test -- 5");
				temp 						= 			driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 1) +"]/td")).getText().split(":");
				cusCon.setHotelName						(temp[1]);
				cusCon.setHotelTele						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td[2]")).getText());
				cusCon.setHotelEmail					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 3) +"]/td[2]")).getText());
				for(count = 1 ; count > 0 ; count++)
				{
					try {
						String row = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td")).getText();
						if(row.contains("Email :"))
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				System.out.println("cuscon test -- 6");
				cusCon.setPortal						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 1) +"]/td")).getText());
			
				for(count = 1 ; count > 0 ; count++)
				{
					try {
						String row = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[1]")).getText();
						if(row.contains("Room Occupancy Details"))
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				System.out.println("cuscon test -- 7");
				ArrayList<roomOccypancy> 	occupancyList = new ArrayList<roomOccypancy>();
				for(int i = 3 ; i > 0 ; i++)
				{
					try{
						roomOccypancy occupancy = new roomOccypancy();
						occupancy.setTitle					(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td/table/tbody/tr["+ i +"]/td[2]")).getText());
						occupancy.setFirstname				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td/table/tbody/tr["+ i +"]/td[3]")).getText());
						occupancy.setLastName				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td/table/tbody/tr["+ i +"]/td[4]")).getText());
						occupancy.setChildAge				(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td/table/tbody/tr["+ i +"]/td[5]")).getText());
					occupancyList.add(occupancy);
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
				
					i++;
				}
				System.out.println("cuscon test -- 8");
				cusCon.setOccupancy(occupancyList);
			
			
				for(count = 1 ; count > 0 ; count++)
				{
					try {
						String row = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ count +"]/td[1]")).getText();
						if(row.contains("Booking Status"))
						{
							break;
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				System.out.println("cuscon test -- 9");
				ArrayList<room>     		roomList	=	new ArrayList<room>();
				for(int i = 2 ; i > 0 ; i++)
				{
				
					try {
						room room  = new room();
						room.setRoomType						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td/table/tbody/tr["+ i +"]/td[2]")).getText());
						System.out.println(room.getRoomType());
						room.setMealPlan						(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td/table/tbody/tr["+ i +"]/td[3]")).getText());
						room.setBedType							(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td/table/tbody/tr["+ i +"]/td[4]")).getText());
						room.setTotal							(driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table/tbody/tr["+ (count + 2) +"]/td/table/tbody/tr["+ i +"]/td[7]")).getText());
						roomList.add(room);
					} catch (Exception e) {
						// TODO: handle exception
						break;
					}
				}
				System.out.println("cuscon test -- 10");
				cusCon.setRoom(roomList);
				
					
				} catch (Exception e1) {
					System.out.println("confirmation failed");
				}
			
			}
			System.out.println("cuscon test -- 11");
			return cusCon;
	}
	
	public String thirdpartySupPayableAftrCancellation(WebDriver driver, String url, conhotel conHotel)
	{

		thirdPartySupplierReport report = new thirdPartySupplierReport();
		driver.get(url);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		String value = "";
		for(int  i = 1 ; i > 0 ; i++)
		{
			try {
				((JavascriptExecutor)driver).executeScript("javascript:getReportData('view','"+ i +"');");
				String temp = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[2]")).getText();
				//System.out.println(temp);
				if(temp.equals("") || temp.equals(null) || temp.isEmpty())
				{
					break;
				}
				else
				{
					for(int j = 1 ; j <= 20 ; j++)
					{
						temp = driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[2]")).getText();
						if(temp.trim().equals(conHotel.getReservationNumber().trim()))
						{
							value 		= 		driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[2]")).getText();
							break;
						}
						else
						{
							value = "not found";
						}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			
			//javascript:getReportData('view','2');
		}
		
		return value;
	}
	
	public thirdPartySupplierReport read3rdPartySupplierPayableReport(WebDriver driver, String url, conhotel conHotel)
	{
		thirdPartySupplierReport report = new thirdPartySupplierReport();
		driver.get(url);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		thirdPartySupplierReport third = new thirdPartySupplierReport();
		for(int  i = 1 ; i > 0 ; i++)
		{
			try {
				((JavascriptExecutor)driver).executeScript("javascript:getReportData('view','"+ i +"');");
				String temp = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[2]")).getText();
				//System.out.println(temp);
				if(temp.equals("") || temp.equals(null) || temp.isEmpty())
				{
					break;
				}
				else
				{
					for(int j = 1 ; j <= 20 ; j++)
					{
						temp = driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[2]")).getText();
						if(temp.trim().equals(conHotel.getReservationNumber().trim()))
						{
							third.setBookingnumber		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[2]")).getText());
							third.setDocument			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_2']/td[1]")).getText());
							third.setBookingdate		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_3']/td[1]")).getText());
							third.setProductType		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[3]")).getText());
							third.setProductName		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_2']/td[2]")).getText());
							third.setServiceElementDate	(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_3']/td[2]")).getText());
							third.setSupplierName		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[4]")).getText());
							third.setBookingStatus		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_3']/td[3]")).getText());
							third.setGuestName			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[5]")).getText());
							third.setPortalCurrency		(driver.findElement(By.xpath(".//*[@id='reportHeaderRow']/td[6]")).getText());
							third.setPayableAmount		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[6]")).getText());
							third.setTotalPaid			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_2']/td[4]")).getText());
							third.setBalanceDue			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_3']/td[4]")).getText());
							third.setSupCurrency		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_4']/td[7]")).getText());
							third.setFpayableAmount		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_1']/td[7]")).getText());
							third.setFtotalpaid			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_2']/td[5]")).getText());
							third.setFblanceDue			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ j +"_3']/td[5]")).getText());
							break;
						}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			
			//javascript:getReportData('view','2');
		}
		return third;
	}
	
	public String getHotelAddress(Map<String, String> txt, resHotelAvailability res)
	{
		WebDriver		driver	 	= null;
		initialize 		init		= 	new initialize();
		driver	= init.initialize();
		
		driver.get(txt.get("dmLogging"));
		driver.findElement(By.xpath(".//*[@id='page1']/header/div[1]/div/nav/form/ul/li[2]/input")).sendKeys(txt.get("dmUser"));
		driver.findElement(By.xpath(".//*[@id='page1']/header/div[1]/div/nav/form/ul/li[4]/input")).sendKeys(txt.get("dmPass"));
		driver.findElement(By.xpath(".//*[@id='page1']/header/div[1]/div/nav/form/ul/li[5]/input")).click();
		WebDriverWait wait	 			= new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/img")));
		driver.get(txt.get("dmAddress"));
		res.getItemName();
		String hotelName = "";
		String hotelAddress = "";
		for(int i = 2 ; i > 0 ; i++)
		{
			try {
				hotelName = driver.findElement(By.xpath(".//*[@id='report']/table/tbody/tr["+ i +"]/td[4]")).getText();
				System.out.println(hotelName);
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			
			if(hotelName.equals(res.getItemName()))
			{
				hotelAddress = driver.findElement(By.xpath(".//*[@id='report']/table/tbody/tr["+ i +"]/td[5]")).getText();
				//System.out.println(hotelAddress);
				break;
			}
		}
		driver.close();
		return hotelAddress;
	}
	
	public readAndWrite.bookingListReport bookingListReport(WebDriver driver, String url, conhotel conHotel)
	{
		System.out.println(url);
		driver.get(url);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(conHotel.getReservationNumber());
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rowstyle0")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		System.out.println("test booking list report -- 1");
		readAndWrite.bookingListReport blr = new readAndWrite.bookingListReport();
		String[] temp = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[1]")));
			System.out.println("test booking list report -- 2");
			blr.setResNumber					(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[1]")).getText());
			blr.setResDate						(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[2]")).getText());
			blr.setResTime						(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[3]")).getText());
			blr.setCusType						(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[4]")).getText());
			blr.setCusName						(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[5]")).getText());
			blr.setLeadName						(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[6]")).getText());
			blr.setFirstElement					(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[7]")).getText());
			blr.setDeadLine						(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[8]")).getText());
			blr.setStatus						(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[12]")).getText());
					temp			=			driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[9]")).getText().split("/");
			blr.setCity							(temp[0]);
			blr.setCountry						(temp[1]);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		System.out.println("test booking list report -- 3");
		try {
			blr.setProduct						(driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[10]/table/tbody/tr/td[1]")).getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("test booking list report -- 4");
		
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[18]/a/img")));
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("test booking list report -- 5");
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[18]/a/img")));
			driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[18]/a/img")).click();
			System.out.println("booking list report clicked successfully");
			driver.switchTo().frame("dialogwindow");
			blr.setCusCountry					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]")).getText());
			blr.setCusCity						(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[2]")).getText());
			blr.setSupplier						(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[3]")).getText());
			blr.setHotelName					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[4]")).getText());
			temp   					= 			driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[5]")).getText().split("/");
			blr.setCheckin						(temp[0]);
			blr.setCheckOut						(temp[1]);
			blr.setNumOfRooms					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[6]")).getText());
			blr.setSectorBookingValue			(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText());
			//System.out.println(driver.findElement(By.xpath(".//*[@id='reportHeaderRow1']/td[8]")).getText());
			temp					= 			driver.findElement(By.xpath(".//*[@id='reportHeaderRow1']/td[8]")).getText().split("\\(");
			temp					=			temp[1].split("\\)");
			blr.setCurrencyCode					(temp[0]);
			driver.findElement(By.xpath(".//*[@id='dialogwindowdragdrop']/td[3]/a/img")).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("reportIframe");
			driver.findElement(By.xpath(".//*[@id='reportData1']/tbody/tr[5]/td[1]/a")).click();
			System.out.println("test booking list report -- 6");
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[1]/td[4]")));
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println("test booking list report -- 7");
			bookingcard bc = new bookingcard();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("dialogwindow");
			bc.setReservationNumber				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[1]/td[4]")).getText());
			bc.setOrigin						(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[2]/td[4]")).getText());
			bc.setDestination					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[3]/td[4]")).getText());
			bc.setProductsBooked				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[4]/td[4]")).getText());
			bc.setBookingStatus					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[5]/td[4]")).getText());
			bc.setReservationDate				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[1]/td[7]")).getText());
			bc.setFirstElement					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[2]/td[7]")).getText());
			bc.setCancellationDeadline			(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[3]/td[7]")).getText());
			bc.setCurrencyCode					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[7]/td[7]")).getText());
			bc.setSubTotal						(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[8]/td[7]")).getText());
			bc.setBookingFee					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[9]/td[7]")).getText());
			bc.setTaxAndOther					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[10]/td[7]")).getText());
			bc.setCreditCardFee					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[11]/td[7]")).getText());
			bc.setTotalBookingvalue				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[12]/td[7]")).getText());
			bc.setPayableUpfront				(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[13]/td[7]")).getText());
			bc.setUtilization					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[14]/td[7]")).getText());
			bc.setAmountPaid					(driver.findElement(By.xpath(".//*[@id='reservationDetailsId']/table/tbody/tr[15]/td[7]")).getText());
			bc.setFirstName						(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[1]/td[4]")).getText());
			bc.setAddress						(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[2]/td[4]")).getText());
			bc.setEmail							(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[3]/td[4]")).getText());
			bc.setCity							(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[4]/td[4]")).getText());
			bc.setCountry						(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[5]/td[4]")).getText());
			bc.setEmergencyContact				(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[1]/td[7]")).getText());
			bc.setPhoneNumber					(driver.findElement(By.xpath(".//*[@id='customerDetailsId']/table/tbody/tr/td/table/tbody/tr[2]/td[7]")).getText());
			driver.findElement(By.xpath(".//*[@id='hotelDetailsimgTD']/img")).click();
			bc.setHotelName						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[2]/td[4]")).getText());
			bc.setHotelAddress					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[3]/td[4]")).getText());
			bc.setCheckOut						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[4]/td[4]")).getText());
			bc.setSupplier						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[5]/td[4]")).getText());
			bc.setHotelBookingStatus			(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[2]/td[7]")).getText());
			bc.setCheckin						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[3]/td[7]")).getText());
			bc.setNumOfNyts						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[5]/td[7]")).getText());
			bc.setNoOfRooms						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[6]/td[7]")).getText());
			bc.setHotelContractType				(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[1]/td/table/tbody/tr[7]/td[7]")).getText());
			bc.sethCurrencyCode					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[1]/td[4]")).getText());
			bc.setHsubTotal						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[2]/td[4]")).getText());
			bc.sethTax							(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[3]/td[4]")).getText());
			bc.setHtotalBookingValue			(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[4]/td[4]")).getText());
			bc.sethUpfront						(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[5]/td[4]")).getText());
			bc.sethAmountpaid					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[6]/td[4]")).getText());
			bc.sethDueAtCheckin					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[5]/td/table/tbody/tr[7]/td[4]")).getText());
			System.out.println("test booking list report -- 8");
			ArrayList<bookingReportRoom> brrList = new ArrayList<bookingReportRoom>();
			for(int i = 2 ; i > 0  ; i++)
			{
				try {
					bookingReportRoom brr = new bookingReportRoom();
					brr.setRoomNumber				(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[1]")).getText());
					temp		 		= 			driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[2]")).getText().split("/");
					brr.setRoomType					(temp[0]);
					brr.setBedType					(temp[1]);
					//System.out.println(temp[2]);
					brr.setRatePlan					(temp[2]);
					brr.setCusname					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[3]/table/tbody/tr/td[1]")).getText());
					brr.setAgeDroup					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[3]/table/tbody/tr/td[2]")).getText());
					brr.setChildAge					(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i +"]/td[3]/table/tbody/tr/td[3]")).getText());
					brr.setTotalRate				(driver.findElement(By.xpath(".//*[@id='hotelDetailsId']/table/tbody/tr[2]/td/table/tbody/tr["+ i+"]/td[4]")).getText());
					brrList.add(brr);
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				
			}
			System.out.println("test booking list report -- 9");
			bc.setRoom(brrList);
			blr.setBc(bc);
		
		} catch (Exception e) {
			// TODO: handle exception
		}
			return blr;
	}
	
	public profitAndLostReport profitAndLossReport(String url, WebDriver driver, conhotel conHotel, String[] searchDetails)
	{
		driver.get(url);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
		profitAndLostReport profit = new profitAndLostReport();
		System.out.println("profit and lost report -- 1");
		outerLoop : for(int j = 1 ; j > 0 ; j++)
		{
			System.out.println("profit and lost report -- 1.1");
			((JavascriptExecutor)driver).executeScript("javascript:getReportData('view','"+ j +"');");
			try {
				System.out.println("profit and lost report -- 1.2");
				String temp =  driver.findElement(By.xpath(".//*[@id='RowNo_1_0']/td[1]")).getText();
				System.out.println("profit and lost report -- 1.3");
				if(temp.equals(""))
				{
					break;
				}
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			System.out.println("profit and lost report -- 1.4");
			for(int i = 1 ; i > 0 ; i++)
			{
				String code = "";
				try {System.out.println("profit and lost report -- 1.5");
					code = driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[1]/a")).getText();
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				
				//System.out.println(code);
				//System.out.println(conHotel.getReservationNumber().trim());
				if(code.trim().equals(conHotel.getReservationNumber().trim()))
				{
					System.out.println("profit and lost report -- 1.6");
					profit.setBookingNumber			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[1]")).getText());
					profit.setBookingDate			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_2']/td[1]")).getText());
					profit.setConsultantName		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_3']/td[1]")).getText());
					profit.setCombination			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[2]")).getText());
					profit.setContractType			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_2']/td[2]")).getText());
					profit.setBookingType			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[3]")).getText());
					profit.setAgentName				(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_2']/td[3]")).getText());
					profit.setBookingChannel		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[4]")).getText());
					profit.setPoslocation			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_2']/td[4]")).getText());
					profit.setState					(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_3']/td[4]")).getText());
					profit.setNoOfGuests			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[5]")).getText());
					profit.setChechIn				(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_2']/td[5]")).getText());
					profit.setNumOfNyts				(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_3']/td[5]")).getText());
					profit.setGrossBookingValue		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[6]")).getText());
					profit.setAgentCommission		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_2']/td[6]")).getText());
					profit.setNetBookingValues		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_3']/td[6]")).getText());
					profit.setCostOfSales			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[7]")).getText());
					profit.setProfit				(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_2']/td[7]")).getText());
					profit.setProfitPercentage		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_3']/td[7]")).getText());
					profit.setInvoiceTotal			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[8]")).getText());
					profit.setInvoicrpaid			(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_2']/td[8]")).getText());
					profit.setInvoiceBalanceDue		(driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_3']/td[8]")).getText());
					profit.setBaseCurrency			(driver.findElement(By.xpath(".//*[@id='reportData']/tbody/tr[6]/td[6]")).getText());
					break outerLoop;
					/*
					driver.findElement(By.xpath(".//*[@id='RowNo_"+ i +"_0']/td[1]/a")).click();
					ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
					driver.switchTo().window(tabs.get(1));
					//try {
						rr.setBookingNumber				(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[3]")).getText());
						rr.setBookingDate				(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[1]")).getText());
						rr.setHotelName					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[4]")).getText());
						String[] temp 			=		driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[2]")).getText().split("/");
						rr.setSupplierName				(driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[3]")).getText());
						rr.setBookingType				(temp[0]);
						rr.setChannel					(temp[1]);
						temp					=		driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[2]")).getText().trim().split(" ");
						rr.setFirtName					(temp[0]);
						rr.setLastName					(temp[1]);
						rr.setCustomerType				(driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[4]")).getText());
						temp					=		driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[5]")).getText().split("/");
						rr.setCheckIn					(temp[0]);
						rr.setCheckOut					(temp[1]);
						rr.setRoomType					(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[3]")).getText());
						try {
							temp					=		driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[3]")).getText().split("/");
							rr.setBedType					(temp[0]);
							rr.setRatePlan					(temp[1]);
						} catch (Exception e) {
							// TODO: handle exception
						}
							
						//System.out.println(rr.getRatePlan());
						rr.setTransId					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[6]")).getText());
						rr.setAppCode					(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[4]")).getText());
						rr.setHotelConfirmationNumber	(driver.findElement(By.xpath(".//*[@id='ConfirmNoId_1']")).getText());
						rr.setCurrency					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[7]")).getText());
						rr.setTotalCost					(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[5]")).getText());
						rr.setBookingFee				(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[5]")).getText());
						rr.setOrderValue				(driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[7]")).getText());
						temp					=		driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText().split("/");
						rr.setNumOfRooms				(temp[0]);
						rr.setNumOfNyts					(temp[1]);
						rr.setTotalCostFinal			(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[6]")).getText());
						rr.setOrderValueFinal			(driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[8]")).getText());
						driver.close();
						driver.switchTo().window(tabs.get(0));
						break outerLoop;
					*/}
			}
			
		}
		
		
		return profit;
	}
	
	public String resReportAfterCancellation(WebDriver driver, String url, conhotel conHotel)
	{
		driver.get(url);
		try {
			Alert alert = driver.switchTo().alert();
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("productType_hotels")));
		} catch (Exception e) {
			// TODO: handle exception
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		System.out.println("res report test -- 1");
		driver.findElement(By.id("productType_hotels")).click();
		//driver.manage().timeouts().setScriptTimeout(5,TimeUnit.SECONDS);
		driver.findElement(By.id("searchby_reservationno")).click();
		
		try {
			//currentFieldFocus('searchby_label');checkRadioAction(this);
			((JavascriptExecutor)driver).executeScript("showSearchParam('reservationnoRow');");
			System.out.println("---");
			//showSearchParam("reservationnoRow");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		driver.findElement(By.id("reservationno")).clear();
		System.out.println(conHotel.getReservationNumber());
		driver.findElement(By.id("reservationno")).sendKeys(conHotel.getReservationNumber());
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rowstyle0")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
		
		String value = "";
		try {
			value = driver.findElement(By.xpath(".//*[@id='reportPaging']/table/tbody/tr/td[4]")).getText();
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println(value);
		String[] records = value.split("Page");
		System.out.println(records[0]);
		return records[0].trim();
	}
	
	public readAndWrite.reservationReport reservationReport(String reportUrl, WebDriver driver, conhotel conHotel)
	{
		driver.get(reportUrl);
		try {
			Alert alert = driver.switchTo().alert();
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("productType_hotels")));
		} catch (Exception e) {
			// TODO: handle exception
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		System.out.println("res report test -- 1");
		driver.findElement(By.id("productType_hotels")).click();
		//driver.manage().timeouts().setScriptTimeout(5,TimeUnit.SECONDS);
		driver.findElement(By.id("searchby_reservationno")).click();
		
		//-----------------------------------------------------------------------------------------------------
		
		/*try {
			((JavascriptExecutor)driver).executeScript("$('#showSearchParam(reservationnoRow)';");
			System.out.println("--");

		} catch (Exception e) {
			// TODO: handle exception
		}*/
		try {
			//currentFieldFocus('searchby_label');checkRadioAction(this);
			((JavascriptExecutor)driver).executeScript("showSearchParam('reservationnoRow');");
			System.out.println("---");
			//showSearchParam("reservationnoRow");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		driver.findElement(By.id("reservationno")).clear();
		System.out.println(conHotel.getReservationNumber());
		driver.findElement(By.id("reservationno")).sendKeys(conHotel.getReservationNumber());
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rowstyle0")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
		
		//WebDriverWait wait	 			= new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reportHeaderRow1']/td[3]")));
		//System.out.println(driver.findElement(By.xpath(".//*[@id='reportHeaderRow1']/td[3]")).getText());
		
		readAndWrite.reservationReport rr = new readAndWrite.reservationReport();
		rr.setBookingNumber				(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[3]/a")).getText());
		rr.setBookingDate				(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[1]")).getText());
		rr.setSupplierName				(driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[3]")).getText());
		rr.setHotelName					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[4]")).getText());
		rr.setHotelConfirmationNumber	(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[4]")).getText());
		rr.setTransId					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[6]")).getText());
		String[] temp 		=			driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[2]")).getText().split("/");
		rr.setBookingType				(temp[0]);
		rr.setChannel					(temp[1]);
		temp				=   		driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[2]")).getText().split(" ");
		System.out.println(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[2]")).getText());
		rr.setFirtName					(temp[0]);
		rr.setLastName					(temp[1]);
		rr.setCustomerType				(driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[4]")).getText());
		temp				= 			driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[5]")).getText().split("/");
		rr.setCheckIn					(temp[0]);
		rr.setCheckOut					(temp[1]);
		rr.setRoomType					(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[3]")).getText());
		temp				=			driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[3]")).getText().split("/");
		//System.out.println(temp[0]);
		rr.setBedType					(temp[0]);
		try {
			rr.setRatePlan					(temp[1]);
		} catch (Exception e) {
			// TODO: handle exception
		}
		rr.setHotelContactNumber		(driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[5]")).getText());
		rr.setCurrency					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[7]")).getText());
		rr.setTotalCost					(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[5]")).getText());
		rr.setBookingFee				(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[5]")).getText());
		rr.setOrderValue				(driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[7]")).getText());
		temp				=			driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText().split("/");
		rr.setNumOfRooms				(temp[0]);
		rr.setNumOfNyts					(temp[1]);
		rr.setTotalCostFinal			(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText());
		rr.setOrderValueFinal			(driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[8]")).getText());
		
		return rr;
	}
	
	public ArrayList<String> navigateToResults(WebDriver driver, String hotel, String sup)
	{
		Properties properties = new Properties();
		ArrayList<String> tracerAndHotel = new ArrayList<String>();
		String hName = "";
		//Properties properties1 = new Properties();
		System.out.println("search test -- 1");
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader =new FileReader(new File("/workspace/hotelXmlValidation/hotelAndSupplier.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value = properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		System.out.println("search test -- 2");
		WebDriverWait wait = new WebDriverWait(driver, 5);
		//driver.get(url);
		//System.out.println("ela");

		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		
		String tracer = null;
		String value;
		String style;
		String supplier;
		String error = "";
		String errorType = "";
		String hotelCountHelp = null;
		System.out.println("search test -- 3");
		try {
			driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
			System.out.println("search test -- 3.1");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='pagination_WJ_2']/div[1]")));
			hotelCountHelp	= driver.findElement(By.xpath(".//*[@id='pagination_WJ_2']/div[1]")).getText().trim();
			System.out.println("search test -- 3.2");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("no results found");
		}
		System.out.println("search test -- 3.3");
		String[] pageCountHelp		= hotelCountHelp.split(" ");
		int pageCount = 0;
		int hotelCount				= Integer.parseInt(pageCountHelp[6]);
		if(hotelCount%10 == 0)
		{
			pageCount = hotelCount/10;
		}
		else
		{
			pageCount = hotelCount/10;
			pageCount = pageCount + 1;
		}
		System.out.println("search test -- 4");
		int resultsIterator = 1;
		outerLoop : for(int pageIterator = 1; pageIterator <= pageCount ; pageIterator++)
		{
			try{
				((JavascriptExecutor)driver).executeScript("doIWC(new IWCParams('PAGING','RESULTS','PAGE','', {'pageNo':"+pageIterator+"},'','WJ_2'))");
			}
			catch(Exception e)
			{
				
			}
			try {
				for(int i = 1 ; i <= 10 ; i++)
				{
					
					value = driver.findElement(By.id("hotel_id_" + resultsIterator)).getAttribute("value");
					style = driver.findElement(By.id("title_" + value)).getAttribute("style");
					String name = driver.findElement(By.id("title_" + value)).getText();
					//System.out.println(style);
					if(style.contains("color"))
					{
						supplier = driver.findElement(By.id("moreinfo_" + value)).getAttribute("href");
						try {
							tracer = driver.findElement(By.id("hotelridetracer_" + value)).getAttribute("value");
							//System.out.println(tracer);
						} catch (Exception e) {
							// TODO: handle exception
						}
						//System.out.println(name);
						//System.out.println(hotel);
						if(name.trim().equals(hotel.trim()))
						{
								System.out.println("search test -- 5");
								hName = driver.findElement(By.id("title_" + value)).getText();
								//System.out.println(driver.findElement(By.id("title_" + value)).getText());
								driver.findElement(By.id("addtocart_" + value)).click();
								try {
									System.out.println("search test -- 5.1");
									driver.switchTo().defaultContent();
									System.out.println("search test -- 5.1.1");
									driver.switchTo().frame("live_message_frame");
									System.out.println("search test -- 5.1.2");
									wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialog-warning-message-WJ_22")));
									System.out.println("search test -- 5.1.3");
									//System.out.println("Warning");
									try {
										errorType = driver.findElement(By.id("ui-dialog-title-dialog-warning-message-WJ_22")).getText();
									} catch (Exception e) {
										// TODO: handle exception
									}
									System.out.println("search test -- 5.1.4");
									//System.out.println(errorType);
									error = driver.findElement(By.id("dialog-warning-message-WJ_22")).getText();
									System.out.println("search test -- 5.1.5");
									//System.out.println(error);
									wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[9]/div[3]/div/button[1]")));
									System.out.println("search test -- 5.1.5.1");
									driver.findElement(By.xpath("html/body/div[9]/div[3]/div/button[1]")).click();
									System.out.println("search test -- 5.1.6");
									System.out.println("search test -- 5.2");
									if(error.equals("") || error.equals(null))
									{
										System.out.println("search test -- 5.3");
										error = driver.findElement(By.xpath(".//*[@id='dialog-error-message-WJ_22']")).getText();
									}
									System.out.println(error);
								} catch (Exception e) {
									
										System.out.println("search test -- 5.4");
										if(error.equals(null) || error.equals("") || error.equals(" ") || error.isEmpty())
										{
											try {
												System.out.println("search test -- 5.5");
												errorType = driver.findElement(By.xpath(".//*[@id='ui-dialog-title-dialog-error-message-WJ_22']")).getText();
												System.out.println("search test -- 5.6");
												error = driver.findElement(By.xpath(".//*[@id='dialog-error-message-WJ_22']")).getText();
												System.out.println("search test -- 5.7");
												driver.findElement(By.xpath("html/body/div[9]/div[3]/div/button[1]")).click();
												System.out.println("search test -- 5.8");
											} catch (Exception e2) {
												// TODO: handle exception
												System.out.println("search test -- 5.9");
											}
											
										}
										
								}
								System.out.println("search test -- 6");
								try {
									System.out.println("search test -- 6.1");
									driver.switchTo().defaultContent();
									driver.switchTo().frame("live_message_frame");
									wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialog-error-message-WJ_21")));
									System.out.println("Error");
									System.out.println("search test -- 6.2");
									if(error.equals(null) || error.equals("") || error.equals(" ") || error.isEmpty())
									{
										System.out.println("search test -- 6.3");
										errorType = driver.findElement(By.id("")).getText();
										error = driver.findElement(By.id("dialog-error-message-WJ_21")).getText();
									}	
									System.out.println("search test -- 6.4");
									System.out.println(style + " " + error);
									if(error.equals("") || error.equals(null))
									{
										error = driver.findElement(By.xpath(".//*[@id='dialog-error-message-WJ_21']")).getText();
									}
									System.out.println("search test -- 6.5");
									try {
										driver.findElement(By.xpath("html/body/div[11]/div[3]/div/button[1]")).click();
									} catch (Exception e) {
										System.out.println("search test -- 6.6");
										try {
											driver.findElement(By.className("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only")).click();
										} catch (Exception e2) {
											// TODO: handle exception
										}
									}
									System.out.println("search test -- 6.7");
								} catch (Exception e) {
									// TODO: handle exception
									try {
										System.out.println("search test -- 6.8");
										if(error.equals(null) || error.equals("") || error.equals(" ") || error.isEmpty())
										{
											errorType 		= driver.findElement(By.xpath(".//*[@id='ui-dialog-title-dialog-error-message-WJ_21']")).getText();
											error		 	= driver.findElement(By.xpath(".//*[@id='dialog-error-message-WJ_21']")).getText();
										}
										System.out.println("search test -- 6.9");
									} catch (Exception e2) {
										// TODO: handle exception
									}
									
								}	
								System.out.println("search test -- 7");
								if(error.trim().equals("*Sorry! This product is no longer available. Please choose another."))
								{
									break outerLoop;
								}
								else
								{
									try {
										/*wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialog-error-message-WJ_21")));
										wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loadpayment")));*/
										wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loadpayment")));
										driver.findElement(By.id("loadpayment")).click();
									} catch (Exception e) {
										// TODO: handle exception
									}
								}
								try {
									driver.switchTo().defaultContent();
									driver.switchTo().frame("live_message_frame");
									String errormsg = driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_21")).getText();
									/*if(errormsg.equals("Error"))
									{
										navigator navi = new navigator();
										//navi.navigateToSearch(driver, txtProperty.get("searchUrl"));
									}*/
								} catch (Exception e) {
									// TODO: handle exception
								}
								//System.out.println(error);
								break outerLoop;	
						}
					}
					resultsIterator++;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
		}
		System.out.println("search test -- 8");
		System.out.println(error);
		try {
			if(error.equals(null) || error.equals("") || error.equals(" ") || error.isEmpty())
			{
				error = driver.findElement(By.xpath(".//*[@id='dialog-error-message-WJ_21']")).getText();
				errorType = driver.findElement(By.xpath(".//*[@id='ui-dialog-title-dialog-error-message-WJ_21']")).getText();
			}
			else
			{
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("search test -- 9");
		try {
			driver.findElement(By.xpath("html/body/div[11]/div[11]/div/button")).click();
		} catch (Exception e) {
			// TODO: handle exception
			try {
				driver.findElement(By.xpath("html/body/div[11]/div[1]/a/span")).click();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		System.out.println("search test -- 10");
		try {
			driver.findElement(By.id("loadpayment")).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("search test -- 11");
		try {
			driver.findElement(By.xpath("html/body/div[9]/div[11]/div/button")).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("search test -- 12");
		try {
			driver.findElement(By.xpath("html/body/div[11]/div[3]/div/button[1]")).click();
		} catch (Exception e) {
			// TODO: handle exception
			try {
				driver.findElement(By.className("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only")).click();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		System.out.println("search test -- 13");
		tracerAndHotel.add(tracer);
		tracerAndHotel.add(hName);
		tracerAndHotel.add(error);
		tracerAndHotel.add(errorType);
		return tracerAndHotel;
	}
	
	public cancellationDetails cancellation(WebDriver driver, String url, String resNumber, Map<String, String> cancellation)
	{
		try {
			Alert alert = driver.switchTo().alert();
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		driver.get(url);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		try {
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		driver.findElement			(By.id("reservationId")).sendKeys(resNumber);
		driver.findElement			(By.id("reservationId_lkup")).click();
		driver.switchTo().frame("lookup");
		driver.findElement			(By.className("rezgLook0")).click();
		driver.switchTo().defaultContent();
		driver.findElement			(By.id("bkgcancel")).click();
		
		cancellationDetails cd = new cancellationDetails();
		cd.setLastname							(driver.findElement(By.id("custname")).getText());
		cd.setBookingNumber						(driver.findElement(By.id("reservationno")).getText());
		cd.setBookingDate						(driver.findElement(By.id("bookingdate")).getText());
		cd.setToName							(driver.findElement(By.id("toname")).getText());
		cd.setBookingChanel						(driver.findElement(By.id("bookingchannel")).getText());
		cd.setSubTotal							(driver.findElement(By.id("subtotal")).getText());
		cd.setTotalPayable						(driver.findElement(By.id("totalpayable")).getText());
		cd.setHotelName							(driver.findElement(By.id("hotelname")).getText());
		cd.setAddress							(driver.findElement(By.id("hoteladd")).getText());
		cd.setCheckin							(driver.findElement(By.id("hotelarrivaldate")).getText());
		cd.setCheckout							(driver.findElement(By.id("hoteldepdate")).getText());
		cd.setAdults							(driver.findElement(By.id("hotelnoofadults")).getText());
		cd.setCity								(driver.findElement(By.id("hotellocation")).getText());
		cd.setNytsNum							(driver.findElement(By.id("hotelnonofnights")).getText());
		cd.setRoomsNum							(driver.findElement(By.id("hotelnoofrooms")).getText());
		cd.setChildren							(driver.findElement(By.id("hoteltotchildren")).getText());
		cd.setHotelCancellationCharge			(driver.findElement(By.id("hotelcancellationfeearray[0]")).getText());
		cd.setCustomerCancellationCharge		(driver.findElement(By.id("customercancellationfeearray[0]")).getText());
		cd.setTotalHotelCancellationCharge		(driver.findElement(By.id("totalHotelCancelfee")).getText());
		cd.setTotalCustomerCancellationCharge	(driver.findElement(By.id("totalCustomerCancelfee")).getText());
		cd.setAdditionalCancellationCharge		(driver.findElement(By.id("additionalcancellationfeearray[0]")).getText());
		cd.setGstCharge							(driver.findElement(By.id("gstchargearray[0]")).getText());
		cd.setTotalCharge						(driver.findElement(By.id("totalcancellationfeearray[0]")).getText());
		//String cc								= driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/vlh:root/table[5]/tbody/tr[6]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[1]")).getText();
		
		ArrayList<cancellationRoomOccupancy> occupancy = new ArrayList<cancellationRoomOccupancy>();
		for(int i = 1 ; i > 0 ; i++)
		{
			try {
				
				ArrayList<WebElement> title = new ArrayList<WebElement>();
				ArrayList<WebElement> fname = new ArrayList<WebElement>();
				ArrayList<WebElement> lname = new ArrayList<WebElement>();
				
				title.addAll						(driver.findElements(By.id("custitle_" + i)));
				fname.addAll						(driver.findElements(By.id("cusfname_" + i)));
				lname.addAll						(driver.findElements(By.id("cuslname_" + i)));

				if(title.size() == 0)
				{
					break;
				}
				for(int j = 0 ; j < title.size() ; j++)
				{
					cancellationRoomOccupancy cro =new cancellationRoomOccupancy();
					cro.setTitle(title.get(j).getText());
					cro.setFirstName(fname.get(j).getText());
					cro.setLastName(lname.get(j).getText());
					occupancy.add(cro);
				}
				/*cro.setTitle						(driver.findElement(By.id("custitle_" + i)).getText());
				cro.setLastName						(driver.findElement(By.id("cuslname_" + i)).getText());
				cro.setFirstName					(driver.findElement(By.id("cusfname_" + i)).getText());*/
				
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
		}
		cd.setRoomOccupancy(occupancy);
		driver.findElement(By.id("customercancellationfeearray[0]")).clear();
		driver.findElement(By.id("customercancellationfeearray[0]")).sendKeys(cancellation.get("cancellationCharge"));
		driver.findElement(By.id("additionalcancellationfeearray[0]")).clear();
		driver.findElement(By.id("additionalcancellationfeearray[0]")).sendKeys(cancellation.get("additional"));
		driver.findElement(By.id("gstchargearray[0]")).clear();
		driver.findElement(By.id("gstchargearray[0]")).sendKeys(cancellation.get("gst"));
		
		/*ArrayList<cancelltionRateDetails> rate = new ArrayList<cancelltionRateDetails>();
		for(int i = 1 ; i > 0 ; i++)
		{
			try {
				cancelltionRateDetails crd = new cancelltionRateDetails();
				//title_141690-5
			} catch (Exception e) {						
				// TODO: handle exception
			}
		}*/
		 
		driver.findElement(By.id("saveButId")).click();
		cd.setMsgType			(driver.findElement(By.id("dialogMsgBox_msgBoxTitle")).getText());
		cd.setMsg				(driver.findElement(By.id("dialogMsgText")).getText());
		System.out.println(cd.getMsgType());
		System.out.println(cd.getMsg());
		try {
			if(cd.getMsg().equals(""))
			{
				cd.setMsg			(driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText());
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
		return cd;
	}
	
	public cancellationReport cancellationreport(WebDriver driver, String resNum, String url)
	{
		driver.get(url);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.id("productType_hotels")).click();
		driver.findElement(By.id("searchby_reservationno")).click();
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(resNum);
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		driver.findElement(By.className("rowstyle0")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("reportIframe");
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[5]/a/img")).click();
		
		cancellationReport cr = new cancellationReport();
		cr.setCanNumber					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[2]")).getText());
		cr.setCanDate					(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[1]")).getText());
		cr.setCanNotes					(driver.findElement(By.xpath(".//*[@id='NotesId_1']")).getText());
		cr.setBookingnum				(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[3]")).getText());
		cr.setDocumentNum				(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[2]")).getText());
		cr.setCancelby					(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[2]")).getText());
		cr.setCancellationreason		(driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[2]")).getText());
		cr.setHotelname					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[4]")).getText());
		cr.setSuppliername				(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[3]")).getText());
		cr.setSuppliernumber			(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[3]")).getText());
		cr.setNumOfRooms				(driver.findElement(By.xpath(".//*[@id='reportHeaderRow1']/td[5]")).getText());
		cr.setNumOfnyts					(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[4]")).getText());
		String[] temp   		=		driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[6]")).getText().split(" ");
		cr.setCuslastName				(temp[0]);
		cr.setCusFirstName				(temp[1]);
		temp 					=		driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[5]")).getText().split("/");
		cr.setCheckin					(temp[0]);
		cr.setCheckout					(temp[1]);
		cr.setBookingDate				(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[5]")).getText());
		cr.setRoomType					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[7]")).getText());
		cr.setBedType					(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[6]")).getText());
		cr.setRatePlan					(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[6]")).getText());
		cr.setSellingCurrency			(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText());
		cr.setTotalCost					(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[9]")).getText());
		cr.setTotalCancellationFee		(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[7]")).getText());
		cr.setAdditionalFee				(driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[7]")).getText());
		cr.setTotalCostUsd				(driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[10]")).getText());
		cr.setTotalCancelationFeeUsd	(driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[8]")).getText());
		
		return cr;
	}
}
