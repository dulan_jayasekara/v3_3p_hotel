package navigation;

import java.util.ArrayList;

import readXml.hbPage1Req;
import readXml.hotelAvailability;
import readXml.readRequest;
import readXml.resHotelAvailability;
import urlGenarator.xmlHome;

public class errorMsgHandler {

	public void resultsPageError(StringBuffer ReportPrinter, xmlHome xmlUrl, String xmlHomeUrl, xmlNavigator xmlNav, String tracer, readRequest rr, String error, String errorType, String sup)
	{
		if(sup.equals("inv27"))
		{
			if(error.equals("*Sorry, rates got changed. please search again!"))
			{
				//Read page1 xml response
				ArrayList<resHotelAvailability>	resAvailability			=  	new ArrayList<resHotelAvailability>();
				String 					inv13Page1ResUrl 				= 	xmlUrl.inv27Page1Res(tracer);
				String 					resPageSource					=	xmlNav.navigater(inv13Page1ResUrl, xmlHomeUrl, "inv27", 0);
										resAvailability 				= 	rr.inv27Page1Response(resPageSource);

				//read room availability res
				resHotelAvailability 	resRoomAvailability 				= 	new resHotelAvailability();
				String					inv27RoomAvailabilityRes 			= 	xmlUrl.inv27roomAvailabilityRes(tracer);
				String					inv27RoomAvailabilityResPageSource 	= 	xmlNav.navigater(inv27RoomAvailabilityRes, xmlHomeUrl, "inv27", 0);			
										resRoomAvailability 				= 	rr.inv27RoomAvailabilityRes(inv27RoomAvailabilityResPageSource);
			
				for(int i = 0 ; i < resRoomAvailability.getRc().size() ; i++)
				{
					if(resRoomAvailability.getItemName().equals(resAvailability.get(i).getHotelName()))
					{
						System.out.println(resRoomAvailability.getItemName());
						System.out.println(resAvailability.get(i).getHotelName());
					}
				}
			}
		}
		
		if(sup.equals("hb"))
		{
			if(error.equals("*Sorry! This product is no longer available. Please choose another."))
			{
				String reqError = "";
				String resError = "";
				//read room availability req
				hbPage1Req  			roomAvailabilityReq 			=  	new hbPage1Req();
				String 					hbRoomAvailabilityReq 			= 	xmlUrl.hbRoomAvailabilityReq(tracer);
				String					hbRoomAvailabilityReqPageSource = 	"" ; 
				hbRoomAvailabilityReqPageSource = 	xmlNav.navigater(hbRoomAvailabilityReq, xmlHomeUrl, "hb", 0);
				try {
										roomAvailabilityReq 			= 	rr.hbRoomaAvailabilityReq(hbRoomAvailabilityReqPageSource);
				} catch (Exception e) {
					// TODO: handle exception
					reqError = "Room Availability Request Error";
				}
				
				//read room availability res
				resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
				String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
				String					hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "hb", 0);	
				try {
										resRoomAvailability 			= 	rr.readResponse(hbRoomAvailabilityResPageSource);
				} catch (Exception e) {
					resError = "Room Availability Response Error";
					// TODO: handle exception
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>Xml Error</span>"
							+ "<br>");
				}
				System.out.println(resRoomAvailability.getError());
				try {
					System.out.println(resRoomAvailability.getTotalItems());
					System.out.println(error);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
					if(resRoomAvailability.getTotalItems().equals("0") && error.equals("*Sorry! This product is no longer available. Please choose another."))
					{
						ReportPrinter.append("<br>"
								+ "<span>"+ error +"</span>"
								+ "<br>"
								+ "<span>No results available</span>"
								+ "<br>");
					}
					else if(error.equals("*Sorry! This product is no longer available. Please choose another."))
					{
						if(reqError.equals("Room Availability Request Error") && resError.equals("Room Availability Response Error"))
						{
							ReportPrinter.append("<br>"
									+ "<span>"+ error +"</span>"
									+ "<br>"
									+ "<span>Room Availability Response and Request Error</span>"
									+ "<br>");
						}
						else if(reqError.equals("Room Availability Request Error"))
						{
							ReportPrinter.append("<br>"
									+ "<span>"+ error +"</span>"
									+ "<br>"
									+ "<span>Room Availability Request Error</span>"
									+ "<br>");
						}
						else if(resError.equals("Room Availability Response Error"))
						{
							ReportPrinter.append("<br>"
									+ "<span>"+ error +"</span>"
									+ "<br>"
									+ "<span>Room Availability Response Error</span>"
									+ "<br>");
						}
						else
						{
							
						}
					}
				
				if(error.equals("*Sorry! This product is no longer available. Please choose another.") &&  resRoomAvailability.getError().contains(". This TO can not make reservations."))
				{
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>"+ resRoomAvailability.getError() +"</span>"
							+ "<br>");
				}

			}
			else if(error.equals("*Sorry, rates got changed. please search again!"))
			{

				//read room availability res
				String hbRoomAvailabilityResPageSource = "";
				resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
				String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
										hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "hb", 0);
				
				try {
					resRoomAvailability 			= 	rr.readResponse(hbRoomAvailabilityResPageSource);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				//read room availability req
				hbPage1Req  			roomAvailabilityReq 			=  	new hbPage1Req();
				String 					hbRoomAvailabilityReq 			= 	xmlUrl.hbRoomAvailabilityReq(tracer);
				String					hbRoomAvailabilityReqPageSource = 	"" ; 
										hbRoomAvailabilityReqPageSource = 	xmlNav.navigater(hbRoomAvailabilityReq, xmlHomeUrl, "hb", 0);
				
				try{
										roomAvailabilityReq 			= 	rr.hbRoomaAvailabilityReq(hbRoomAvailabilityReqPageSource);
				} catch (Exception e) {
					// TODO: handle exception
				}
				if(hbRoomAvailabilityResPageSource.equals(null) && hbRoomAvailabilityReqPageSource.equals(null))
				{
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>Room availabity request and response both not available</span>"
							+ "<br>");
				}
				else if(hbRoomAvailabilityResPageSource.equals(null))
				{
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>Room availabity response not available</span>"
							+ "<br>");
				}
				else if(hbRoomAvailabilityReqPageSource.equals(null))
				{
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>Room availabity request not available</span>"
							+ "<br>");
				}
				else
				{
					
				}
				System.out.println(hbRoomAvailabilityResPageSource);
				System.out.println(resRoomAvailability.getTotalAmount());
			}
		}
		
	}
	
	public void paymentError(StringBuffer ReportPrinter, String paymentError, xmlHome xmlUrl, xmlNavigator xmlNav, String tracer, String xmlHomeUrl, readRequest rr)
	{
		String resPageSource = "";
		String xmlError = "";
		hotelAvailability 		reqAvailabilityFinalReq 		=	new hotelAvailability();
		resHotelAvailability	resAvailabilityFinalRes 		= 	new resHotelAvailability();
		if(paymentError.equals("*BLOCKER~Hotel confirmation failed."))
		{
			try {
				//pre reservation request
				hotelAvailability 		preReqAvailability 				=	new hotelAvailability();
				String					preReqUrl						= 	xmlUrl.hbPreReq(tracer);
										resPageSource 					= 	xmlNav.navigater(preReqUrl, xmlHomeUrl, "hb", 0);
										preReqAvailability 				= 	rr.readPreRequest(resPageSource);
			} catch (Exception e) {
				// TODO: handle exception
				xmlError = "Pre reservation request error";
			}
			try {
				//pre reservation response
				resHotelAvailability 	preResAvailability 				= 	new resHotelAvailability();
				String					preResUrl						= 	xmlUrl.hbPreRes(tracer);
										resPageSource 					= 	xmlNav.navigater(preResUrl, xmlHomeUrl, "hb", 0);
										preResAvailability				=	rr.readResponse(resPageSource);
			} catch (Exception e) {
				// TODO: handle exception
				xmlError = "Pre reservation response Error";
			}
			
			try {
				//final reservation request
				String 					finalReqUrl 					= 	xmlUrl.hbFinalReq(tracer);
										resPageSource 					= 	xmlNav.navigater(finalReqUrl, xmlHomeUrl, "hb", 0);
										reqAvailabilityFinalReq 		= 	rr.readRequest(resPageSource);
			} catch (Exception e) {
				// TODO: handle exception
				xmlError = "Final reservation request Error";
			}
			
				
			//final reservation response
			String 					finalResUrl 					= 	xmlUrl.hbFinalRes(tracer);
			try {
				resPageSource 					= 	xmlNav.navigater(finalResUrl, xmlHomeUrl, "hb", 0);
				resAvailabilityFinalRes 		= 	rr.readFinalResponse(resPageSource);
			} catch (Exception e) {
				// TODO: handle exception
				xmlError = "Final Reservation Response Error";
				
			}
			
			
		}
		System.out.println(resAvailabilityFinalRes.getStatus());
		System.out.println(xmlError);
		
		if(xmlError.equals(""))
		{
			ReportPrinter.append("<br>"
					+ "<span class='Failed'>Error Message -: "+ paymentError +"</span>"
					+ "<br>"
					+ "<br>"
					+ "<span class='Failed'>Xml Error -: "+ xmlError +"</span>"
					+ "<br>");
		}
		else
		{
			ReportPrinter.append("<br>"
					+ "<span class='passed'>Error Message -: "+ paymentError +"</span>"
					+ "<br>"
					+ "<br>"
					+ "<span class='passed'>Xml Error -: "+ xmlError +"</span>"
					+ "<br>");
		}
	}
	
	
}
