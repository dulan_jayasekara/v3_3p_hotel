package readConfirmation;

public class roomOccupancy {

	String title =""; 
	String firstName =""; 
	String lastName =""; 
	String smoking =""; 
	String handicap =""; 
	String wheelChair ="";
	String roomNumber = "";

	
	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the smoking
	 */
	public String getSmoking() {
		return smoking;
	}

	/**
	 * @param smoking the smoking to set
	 */
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}

	/**
	 * @return the handicap
	 */
	public String getHandicap() {
		return handicap;
	}

	/**
	 * @param handicap the handicap to set
	 */
	public void setHandicap(String handicap) {
		this.handicap = handicap;
	}

	/**
	 * @return the wheelChair
	 */
	public String getWheelChair() {
		return wheelChair;
	}

	/**
	 * @param wheelChair the wheelChair to set
	 */
	public void setWheelChair(String wheelChair) {
		this.wheelChair = wheelChair;
	}
	
	
	
}
