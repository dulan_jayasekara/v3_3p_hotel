package readConfirmation;

import java.util.ArrayList;
import readConfirmation.*;
public class conhotel {

	
	String bookingStatus 		= "";
	String reservationNumber	= "";
	String hotelName			= "";
	String address				= "";
	String checkIn				= "";
	String checkout				= "";
	String nights				= "";
	String rooms				= ""; 
	String hotelBookingStatus	= ""; 
	String supConfirmationNumber= "";
	String cancellationDeadLine	= "";
	String currencyCode			= "";
	String total				= "";
	String errorType			= "";
	String error				= "";
	String supConfirmation		= "";
	String referenceNumber   	= "";
	String merchantTrackId 		= "";
	String paymentid			= "";
	String amount 				= "";

	ArrayList<room> 		room 		= new ArrayList<room>();
	customerDetails 		customer 	= new customerDetails();
	ArrayList<roomOccupancy>	occypancy2	= new ArrayList<roomOccupancy>();
	ArrayList<ArrayList<roomOccupancy>> occypancy = new ArrayList<ArrayList<roomOccupancy>>();
	public ArrayList<ArrayList<roomOccupancy>> getOccypancy() {
		return occypancy;
	}
	public void setOccypancy(ArrayList<ArrayList<roomOccupancy>> occypancy) {
		this.occypancy = occypancy;
	}


	onlinePaymentDetails	online		= new onlinePaymentDetails();
	payments				pay			= new payments();

	
	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getMerchantTrackId() {
		return merchantTrackId;
	}
	public void setMerchantTrackId(String merchantTrackId) {
		this.merchantTrackId = merchantTrackId;
	}
	public String getPaymentid() {
		return paymentid;
	}
	public void setPaymentid(String paymentid) {
		this.paymentid = paymentid;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the supConfirmation
	 */
	public String getSupConfirmation() {
		return supConfirmation;
	}
	/**
	 * @param supConfirmation the supConfirmation to set
	 */
	public void setSupConfirmation(String supConfirmation) {
		this.supConfirmation = supConfirmation;
	}
	/**
	 * @return the pay
	 */
	public payments getPay() {
		return pay;
	}
	/**
	 * @param pay the pay to set
	 */
	public void setPay(payments pay) {
		this.pay = pay;
	}
	/**
	 * @return the bookingStatus
	 */
	public String getBookingStatus() {
		return bookingStatus;
	}
	/**
	 * @param bookingStatus the bookingStatus to set
	 */
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	/**
	 * @return the reservationNumber
	 */
	public String getReservationNumber() {
		return reservationNumber;
	}
	/**
	 * @param reservationNumber the reservationNumber to set
	 */
	public void setReservationNumber(String reservationNumber) {
		this.reservationNumber = reservationNumber;
	}
	/**
	 * @return the hotelName
	 */
	public String getHotelName() {
		return hotelName;
	}
	/**
	 * @param hotelName the hotelName to set
	 */
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the checkIn
	 */
	public String getCheckIn() {
		return checkIn;
	}
	/**
	 * @param checkIn the checkIn to set
	 */
	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}
	/**
	 * @return the checkout
	 */
	public String getCheckout() {
		return checkout;
	}
	/**
	 * @param checkout the checkout to set
	 */
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	/**
	 * @return the nights
	 */
	public String getNights() {
		return nights;
	}
	/**
	 * @param nights the nights to set
	 */
	public void setNights(String nights) {
		this.nights = nights;
	}
	/**
	 * @return the rooms
	 */
	public String getRooms() {
		return rooms;
	}
	/**
	 * @param rooms the rooms to set
	 */
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	/**
	 * @return the hotelBookingStatus
	 */
	public String getHotelBookingStatus() {
		return hotelBookingStatus;
	}
	/**
	 * @param hotelBookingStatus the hotelBookingStatus to set
	 */
	public void setHotelBookingStatus(String hotelBookingStatus) {
		this.hotelBookingStatus = hotelBookingStatus;
	}
	/**
	 * @return the supConfirmationNumber
	 */
	public String getSupConfirmationNumber() {
		return supConfirmationNumber;
	}
	/**
	 * @param supConfirmationNumber the supConfirmationNumber to set
	 */
	public void setSupConfirmationNumber(String supConfirmationNumber) {
		this.supConfirmationNumber = supConfirmationNumber;
	}
	/**
	 * @return the cancellationDeadLine
	 */
	public String getCancellationDeadLine() {
		return cancellationDeadLine;
	}
	/**
	 * @param cancellationDeadLine the cancellationDeadLine to set
	 */
	public void setCancellationDeadLine(String cancellationDeadLine) {
		this.cancellationDeadLine = cancellationDeadLine;
	}
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}
	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}
	/**
	 * @return the errorType
	 */
	public String getErrorType() {
		return errorType;
	}
	/**
	 * @param errorType the errorType to set
	 */
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	/**
	 * @return the room
	 */
	public ArrayList<room> getRoom() {
		return room;
	}
	/**
	 * @param room the room to set
	 */
	public void setRoom(ArrayList<room> room) {
		this.room = room;
	}
	/**
	 * @return the customer
	 */
	public customerDetails getCustomer() {
		return customer;
	}
	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(customerDetails customer) {
		this.customer = customer;
	}
	/**
	 * @return the occypancy
	 */
	
	/**
	 * @return the online
	 */
	public onlinePaymentDetails getOnline() {
		return online;
	}
	
	public ArrayList<roomOccupancy> getOccypancy2() {
		return occypancy2;
	}
	public void setOccypancy2(ArrayList<roomOccupancy> occypancy2) {
		this.occypancy2 = occypancy2;
	}
	/**
	 * @param online the online to set
	 */
	public void setOnline(onlinePaymentDetails online) {
		this.online = online;
	}
	
	
	public static void main(String[] args) {
		conhotel ht = new conhotel();
		
		ht.setBookingStatus("Confirmed");
		
		System.out.println(ht.getBookingStatus());
	}
	
	
}
