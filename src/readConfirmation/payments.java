package readConfirmation;

public class payments {
	
	String subTotal =""; 
	String currencyCode  =""; 
	String total =""; 
	String totalPackageBookingValue ="";   
	String totalTaxAndServiceCharges =""; 
	String totalHotelBookingValue =""; 
	String totalGrossPackageBookingValue =""; 
	String totalTaxesAndOtherCharges =""; 
	String totalBookingValue =""; 
	String amountbeingProcessedNow =""; 
	String amountdueatCheckInUtilization ="";

	
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}

	/**
	 * @return the totalPackageBookingValue
	 */
	public String getTotalPackageBookingValue() {
		return totalPackageBookingValue;
	}

	/**
	 * @param totalPackageBookingValue the totalPackageBookingValue to set
	 */
	public void setTotalPackageBookingValue(String totalPackageBookingValue) {
		this.totalPackageBookingValue = totalPackageBookingValue;
	}

	/**
	 * @return the subTotal
	 */
	public String getSubTotal() {
		return subTotal;
	}

	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	/**
	 * @return the totalTaxAndServiceCharges
	 */
	public String getTotalTaxAndServiceCharges() {
		return totalTaxAndServiceCharges;
	}

	/**
	 * @param totalTaxAndServiceCharges the totalTaxAndServiceCharges to set
	 */
	public void setTotalTaxAndServiceCharges(String totalTaxAndServiceCharges) {
		this.totalTaxAndServiceCharges = totalTaxAndServiceCharges;
	}

	/**
	 * @return the totalHotelBookingValue
	 */
	public String getTotalHotelBookingValue() {
		return totalHotelBookingValue;
	}

	/**
	 * @param totalHotelBookingValue the totalHotelBookingValue to set
	 */
	public void setTotalHotelBookingValue(String totalHotelBookingValue) {
		this.totalHotelBookingValue = totalHotelBookingValue;
	}

	/**
	 * @return the totalGrossPackageBookingValue
	 */
	public String getTotalGrossPackageBookingValue() {
		return totalGrossPackageBookingValue;
	}

	/**
	 * @param totalGrossPackageBookingValue the totalGrossPackageBookingValue to set
	 */
	public void setTotalGrossPackageBookingValue(
			String totalGrossPackageBookingValue) {
		this.totalGrossPackageBookingValue = totalGrossPackageBookingValue;
	}

	/**
	 * @return the totalTaxesAndOtherCharges
	 */
	public String getTotalTaxesAndOtherCharges() {
		return totalTaxesAndOtherCharges;
	}

	/**
	 * @param totalTaxesAndOtherCharges the totalTaxesAndOtherCharges to set
	 */
	public void setTotalTaxesAndOtherCharges(String totalTaxesAndOtherCharges) {
		this.totalTaxesAndOtherCharges = totalTaxesAndOtherCharges;
	}

	/**
	 * @return the totalBookingValue
	 */
	public String getTotalBookingValue() {
		return totalBookingValue;
	}

	/**
	 * @param totalBookingValue the totalBookingValue to set
	 */
	public void setTotalBookingValue(String totalBookingValue) {
		this.totalBookingValue = totalBookingValue;
	}

	/**
	 * @return the amountbeingProcessedNow
	 */
	public String getAmountbeingProcessedNow() {
		return amountbeingProcessedNow;
	}

	/**
	 * @param amountbeingProcessedNow the amountbeingProcessedNow to set
	 */
	public void setAmountbeingProcessedNow(String amountbeingProcessedNow) {
		this.amountbeingProcessedNow = amountbeingProcessedNow;
	}

	/**
	 * @return the amountdueatCheckInUtilization
	 */
	public String getAmountdueatCheckInUtilization() {
		return amountdueatCheckInUtilization;
	}

	/**
	 * @param amountdueatCheckInUtilization the amountdueatCheckInUtilization to set
	 */
	public void setAmountdueatCheckInUtilization(
			String amountdueatCheckInUtilization) {
		this.amountdueatCheckInUtilization = amountdueatCheckInUtilization;
	}

	
	
	

}
