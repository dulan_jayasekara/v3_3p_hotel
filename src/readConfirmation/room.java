package readConfirmation;

public class room {

	String room =""; 
	String roomType =""; 
	String boardType =""; 
	String bedType =""; 
	String totalRate =""; 
	String adultCount =""; 
	String childCount ="";

	/**
	 * @return the room
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * @return the adultCount
	 */
	public String getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount the adultCount to set
	 */
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}

	/**
	 * @return the childCount
	 */
	public String getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount the childCount to set
	 */
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}

	/**
	 * @param room the room to set
	 */
	public void setRoom(String room) {
		this.room = room;
	}

	/**
	 * @return the roomType
	 */
	public String getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return the boardType
	 */
	public String getBoardType() {
		return boardType;
	}

	/**
	 * @param boardType the boardType to set
	 */
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}

	/**
	 * @return the bedType
	 */
	public String getBedType() {
		return bedType;
	}

	/**
	 * @param bedType the bedType to set
	 */
	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	/**
	 * @return the totalRate
	 */
	public String getTotalRate() {
		return totalRate;
	}

	/**
	 * @param totalRate the totalRate to set
	 */
	public void setTotalRate(String totalRate) {
		this.totalRate = totalRate;
	}
	
	
}
