package readConfirmation;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import readXml.resHotelAvailability;

public class confirmationRead {
	
	public conhotel confirmationReadWeb(WebDriver driver, resHotelAvailability resAvailability, StringBuffer ReportPrinter)
	{
		conhotel				hotel		= new conhotel();
		customerDetails			customer 	= new customerDetails();
		onlinePaymentDetails	online		= new onlinePaymentDetails();
		payments				pay			= new payments();
		room					room		= new room();
		
		hotel.setHotelBookingStatus		(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[1]/div[2]/span")).getText());
		String[] temp 		=			driver.findElement(By.xpath(".//*[@id='main_content']/div[2]/div")).getText().split(":"); 
		temp				=			temp[2].trim().split("A");
		hotel.setReservationNumber		(temp[0]);
		System.out.println("---" + hotel.getReservationNumber());
		hotel.setHotelBookingStatus		(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[1]/div[2]")).getText());
		hotel.setHotelName				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[1]/div[1]/div[1]/h2")).getText());
		hotel.setAddress				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[1]/div[1]/div[3]/h3")).getText());
		try {
			hotel.setSupConfirmationNumber	(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[1]/div[3]/b")).getText());
		} catch (Exception e) {
			// TODO: handle exception
		}
		String dateHelp		= 			driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]")).getText();
		String[] checkin 	=			dateHelp.split("Date");
		String checkOut 	= 			checkin[2].trim();
		checkin 			= 			checkin[1].split("CheckOut");
		hotel.setCheckIn(checkin[0].trim());
		hotel.setCheckout(checkOut);
		hotel.setCurrencyCode			(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[2]/div[2]/div/div[2]")).getText());
		customer.setFirstName			(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div[1]/ul/li[1]/div[2]")).getText());
		customer.setLastName			(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div[1]/ul/li[2]/div[2]")).getText());
		customer.setAddress				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div[1]/ul/li[3]/div[2]")).getText());
		customer.setCity				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div[1]/ul/li[5]/div[2]")).getText());
		customer.setCountry				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div[1]/ul/li[6]/div[2]")).getText());
		customer.setPhoneNumber			(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div[2]/ul/li[2]/div[2]")).getText());
		customer.setEmail				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[2]/div[2]/ul/li[4]/div[2]")).getText());
		
		hotel.setCustomer(customer);
		
		//hotel.setCheckIn				();
		//hotel.setCheckout				();
		ArrayList<room> roomList = new ArrayList<room>();
		for(int i = 2 ; i > 0 ; i++)
		{
			try {
				room.setRoom						(driver.findElement(By.xpath	(".//*[@id='tripsummary']/div[1]/div[3]/table/tbody/tr["+ i +"]/td[1]")).getText());
				room.setRoomType					(driver.findElement(By.xpath	(".//*[@id='tripsummary']/div[1]/div[3]/table/tbody/tr["+ i +"]/td[2]")).getText());
				System.out.println(room.getRoomType());
				room.setBoardType					(driver.findElement(By.xpath	(".//*[@id='tripsummary']/div[1]/div[3]/table/tbody/tr["+ i +"]/td[3]")).getText());
				room.setBedType						(driver.findElement(By.xpath	(".//*[@id='tripsummary']/div[1]/div[3]/table/tbody/tr["+ i +"]/td[4]")).getText());
				String[] count						= driver.findElement(By.xpath	(".//*[@id='tripsummary']/div[1]/div[3]/table/tbody/tr["+ i +"]/td[1]")).getText().split("s");
				String[] adultCount					= count[1].split(" ");
				room.setAdultCount					(adultCount[0]);
				try {
					String[] childCount					= count[1].split("n");
					childCount							= childCount[1].split(" ");
					room.setChildCount					(childCount[0]);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				roomList.add(room);
			} catch (Exception e) {
				// TODO: handle exception
				break;
			}
			
			i++;
		}
		hotel.setRoom(roomList);
		
		ArrayList<ArrayList<roomOccupancy>> occlistBig = new ArrayList<ArrayList<roomOccupancy>>();
		int fname = 3;
		int lname = 4;
		int flag = 4;
		for(int i = 1 ; i < 5 ; i++)
		{
			ArrayList<roomOccupancy> occList = new ArrayList<roomOccupancy>();
			for(int j = 0 ; j > -1 ; j++)
			{
				try {
					roomOccupancy			occupancy	= new roomOccupancy();
					occupancy.setRoomNumber	(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[2]/div/div[2]/div["+ flag +"]/div["+ (fname-2) +"]")).getText());
					occupancy.setTitle		(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[2]/div/div[2]/div["+ flag +"]/div["+ (fname-1) +"]")).getText());
					occupancy.setFirstName	(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[2]/div/div[2]/div["+ flag +"]/div["+ fname +"]")).getText());
					System.out.println(occupancy.getFirstName());
					occupancy.setLastName	(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[2]/div/div[2]/div["+ flag +"]/div["+ lname +"]")).getText());
					occList.add(occupancy);
					fname = fname + 6 ;
					lname = lname + 6 ;
				} catch (Exception e) {
					// TODO: handle exception
					flag++;
					fname = 3;
					lname = 4;
					break;
				}
				
			}
			
			occlistBig.add(occList);
		}
		hotel.setOccypancy(occlistBig);
 		pay.setSubTotal							(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[2]/div[2]/div/div[4]")).getText());
		pay.setTotalTaxAndServiceCharges		(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[1]/div[4]/div[6]")).getText());
		pay.setTotalTaxesAndOtherCharges		(driver.findElement(By.xpath(".//*[@id='tottaxamountdivid']")).getText());
		pay.setTotal							(driver.findElement(By.xpath(".//*[@id='totchargeamt']")).getText());
		pay.setAmountbeingProcessedNow			(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[2]/div[2]/div/div[8]/div[2]")).getText());
		pay.setAmountdueatCheckInUtilization	(driver.findElement(By.xpath(".//*[@id='tripsummary']/div[2]/div[2]/div/div[8]/div[4]")).getText());
		hotel.setPay(pay);
		ArrayList<WebElement> xpathList = new ArrayList<WebElement>();
		xpathList.addAll(driver.findElements(By.xpath(".//*[@id='tripsummary']/div/div/div/div[2]")));
		hotel.setReferenceNumber				(xpathList.get(7).getText());
		hotel.setMerchantTrackId				(driver.findElement(By.xpath(".//*[@id='tripsummary']/div/div/div/div[4]")).getText());
		System.out.println(driver.findElement(By.xpath(".//*[@id='tripsummary']/div/div/div/div[6]")).getText());
		ArrayList<WebElement> paymentList = new ArrayList<WebElement>();
		paymentList.addAll(driver.findElements(By.xpath(".//*[@id='tripsummary']/div/div/div/div[6]")));
		hotel.setPaymentid						(paymentList.get(1).getText());
		hotel.setAmount							(driver.findElement(By.xpath(".//*[@id='tripsummary']/div/div/div/div[8]")).getText());
		return hotel;
	}
	
	
	public conhotel comfirmationRead(WebDriver driver, resHotelAvailability resAvailability2, StringBuffer ReportPrinter, String[] searchDetails)
	{
		WebDriverWait 	wait 	= new WebDriverWait(driver, 10);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("live_message_frame");
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("confsent")));
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		String errorType = "";
		String error = "";
		
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			errorType 		= driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_23")).getText();
			error		 	= driver.findElement(By.id("dialog-error-message-WJ_23")).getText();

			/*ReportPrinter.append("<span>Confirmation page error</span>");
			
			ReportPrinter.append("<table border=1 style=width:800px>"
				+ "<tr><th>Error type</th>"
				+ "<th>Error</t></tr>");
			
			ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
					+ 	"<td>"+ error +"</td></tr></table>");*/
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			errorType		= driver.findElement(By.id("ui-dialog-title-dialog-error-message-WJ_0")).getText();
			error			= driver.findElement(By.id("dialog-error-message-WJ_0")).getText();

			/*ReportPrinter.append("<span>Confirmation page error</span>");
			
			ReportPrinter.append("<table border=1 style=width:800px>"
				+ "<tr><th>Error type</th>"
				+ "<th>Error</t></tr>");
			
			ReportPrinter.append	(	"<tr><td>"+ errorType +"</td>"
					+ 	"<td>"+ error +"</td></tr></table>");*/
			driver.findElement(By.xpath("html/body/div[10]/div[11]/div/button")).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		conhotel				hotel		= new conhotel();
		customerDetails			customer 	= new customerDetails();
		onlinePaymentDetails	online		= new onlinePaymentDetails();
		payments				pay			= new payments();
		
		
		
		
		System.out.println(driver.findElement(By.id("p_hotel_bookings_1")).getText());
		try {
			System.out.println(errorType);
			hotel.setErrorType(errorType);
			System.out.println(error);
			hotel.setError(error);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error");
		}
		
		//try
		//{
			hotel.setHotelBookingStatus		(driver.findElement(By.id("p_hotel_bookings_1")).getText());
			String[] temp 			= 		driver.findElement(By.id("p_hotel_booking_reservation_no_1")).getText().split(":");
			hotel.setReservationNumber		(temp[1].trim());
			hotel.setHotelBookingStatus		(driver.findElement(By.id("p_hotel_bookings_1")).getText());
			hotel.setHotelName				(driver.findElement(By.id("hotelname_0")).getText());
			hotel.setAddress				(driver.findElement(By.id("hoteladdress_0")).getText());
			hotel.setCheckIn				(driver.findElement(By.id("hotelchkin_0")).getText());
			hotel.setCheckout				(driver.findElement(By.id("hotelchkout_0")).getText());
			hotel.setNights					(driver.findElement(By.id("hotelnonights_0")).getText());
			hotel.setRooms					(driver.findElement(By.id("hotelnorooms")).getText());
			hotel.setCancellationDeadLine	(driver.findElement(By.id("hotelduedate_0")).getText());
			hotel.setBookingStatus			(driver.findElement(By.id("hotelstatus_0")).getText());
			hotel.setSupConfirmation		(driver.findElement(By.xpath(".//*[@id='booking_summery_hotel_details_table']/tbody/tr[5]/td[2]")).getText());
			String	cc	=	driver.findElement(By.id("hotelcurrency_0")).getText();
			System.out.println(cc);
			String[] currency = cc.split("\\(");
			System.out.println(currency[1]);
			currency = currency[1].split("\\)");
			cc = currency[0];
			hotel.setCurrencyCode(cc);
			ArrayList<room> roomList = new ArrayList<room>();
			for(int i = 0 ; i>-1 ; i++ )
			{
				try {
					room					room		= new room();
					room.setRoom				(driver.findElement(By.id("roominfo_0_" + i)).getText());
					System.out.println(room.getRoom());
					String[] asd = room.getRoom().trim().split(" ");
					String adultCount = asd[5];
					String childCount = asd[8];
					room.setAdultCount(asd[5]);
					room.setChildCount(asd[8]);
					room.setRoomType			(driver.findElement(By.id("roomtype_0_" + i)).getText());
					room.setBoardType			(driver.findElement(By.id("broadtype_0_" + i)).getText());
					room.setBedType				(driver.findElement(By.id("bedtype_0_" + i)).getText());
					System.out.println(room.getBedType());
					room.setTotalRate			(driver.findElement(By.id("sellrate_0_" + i)).getText());
					System.out.println(room.getTotalRate());
					roomList.add(room);
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				
			}
			pay.setTotalGrossPackageBookingValue	(driver.findElement(By.id("pkg_totalbeforetax")).getText());
			pay.setTotalTaxAndServiceCharges		(driver.findElement(By.id("pkg_totaltaxandothercharges")).getText());
			pay.setTotal							(driver.findElement(By.id("hotelbookingvalue_0")).getText());
			System.out.println(pay.getTotal());
			pay.setTotalPackageBookingValue			(driver.findElement(By.id("pkg_totalpayable")).getText());
			pay.setCurrencyCode						(driver.findElement(By.id("hotelcurrency_0")).getText());
			hotel.setPay(pay);
			hotel.setRoom(roomList);
			customer.setFirstName			(driver.findElement(By.id("cusfirstname")).getText());
			System.out.println(customer.getFirstName());
			customer.setLastName			(driver.findElement(By.id("cuslastname")).getText());
			customer.setPhoneNumber			(driver.findElement(By.id("custelephone")).getText());
			customer.setEmail				(driver.findElement(By.id("cusemail1")).getText());
			customer.setAddress				(driver.findElement(By.id("cusaddress1")).getText());
			customer.setCountry				(driver.findElement(By.id("cuscountry")).getText());
			customer.setCity				(driver.findElement(By.id("cuscity")).getText());
			hotel.setCustomer(customer);
			ArrayList<ArrayList<roomOccupancy>>  	alloccupancy		= new ArrayList<ArrayList<roomOccupancy>>();
			ArrayList<roomOccupancy> 				roomOccupancy	 	= new ArrayList<roomOccupancy>();
			
			try {
				int roomCount = resAvailability2.getRoomAvailability().size();
				System.out.println(roomCount);
				String adultCount = resAvailability2.getRoomAvailability().get(0).getAdultCount();
				System.out.println(adultCount);
				String childCount = resAvailability2.getRoomAvailability().get(0).getChildCount();
				System.out.println(childCount);
				String[] adultCountPerRoom = searchDetails[5].split("/");
				System.out.println(searchDetails[5]);
				outerLoop : for(int i = 0 ; i < Integer.parseInt(searchDetails[4]) ; i++)
				{
					try {
						for(int j = 0 ; j< Integer.parseInt(adultCountPerRoom[i]) ; j++)
						{
								System.out.println(resAvailability2.getRoomAvailability().get(i).getAdultCount());
								roomOccupancy			occupancy	= new roomOccupancy();
								occupancy.setTitle			(driver.findElement(By.id("adult_roomocc_title_0_"+ i +"_" + j)).getText());
								occupancy.setFirstName		(driver.findElement(By.id("adult_roomocc_fname_0_"+ i +"_" + j)).getText());
								System.out.println(occupancy.getFirstName());
								occupancy.setLastName		(driver.findElement(By.id("adult_roomocc_lname_0_"+ i +"_" + j)).getText());
								occupancy.setSmoking		(driver.findElement(By.id("adult_roomocc_smoke_0_"+ i +"_" + j)).getText());
								occupancy.setHandicap		(driver.findElement(By.id("adult_roomocc_handicap_0_"+ i +"_" + j)).getText());
								occupancy.setWheelChair		(driver.findElement(By.id("adult_roomocc_weelchair_0_"+ i +"_" + j)).getText());
								roomOccupancy.add(occupancy);
						}
						alloccupancy.add(roomOccupancy);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
				String[] childPerRoom = searchDetails[6].split("/");
				outerLoop2 : for(int i = 0 ; i < Integer.parseInt(searchDetails[4]) ; i++)
				{
					try {
						for(int j = 0 ; j < Integer.parseInt(childPerRoom[i]) ; j++)
						{
								roomOccupancy			occupancy	= new roomOccupancy();
								occupancy.setTitle			(driver.findElement(By.id("child_roomocc_title_0_"+ i +"_" + j)).getText());
								occupancy.setFirstName		(driver.findElement(By.id("child_roomocc_fname_0_"+ i +"_" + j)).getText());
								System.out.println(occupancy.getFirstName());
								occupancy.setLastName		(driver.findElement(By.id("child_roomocc_lname_0_"+ i +"_" + j)).getText());
								occupancy.setSmoking		(driver.findElement(By.id("child_roomocc_smoke_0_"+ i +"_" + j)).getText());
								occupancy.setHandicap		(driver.findElement(By.id("child_roomocc_handicap_0_"+ i +"_" + j)).getText());
								occupancy.setWheelChair		(driver.findElement(By.id("child_roomocc_weelchair_0_"+ i +"_" + j)).getText());
								roomOccupancy.add(occupancy);
						}
						alloccupancy.add(roomOccupancy);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			hotel.setOccypancy(alloccupancy);
			
			try {
				online.setReferenceNumber			(driver.findElement(By.id("paymentrefno")).getText());

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				online.setTrackId					(driver.findElement(By.id("paymentid")).getText());

			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				online.setPaymentId					(driver.findElement(By.id("paymenttrackid")).getText());
				System.out.println(online.getPaymentId());
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				online.setAmount					(driver.findElement(By.id("onlinecharge")).getText());

			} catch (Exception e) {
				// TODO: handle exception
			}
			hotel.setOnline(online);
		//}	
		//catch(Exception e)
		//{
			
		//}
		return hotel;
	}
}
