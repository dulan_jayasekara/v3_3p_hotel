package readXml;

import java.util.ArrayList;

public class availableRoom {

	String roomCount =""; 
	String adultCount =""; 
	String childCount =""; 
	String shrui =""; 
	String roomCode =""; 
	String roomType =""; 
	String boardCode =""; 
	String boardType =""; 
	String serviceDateType =""; 
	String spui ="";

	ArrayList<guestList> guestList = new ArrayList<guestList>();
	
	/**
	 * @return the guestList
	 */
	public ArrayList<guestList> getGuestList() {
		return guestList;
	}

	public String getServiceDateType() {
		return serviceDateType;
	}

	public void setServiceDateType(String serviceDateType) {
		this.serviceDateType = serviceDateType;
	}

	public String getSpui() {
		return spui;
	}

	public void setSpui(String spui) {
		this.spui = spui;
	}

	/**
	 * @param guestList the guestList to set
	 */
	public void setGuestList(ArrayList<guestList> guestList) {
		this.guestList = guestList;
	}

	/**
	 * @return the roomCount
	 */
	public String getRoomCount() {
		return roomCount;
	}

	/**
	 * @param roomCount the roomCount to set
	 */
	public void setRoomCount(String roomCount) {
		this.roomCount = roomCount;
	}

	/**
	 * @return the adultCount
	 */
	public String getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount the adultCount to set
	 */
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}

	/**
	 * @return the childCount
	 */
	public String getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount the childCount to set
	 */
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}

	/**
	 * @return the shrui
	 */
	public String getShrui() {
		return shrui;
	}

	/**
	 * @param shrui the shrui to set
	 */
	public void setShrui(String shrui) {
		this.shrui = shrui;
	}

	/**
	 * @return the roomCode
	 */
	public String getRoomCode() {
		return roomCode;
	}

	/**
	 * @param roomCode the roomCode to set
	 */
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	/**
	 * @return the roomType
	 */
	public String getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return the boardCode
	 */
	public String getBoardCode() {
		return boardCode;
	}

	/**
	 * @param boardCode the boardCode to set
	 */
	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	/**
	 * @return the boardType
	 */
	public String getBoardType() {
		return boardType;
	}

	/**
	 * @param boardType the boardType to set
	 */
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}
	
	
}
