package readXml;

import java.util.ArrayList;

public class resHotelRoom {
	String shrui ="";
	String room ="";  
	String availableCount =""; 
	String status =""; 
	String board =""; 
	String boardType =""; 
	String boardCode =""; 
	String roomType =""; 
	String roomCode =""; 
	String characteristic =""; 
	String price =""; 
	String cancellationCodet =""; 
	String dateTimeFromDate =""; 
	String dateTimeToDate =""; 
	String roomId =""; 
	String roomName =""; 
	String nights =""; 
	String availability ="";
	String numOfRooms = "";

	ArrayList<resExtendedInfo> extended =  new ArrayList<resExtendedInfo>();
	ArrayList<resavailableRoom> availableRoom = new ArrayList<resavailableRoom>();
	
	public String getNumOfRooms() {
		return numOfRooms;
	}

	public void setNumOfRooms(String numOfRooms) {
		this.numOfRooms = numOfRooms;
	}

	/**
	 * @return the availableRoom
	 */
	public ArrayList<resavailableRoom> getAvailableRoom() {
		return availableRoom;
	}

	/**
	 * @param availableRoom the availableRoom to set
	 */
	public void setAvailableRoom(ArrayList<resavailableRoom> availableRoom) {
		this.availableRoom = availableRoom;
	}

	/**
	 * @return the shrui
	 */
	public String getShrui() {
		return shrui;
	}

	/**
	 * @return the roomId
	 */
	public String getRoomId() {
		return roomId;
	}

	/**
	 * @param roomId the roomId to set
	 */
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	/**
	 * @return the roomName
	 */
	public String getRoomName() {
		return roomName;
	}

	/**
	 * @param roomName the roomName to set
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/**
	 * @return the nights
	 */
	public String getNights() {
		return nights;
	}

	/**
	 * @param nights the nights to set
	 */
	public void setNights(String nights) {
		this.nights = nights;
	}

	/**
	 * @return the availability
	 */
	public String getAvailability() {
		return availability;
	}

	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public ArrayList<resExtendedInfo> getExtended() {
		return extended;
	}

	public void setExtended(ArrayList<resExtendedInfo> extended) {
		this.extended = extended;
	}

	/**
	 * @return the room
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * @param room the room to set
	 */
	public void setRoom(String room) {
		this.room = room;
	}

	/**
	 * @param shrui the shrui to set
	 */
	public void setShrui(String shrui) {
		this.shrui = shrui;
	}

	/**
	 * @return the availableCount
	 */
	public String getAvailableCount() {
		return availableCount;
	}

	/**
	 * @param availableCount the availableCount to set
	 */
	public void setAvailableCount(String availableCount) {
		this.availableCount = availableCount;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the board
	 */
	public String getBoard() {
		return board;
	}

	/**
	 * @param board the board to set
	 */
	public void setBoard(String board) {
		this.board = board;
	}

	/**
	 * @return the boardType
	 */
	public String getBoardType() {
		return boardType;
	}

	/**
	 * @param boardType the boardType to set
	 */
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}

	/**
	 * @return the boardCode
	 */
	public String getBoardCode() {
		return boardCode;
	}

	/**
	 * @param boardCode the boardCode to set
	 */
	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	/**
	 * @return the roomType
	 */
	public String getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return the roomCode
	 */
	public String getRoomCode() {
		return roomCode;
	}

	/**
	 * @param roomCode the roomCode to set
	 */
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	/**
	 * @return the characteristic
	 */
	public String getCharacteristic() {
		return characteristic;
	}

	/**
	 * @param characteristic the characteristic to set
	 */
	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the cancellationCodet
	 */
	public String getCancellationCodet() {
		return cancellationCodet;
	}

	/**
	 * @param cancellationCodet the cancellationCodet to set
	 */
	public void setCancellationCodet(String cancellationCodet) {
		this.cancellationCodet = cancellationCodet;
	}

	/**
	 * @return the dateTimeFromDate
	 */
	public String getDateTimeFromDate() {
		return dateTimeFromDate;
	}

	/**
	 * @param dateTimeFromDate the dateTimeFromDate to set
	 */
	public void setDateTimeFromDate(String dateTimeFromDate) {
		this.dateTimeFromDate = dateTimeFromDate;
	}

	/**
	 * @return the dateTimeToDate
	 */
	public String getDateTimeToDate() {
		return dateTimeToDate;
	}

	/**
	 * @param dateTimeToDate the dateTimeToDate to set
	 */
	public void setDateTimeToDate(String dateTimeToDate) {
		this.dateTimeToDate = dateTimeToDate;
	}
	
}
