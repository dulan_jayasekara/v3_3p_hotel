package readXml;

import java.util.ArrayList;

public class resHotelDetails {
	
	String hotelTypr = ""; 
	String currencyCode = ""; 
	String checkin = ""; 
	String checkout = ""; 
	String hotelCode = ""; 
	String hotelName = ""; 
	String starCategory = ""; 
	String destinationCode = ""; 
	String destination = ""; 
	String zone = ""; 
	String childAgrFrom = ""; 
	String childAgeTo = ""; 
	String reservationId = ""; 
	String fromDate = ""; 
	String toDate = ""; 
	String totalTax = ""; 
	String price = ""; 
	String currency = ""; 
	String status = ""; 
	String numOfAdults = "";
	String numOfChilds = ""; 
	String hotelid = ""; 
	String address = ""; 
	String category = ""; 
	String starLevel = ""; 
	String roomType = ""; 
	String doubleBed = ""; 
	String firstName = ""; 
	String lastName = "";

	ArrayList<resHotelRoom> 	rhRoom 		= new ArrayList<resHotelRoom>();
	ArrayList<resavailableRoom> availRoom	= new ArrayList<resavailableRoom>();
	/**
	 * @return the hotelTypr
	 */
	public String getHotelTypr() {
		return hotelTypr;
	}

	/**
	 * @return the reservationId
	 */
	public String getReservationId() {
		return reservationId;
	}

	/**
	 * @param reservationId the reservationId to set
	 */
	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the totalTax
	 */
	public String getTotalTax() {
		return totalTax;
	}

	/**
	 * @param totalTax the totalTax to set
	 */
	public void setTotalTax(String totalTax) {
		this.totalTax = totalTax;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the numOfAdults
	 */
	public String getNumOfAdults() {
		return numOfAdults;
	}

	/**
	 * @param numOfAdults the numOfAdults to set
	 */
	public void setNumOfAdults(String numOfAdults) {
		this.numOfAdults = numOfAdults;
	}

	/**
	 * @return the numOfChilds
	 */
	public String getNumOfChilds() {
		return numOfChilds;
	}

	/**
	 * @param numOfChilds the numOfChilds to set
	 */
	public void setNumOfChilds(String numOfChilds) {
		this.numOfChilds = numOfChilds;
	}

	/**
	 * @return the hotelid
	 */
	public String getHotelid() {
		return hotelid;
	}

	/**
	 * @param hotelid the hotelid to set
	 */
	public void setHotelid(String hotelid) {
		this.hotelid = hotelid;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the starLevel
	 */
	public String getStarLevel() {
		return starLevel;
	}

	/**
	 * @param starLevel the starLevel to set
	 */
	public void setStarLevel(String starLevel) {
		this.starLevel = starLevel;
	}

	/**
	 * @return the roomType
	 */
	public String getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return the doubleBed
	 */
	public String getDoubleBed() {
		return doubleBed;
	}

	/**
	 * @param doubleBed the doubleBed to set
	 */
	public void setDoubleBed(String doubleBed) {
		this.doubleBed = doubleBed;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the checkin
	 */
	public String getCheckin() {
		return checkin;
	}

	/**
	 * @param checkin the checkin to set
	 */
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	/**
	 * @return the checkout
	 */
	public String getCheckout() {
		return checkout;
	}

	/**
	 * @param checkout the checkout to set
	 */
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the rhRoom
	 */
	public ArrayList<resHotelRoom> getRhRoom() {
		return rhRoom;
	}

	/**
	 * @param rhRoom the rhRoom to set
	 */
	public void setRhRoom(ArrayList<resHotelRoom> rhRoom) {
		this.rhRoom = rhRoom;
	}

	/**
	 * @return the availRoom
	 */
	public ArrayList<resavailableRoom> getAvailRoom() {
		return availRoom;
	}

	/**
	 * @param availRoom the availRoom to set
	 */
	public void setAvailRoom(ArrayList<resavailableRoom> availRoom) {
		this.availRoom = availRoom;
	}

	/**
	 * @param hotelTypr the hotelTypr to set
	 */
	public void setHotelTypr(String hotelTypr) {
		this.hotelTypr = hotelTypr;
	}

	/**
	 * @return the hotelCode
	 */
	public String getHotelCode() {
		return hotelCode;
	}

	/**
	 * @param hotelCode the hotelCode to set
	 */
	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}

	/**
	 * @return the hotelName
	 */
	public String getHotelName() {
		return hotelName;
	}

	/**
	 * @param hotelName the hotelName to set
	 */
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	/**
	 * @return the starCategory
	 */
	public String getStarCategory() {
		return starCategory;
	}

	/**
	 * @param starCategory the starCategory to set
	 */
	public void setStarCategory(String starCategory) {
		this.starCategory = starCategory;
	}

	/**
	 * @return the destinationCode
	 */
	public String getDestinationCode() {
		return destinationCode;
	}

	/**
	 * @param destinationCode the destinationCode to set
	 */
	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the zone
	 */
	public String getZone() {
		return zone;
	}

	/**
	 * @param zone the zone to set
	 */
	public void setZone(String zone) {
		this.zone = zone;
	}

	/**
	 * @return the childAgrFrom
	 */
	public String getChildAgrFrom() {
		return childAgrFrom;
	}

	/**
	 * @param childAgrFrom the childAgrFrom to set
	 */
	public void setChildAgrFrom(String childAgrFrom) {
		this.childAgrFrom = childAgrFrom;
	}

	/**
	 * @return the childAgeTo
	 */
	public String getChildAgeTo() {
		return childAgeTo;
	}

	/**
	 * @param childAgeTo the childAgeTo to set
	 */
	public void setChildAgeTo(String childAgeTo) {
		this.childAgeTo = childAgeTo;
	}
	
}
