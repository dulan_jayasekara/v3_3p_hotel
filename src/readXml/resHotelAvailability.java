package readXml;

import java.util.ArrayList;

public class resHotelAvailability {

	String referenceTime			="";
	String itemCode					="";
	String itemName 				="";
	String confirmation				="";
	String totalItems 				="";
	String processTime 				=""; 
	String totalAmount 				=""; 
	String timeStamp 				=""; 
	String requestHost 				=""; 
	String serverName 				=""; 
	String serverId 				=""; 
	String schemaRelease 			=""; 
	String hydraCodeRelease 		=""; 
	String hydraERelease 			=""; 
	String merlineRelease 			=""; 
	String purchaseToken 			=""; 
	String timeToExpiration 		=""; 
	String status 					=""; 
	String agencyCode 				=""; 
	String agencyBranch 			=""; 
	String language 				=""; 
	String creationUser 			=""; 
	String serviceType 				=""; 
	String spui 					=""; 
	String serviceStatus 			=""; 
	String contractName 			=""; 
	String incomingOfficeCode 		=""; 
	String comment 					=""; 
	String commentType 				=""; 
	String supplierName 			=""; 
	String supplierVatNumber 		=""; 
	String dateFromDate 			=""; 
	String dateToDate 				=""; 
	String currency 				=""; 
	String currencyCode 			=""; 
	String modificationPolicyList 	=""; 
	String hotelInfoType 			=""; 
	String hotelInfoCode 			=""; 
	String hotelInfoname 			=""; 
	String hotelInfoCategory 		=""; 
	String destinationType 			=""; 
	String destinationCode 			=""; 
	String destinationName 			=""; 
	String zoneType 				=""; 
	String zoneCode 				=""; 
	String referenceFileNumber 		=""; 
	String referenceincomingofficeCode =""; 
	String creationdate 			=""; 
	String creationuser 			=""; 
	String holderType 				=""; 
	String holderAge	 			=""; 
	String holdername 				=""; 
	String holderlastName 			=""; 
	String commnetList 				=""; 
	String hotelName 				=""; 
	String hotelAddress 			=""; 
	String starLevel 				=""; 
	String hotelCategory 			=""; 
	String minPrice 				=""; 
	String desdription 				=""; 
	String location 				=""; 
	String roomName 				=""; 
	String nights 					=""; 
	String checkinDate 				=""; 
	String availability 			=""; 
	String roomId 					=""; 
	String xmlError 				=""; 
	String finalPrice 				=""; 
	String finalcurrency 			="";
	String error 					= "";
	String units					= ""; 
	String nyts						= ""; 
	String fromDate					= ""; 
	String toDate					= ""; 
	String timeLimit				= ""; 
	String maxAdult					= ""; 
	String freeNyts					= ""; 
	String rateType					= ""; 
	String creditLimit				= ""; 
	String minimumNyts				= ""; 
	String childAgeStart			= ""; 
	String adultAgeStart			= ""; 
	String childPolicy				= ""; 
	String cancellationPolicy		= ""; 
	String fromDate2				= ""; 
	String toDate2					= ""; 
	String price2					= ""; 
	String freenyts2				= ""; 
	String nyts2					= ""; 
	String plistcode				= ""; 
	String plistlineno				= "";
	String maxChildren				= "";
	String roomCode					= "";
	String extraBed					= "";
	String numOfExtraBeds			= "";
	String numOfRooms				= "";
	
	ArrayList<whitesandsPreReservationResOccupancy> room	= new ArrayList<whitesandsPreReservationResOccupancy>();
	ArrayList<resHotelAvailability> hotelAvailability 		= new ArrayList<resHotelAvailability>();
	ArrayList<resAdditionalCost> 	aditionalCost			= new ArrayList<resAdditionalCost>();
	ArrayList<resavailableRoom> 	roomAvailability		= new ArrayList<resavailableRoom>();
	//ArrayList<resGuestList>		guestList				= new ArrayList<resGuestList>();
	ArrayList<resHotelRoom> 		hotelRoom				= new ArrayList<resHotelRoom>();
	ArrayList<resHotelDetails> 		hotelDetails			= new ArrayList<resHotelDetails>();
	resHotelRoom     				singleRoom				= new resHotelRoom();
	ArrayList<inv27rooms>           inv27Room				= new ArrayList<inv27rooms>();
	ArrayList<roomCategory> rc = new ArrayList<roomCategory>();
	ArrayList<inv27Cancellation> cancel = new ArrayList<inv27Cancellation>();
	
	
	
	public String getReferenceTime() {
		return referenceTime;
	}

	public void setReferenceTime(String referenceTime) {
		this.referenceTime = referenceTime;
	}

	public ArrayList<inv27Cancellation> getCancel() {
		return cancel;
	}

	public void setCancel(ArrayList<inv27Cancellation> cancel) {
		this.cancel = cancel;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}

	public ArrayList<inv27rooms> getInv27Room() {
		return inv27Room;
	}

	public void setInv27Room(ArrayList<inv27rooms> inv27Room) {
		this.inv27Room = inv27Room;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public String getExtraBed() {
		return extraBed;
	}

	public void setExtraBed(String extraBed) {
		this.extraBed = extraBed;
	}

	public String getNumOfExtraBeds() {
		return numOfExtraBeds;
	}

	public void setNumOfExtraBeds(String numOfExtraBeds) {
		this.numOfExtraBeds = numOfExtraBeds;
	}

	public String getNumOfRooms() {
		return numOfRooms;
	}

	public void setNumOfRooms(String numOfRooms) {
		this.numOfRooms = numOfRooms;
	}

	public ArrayList<roomCategory> getRc() {
		return rc;
	}

	public void setRc(ArrayList<roomCategory> rc) {
		this.rc = rc;
	}

	public ArrayList<whitesandsPreReservationResOccupancy> getRoom() {
		return room;
	}

	public void setRoom(ArrayList<whitesandsPreReservationResOccupancy> room) {
		this.room = room;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getNyts() {
		return nyts;
	}

	public void setNyts(String nyts) {
		this.nyts = nyts;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}

	public String getMaxAdult() {
		return maxAdult;
	}

	public void setMaxAdult(String maxAdult) {
		this.maxAdult = maxAdult;
	}

	public String getFreeNyts() {
		return freeNyts;
	}

	public void setFreeNyts(String freeNyts) {
		this.freeNyts = freeNyts;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getMinimumNyts() {
		return minimumNyts;
	}

	public void setMinimumNyts(String minimumNyts) {
		this.minimumNyts = minimumNyts;
	}

	public String getChildAgeStart() {
		return childAgeStart;
	}

	public void setChildAgeStart(String childAgeStart) {
		this.childAgeStart = childAgeStart;
	}

	public String getAdultAgeStart() {
		return adultAgeStart;
	}

	public void setAdultAgeStart(String adultAgeStart) {
		this.adultAgeStart = adultAgeStart;
	}

	public String getChildPolicy() {
		return childPolicy;
	}

	public void setChildPolicy(String childPolicy) {
		this.childPolicy = childPolicy;
	}

	public String getCancellationPolicy() {
		return cancellationPolicy;
	}

	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}

	public String getFromDate2() {
		return fromDate2;
	}

	public void setFromDate2(String fromDate2) {
		this.fromDate2 = fromDate2;
	}

	public String getToDate2() {
		return toDate2;
	}

	public void setToDate2(String toDate2) {
		this.toDate2 = toDate2;
	}

	public String getPrice2() {
		return price2;
	}

	public void setPrice2(String price2) {
		this.price2 = price2;
	}

	public String getFreenyts2() {
		return freenyts2;
	}

	public void setFreenyts2(String freenyts2) {
		this.freenyts2 = freenyts2;
	}

	public String getNyts2() {
		return nyts2;
	}

	public void setNyts2(String nyts2) {
		this.nyts2 = nyts2;
	}

	public String getPlistcode() {
		return plistcode;
	}

	public void setPlistcode(String plistcode) {
		this.plistcode = plistcode;
	}

	public String getPlistlineno() {
		return plistlineno;
	}

	public void setPlistlineno(String plistlineno) {
		this.plistlineno = plistlineno;
	}

	public String getMaxChildren() {
		return maxChildren;
	}

	public void setMaxChildren(String maxChildren) {
		this.maxChildren = maxChildren;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the totalItems
	 */
	public String getTotalItems() {
		return totalItems;
	}

	/**
	 * @param totalItems the totalItems to set
	 */
	public void setTotalItems(String totalItems) {
		this.totalItems = totalItems;
	}

	/**
	 * @return the xmlError
	 */
	public String getXmlError() {
		return xmlError;
	}

	/**
	 * @return the finalPrice
	 */
	public String getFinalPrice() {
		return finalPrice;
	}

	/**
	 * @param finalPrice the finalPrice to set
	 */
	public void setFinalPrice(String finalPrice) {
		this.finalPrice = finalPrice;
	}

	/**
	 * @return the finalcurrency
	 */
	public String getFinalcurrency() {
		return finalcurrency;
	}

	/**
	 * @param finalcurrency the finalcurrency to set
	 */
	public void setFinalcurrency(String finalcurrency) {
		this.finalcurrency = finalcurrency;
	}

	/**
	 * @param xmlError the xmlError to set
	 */
	public void setXmlError(String xmlError) {
		this.xmlError = xmlError;
	}

	/**
	 * @return the sigleRoom
	 */
	public resHotelRoom getSingleRoom() {
		return singleRoom;
	}

	/**
	 * @param sigleRoom the sigleRoom to set
	 */
	public void setSingleRoom(resHotelRoom sigleRoom) {
		this.singleRoom = sigleRoom;
	}

	/**
	 * @return the hotelName
	 */
	public String getHotelName() {
		return hotelName;
	}

	/**
	 * @param hotelName the hotelName to set
	 */
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	/**
	 * @return the hotelAddress
	 */
	public String getHotelAddress() {
		return hotelAddress;
	}

	/**
	 * @param hotelAddress the hotelAddress to set
	 */
	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}

	/**
	 * @return the starLevel
	 */
	public String getStarLevel() {
		return starLevel;
	}

	/**
	 * @param starLevel the starLevel to set
	 */
	public void setStarLevel(String starLevel) {
		this.starLevel = starLevel;
	}

	/**
	 * @return the hotelCategory
	 */
	public String getHotelCategory() {
		return hotelCategory;
	}

	/**
	 * @param hotelCategory the hotelCategory to set
	 */
	public void setHotelCategory(String hotelCategory) {
		this.hotelCategory = hotelCategory;
	}

	/**
	 * @return the minPrice
	 */
	public String getMinPrice() {
		return minPrice;
	}

	/**
	 * @param minPrice the minPrice to set
	 */
	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}

	/**
	 * @return the desdription
	 */
	public String getDesdription() {
		return desdription;
	}

	/**
	 * @param desdription the desdription to set
	 */
	public void setDesdription(String desdription) {
		this.desdription = desdription;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the roomName
	 */
	public String getRoomName() {
		return roomName;
	}

	/**
	 * @param roomName the roomName to set
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/**
	 * @return the nights
	 */
	public String getNights() {
		return nights;
	}

	/**
	 * @param nights the nights to set
	 */
	public void setNights(String nights) {
		this.nights = nights;
	}

	/**
	 * @return the checkinDate
	 */
	public String getCheckinDate() {
		return checkinDate;
	}

	/**
	 * @param checkinDate the checkinDate to set
	 */
	public void setCheckinDate(String checkinDate) {
		this.checkinDate = checkinDate;
	}

	/**
	 * @return the availability
	 */
	public String getAvailability() {
		return availability;
	}

	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(String availability) {
		this.availability = availability;
	}

	/**
	 * @return the roomId
	 */
	public String getRoomId() {
		return roomId;
	}

	/**
	 * @param roomId the roomId to set
	 */
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	/**
	 * @return the hotelDetails
	 */
	public ArrayList<resHotelDetails> getHotelDetails() {
		return hotelDetails;
	}

	/**
	 * @param hotelDetails the hotelDetails to set
	 */
	public void setHotelDetails(ArrayList<resHotelDetails> hotelDetails) {
		this.hotelDetails = hotelDetails;
	}

	/**
	 * @return the hotelAvailability
	 */
	public ArrayList<resHotelAvailability> getHotelAvailability() {
		return hotelAvailability;
	}

	public String getReferenceFileNumber() {
		return referenceFileNumber;
	}

	public void setReferenceFileNumber(String referenceFileNumber) {
		this.referenceFileNumber = referenceFileNumber;
	}

	public String getReferenceincomingofficeCode() {
		return referenceincomingofficeCode;
	}

	public void setReferenceincomingofficeCode(String referenceincomingofficeCode) {
		this.referenceincomingofficeCode = referenceincomingofficeCode;
	}

	public String getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(String creationdate) {
		this.creationdate = creationdate;
	}

	public String getCreationuser() {
		return creationuser;
	}

	public void setCreationuser(String creationuser) {
		this.creationuser = creationuser;
	}

	public String getHolderType() {
		return holderType;
	}

	public void setHolderType(String holderType) {
		this.holderType = holderType;
	}

	public String getHolderAge() {
		return holderAge;
	}

	public void setHolderAge(String holderAge) {
		this.holderAge = holderAge;
	}

	public String getHoldername() {
		return holdername;
	}

	public void setHoldername(String holdername) {
		this.holdername = holdername;
	}

	public String getHolderlastName() {
		return holderlastName;
	}

	public void setHolderlastName(String holderlastName) {
		this.holderlastName = holderlastName;
	}

	public String getCommnetList() {
		return commnetList;
	}

	public void setCommnetList(String commnetList) {
		this.commnetList = commnetList;
	}

	/**
	 * @param hotelAvailability the hotelAvailability to set
	 */
	public void setHotelAvailability(
			ArrayList<resHotelAvailability> hotelAvailability) {
		this.hotelAvailability = hotelAvailability;
	}

	/**
	 * @return the aditionalCost
	 */
	public ArrayList<resAdditionalCost> getAditionalCost() {
		return aditionalCost;
	}

	/**
	 * @return the totalAmount
	 */
	public String getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @param aditionalCost the aditionalCost to set
	 */
	public void setAditionalCost(ArrayList<resAdditionalCost> aditionalCost) {
		this.aditionalCost = aditionalCost;
	}

	/**
	 * @return the roomAvailability
	 */
	public ArrayList<resavailableRoom> getRoomAvailability() {
		return roomAvailability;
	}

	/**
	 * @param roomAvailability the roomAvailability to set
	 */
	public void setRoomAvailability(ArrayList<resavailableRoom> roomAvailability) {
		this.roomAvailability = roomAvailability;
	}

	/**
	 * @return the guestList
	 */
	


	/**
	 * @return the hotelRoom
	 */
	public ArrayList<resHotelRoom> getHotelRoom() {
		return hotelRoom;
	}

	/**
	 * @param hotelRoom the hotelRoom to set
	 */
	public void setHotelRoom(ArrayList<resHotelRoom> hotelRoom) {
		this.hotelRoom = hotelRoom;
	}

	/**
	 * @return the processTime
	 */
	public String getProcessTime() {
		return processTime;
	}

	/**
	 * @param processTime the processTime to set
	 */
	public void setProcessTime(String processTime) {
		this.processTime = processTime;
	}

	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the requestHost
	 */
	public String getRequestHost() {
		return requestHost;
	}

	/**
	 * @param requestHost the requestHost to set
	 */
	public void setRequestHost(String requestHost) {
		this.requestHost = requestHost;
	}

	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * @return the serverId
	 */
	public String getServerId() {
		return serverId;
	}

	/**
	 * @param serverId the serverId to set
	 */
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	/**
	 * @return the schemaRelease
	 */
	public String getSchemaRelease() {
		return schemaRelease;
	}

	/**
	 * @param schemaRelease the schemaRelease to set
	 */
	public void setSchemaRelease(String schemaRelease) {
		this.schemaRelease = schemaRelease;
	}

	/**
	 * @return the hydraCodeRelease
	 */
	public String getHydraCodeRelease() {
		return hydraCodeRelease;
	}

	/**
	 * @param hydraCodeRelease the hydraCodeRelease to set
	 */
	public void setHydraCodeRelease(String hydraCodeRelease) {
		this.hydraCodeRelease = hydraCodeRelease;
	}

	/**
	 * @return the hydraERelease
	 */
	public String getHydraERelease() {
		return hydraERelease;
	}

	/**
	 * @param hydraERelease the hydraERelease to set
	 */
	public void setHydraERelease(String hydraERelease) {
		this.hydraERelease = hydraERelease;
	}

	/**
	 * @return the merlineRelease
	 */
	public String getMerlineRelease() {
		return merlineRelease;
	}

	/**
	 * @param merlineRelease the merlineRelease to set
	 */
	public void setMerlineRelease(String merlineRelease) {
		this.merlineRelease = merlineRelease;
	}

	/**
	 * @return the purchaseToken
	 */
	public String getPurchaseToken() {
		return purchaseToken;
	}

	/**
	 * @param purchaseToken the purchaseToken to set
	 */
	public void setPurchaseToken(String purchaseToken) {
		this.purchaseToken = purchaseToken;
	}

	/**
	 * @return the timeToExpiration
	 */
	public String getTimeToExpiration() {
		return timeToExpiration;
	}

	/**
	 * @param timeToExpiration the timeToExpiration to set
	 */
	public void setTimeToExpiration(String timeToExpiration) {
		this.timeToExpiration = timeToExpiration;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the agencyCode
	 */
	public String getAgencyCode() {
		return agencyCode;
	}

	/**
	 * @param agencyCode the agencyCode to set
	 */
	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	/**
	 * @return the agencyBranch
	 */
	public String getAgencyBranch() {
		return agencyBranch;
	}

	/**
	 * @param agencyBranch the agencyBranch to set
	 */
	public void setAgencyBranch(String agencyBranch) {
		this.agencyBranch = agencyBranch;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the creationUser
	 */
	public String getCreationUser() {
		return creationUser;
	}

	/**
	 * @param creationUser the creationUser to set
	 */
	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}

	/**
	 * @return the serviceType
	 */
	public String getServiceType() {
		return serviceType;
	}

	/**
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * @return the spui
	 */
	public String getSpui() {
		return spui;
	}

	/**
	 * @param spui the spui to set
	 */
	public void setSpui(String spui) {
		this.spui = spui;
	}

	/**
	 * @return the serviceStatus
	 */
	public String getServiceStatus() {
		return serviceStatus;
	}

	/**
	 * @param serviceStatus the serviceStatus to set
	 */
	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	/**
	 * @return the contractName
	 */
	public String getContractName() {
		return contractName;
	}

	/**
	 * @param contractName the contractName to set
	 */
	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	/**
	 * @return the incomingOfficeCode
	 */
	public String getIncomingOfficeCode() {
		return incomingOfficeCode;
	}

	/**
	 * @param incomingOfficeCode the incomingOfficeCode to set
	 */
	public void setIncomingOfficeCode(String incomingOfficeCode) {
		this.incomingOfficeCode = incomingOfficeCode;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the commentType
	 */
	public String getCommentType() {
		return commentType;
	}

	/**
	 * @param commentType the commentType to set
	 */
	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}

	/**
	 * @return the supplierName
	 */
	public String getSupplierName() {
		return supplierName;
	}

	/**
	 * @param supplierName the supplierName to set
	 */
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	/**
	 * @return the supplierVatNumber
	 */
	public String getSupplierVatNumber() {
		return supplierVatNumber;
	}

	/**
	 * @param supplierVatNumber the supplierVatNumber to set
	 */
	public void setSupplierVatNumber(String supplierVatNumber) {
		this.supplierVatNumber = supplierVatNumber;
	}

	/**
	 * @return the dateFromDate
	 */
	public String getDateFromDate() {
		return dateFromDate;
	}

	/**
	 * @param dateFromDate the dateFromDate to set
	 */
	public void setDateFromDate(String dateFromDate) {
		this.dateFromDate = dateFromDate;
	}

	/**
	 * @return the dateToDate
	 */
	public String getDateToDate() {
		return dateToDate;
	}

	/**
	 * @param dateToDate the dateToDate to set
	 */
	public void setDateToDate(String dateToDate) {
		this.dateToDate = dateToDate;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the modificationPolicyList
	 */
	public String getModificationPolicyList() {
		return modificationPolicyList;
	}

	/**
	 * @param modificationPolicyList the modificationPolicyList to set
	 */
	public void setModificationPolicyList(String modificationPolicyList) {
		this.modificationPolicyList = modificationPolicyList;
	}

	/**
	 * @return the hotelInfoType
	 */
	public String getHotelInfoType() {
		return hotelInfoType;
	}

	/**
	 * @param hotelInfoType the hotelInfoType to set
	 */
	public void setHotelInfoType(String hotelInfoType) {
		this.hotelInfoType = hotelInfoType;
	}

	/**
	 * @return the hotelInfoCode
	 */
	public String getHotelInfoCode() {
		return hotelInfoCode;
	}

	/**
	 * @param hotelInfoCode the hotelInfoCode to set
	 */
	public void setHotelInfoCode(String hotelInfoCode) {
		this.hotelInfoCode = hotelInfoCode;
	}

	/**
	 * @return the hotelInfoname
	 */
	public String getHotelInfoname() {
		return hotelInfoname;
	}

	/**
	 * @param hotelInfoname the hotelInfoname to set
	 */
	public void setHotelInfoname(String hotelInfoname) {
		this.hotelInfoname = hotelInfoname;
	}

	/**
	 * @return the hotelInfoCategory
	 */
	public String getHotelInfoCategory() {
		return hotelInfoCategory;
	}

	/**
	 * @param hotelInfoCategory the hotelInfoCategory to set
	 */
	public void setHotelInfoCategory(String hotelInfoCategory) {
		this.hotelInfoCategory = hotelInfoCategory;
	}

	/**
	 * @return the destinationType
	 */
	public String getDestinationType() {
		return destinationType;
	}

	/**
	 * @param destinationType the destinationType to set
	 */
	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	/**
	 * @return the destinationCode
	 */
	public String getDestinationCode() {
		return destinationCode;
	}

	/**
	 * @param destinationCode the destinationCode to set
	 */
	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}

	/**
	 * @return the destinationName
	 */
	public String getDestinationName() {
		return destinationName;
	}

	/**
	 * @param destinationName the destinationName to set
	 */
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	/**
	 * @return the zoneType
	 */
	public String getZoneType() {
		return zoneType;
	}

	/**
	 * @param zoneType the zoneType to set
	 */
	public void setZoneType(String zoneType) {
		this.zoneType = zoneType;
	}

	/**
	 * @return the zoneCode
	 */
	public String getZoneCode() {
		return zoneCode;
	}

	/**
	 * @param zoneCode the zoneCode to set
	 */
	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}
	
}
