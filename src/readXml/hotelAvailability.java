package readXml;

import java.util.ArrayList;

public class hotelAvailability {
	
	String	user =""; 
	String	password =""; 
	String	destinationType =""; 
	String	contractname =""; 
	String	incomingOffice =""; 
	String	dateFromDate =""; 
	String	dateTodate =""; 
	String	hotelType =""; 
	String	hotelCode =""; 
	String	destination =""; 
	String	confirmationDatePurchaseToken =""; 
	String	holderType =""; 
	String	holderName =""; 
	String	lastName =""; 
	String	agencyReference =""; 
	String	serviceDataType =""; 
	String	spui ="";
	String  email = "";
	String 	bookingReference = "";
	String  bookingDepartureDate = "";

	
	ArrayList<availableRoom> 		roomAvailability 	= new ArrayList<availableRoom>();
	ArrayList<guestList> 			guestList			= new ArrayList<guestList>();
	
	
	public String getBookingReference() {
		return bookingReference;
	}

	public void setBookingReference(String bookingReference) {
		this.bookingReference = bookingReference;
	}

	public String getBookingDepartureDate() {
		return bookingDepartureDate;
	}

	public void setBookingDepartureDate(String bookingDepartureDate) {
		this.bookingDepartureDate = bookingDepartureDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the guestList
	 */
	public ArrayList<guestList> getGuestList() {
		return guestList;
	}

	/**
	 * @param guestList the guestList to set
	 */
	public void setGuestList(ArrayList<guestList> guestList) {
		this.guestList = guestList;
	}

	/**
	 * @return the destinationType
	 */
	public String getDestinationType() {
		return destinationType;
	}

	public String getConfirmationDatePurchaseToken() {
		return confirmationDatePurchaseToken;
	}

	public void setConfirmationDatePurchaseToken(
			String confirmationDatePurchaseToken) {
		this.confirmationDatePurchaseToken = confirmationDatePurchaseToken;
	}

	public String getHolderType() {
		return holderType;
	}

	public void setHolderType(String holderType) {
		this.holderType = holderType;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAgencyReference() {
		return agencyReference;
	}

	public void setAgencyReference(String agencyReference) {
		this.agencyReference = agencyReference;
	}

	public String getServiceDataType() {
		return serviceDataType;
	}

	public void setServiceDataType(String serviceDataType) {
		this.serviceDataType = serviceDataType;
	}

	public String getSpui() {
		return spui;
	}

	public void setSpui(String spui) {
		this.spui = spui;
	}

	/**
	 * @param destinationType the destinationType to set
	 */
	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	/**
	 * @return the hotelAvailability
	 */
	

	/**
	 * @return the roomAvailability
	 */
	

	/**
	 * @return the guestList
	 */
	

	/**
	 * @param guestList the guestList to set
	 */
	

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	public ArrayList<availableRoom> getRoomAvailability() {
		return roomAvailability;
	}

	public void setRoomAvailability(ArrayList<availableRoom> roomAvailability2) {
		this.roomAvailability = roomAvailability2;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the contractname
	 */
	public String getContractname() {
		return contractname;
	}

	/**
	 * @param contractname the contractname to set
	 */
	public void setContractname(String contractname) {
		this.contractname = contractname;
	}

	/**
	 * @return the incomingOffice
	 */
	public String getIncomingOffice() {
		return incomingOffice;
	}

	/**
	 * @param incomingOffice the incomingOffice to set
	 */
	public void setIncomingOffice(String incomingOffice) {
		this.incomingOffice = incomingOffice;
	}

	/**
	 * @return the dateFromDate
	 */
	public String getDateFromDate() {
		return dateFromDate;
	}

	/**
	 * @param dateFromDate the dateFromDate to set
	 */
	public void setDateFromDate(String dateFromDate) {
		this.dateFromDate = dateFromDate;
	}

	/**
	 * @return the dateTodate
	 */
	public String getDateTodate() {
		return dateTodate;
	}

	/**
	 * @param dateTodate the dateTodate to set
	 */
	public void setDateTodate(String dateTodate) {
		this.dateTodate = dateTodate;
	}

	/**
	 * @return the hotelType
	 */
	public String getHotelType() {
		return hotelType;
	}

	/**
	 * @param hotelType the hotelType to set
	 */
	public void setHotelType(String hotelType) {
		this.hotelType = hotelType;
	}

	/**
	 * @return the hotelCode
	 */
	public String getHotelCode() {
		return hotelCode;
	}

	/**
	 * @param hotelCode the hotelCode to set
	 */
	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
}
