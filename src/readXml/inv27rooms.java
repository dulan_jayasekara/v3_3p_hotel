package readXml;

public class inv27rooms {

	String roomCode 		= "";
	String numOfRooms 		= "";
	String numOfCots 		= "";
	String roomId 			= "";
	String roomname 		= "";
	String extraBed 		= "";
	String numOfExtraBeds 	= "";
	
	
	public String getExtraBed() {
		return extraBed;
	}
	public void setExtraBed(String extraBed) {
		this.extraBed = extraBed;
	}
	public String getNumOfExtraBeds() {
		return numOfExtraBeds;
	}
	public void setNumOfExtraBeds(String numOfExtraBeds) {
		this.numOfExtraBeds = numOfExtraBeds;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getRoomname() {
		return roomname;
	}
	public void setRoomname(String roomname) {
		this.roomname = roomname;
	}
	public String getRoomCode() {
		return roomCode;
	}
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}
	public String getNumOfRooms() {
		return numOfRooms;
	}
	public void setNumOfRooms(String numOfRooms) {
		this.numOfRooms = numOfRooms;
	}
	public String getNumOfCots() {
		return numOfCots;
	}
	public void setNumOfCots(String numOfCots) {
		this.numOfCots = numOfCots;
	}
	
	
}
