package readXml;

public class resAdditionalCost {
	String additionalCostType =""; 
	String price = "";

	/**
	 * @return the additionalCostType
	 */
	public String getAdditionalCostType() {
		return additionalCostType;
	}

	/**
	 * @param additionalCostType the additionalCostType to set
	 */
	public void setAdditionalCostType(String additionalCostType) {
		this.additionalCostType = additionalCostType;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	
	
}
