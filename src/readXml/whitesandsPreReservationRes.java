package readXml;

import java.util.ArrayList;

public class whitesandsPreReservationRes {

	String fromdate = ""; 
	String toDate = ""; 
	String nyts = ""; 
	String timeLimit = ""; 
	String maxAdults = ""; 
	String maxeb = ""; 
	String maxChildFree = ""; 
	String freeNyts = ""; 
	String avaialability = ""; 
	String rateType = ""; 
	String creditLimit = ""; 
	String promotionid = ""; 
	String minimumNyts = ""; 
	String block = ""; 
	String XOFlag = ""; 
	String childAgeStart = ""; 
	String adultAgeStart = ""; 
	String childPolicy = ""; 
	String cancellationPolicy = ""; 
	String priceremarks = ""; 
	String productType = ""; 
	String fromDate2 = ""; 
	String toDate2 = ""; 
	String price2 = ""; 
	String nyts2 = ""; 
	String freeNyts2 = ""; 
	String plistcode = ""; 
	String plistlineno = "";
	
	ArrayList<whitesandsPreReservationResOccupancy> room = new ArrayList<whitesandsPreReservationResOccupancy>();
	
	
	
	public ArrayList<whitesandsPreReservationResOccupancy> getRoom() {
		return room;
	}
	public void setRoom(ArrayList<whitesandsPreReservationResOccupancy> room) {
		this.room = room;
	}
	public String getFromdate() {
		return fromdate;
	}
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getNyts() {
		return nyts;
	}
	public void setNyts(String nyts) {
		this.nyts = nyts;
	}
	public String getTimeLimit() {
		return timeLimit;
	}
	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}
	public String getMaxAdults() {
		return maxAdults;
	}
	public void setMaxAdults(String maxAdults) {
		this.maxAdults = maxAdults;
	}
	public String getMaxeb() {
		return maxeb;
	}
	public void setMaxeb(String maxeb) {
		this.maxeb = maxeb;
	}
	public String getMaxChildFree() {
		return maxChildFree;
	}
	public void setMaxChildFree(String maxChildFree) {
		this.maxChildFree = maxChildFree;
	}
	public String getFreeNyts() {
		return freeNyts;
	}
	public void setFreeNyts(String freeNyts) {
		this.freeNyts = freeNyts;
	}
	public String getAvaialability() {
		return avaialability;
	}
	public void setAvaialability(String avaialability) {
		this.avaialability = avaialability;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}
	public String getPromotionid() {
		return promotionid;
	}
	public void setPromotionid(String promotionid) {
		this.promotionid = promotionid;
	}
	public String getMinimumNyts() {
		return minimumNyts;
	}
	public void setMinimumNyts(String minimumNyts) {
		this.minimumNyts = minimumNyts;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getXOFlag() {
		return XOFlag;
	}
	public void setXOFlag(String xOFlag) {
		XOFlag = xOFlag;
	}
	public String getChildAgeStart() {
		return childAgeStart;
	}
	public void setChildAgeStart(String childAgeStart) {
		this.childAgeStart = childAgeStart;
	}
	public String getAdultAgeStart() {
		return adultAgeStart;
	}
	public void setAdultAgeStart(String adultAgeStart) {
		this.adultAgeStart = adultAgeStart;
	}
	public String getChildPolicy() {
		return childPolicy;
	}
	public void setChildPolicy(String childPolicy) {
		this.childPolicy = childPolicy;
	}
	public String getCancellationPolicy() {
		return cancellationPolicy;
	}
	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}
	public String getPriceremarks() {
		return priceremarks;
	}
	public void setPriceremarks(String priceremarks) {
		this.priceremarks = priceremarks;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getFromDate2() {
		return fromDate2;
	}
	public void setFromDate2(String fromDate2) {
		this.fromDate2 = fromDate2;
	}
	public String getToDate2() {
		return toDate2;
	}
	public void setToDate2(String toDate2) {
		this.toDate2 = toDate2;
	}
	public String getPrice2() {
		return price2;
	}
	public void setPrice2(String price2) {
		this.price2 = price2;
	}
	public String getNyts2() {
		return nyts2;
	}
	public void setNyts2(String nyts2) {
		this.nyts2 = nyts2;
	}
	public String getFreeNyts2() {
		return freeNyts2;
	}
	public void setFreeNyts2(String freeNyts2) {
		this.freeNyts2 = freeNyts2;
	}
	public String getPlistcode() {
		return plistcode;
	}
	public void setPlistcode(String plistcode) {
		this.plistcode = plistcode;
	}
	public String getPlistlineno() {
		return plistlineno;
	}
	public void setPlistlineno(String plistlineno) {
		this.plistlineno = plistlineno;
	}
	
	
	
}
