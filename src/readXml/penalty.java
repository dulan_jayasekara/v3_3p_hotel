package readXml;

public class penalty {

	String timeunit = ""; 
	String multiplier = ""; 
	String dropTime = ""; 
	String amountPercent = ""; 
	String basisType = ""; 
	String numofnyts = ""; 
	String nsTimeUnit = ""; 
	String nsMultiplier = ""; 
	String nsDropTime = ""; 
	String npNumOfNyts = ""; 
	String nsBasisType = "";
	public String getTimeunit() {
		return timeunit;
	}
	public void setTimeunit(String timeunit) {
		this.timeunit = timeunit;
	}
	public String getMultiplier() {
		return multiplier;
	}
	public void setMultiplier(String multiplier) {
		this.multiplier = multiplier;
	}
	public String getDropTime() {
		return dropTime;
	}
	public void setDropTime(String dropTime) {
		this.dropTime = dropTime;
	}
	public String getAmountPercent() {
		return amountPercent;
	}
	public void setAmountPercent(String amountPercent) {
		this.amountPercent = amountPercent;
	}
	public String getBasisType() {
		return basisType;
	}
	public void setBasisType(String basisType) {
		this.basisType = basisType;
	}
	public String getNumofnyts() {
		return numofnyts;
	}
	public void setNumofnyts(String numofnyts) {
		this.numofnyts = numofnyts;
	}
	public String getNsTimeUnit() {
		return nsTimeUnit;
	}
	public void setNsTimeUnit(String nsTimeUnit) {
		this.nsTimeUnit = nsTimeUnit;
	}
	public String getNsMultiplier() {
		return nsMultiplier;
	}
	public void setNsMultiplier(String nsMultiplier) {
		this.nsMultiplier = nsMultiplier;
	}
	public String getNsDropTime() {
		return nsDropTime;
	}
	public void setNsDropTime(String nsDropTime) {
		this.nsDropTime = nsDropTime;
	}
	public String getNpNumOfNyts() {
		return npNumOfNyts;
	}
	public void setNpNumOfNyts(String npNumOfNyts) {
		this.npNumOfNyts = npNumOfNyts;
	}
	public String getNsBasisType() {
		return nsBasisType;
	}
	public void setNsBasisType(String nsBasisType) {
		this.nsBasisType = nsBasisType;
	}
	
	
	
}
