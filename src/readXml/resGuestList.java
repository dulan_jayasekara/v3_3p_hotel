package readXml;

import java.util.ArrayList;

public class resGuestList {

	String customerType =""; 
	String customerid =""; 
	String customerAge =""; 
	String name =""; 
	String lastName =""; 
	String adultCount =""; 
	String childCount =""; 
	String bedType ="";
	ArrayList<customer> cus = new ArrayList<customer>();
	

	/**
	 * @return the rgl
	 */
	

	/**
	 * @return the cus
	 */
	public ArrayList<customer> getCus() {
		return cus;
	}

	/**
	 * @return the bedType
	 */
	public String getBedType() {
		return bedType;
	}

	/**
	 * @param bedType the bedType to set
	 */
	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	/**
	 * @param cus the cus to set
	 */
	public void setCus(ArrayList<customer> cus) {
		this.cus = cus;
	}

	/**
	 * @return the adultCount
	 */
	public String getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount the adultCount to set
	 */
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}

	/**
	 * @return the childCount
	 */
	public String getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount the childCount to set
	 */
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the customerid
	 */
	public String getCustomerid() {
		return customerid;
	}

	/**
	 * @param customerid the customerid to set
	 */
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	/**
	 * @return the customerAge
	 */
	public String getCustomerAge() {
		return customerAge;
	}

	/**
	 * @param customerAge the customerAge to set
	 */
	public void setCustomerAge(String customerAge) {
		this.customerAge = customerAge;
	}
	
}
