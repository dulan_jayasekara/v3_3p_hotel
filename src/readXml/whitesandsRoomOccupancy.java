package readXml;

public class whitesandsRoomOccupancy {

	String fromDate 	= ""; 
	String toDate 		= ""; 
	String roomCode 	= ""; 
	String roomName 	= ""; 
	String pricce 		= ""; 
	String CalcByPax 	= ""; 
	String CalcByUnits 	= ""; 
	String autoconfirm	= "";
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getRoomCode() {
		return roomCode;
	}
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getPricce() {
		return pricce;
	}
	public void setPricce(String pricce) {
		this.pricce = pricce;
	}
	public String getCalcByPax() {
		return CalcByPax;
	}
	public void setCalcByPax(String calcByPax) {
		CalcByPax = calcByPax;
	}
	public String getCalcByUnits() {
		return CalcByUnits;
	}
	public void setCalcByUnits(String calcByUnits) {
		CalcByUnits = calcByUnits;
	}
	public String getAutoconfirm() {
		return autoconfirm;
	}
	public void setAutoconfirm(String autoconfirm) {
		this.autoconfirm = autoconfirm;
	}
	
	
}
