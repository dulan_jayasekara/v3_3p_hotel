package readXml;

import java.util.ArrayList;

public class roomCategory {

	String description = "";
	String currency = "";
	String price = "";
	String availability = "";
	String mealBasis = "";
	String mealBreakFast = "";
	
	ArrayList<resHotelRoom> room = new ArrayList<resHotelRoom>();
	
	
	
	
	public ArrayList<resHotelRoom> getRoom() {
		return room;
	}
	public void setRoom(ArrayList<resHotelRoom> room) {
		this.room = room;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getMealBasis() {
		return mealBasis;
	}
	public void setMealBasis(String mealBasis) {
		this.mealBasis = mealBasis;
	}
	public String getMealBreakFast() {
		return mealBreakFast;
	}
	public void setMealBreakFast(String mealBreakFast) {
		this.mealBreakFast = mealBreakFast;
	}
	
	
	
}
