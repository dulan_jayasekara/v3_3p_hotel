package readXml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.xml.crypto.dsig.XMLValidateContext;

import navigation.errorMsgHandler;
import navigation.navigator;
import navigation.xmlNavigator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import currencyConvertion.currencyConveter;
import excelReader.ExcelReader;
import readAndWrite.bookingListReport;
import readAndWrite.cancellationDetails;
import readAndWrite.cancellationReport;
import readAndWrite.hotel;
import readAndWrite.paymentReadAndWrite;
import readAndWrite.profitAndLostReport;
import readAndWrite.readHotelResults;
import readAndWrite.readSearchDetails;
import readAndWrite.reservationReport;
import readAndWrite.thirdPartySupplierReport;
import readConfirmation.confirmationRead;
import readConfirmation.conhotel;
import reports.customerConfirmationEmail;
import reports.voucherMail;
import urlGenarator.xmlHome;
import verification.bookingListVerification;
import verification.cancellationVerify;
import verification.errorVerification;
import verification.hbFinalVerification;
import verification.hbPage1Res;
import verification.hbPreReservation;
import verification.hbRoomAvailability;
import verification.hbpage1Req;
import verification.hotelBedsPaymentResponse;
import verification.inv27Verification;
import verification.reservationReportVerify;
import verification.thirdpartySupReportVerification;
import verification.verifyNumberOfHotels;
import verification.whitesandsVerifications;

public class readXml {

	private static Map<String, String>  	txtProperty 		= new HashMap<String, String>();
	private static Map<String, String>  	supNhotel 			= new HashMap<String, String>();
	private static Map<String, String>  	profitMarkUp 		= new HashMap<String, String>();
	private static Map<String, String>  	bookingFee 			= new HashMap<String, String>();
	private static Map<String, String>  	cancellation 		= new HashMap<String, String>();
	
	private static WebDriver 				driverReadAndWrite;
	StringBuffer 							ReportPrinter 		= null;
	@Before
	public void setup()
	{
		
		 Properties properties 	= new Properties();
			try 
			{
				FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//logginInfo.properties"));
				properties.load(reader);
			} 
			catch (IOException e)
			{
				 System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value 		= properties.getProperty(key);
			    txtProperty.put(key, value);
			}
			
			try 
			{
				FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//profitMarkUps.properties"));
				properties.load(reader);
			} 
			catch (IOException e)
			{
				 System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value 		= properties.getProperty(key);
			    profitMarkUp.put(key, value);
			}
			
			try 
			{
				FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//bookingFee.properties"));
				properties.load(reader);
			} 
			catch (IOException e)
			{
				 System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value 		= properties.getProperty(key);
			    bookingFee.put(key, value);
			}
			
			try 
			{
				FileReader reader 	=new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//cancellation.properties"));
				properties.load(reader);
			} 
			catch (IOException e)
			{
				 System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value 		= properties.getProperty(key);
			    cancellation.put(key, value);
			}
		
		ReportPrinter        		= new StringBuffer();
		SimpleDateFormat sdf 		= new SimpleDateFormat("MM/dd/yyyy");
		
		ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>3rd Party Hotel Reservations Report("+sdf.format(Calendar.getInstance().getTime())+")</p></div>");
		ReportPrinter.append("<body>");
		String profile = txtProperty.get("firefoxProfile");
		FirefoxProfile prof			= new FirefoxProfile(new File(profile));
		//driver 					= new FirefoxDriver(prof);
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driverReadAndWrite			= new FirefoxDriver(prof);
		driverReadAndWrite.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		
			
			try 
			{
				//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
				FileReader reader =new FileReader(new File("D://workSpace//hotelXmlValidation//hotelAndSupplier.properties"));
				
				properties.load(reader);
			} 
			catch (IOException e)
			{
				 System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value = properties.getProperty(key);
			    supNhotel.put(key, value);
			}
		
	}
	
	@Test
	public void test() throws FileNotFoundException, InterruptedException, ParseException
	{
		System.out.println("test");
		System.out.println(txtProperty.get("url"));
		String currencyUrl					=	txtProperty.get("currencyUrl");
		String searchUrl					= 	txtProperty.get("searchUrl");
		String searchUrlWeb					= 	txtProperty.get("searchUrlWeb");
		xmlNavigator			xmlNav		= 	new xmlNavigator();
		readRequest	 			rr 			= 	new readRequest();
		ExcelReader 		newReader 		= new ExcelReader();
		readSearchDetails	searchgRead 	= new readSearchDetails();
		FileInputStream 	stream 			=new FileInputStream(new File("excel/searchDetails.xls"));
		
		ArrayList<Map<Integer, String>> ContentList = new ArrayList<Map<Integer,String>>();
		ContentList 						= newReader.contentReading(stream);
		
		Map<Integer, String>  content 		= new HashMap<Integer, String>();
		content   							= ContentList.get(0);
		
		Iterator<Map.Entry<Integer, String>>   mapiterator = content.entrySet().iterator();
		
		ScenarioIterator:while(mapiterator.hasNext())
		{
			Map.Entry<Integer, String> Entry = (Map.Entry<Integer, String>)mapiterator.next();
			String Content = Entry.getValue();
			String[] searchDetails = Content.split(",");
			
			if(searchDetails[11].equalsIgnoreCase("YES"))
			{
				System.out.println(searchDetails[9]);
				//logging
				driverReadAndWrite.get(txtProperty.get("url"));
				try {
					Alert alert = driverReadAndWrite.switchTo().alert();
			        alert.accept(); 
				} catch (Exception e) {
					// TODO: handle exception
				}
				driverReadAndWrite.findElement(By.id("user_id")).sendKeys(txtProperty.get("username"));
				driverReadAndWrite.findElement(By.id("password")).sendKeys(txtProperty.get("password"));
				driverReadAndWrite.findElement(By.id("loginbutton")).click();
				try 
				{
					WebDriverWait wait	 			= new WebDriverWait(driverReadAndWrite, 5);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("moduleimage")));
				    System.out.println("Logged in Successfully");
				} 
				catch (Exception e)
				{
					System.out.println("Login Failed");
				}
				
				//get currency details
				navigator 				nav			= new navigator();
				Map<String, String> 	currency	= new HashMap<String, String>();
										currency	= nav.getCurrency(driverReadAndWrite, currencyUrl);
				
				// get defualt currency
				String defaultCurrency 				= nav.getDefaultCurrency(driverReadAndWrite);
				System.out.println(defaultCurrency);
				
				WebDriverWait wait = new WebDriverWait(driverReadAndWrite, 20);
			/*	try 
				{
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("moduleimage")));
				    System.out.println("Logged in Successfully");
				} 
				catch (Exception e)
				{
					System.out.println("Login Failed");
				}*/
				
				//choose portal
				
				String hotel = searchDetails[9];
				String sup	= searchDetails[10];
				System.out.println(hotel);
				System.out.println(sup);
				
				if(searchDetails[8].equals("cc"))
				{
					System.out.println(supNhotel.get("supplier"));
					if(searchDetails[10].equals("hb"))
					{
						hotelbeds(xmlNav, rr, currency, defaultCurrency, nav, searchUrl, hotel, sup, searchDetails);
					}
					else if(searchDetails[10].equals("inv13"))
					{
						inv13(xmlNav, rr, currency, defaultCurrency, nav, searchUrl, hotel, sup, searchDetails);
					}
					else if(searchDetails[10].equals("white"))
					{
						whitesands(xmlNav, rr, currency, defaultCurrency, nav, searchUrl, hotel, sup, searchDetails);
					}
					else if(searchDetails[10].equals("inv27"))
					{
						inv27(xmlNav, rr, currency, defaultCurrency, nav, searchUrl, hotel, sup, searchDetails);
					}
				}
				else if(searchDetails[8].equals("web"))
				{
					if(searchDetails[10].equals("hb"))
					{
						hbWeb(xmlNav, rr, currency, defaultCurrency, nav, searchUrlWeb, hotel, sup, searchDetails);
					}	
					else if(searchDetails[10].equals("inv13"))
					{
						inv13Web(xmlNav, rr, currency, defaultCurrency, nav, searchUrlWeb, hotel, sup, searchDetails);
					}
				}
			}
		}	
	}
	
	@After
	public void terminate() throws IOException {
		try {
			Alert alert = driverReadAndWrite.switchTo().alert();
	        alert.accept(); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("Quit");
		//driverReadAndWrite.quit();
		ReportPrinter.append("</body></html>");
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("C://Users//Akila//git//v3_3p_hotel//reports//DynamicRateReport.html")));
		bwr.write(ReportPrinter.toString());
		bwr.flush();
		bwr.close();
	}
	
	public void whitesands(xmlNavigator xmlNav, readRequest rr, Map<String, String> currency, String defaultCurrency, navigator nav, String searchUrl, String hotel2, String sup, String[] searchDetails) throws FileNotFoundException, InterruptedException
	{
				System.out.println("Test -- 1");
				// search and get search details
				String url;
				readSearchDetails 		rsd								= 	nav.navigateToSearch(driverReadAndWrite, searchUrl, searchDetails);
				
				System.out.println("Test -- 2");
				//Read results
				ArrayList<readHotelResults> hotelList					= 	new ArrayList<readHotelResults>();
											hotelList 					= 	nav.readResults(driverReadAndWrite, rsd.getRoomCount(), "whitesands");
				System.out.println("Test -- 3");
				//chose from results
				ArrayList<String> 		tracerAndHotel 					= 	nav.navigateToResults(driverReadAndWrite,hotel2,sup);
				String 					tracer 							= 	tracerAndHotel.get(0);
				String					hotelName 						= 	tracerAndHotel.get(1);
				String 					error 							= 	tracerAndHotel.get(2);
				System.out.println(error);
				String 					errorType 						= 	tracerAndHotel.get(3);
				System.out.println(error);
				//get xml url
				xmlHome 				xmlUrl 							= 	new xmlHome();
				String 					xmlHomeUrl	 					= 	xmlUrl.generateUrl();
				
				if(error.equals("*Sorry! This product is no longer available. Please choose another."))
				{
					//read room availability res
					resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
					String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
					String					hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "white", 0);	
					try {
								
											resRoomAvailability 			= 	rr.readResponse(hbRoomAvailabilityResPageSource);
					} catch (Exception e) {
						// TODO: handle exception
						ReportPrinter.append("<br>"
								+ "<span>"+ error +"</span>"
								+ "<br>"
								+ "<span>Xml Error</span>"
								+ "<br>");
					}
					
					try {
						System.out.println(resRoomAvailability.getTotalItems());
						System.out.println(error);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						if(resRoomAvailability.getTotalItems().equals("0") && error.equals("*Sorry! This product is no longer available. Please choose another."))
						{
							ReportPrinter.append("<br>"
									+ "<span>"+ error +"</span>"
									+ "<br>"
									+ "<span>No results available</span>"
									+ "<br>");
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
					
				}
				else
				{
					System.out.println("Test -- 4");
					ArrayList<whiteSandsAvailabilityRes>	resAvailabilityHotels		=  	new ArrayList<whiteSandsAvailabilityRes>();
					for(int i = 0 ; i < 5 ; i++)
					{
						try {
							//read results xml response (page1)
							String 						whitesandsPage1ResUrl 			= 	xmlUrl.whitesandsPage1Res(tracer);
							String 						resPageSource					=	xmlNav.navigater(whitesandsPage1ResUrl, xmlHomeUrl, "white", i);
							resAvailabilityHotels 				= 	rr.whitesandshotelAvailabilityRes(resPageSource);
							System.out.println("Num of hotels -- " + resAvailabilityHotels.size());	
							break;
						}
						catch(Exception e)
						{
							
						}
					}
					
					System.out.println("Test -- 5");
					// read room availability request 
					for(int i = 0 ; i < 5 ; i++)
					{
						try {
							whitesandsRoomAvailabilityReq 	roomAvailabilityReq 			= 	new whitesandsRoomAvailabilityReq();
							String 							roomAvailabityReqUrl 			= 	xmlUrl.whitesandsroomAvailabilityReq(tracer);
							String 							roomAvailabilityReqPageSource	=	xmlNav.navigater(roomAvailabityReqUrl, xmlHomeUrl, "white" ,i);
															roomAvailabilityReq 			= 	rr.whitesandsRoomAvailabilityReq(roomAvailabilityReqPageSource);
															break;
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					
					
					System.out.println("Test -- 6");	
					// read room availability response
					for(int i = 0 ; i < 5 ; i++)
					{
						try {
							resHotelAvailability 			resAvailability 				= 	new resHotelAvailability();
							String							roomAvailabilityResUrl			= 	xmlUrl.whitesandsroomAvailabilityRes(tracer);
							String							roomAvailabilityResPageSource	=	xmlNav.navigater(roomAvailabilityResUrl, xmlHomeUrl, "white", i);
															resAvailability					=	rr.whitesandsRoomAvailabilityRes(roomAvailabilityResPageSource);
															break;
						} catch (Exception e) {
							// TODO: handle exception
						}
						
					}
					System.out.println("Test -- 7");
					// verify hotel list 1
					whitesandsVerifications wv = new whitesandsVerifications();
					wv.verifyHotelList(ReportPrinter, hotelList, resAvailabilityHotels, defaultCurrency, currency, searchDetails[2], searchDetails[3]);
					
					
					System.out.println("Test -- 8");
					//Read and fill payment page
					paymentReadAndWrite 	payment		= 	new paymentReadAndWrite();
					hotel					hotel		= 	new hotel();
											hotel 		= 	payment.paymentRead(driverReadAndWrite);
					System.out.println(hotel.getCurrencyCode());
					System.out.println("Test -- 9");
					// read room availability request 
					whitesandsRoomAvailabilityReq 	roomAvailabilityReq2 			= 	new whitesandsRoomAvailabilityReq();
					String 							roomAvailabityReqUrl2 			= 	xmlUrl.whitesandsroomAvailabilityReq(tracer);
					String 							roomAvailabilityReqPageSource2	=	xmlNav.navigater(roomAvailabityReqUrl2, xmlHomeUrl, "white", 0);
													roomAvailabilityReq2 			= 	rr.whitesandsRoomAvailabilityReq(roomAvailabilityReqPageSource2);
					System.out.println("Test -- 10");	
					// read room availability response
					resHotelAvailability 					resAvailability2 				= new resHotelAvailability();
					for(int i = 0 ; i < 5 ; i ++)
					{
						try {
							
							String							roomAvailabilityResUrl2			= 	xmlUrl.whitesandsroomAvailabilityRes(tracer);
							String							roomAvailabilityResPageSource2	=	xmlNav.navigater(roomAvailabilityResUrl2, xmlHomeUrl, "white", i);
															resAvailability2				=	rr.whitesandsRoomAvailabilityRes(roomAvailabilityResPageSource2);
															break;
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					
					System.out.println("Test -- 11");
					// read pre reservation request
					whitesandsRoomAvailabilityReq 	roomAvailabilityPreReq 				= 	new whitesandsRoomAvailabilityReq();
					String 							roomAvailabilityPreReqUrl			= 	xmlUrl.whitesandsroomAvailabilityPreReq(tracer);
					String 							roomAvailabilityPreReqPageSource	=	xmlNav.navigater(roomAvailabilityPreReqUrl, xmlHomeUrl, "white", 0);
													roomAvailabilityPreReq 				= 	rr.whitesandsRoomAvailabilityReq(roomAvailabilityPreReqPageSource);
					System.out.println("Test -- 12");								
					// read pre reservation response
					for(int i = 0 ; i < 5 ; i++)
					{
						try {
							System.out.println(i);
							whitesandsPreReservationRes		roomAvailabilityPreRes				=	new whitesandsPreReservationRes();
							String							roomAvailabilityPreResUrl			= 	xmlUrl.whitesandsroomAvailabilityPreRes(tracer);
							String							roomAvailabilityPreResPageSource	=	xmlNav.navigater(roomAvailabilityPreResUrl, xmlHomeUrl, "white", i);
															roomAvailabilityPreRes				=	rr.whitesandsPreReservetionresponse(roomAvailabilityPreResPageSource);
							break;
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					System.out.println("Test -- 13");
					
					//read the bill
					confirmationRead 		conRead 						= 	new confirmationRead();
					conhotel				conHotel 						= 	new conhotel();
											conHotel						= 	conRead.comfirmationRead(driverReadAndWrite, resAvailability2, ReportPrinter, searchDetails);
					resHotelAvailability	resAvailabilityFinalRes 		= 	new resHotelAvailability();
					hotelAvailability 		reqAvailabilityFinalReq 		=	new hotelAvailability();
					hbFinalVerification 	finalVeri 						= 	new hbFinalVerification();
					
					//read reservation request
					for(int i = 0 ; i < 5 ; i++)
					{
						try {
							ArrayList<whitesandsReservationReq> reservationReq 				= 	new ArrayList<whitesandsReservationReq>();
							String								reservationReqUrl 			= 	xmlUrl.whitesandsReservationReq(tracer);
							String								reservationReqPageSource	= 	xmlNav.navigater(reservationReqUrl, xmlHomeUrl, "white", i);
																reservationReq 				= 	rr.whitesandsReservationReq(reservationReqPageSource);
							break;
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					
					try {
						//reservation report
						reservationReport resReport = new reservationReport();	
						resReport = nav.reservationReport(txtProperty.get("reservationReport"), driverReadAndWrite, conHotel);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						//booking list report
						bookingListReport blr = new bookingListReport();
						blr = nav.bookingListReport(driverReadAndWrite, txtProperty.get("bookingListReport"), conHotel);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						//profit and lose report
						profitAndLostReport profitNloss = new profitAndLostReport();
						profitNloss = nav.profitAndLossReport(txtProperty.get("profitNlossReportUrl"), driverReadAndWrite, conHotel, searchDetails);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						//read 3rd party supplier payable report
						thirdPartySupplierReport tpsr = new thirdPartySupplierReport();
						tpsr = nav.read3rdPartySupplierPayableReport(driverReadAndWrite, txtProperty.get("thirdPartySupplierPayableUrl"), conHotel);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						//read customer confirmation mail
						customerConfirmationEmail cusCon = new customerConfirmationEmail();
						cusCon	=	nav.customerConfirmationMail(driverReadAndWrite, txtProperty.get("confirmationreportUrl"), conHotel);
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					
					//cancellation process
					nav.cancellation(driverReadAndWrite, txtProperty.get("cancellationUrl"), conHotel.getReservationNumber(), cancellation);
					
				}	
	}
	
	public void miki(xmlNavigator xmlNav, readRequest rr, Map<String, String> currency, String defaultCurrency, navigator nav, String searchUrl, String hotel2, String sup, String[] searchDetails)
	{
		
	}
	
	public void inv27(xmlNavigator xmlNav, readRequest rr, Map<String, String> currency, String defaultCurrency, navigator nav, String searchUrl, String hotel2, String sup, String[] searchDetails) throws FileNotFoundException, InterruptedException
	{
		// search and get search details
		
		String url;
		readSearchDetails 		rsd								= 	nav.navigateToSearch(driverReadAndWrite, searchUrl, searchDetails);
				
		//Read results
		ArrayList<readHotelResults> hotelList					= 	new ArrayList<readHotelResults>();
									hotelList 					= 	nav.readResults(driverReadAndWrite, rsd.getRoomCount(), "INV27");
					
		//chose from results
		ArrayList<String> 		tracerAndHotel 					= 	nav.navigateToResults(driverReadAndWrite,hotel2,sup);
		String 					tracer 							= 	tracerAndHotel.get(0);
		String					hotelName 						= 	tracerAndHotel.get(1);
		String 					error 							= 	"";
		
		String 					errorType 						= 	tracerAndHotel.get(3);
		//get xml url
		xmlHome 				xmlUrl 							= 	new xmlHome();
		String 					xmlHomeUrl	 					= 	xmlUrl.generateUrl();
		
		//Results page error verification
		errorMsgHandler emh = new errorMsgHandler();
		if(error.equals("*Sorry, rates got changed. please search again!"))
		{
			emh.resultsPageError(ReportPrinter, xmlUrl, xmlHomeUrl, xmlNav, tracer, rr, error, errorType, "inv27");
		}
		
		//Read page1 xml response
		ArrayList<resHotelAvailability>	resAvailability			=  	new ArrayList<resHotelAvailability>();
		String 					inv13Page1ResUrl 				= 	xmlUrl.inv27Page1Res(tracer);
		String 					resPageSource					=	xmlNav.navigater(inv13Page1ResUrl, xmlHomeUrl, "inv27", 0);
								resAvailability 				= 	rr.inv27Page1Response(resPageSource);
		
		//hotel list verification
		inv27Verification veri = new inv27Verification();
		veri.hotelList(ReportPrinter, hotelList, resAvailability, defaultCurrency, currency);
								
		// read search request xml
		hbPage1Req				page1Req;
		String 					inv13SearchUrl					= 	xmlUrl.inv27SearchReq(tracer);
		String 					hbSearchPageSource				= 	xmlNav.navigater(inv13SearchUrl, xmlHomeUrl, "inv27", 0);
								page1Req 						= 	rr.inv27page1Req(hbSearchPageSource);
								

		//read room availability req
		hbPage1Req  			roomAvailabilityReq 				=  	new hbPage1Req();
		String 					inv27RoomAvailabilityReq 			= 	xmlUrl.inv27RoomAvailabilityReq(tracer);
		String					inv27RoomAvailabilityReqPageSource	= 	"" ; 
								inv27RoomAvailabilityReqPageSource 	= 	xmlNav.navigater(inv27RoomAvailabilityReq, xmlHomeUrl, "inv27", 0);
								roomAvailabilityReq 				= 	rr.inv27RoomaAvailabilityReq(inv27RoomAvailabilityReqPageSource);
							
		//read room availability res
		resHotelAvailability 	resRoomAvailability 				= 	new resHotelAvailability();
		String					inv27RoomAvailabilityRes 			= 	xmlUrl.inv27roomAvailabilityRes(tracer);
		String					inv27RoomAvailabilityResPageSource 	= 	xmlNav.navigater(inv27RoomAvailabilityRes, xmlHomeUrl, "inv27", 0);			
								resRoomAvailability 				= 	rr.inv27RoomAvailabilityRes(inv27RoomAvailabilityResPageSource);
								
		// verify room availability req against res
		veri.roomAvailabilityResAgainstReq(ReportPrinter, roomAvailabilityReq, resRoomAvailability);
								
		//Read and fill payment page
		paymentReadAndWrite 	payment		= 	new paymentReadAndWrite();
		hotel					hotel		= 	new hotel();
		hotel = payment.paymentRead(driverReadAndWrite);
	    System.out.println(hotel.getPaymentError());
	    
	    //Get hotel address from DM
	    String hotelAddress = nav.getHotelAddress(txtProperty, resRoomAvailability);
	    
	    // verify payment page against room availability response
	    veri.paymentPageAgainstRoomAvalabilityRes(ReportPrinter, hotel, resRoomAvailability, profitMarkUp, hotelAddress);
	    
	    //pre reservation request
		hbPage1Req 				preReqAvailability 				=	new hbPage1Req();
		String					preReqUrl						= 	xmlUrl.inv27PreReq(tracer);
								resPageSource 					= 	xmlNav.navigater(preReqUrl, xmlHomeUrl, "inv27", 0);
								preReqAvailability				=	 rr.inv27PreReq(resPageSource);
		
		//pre reservation response
		resHotelAvailability 	preResAvailability 				= 	new resHotelAvailability();
		String					preResUrl						= 	xmlUrl.inv27PreRes(tracer);
								resPageSource 					= 	xmlNav.navigater(preResUrl, xmlHomeUrl, "inv27", 0);
								preResAvailability				=	rr.inv27PreRes(resPageSource);
	    
		//read the bill
		confirmationRead 		conRead 						= 	new confirmationRead();
		conhotel				conHotel 						= 	new conhotel();
								conHotel						= 	conRead.comfirmationRead(driverReadAndWrite, resRoomAvailability, ReportPrinter, searchDetails);
		resHotelAvailability	resAvailabilityFinalRes 		= 	new resHotelAvailability();
		hbPage1Req 				reqAvailabilityFinalReq 		=	new hbPage1Req();
		hbFinalVerification 	finalVeri 						= 	new hbFinalVerification();
		System.out.println(conHotel.getError());
		
		//final reservation request
		String 					inv27finalReqUrl 				= 	xmlUrl.inv27FinalReq(tracer);
								resPageSource 					= 	xmlNav.navigater(inv27finalReqUrl, xmlHomeUrl, "hb", 0);
								reqAvailabilityFinalReq 		= 	rr.inv27FinalReq(resPageSource);
			
		//final reservation response
		String 					finalResUrl 					= 	xmlUrl.hbFinalRes(tracer);
		try {
			resPageSource 					= 	xmlNav.navigater(finalResUrl, xmlHomeUrl, "hb", 0);
			resAvailabilityFinalRes 		= 	rr.readFinalResponse(resPageSource);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			System.out.println(resAvailabilityFinalRes.getError());
		} catch (Exception e) {
			// TODO: handle exception
		}
	    
		
	}
	
	public void hotelbeds(xmlNavigator xmlNav, readRequest rr, Map<String, String> currency, String defaultCurrency, navigator nav, String searchUrl, String hotel2, String sup, String[] searchDetails) throws FileNotFoundException, InterruptedException
	{
		// search and get search details
		
		String url;
		readSearchDetails 		rsd								= 	nav.navigateToSearch(driverReadAndWrite, searchUrl, searchDetails);
		
		//Read results
		//ArrayList<readHotelResults> hotelList					= 	new ArrayList<readHotelResults>();
									//hotelList 					= 	nav.readResults(driverReadAndWrite, rsd.getRoomCount(), "hotelbeds_v2");
			
		//chose from results
		ArrayList<String> 		tracerAndHotel 					= 	nav.navigateToResults(driverReadAndWrite,hotel2,sup);
		String 					tracer 							= 	tracerAndHotel.get(0);
		String					hotelName 						= 	tracerAndHotel.get(1);
		String 					error 							= 	"";
		if(tracerAndHotel.get(2).equals(null) || tracerAndHotel.get(2).equals("") || tracerAndHotel.get(2).isEmpty())
		{
			
		}
		else
		{
								error							= 	tracerAndHotel.get(2);
		}
								
		System.out.println(error);
		String 					errorType 						= 	tracerAndHotel.get(3);
		//get xml url
		xmlHome 				xmlUrl 							= 	new xmlHome();
		String 					xmlHomeUrl	 					= 	xmlUrl.generateUrl();
		errorMsgHandler emh = new errorMsgHandler();
		if(error.equals("*Sorry! This product is no longer available. Please choose another."))
		{
			emh.resultsPageError(ReportPrinter, xmlUrl, xmlHomeUrl, xmlNav, tracer, rr, error, errorType, "hb");
		}
		else if(error.equals("*Sorry, rates got changed. please search again!"))
		{
			emh.resultsPageError(ReportPrinter, xmlUrl, xmlHomeUrl, xmlNav, tracer, rr, error, errorType, "hb");
		}
		else
		{
			
			//Read page1 xml response
			resHotelAvailability	resAvailability					=  	new resHotelAvailability();
			String 					hbPage1ResUrl 					= 	xmlUrl.hbPage1Res(tracer);
			String 					resPageSource					=	xmlNav.navigater(hbPage1ResUrl, xmlHomeUrl, "hb", 0);
			try {
									resAvailability 				= 	rr.hbPage1Response(resPageSource);
			} catch (Exception e) {
				// TODO: handle exception
			}
								
			System.out.println(resPageSource);
			if(resPageSource.equals(null))
			{
				System.out.println("no hb results");
			}
			//verify number of hotels 1.1
			verifyNumberOfHotels numHotels = new verifyNumberOfHotels();
			//numHotels.verify(hotelList.size(), resAvailability.getHotelDetails().size(), ReportPrinter);
			
			//verify searching hotel availability 1.2
			//numHotels.searchingHotel(resAvailability, hotel2, ReportPrinter, hotelList);
			
			
			// read search request xml
			hbPage1Req				page1Req;
 			String 					hbSearchUrl						= 	xmlUrl.hbSearchReq(tracer);
			String 					hbSearchPageSource				= 	xmlNav.navigater(hbSearchUrl, xmlHomeUrl, "hb", 0);
									page1Req 						= 	rr.page1Req(hbSearchPageSource);
			//System.out.println(page1Req.getCheckin());
			//System.out.println(page1Req.getGuest().get(0).getCus().get(0).getAge());
			
			// verify search 1.4
			hbpage1Req				searchReq 						= 	new hbpage1Req();
			searchReq.verify(page1Req, rsd, ReportPrinter, "hotelbeds");
			
			//Hotel count
			countHotels 			hotelCount						= 	new countHotels();
			//hotelCount.hotelCount(hotelList);
			
			//read room availability req
			hbPage1Req  			roomAvailabilityReq 			=  	new hbPage1Req();
			String 					hbRoomAvailabilityReq 			= 	xmlUrl.hbRoomAvailabilityReq(tracer);
			String					hbRoomAvailabilityReqPageSource = 	"" ; 
			hbRoomAvailabilityReqPageSource = 	xmlNav.navigater(hbRoomAvailabilityReq, xmlHomeUrl, "hb", 0);
			try {
									roomAvailabilityReq 			= 	rr.hbRoomaAvailabilityReq(hbRoomAvailabilityReqPageSource);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			
			//read room availability res
			resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
			String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
			String					hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "hb", 0);			
									resRoomAvailability 			= 	rr.readResponse(hbRoomAvailabilityResPageSource);
			
			//rates changed verification 1.4
			if(error.equals("*Sorry, rates got changed. please search again!"))
			{
				numHotels.rateChange(error, resRoomAvailability, resAvailability, hotel2, ReportPrinter);
			}
				
			numHotels.rateChange(error, resRoomAvailability, resAvailability, hotel2, ReportPrinter);
			//verify room availability req against web
			hbRoomAvailability 		hbRmAvailability 				= 	new hbRoomAvailability();
			//hbRmAvailability.availablityReqAgainstWeb(ReportPrinter, roomAvailabilityReq, hotelList, hotelName);
			
			//verify room availability req against res 3 and 3.1
			hbRmAvailability.availablityReqAgaistRes(ReportPrinter, roomAvailabilityReq, resRoomAvailability, error, errorType, defaultCurrency, currency);
			
			//verify search req against res
			//searchReq.searchReqAgainstRes(ReportPrinter, page1Req, resAvailability);
			
			//check whether results available
			String 					hbhotelCount 					= 	rr.hbhotelCount(resPageSource);
			
			//verify page1 response
			//hbPage1Res 				hbPage1verify 					= 	new hbPage1Res();
			//hbPage1verify.verify(hotelList, resAvailability, ReportPrinter, defaultCurrency, currency, hbhotelCount);
			
			// get hotelBeds payment request url 
			String 					hbReqUrl 						= 	xmlUrl.hotelBedsReqUrl(tracer);
			System.out.println(hbReqUrl);
				
			//Read and fill payment page
			paymentReadAndWrite 	payment		= 	new paymentReadAndWrite();
			hotel					hotel		= 	new hotel();
			hotel = payment.paymentRead(driverReadAndWrite);
			System.out.println(hotel.getPaymentError());
			
			if(!hotel.getPaymentError().equals(""))
			{
				if(hotel.getPaymentError().equals("*BLOCKER~Hotel confirmation failed."))
				{
					emh.paymentError(ReportPrinter, hotel.getPaymentError(), xmlUrl, xmlNav, tracer, xmlHomeUrl, rr);
				}
			}
			else
			{
				
			}
			
			//hb verification room availability respons against web (6)
			hbRmAvailability.availablityResAgainstWeb(hotel, ReportPrinter, resRoomAvailability, currency, defaultCurrency);
			
			//reservation request
			/*hotelAvailability reqAvailability 	=	new hotelAvailability();
			driver.get("http://xml-test.secure-reservation.com/jsp/Browser.jsp?sort=-3&file=%2Fvar%2Flog%2Frezg%2Fapp%2Fhotelxml%2F30-06-2014%2Fstaging%2Frezpackage%2Frezpackage_hotelbeds_v2_RoomAvailabilityRequest_0ae96695-4f89-49f2-a686-e2e5bdb6c49c_2014-06-30T05-37-53-901.xml");
			String pageSource 					= driver.getPageSource();
			readRequest rr 						= new readRequest();
			reqAvailability 					= rr.readRequest(pageSource);
			*/
			//reservation response
			
			
			/*
			//reservation request
			hotelAvailability reqAvailability2 =	new hotelAvailability();
			driver.get("http://xml-test.secure-reservation.com/jsp/Browser.jsp?sort=-3&file=%2Fvar%2Flog%2Frezg%2Fapp%2Fhotelxml%2F01-07-2014%2Fstaging%2Frezpackage%2Frezpackage_hotelbeds_v2_RoomAvailabilityRequest_c2ed8e59-47a7-440a-99ef-05680928a592_2014-07-01T06-40-59-579.xml");
			pageSource = driver.getPageSource();
			reqAvailability2 = rr.readRequest(pageSource);
			*/
			
			
			//reservation response
			resHotelAvailability 	resAvailability2 = new resHotelAvailability();
			
			// get hotelBeds payment response url
			String 					hbResUrl 						= 	xmlUrl.hotelBedsResUrl(tracer);
									resPageSource					=	xmlNav.navigater(hbResUrl, xmlHomeUrl, "hb", 0);
									resAvailability2 				= 	rr.readResponse(resPageSource);
			
			// payment verification 7
			hotelBedsPaymentResponse	hbPaymentResponse			=  	new hotelBedsPaymentResponse();
			hbPaymentResponse.verify(resAvailability2, hotel, defaultCurrency, currency, ReportPrinter);
			
			//pre reservation request
			hotelAvailability 		preReqAvailability 				=	new hotelAvailability();
			String					preReqUrl						= 	xmlUrl.hbPreReq(tracer);
									resPageSource 					= 	xmlNav.navigater(preReqUrl, xmlHomeUrl, "hb", 0);
									preReqAvailability 				= 	rr.readPreRequest(resPageSource);
			
			//pre reservation response
			resHotelAvailability 	preResAvailability 				= 	new resHotelAvailability();
			String					preResUrl						= 	xmlUrl.hbPreRes(tracer);
									resPageSource 					= 	xmlNav.navigater(preResUrl, xmlHomeUrl, "hb", 0);
									preResAvailability				=	rr.readResponse(resPageSource);
			
			//hb pre reservation verification (8)
			hbPreReservation 		hbPre 							= 	new hbPreReservation();
			hbPre.verifyPreReservation(preReqAvailability, preResAvailability, ReportPrinter);
			
			
			
			//read the bill
			confirmationRead 		conRead 						= 	new confirmationRead();
			conhotel				conHotel 						= 	new conhotel();
									conHotel						= 	conRead.comfirmationRead(driverReadAndWrite, resAvailability2, ReportPrinter, searchDetails);
			resHotelAvailability	resAvailabilityFinalRes 		= 	new resHotelAvailability();
			hotelAvailability 		reqAvailabilityFinalReq 		=	new hotelAvailability();
			hbFinalVerification 	finalVeri 						= 	new hbFinalVerification();
			System.out.println(conHotel.getError());
			
			//final reservation request
			String 					finalReqUrl 					= 	xmlUrl.hbFinalReq(tracer);
									resPageSource 					= 	xmlNav.navigater(finalReqUrl, xmlHomeUrl, "hb", 0);
									reqAvailabilityFinalReq 		= 	rr.readRequest(resPageSource);
				
			//final reservation response
			String 					finalResUrl 					= 	xmlUrl.hbFinalRes(tracer);
			try {
				resPageSource 					= 	xmlNav.navigater(finalResUrl, xmlHomeUrl, "hb", 0);
				resAvailabilityFinalRes 		= 	rr.readFinalResponse(resPageSource);
			} catch (Exception e) {
				// TODO: handle exception
				
			}
			try {
				System.out.println(resAvailabilityFinalRes.getError());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//confirmation error
			finalVeri.confirmationError(conHotel.getError(), resAvailabilityFinalRes.getError(), ReportPrinter);
				
			//verify req against confirmation page 9
			try {
				finalVeri.reqAgainstCon(reqAvailabilityFinalReq, conHotel, ReportPrinter);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
				
			//verify res against confirmation page 10
			try {
				finalVeri.ccresAgainstCon(resAvailabilityFinalRes, conHotel, ReportPrinter, defaultCurrency, currency);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
				
			//read the bill
			//confirmationRead 		conRead 						= new confirmationRead();
			//conhotel				conHotel 						= new conhotel();
			//conHotel												= conRead.comfirmationRead(driverReadAndWrite, resAvailability2, ReportPrinter);
			//resHotelAvailability	resAvailabilityFinalRes 		= new resHotelAvailability();
			//hbPage1Req 				reqAvailabilityFinalReq 		= new hbPage1Req();
			//hbFinalVerification 	finalVeri 						= new hbFinalVerification();
			
			//reservation report
			reservationReport resReport = new reservationReport();	
			try {
				resReport = nav.reservationReport(txtProperty.get("reservationReport"), driverReadAndWrite, conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//booking list report
			bookingListReport blr = new bookingListReport();
			try {
				blr = nav.bookingListReport(driverReadAndWrite, txtProperty.get("bookingListReport"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//verify reservation report 11
			reservationReportVerify resReportVeri = new reservationReportVerify();
			try {
				resReportVeri.verify(resAvailabilityFinalRes, resReport, conHotel, ReportPrinter, "hotelbeds", "Call Center", searchDetails, currency, defaultCurrency, profitMarkUp);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			canDeadRes 				pen 							= 	new canDeadRes();
			//verify booking list report 12
			bookingListVerification blv = new bookingListVerification();
			try {
				blv.verify(resAvailabilityFinalRes, blr, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, currency, finalResUrl, pen, currency);
			} catch (Exception e) {
			}
			
			
			//profit and lose report
			profitAndLostReport profitNloss = new profitAndLostReport();
			try {
				profitNloss = nav.profitAndLossReport(txtProperty.get("profitNlossReportUrl"), driverReadAndWrite, conHotel, searchDetails);
			} catch (Exception e) {
			}
			
			
			//verify profit and loss report 13
			try {
				resReportVeri.profitAndLossVerify(resAvailabilityFinalRes, profitNloss, conHotel, ReportPrinter, "hotelbeds", "Call Center", searchDetails, defaultCurrency, currency, profitMarkUp);
			} catch (Exception e) {
			}
			
			//read 3rd party supplier payable report
			thirdPartySupplierReport tpsr = new thirdPartySupplierReport();
			try {
				tpsr = nav.read3rdPartySupplierPayableReport(driverReadAndWrite, txtProperty.get("thirdPartySupplierPayableUrl"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//verify 3rd party supplier payable report 14
			thirdpartySupReportVerification tpsrv = new thirdpartySupReportVerification();
			try {
				tpsrv.verify(resAvailabilityFinalRes, tpsr, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, defaultCurrency, currency, bookingFee, profitMarkUp);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//read customer confirmation mail
			customerConfirmationEmail cusCon = new customerConfirmationEmail();
			try {
				cusCon	=	nav.customerConfirmationMail(driverReadAndWrite, txtProperty.get("confirmationreportUrl"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//verifyn customer confirmation mail 15
			try {
				resReportVeri.cchbCustomerConfirmationmail(cusCon, resAvailabilityFinalRes, conHotel, ReportPrinter, defaultCurrency, currency);
			} catch (Exception e) {
				// TODO: handle exception
			}
	
			
			//cancellation process
			cancellationDetails   cd = new cancellationDetails();
			cd = nav.cancellation(driverReadAndWrite, txtProperty.get("cancellationUrl"), conHotel.getReservationNumber(), cancellation);
			System.out.println(cd.getMsgType());
			System.out.println(cd.getMsg());		
			//read cancellation request
			cancellationReq 					canReq 								= 	new cancellationReq();
			try {
				String							canReqUrl							= 	xmlUrl.hbcancellationReq(tracer);
				String							canReqPageSource					= 	xmlNav.navigater(canReqUrl, xmlHomeUrl, "white", 0);
												canReq								= 	rr.cancelReq(canReqPageSource);
			} catch (Exception e) {
				 //TODO: handle exception
			}
			
			//read cancellation response
			resHotelAvailability		canResponse							= 	new resHotelAvailability();
			try {
				String 					cancellationResUrl 					= 	xmlUrl.hbCancellationRes(tracer);
										resPageSource 						= 	xmlNav.navigater(cancellationResUrl, xmlHomeUrl, "hb", 0);
										canResponse 						= 	rr.hbcancellationResponse(resPageSource);	
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println(canResponse.getStatus());
			
			//read Cancellation report
			cancellationReport cr = new cancellationReport();	
			try {								
				cr	=	nav.cancellationreport(driverReadAndWrite, conHotel.getReservationNumber(), txtProperty.get("cancellationReportUrl"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//verify cancellation report 16
			cancellationVerify cv = new cancellationVerify();
			cv.hb(ReportPrinter, cd, canResponse, conHotel, "Call Center", defaultCurrency, currency, cr);
			
			//read 3rd party supplier payable report
			String thirdPartyAvailability = "";
			try {
				thirdPartyAvailability = nav.thirdpartySupPayableAftrCancellation(driverReadAndWrite, txtProperty.get("thirdPartySupplierPayableUrl"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			// verify third party sup payable report after cancellation17
			tpsrv.verifyAfterCancellation(ReportPrinter, thirdPartyAvailability);
			
			//reservation report
			String resReport2 = "";	
			String resReportAvailability = "available";
			try {
				resReport2 = nav.resReportAfterCancellation(driverReadAndWrite, txtProperty.get("reservationReport"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
				resReportAvailability = "not available";
			}
			
			// verify reservation report after cancellation 18
			resReportVeri.verifyResReportAfterCancellation(ReportPrinter, resReport2);
			
			System.out.println(resReportAvailability);
			//booking list report
			bookingListReport blr2 = new bookingListReport();
			try {
				blr2 = nav.bookingListReport(driverReadAndWrite, txtProperty.get("bookingListReport"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	
	public void hbWeb(xmlNavigator xmlNav, readRequest rr, Map<String, String> currency, String defaultCurrency, navigator nav, String searchUrlWeb, String hotel2, String sup, String[] searchDetails) throws FileNotFoundException, InterruptedException, ParseException
	{
		/*//read customer confirmation mail
		customerConfirmationEmail cce = new customerConfirmationEmail();
		cce = nav.customerConfirmationMail(driverReadAndWrite, txtProperty.get("confirmationreportUrl"));
		System.out.println(cce.getAddress1());
		System.out.println(cce.getRoom().get(0).getMealPlan());
		System.out.println(cce.getOccupancy().get(0).getFirstname());
		
		//read customer voucher mail
		voucherMail voucher = new voucherMail();
		voucher = nav.customerVoucherMail(driverReadAndWrite, txtProperty.get("confirmationreportUrl"));*/
		
		// search and get search details
		System.out.println("test -- 0");
		String url;
		readSearchDetails rsd	 = 	nav.navigateToSearchWeb(driverReadAndWrite, searchUrlWeb, ReportPrinter, searchDetails);
		System.out.println("test -- 0.1");
		
		
		//Read results
		ArrayList<readHotelResults> hotelList	= new ArrayList<readHotelResults>();
		hotelList = nav.readResultsWeb(driverReadAndWrite, rsd.getRoomCount());
		System.out.println("test -- 0.2");
		
		
		//chose from results
		ArrayList<String> tracerNhotel = new ArrayList<String>();
		String 	hotelName 	= 	"";
		String	tracer		=	"";
		String	errorType	=	"";
		String	error		=	"";
		try {
			tracerNhotel			=	nav.navigateToResultsWeb(driverReadAndWrite, ReportPrinter, hotel2, sup);
			hotelName				= 	tracerNhotel.get(0);
			tracer 					= 	tracerNhotel.get(1);
			errorType 				= 	tracerNhotel.get(2);
			error 					= 	tracerNhotel.get(3);
			System.out.println(error);
			System.out.println(tracer);		
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("test -- 0.3");
		//get xml url
		xmlHome xmlUrl 			= new xmlHome();
		String xmlHomeUrl	 	= xmlUrl.generateUrl();
		System.out.println(xmlHomeUrl);
		//System.out.println(error);
		System.out.println("test -- 0.4");
		
		if(error.equals("*Sorry! This product is no longer available. Please choose another."))
		{
			//read room availability res
			resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
			String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
			String					hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "hb", 0);	
			try {
									resRoomAvailability 			= 	rr.readResponse(hbRoomAvailabilityResPageSource);
									System.out.println(resRoomAvailability.getError());
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(resRoomAvailability.getError());
				ReportPrinter.append("<br>"
						+ "<span>"+ error +"</span>"
						+ "<br>"
						+ "<span>Xml Error</span>"
						+ "<br>");
			}
			System.out.println(error);
			if(resRoomAvailability.getError().contains(". This TO can not make reservations."))
			{
				ReportPrinter.append("<br>"
						+ "<span>"+ error +"</span>"
						+ "<br>"
						+ "<span>"+ resRoomAvailability.getError() +"</span>"
						+ "<br>");
			}
			System.out.println(resRoomAvailability.getError());
			try {
				System.out.println(resRoomAvailability.getTotalItems());
				System.out.println(error);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				if(resRoomAvailability.getTotalItems().equals("0") && error.equals("*Sorry! This product is no longer available. Please choose another."))
				{
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>No results available</span>"
							+ "<br>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		else
		{
			System.out.println("test --- 1");
			
			// read search request xml
			hbPage1Req				page1Req;
			String 					hbSearchUrl						= 	xmlUrl.hbSearchReq(tracer);
			String 					hbSearchPageSource				=	xmlNav.navigater(hbSearchUrl, xmlHomeUrl, "hb", 0);
			page1Req 				= rr.page1Req(hbSearchPageSource);
			System.out.println(page1Req.getCheckin());
			
			//System.out.println(page1Req.getGuest().get(0).getCus().get(0).getAge());
			System.out.println("test --- 2");
			
			// verify search
			hbpage1Req				searchReq 						= 	new hbpage1Req();
			searchReq.verify(page1Req, rsd, ReportPrinter, "hotelbeds");
			System.out.println("test --- 3");
			
			//Hotel count
			countHotels 			hotelCount						= 	new countHotels();
			hotelCount.hotelCount(hotelList);
			System.out.println("test --- 4");
			
			//read room availability req
			hbPage1Req  			roomAvailabilityReq 			=  	new hbPage1Req();
			String 					hbRoomAvailabilityReq 			= 	xmlUrl.hbRoomAvailabilityReq(tracer);
			String					hbRoomAvailabilityReqPageSource = 	"" ; 
			try {
									hbRoomAvailabilityReqPageSource = 	xmlNav.navigater(hbRoomAvailabilityReq, xmlHomeUrl, "hb", 0);
									roomAvailabilityReq 			= 	rr.hbRoomaAvailabilityReq(hbRoomAvailabilityReqPageSource);
			} catch (Exception e) {
				// TODO: handle exception
			}
			System.out.println("test --- 5");
			
			//read room availability res
			resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
			String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
			String					hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "hb", 0);			
			resRoomAvailability 	= rr.readResponse(hbRoomAvailabilityResPageSource);
			System.out.println("test --- 6");
			
			//verify room availability req against web
			hbRoomAvailability hbRmAvailability = new hbRoomAvailability();
			hbRmAvailability.availablityReqAgainstWeb(ReportPrinter, roomAvailabilityReq, hotelList, hotelName);
			System.out.println("test --- 7");
			
			//verify room availability req against res
			hbRmAvailability.availablityReqAgaistRes(ReportPrinter, roomAvailabilityReq, resRoomAvailability, error, errorType, defaultCurrency, currency);
			System.out.println("test --- 8");
			
			//Read page1 xml response
			resHotelAvailability	resAvailability					=  	new resHotelAvailability();
			String 					hbPage1ResUrl 					= 	xmlUrl.hbPage1Res(tracer);
			String 					resPageSource					= 	xmlNav.navigater(hbPage1ResUrl, xmlHomeUrl, "hb", 0);
			resAvailability 		= rr.hbPage1Response(resPageSource);
			System.out.println("test --- 9");
			
			//Select hotel error handling
			errorVerification ev = new errorVerification();
			ev.selectHotelError(error, errorType, resAvailability, resRoomAvailability, hotelName, defaultCurrency, currency, ReportPrinter, hotelList);
			System.out.println("test --- 10");
			
			//verify search req against res
			searchReq.searchReqAgainstRes(ReportPrinter, page1Req, resAvailability);
			System.out.println("test --- 11");
			
			//check whether results available
			String hbhotelCount 	= rr.hbhotelCount(resPageSource);
			System.out.println("test --- 12");
			
			//verify page1 response
			hbPage1Res 				hbPage1verify 					= new hbPage1Res();
			hbPage1verify.verify(hotelList, resAvailability, ReportPrinter, defaultCurrency, currency, hbhotelCount);
			System.out.println("test --- 13");
			
			// get hotelBeds payment request url 
			String 					hbReqUrl 						= 	xmlUrl.hotelBedsReqUrl(tracer);
			System.out.println(hbReqUrl);
			System.out.println("test --- 14");
			
			//Read and fill payment page
			paymentReadAndWrite 	payment							= 	new paymentReadAndWrite();
			hotel					hotel							= 	new hotel();
			hotel = payment.paymentReadWeb(driverReadAndWrite, ReportPrinter);
			System.out.println("test --- 15");
			System.out.println(hotel.getErroType());
			System.out.println(hotel.getError());
			
			if(hotel.getErroType().equals(""))
			{
				System.out.println("test --- 16");
				
				//hb verification room availability respons against web
				hbRmAvailability.availablityResAgainstWeb(hotel, ReportPrinter, resRoomAvailability, currency, defaultCurrency);
				System.out.println("test ---17");
				
				//reservation response
				resHotelAvailability 	resAvailability2 = new resHotelAvailability();
				System.out.println("test --- 18");
				
				// get hotelBeds payment response url
				String 					hbResUrl 						= 	xmlUrl.hotelBedsResUrl(tracer);
										resPageSource					=	xmlNav.navigater(hbResUrl, xmlHomeUrl, "hb", 0);
										resAvailability2 				= 	rr.readResponse(resPageSource);
				hotelBedsPaymentResponse	hbPaymentResponse			=  	new hotelBedsPaymentResponse();
				System.out.println("test --- 19");
				
				//hbPaymentResponse.verify(resAvailability2, hotel, defaultCurrency, currency, ReportPrinter);
				//read room availability res
				resHotelAvailability 	resRoomAvailability2 			= 	new resHotelAvailability();
				String					hbRoomAvailabilityRes2 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
				String					hbRoomAvailabilityResPageSource2 = 	xmlNav.navigater(hbRoomAvailabilityRes2, xmlHomeUrl, "hb", 0);			
										resRoomAvailability2 			= 	rr.readResponse(hbRoomAvailabilityResPageSource2);		
				System.out.println("test --- 20");
				
				//pre reservation request
				hotelAvailability 		preReqAvailability 				=	new hotelAvailability();
				String					preReqUrl						= 	xmlUrl.hbPreReq(tracer);
										resPageSource 					= 	xmlNav.navigater(preReqUrl, xmlHomeUrl, "hb", 0);
										preReqAvailability 				= 	rr.readPreRequest(resPageSource);
				System.out.println("test --- 21");						
				
				//pre reservation response
				resHotelAvailability 	preResAvailability 				= 	new resHotelAvailability();
				String					preResUrl						= 	xmlUrl.hbPreRes(tracer);
										resPageSource 					= 	xmlNav.navigater(preResUrl, xmlHomeUrl, "hb", 0);
										preResAvailability				=	rr.readResponse(resPageSource);
				System.out.println("test --- 22");						
				
				//payment page error verification
				ev.paymentPageError(ReportPrinter, preReqAvailability, preResAvailability, resAvailability2 ,resRoomAvailability2, resRoomAvailability,hotel.getAvailability(), hotel.getAvailabilityError(), hotel);
				System.out.println("test --- 23");						
				
				//pre reservation verification
				hbPreReservation 		hbPre 							= 	new hbPreReservation();
				hbPre.verifyPreReservation(preReqAvailability, preResAvailability, ReportPrinter);
				System.out.println("test --- 24");
				
				//read the bill
				confirmationRead 		conRead 						= 	new confirmationRead();
				conhotel				conHotel 						= 	new conhotel();
										conHotel						= 	conRead.confirmationReadWeb(driverReadAndWrite, resAvailability2, ReportPrinter);
				System.out.println("test --- 25");
				resHotelAvailability	resAvailabilityFinalRes 		= 	new resHotelAvailability();
				hotelAvailability 		reqAvailabilityFinalReq 		=	new hotelAvailability();
				hbFinalVerification 	finalVeri 						= 	new hbFinalVerification();
				
				//final reservation request
				String 					finalReqUrl 					= 	xmlUrl.hbFinalReq(tracer);
										resPageSource 					= 	xmlNav.navigater(finalReqUrl, xmlHomeUrl, "hb", 0);
										reqAvailabilityFinalReq 		= 	rr.readRequest(resPageSource);
				System.out.println("test --- 26");	
				
				//final reservation response
				String 					finalResUrl 					= 	xmlUrl.hbFinalRes(tracer);
										resPageSource 					= 	xmlNav.navigater(finalResUrl, xmlHomeUrl, "hb", 0);
										resAvailabilityFinalRes 		= 	rr.readFinalResponse(resPageSource);
				System.out.println("test --- 27");	
				
				//verify req against confirmation page
				finalVeri.reqAgainstCon(reqAvailabilityFinalReq, conHotel, ReportPrinter);
				System.out.println("test --- 28");	
				
				//verify res against confirmation page
				finalVeri.resAgainstCon(resAvailabilityFinalRes, conHotel, ReportPrinter, defaultCurrency, currency);
				conHotel												= conRead.confirmationReadWeb(driverReadAndWrite, resAvailability2, ReportPrinter);
				System.out.println("test --- 29");
				
				//reservation report
	 			reservationReport resReport = new reservationReport();	
				resReport = nav.reservationReport(txtProperty.get("reservationReport"), driverReadAndWrite, conHotel);
				System.out.println("test --- 30");
				
				//booking list report
				bookingListReport blr = new bookingListReport();
				blr = nav.bookingListReport(driverReadAndWrite, txtProperty.get("bookingListReport"), conHotel);
				System.out.println("test --- 31");
				
				//verify reservation report
				reservationReportVerify resReportVeri = new reservationReportVerify();
				resReportVeri.verify(resAvailabilityFinalRes, resReport, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, currency, finalResUrl, currency);
				System.out.println("test --- 32");
				
				
				canDeadRes 				pen 							= 	new canDeadRes();
				//verify booking list report
				bookingListVerification blv = new bookingListVerification();
				blv.verify(resAvailabilityFinalRes, blr, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, currency, defaultCurrency, pen, currency);
				System.out.println("test --- 33");
				
				//profit and lose report
				profitAndLostReport profitNloss = new profitAndLostReport();
				profitNloss = nav.profitAndLossReport(txtProperty.get("profitNlossReportUrl"), driverReadAndWrite, conHotel, searchDetails);
				System.out.println("test --- 34");
				
				//verify profit and loss report
				resReportVeri.profitAndLossVerify(resAvailabilityFinalRes, profitNloss, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, defaultCurrency, currency, profitMarkUp);
				System.out.println("test --- 35");
				
				//read 3rd party supplier payable report
				thirdPartySupplierReport tpsr = new thirdPartySupplierReport();
				tpsr = nav.read3rdPartySupplierPayableReport(driverReadAndWrite, txtProperty.get("thirdPartySupplierPayableUrl"), conHotel);
				System.out.println("test --- 36");
				
				//verify 3rd party supplier payable report
				thirdpartySupReportVerification tpsrv = new thirdpartySupReportVerification();
				tpsrv.verify(resAvailabilityFinalRes, tpsr, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, defaultCurrency, currency, profitMarkUp,  bookingFee);
				System.out.println("test --- 37");
				
				//read customer confirmation mail
				customerConfirmationEmail cusCon = new customerConfirmationEmail();
				cusCon	=	nav.customerConfirmationMail(driverReadAndWrite, txtProperty.get("confirmationreportUrl"), conHotel);
				System.out.println("test --- 38");
				
				//verifyn customer confirmation mail
				resReportVeri.hbCustomerConfirmationmail(cusCon, resAvailabilityFinalRes, conHotel, ReportPrinter, currency, defaultCurrency);
				System.out.println("test --- 39");
				
				//read customer voucher email
				voucherMail  voucher = new voucherMail();
				voucher = nav.voucherMail(driverReadAndWrite, conHotel);
				
				if(cusCon.getFlag().equals("null"))
				{
					
				}
				else
				{
					//verifyn customer confirmation mail
					resReportVeri.hbCustomerConfirmationmail(cusCon, resAvailabilityFinalRes, conHotel, ReportPrinter, currency, finalResUrl);
					
					resReportVeri.hbCustomerVoucher(voucher, resAvailabilityFinalRes, conHotel, ReportPrinter);
				}
				//cancellation process
				nav.cancellation(driverReadAndWrite, txtProperty.get("cancellationUrl"), conHotel.getReservationNumber(), cancellation);
			}
			else
			{
				ReportPrinter.append("<br>");
				ReportPrinter.append("<span>Payment gateway error</span>");
				
				ReportPrinter.append("<table border=1 style=width:800px>"
					+ "<tr><th>Error type</th>"
					+ "<th>Error</th></tr>");
				
				ReportPrinter.append	(	"<tr><td>"+ hotel.getErroType() +"</td>"
						+ 	"<td class='Failed'>"+ hotel.getError() +"</td></tr>");
			}
			
			
			
		}
		
	}
	
	public void inv13Web(xmlNavigator xmlNav, readRequest rr, Map<String, String> currency, String defaultCurrency, navigator nav, String searchUrlWeb, String hotel2, String sup, String[] searchDetails) throws FileNotFoundException, InterruptedException
	{
		// search and get search details
		System.out.println("test -- 1");
		String url;
		readSearchDetails rsd	 = 	nav.navigateToSearchWeb(driverReadAndWrite, searchUrlWeb, ReportPrinter, searchDetails);
				
		//Read results
		ArrayList<readHotelResults> hotelList	= new ArrayList<readHotelResults>();
									hotelList 	= nav.readResultsWeb(driverReadAndWrite, rsd.getRoomCount());
		System.out.println(hotelList.size());		
		//chose from results
		ArrayList<String> tracerNhotel = new ArrayList<String>();
		String 	hotelName 	= 	"";
		String	tracer		=	"";
		String	errorType	=	"";
		String	error		=	"";
		//try {
			tracerNhotel			=	nav.navigateToResultsWeb(driverReadAndWrite, ReportPrinter, hotel2, sup);
			hotelName				= 	tracerNhotel.get(0);
			tracer 					= 	tracerNhotel.get(1);
			System.out.println("Tracer id : " + tracer);
			errorType 				= 	tracerNhotel.get(2);
			error 					= 	tracerNhotel.get(3);
			System.out.println(tracer);		
		//} catch (Exception e) {
			// TODO: handle exception
		//}
			//get xml url
			xmlHome 				xmlUrl 							= new xmlHome();
			String 					xmlHomeUrl	 					= xmlUrl.generateUrl();
			System.out.println(xmlHomeUrl);
			
		if(error.equals("*Sorry! This product is no longer available. Please choose another."))
		{

			//read room availability res
			resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
			String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
			String					hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "inv13", 0);	
			try {
						
									resRoomAvailability 			= 	rr.readResponse(hbRoomAvailabilityResPageSource);
			} catch (Exception e) {
				// TODO: handle exception
				ReportPrinter.append("<br>"
						+ "<span>"+ error +"</span>"
						+ "<br>"
						+ "<span>Xml Error</span>"
						+ "<br>");
			}
			
			try {
				System.out.println(resRoomAvailability.getTotalItems());
				System.out.println(error);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				if(resRoomAvailability.getTotalItems().equals("0") && error.equals("*Sorry! This product is no longer available. Please choose another."))
				{
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>No results available</span>"
							+ "<br>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		
		}
		else
		{	
			String flag = "";
			System.out.println("test -- 2");
			// read search request xml
			hbPage1Req				page1Req= null;
			String 					inv13SearchUrl					= xmlUrl.inv13SearchReq(tracer);
			System.out.println(inv13SearchUrl);
			try {
				String 				inv13SearchPageSource			= xmlNav.navigater(inv13SearchUrl, xmlHomeUrl, "inv13", 0);
									page1Req						= rr.inv13AvailabilityReq(inv13SearchPageSource);
			} catch (Exception e) {
				ReportPrinter.append("<span>Search Verification</span>");
							
				ReportPrinter.append("<table border=1 style=width:800px>"
									+ "<tr><th>Error</th></tr>");
				ReportPrinter.append	(	"<tr><td>Availability request xml not found</td></tr>");
				flag = "no";
							
			}
				if(flag.equals("no"))
				{
					
				}
				else
				{
					//------verify------
					// verify search
					hbpage1Req				searchReq 						= new hbpage1Req();
					searchReq.verify(page1Req, rsd, ReportPrinter, "inv13");
							
					//Hotel count
					countHotels 			hotelCount						= new countHotels();
					hotelCount.hotelCount(hotelList);
							
					//read room availability req
					hbPage1Req  			roomAvailabilityReq 			=  new hbPage1Req();
					String 					inv13RoomAvailabilityReq		= xmlUrl.inv13RoomAvailabilityReq(tracer);
					System.out.println(inv13RoomAvailabilityReq);
					String					inv13RoomAvailabilityReqPageSource 	= "" ; 
					try {
						inv13RoomAvailabilityReqPageSource 						= xmlNav.navigater(inv13RoomAvailabilityReq, xmlHomeUrl, "inv13", 0);
						roomAvailabilityReq 									= rr.inv13RoomAvailabilityReq(inv13RoomAvailabilityReqPageSource);
					} catch (Exception e) {
						ReportPrinter.append("<span>Search Verification</span>");
								
						ReportPrinter.append("<table border=1 style=width:800px>"
								+ "<tr><th>Error</th></tr>");
						ReportPrinter.append	(	"<tr><td>Room Availability request xml not found</td></tr>");
					}
													
					//read room availability res
					resHotelAvailability 	resRoomAvailability 			= new resHotelAvailability();
					String					inv13RoomAvailabilityRes 		= xmlUrl.inv13RoomAvailabilityRes(tracer);
					System.out.println(tracer);
					String					inv13RoomAvailabilityResPageSource = 	xmlNav.navigater(inv13RoomAvailabilityRes, xmlHomeUrl, "inv13", 0);			
											resRoomAvailability 			= rr.inv13RoomAvailabilityRes(inv13RoomAvailabilityResPageSource);
					
					//------verify------
					//verify room availability req against web
					hbRoomAvailability 		hbRmAvailability 				= new hbRoomAvailability();
					hbRmAvailability.inv13AvailabilityReqAgainstWeb(ReportPrinter, roomAvailabilityReq, hotelList, hotelName, searchDetails);
						
					//************************************************************************************************************
					//------verify------
					//verify room availability req against res
					//hbRmAvailability.inv13AvailabilityReqAgainstRes(ReportPrinter, roomAvailabilityReq, resRoomAvailability, error, errorType, defaultCurrency, currency);
							
					//Read inv13 page1 xml response
					ArrayList<resHotelAvailability>	inv13resAvailability		=  	new ArrayList<resHotelAvailability>();
					String 					inv13Page1ResUrl 					= 	xmlUrl.inv13Page1Res(tracer);
					String 					inv13resPageSource					=	xmlNav.navigater(inv13Page1ResUrl, xmlHomeUrl, "inv13", 0);
											inv13resAvailability 				= 	rr.inv13AvailabilityResponse(inv13resPageSource);
													
					//Read hb page1 xml response
					resHotelAvailability	hbresAvailability					=  	new resHotelAvailability();
					String 					hbPage1ResUrl 						= 	xmlUrl.hbPage1Res(tracer);
					String 					hbresPageSource						= 	xmlNav.navigater(hbPage1ResUrl, xmlHomeUrl, "inv13", 0);
											hbresAvailability 					= 	rr.hbPage1Response(hbresPageSource);
					//************************************************************************************************************
					//------verify------
					//verify search req against res
					//searchReq.inv13SearchReqAgainstRes(ReportPrinter, page1Req, resAvailability);
							
					//check whether results available
					int 					inv13HotelCount 				= 	rr.inv13HotelCount(inv13resPageSource);
					
					//------verify------
					//verify page1 response
					hbPage1Res 				hbPage1verify 					= 	new hbPage1Res();
					hbPage1verify.inv13VerifyAvailabilityResponseWeb(hotelList, inv13resAvailability, ReportPrinter, defaultCurrency, currency, inv13HotelCount, hbresAvailability);
					        
					// get inv13 payment request url 
					String 					hbReqUrl 						= 	xmlUrl.inv13ReqUrl(tracer);
					System.out.println(hbReqUrl);
					System.out.println("test -- 3");	
					//Read and fill payment page
					paymentReadAndWrite 	payment							= 	new paymentReadAndWrite();
					hotel					hotel							= 	new hotel();
					hotel = payment.paymentReadWeb(driverReadAndWrite, ReportPrinter);
					System.out.println(hotel.getCurrencyCode());
					driverReadAndWrite.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);	
					//INV13 es
					//hbRmAvailability.inv13AvailablityResAgainstWeb(hotel, ReportPrinter, resRoomAvailability, currency, defaultCurrency);
					System.out.println("test -- 4");	
					//reservation response
					resHotelAvailability 	resAvailability2 				= 	new resHotelAvailability();
					// get INV13 payment response url
					String 					hbResUrl 						= 	xmlUrl.inv13ResUrl(tracer);
										inv13resPageSource					=	xmlNav.navigater(hbResUrl, xmlHomeUrl, "inv13", 0);
											resAvailability2 				= 	rr.inv13ReadPaymentResponse(inv13resPageSource);
					hotelBedsPaymentResponse	hbPaymentResponse			= 	new hotelBedsPaymentResponse();
					System.out.println("test -- 5");		
					//pre reservation request
					hbPage1Req 				preReqAvailability 				= 	new hbPage1Req();
					String					preReqUrl						= 	xmlUrl.inv13PreReq(tracer);
										inv13resPageSource 					= 	xmlNav.navigater(preReqUrl, xmlHomeUrl, "inv13", 0);
											preReqAvailability 				= 	rr.inv13ReadPreRequest(inv13resPageSource);
					System.out.println("test -- 6");		
					//pre reservation response
					resHotelAvailability 	preResAvailability 				= 	new resHotelAvailability();
					String					preResUrl						= 	xmlUrl.inv13PreRes(tracer);
										inv13resPageSource 					= 	xmlNav.navigater(preResUrl, xmlHomeUrl, "inv13", 0);
											preResAvailability				=	rr.inv13ReadPreResponse(inv13resPageSource);
					System.out.println("test -- 7");
					//------verify------
					//pre reservation verification
					hbPreReservation 		hbPre 							= 	new hbPreReservation();
					hbPre.inv13VerifyPreReservation(preReqAvailability, preResAvailability, ReportPrinter);
					System.out.println("test -- 8");	
					//read the bill
					confirmationRead 		conRead 						= 	new confirmationRead();
					conhotel				conHotel 						= 	new conhotel();
											conHotel						= 	conRead.confirmationReadWeb(driverReadAndWrite, resAvailability2, ReportPrinter);
					resHotelAvailability	resAvailabilityFinalRes 		= 	new resHotelAvailability();
					hbPage1Req 				reqAvailabilityFinalReq 		=	new hbPage1Req();
					hbFinalVerification 	finalVeri 						= 	new hbFinalVerification();
					System.out.println("test -- 9");
							
					//final reservation request
					String 					finalReqUrl 					= 	xmlUrl.inv13FinalReq(tracer);
										inv13resPageSource 					= 	xmlNav.navigater(finalReqUrl, xmlHomeUrl, "inv13", 0);
											reqAvailabilityFinalReq 		= 	rr.inv13FinalAvailability(inv13resPageSource);
					System.out.println("test -- 10");
					//final reservation response
					String 					finalResUrl 					= 	xmlUrl.inv13FinalRes(tracer);
										inv13resPageSource 					= 	xmlNav.navigater(finalResUrl, xmlHomeUrl, "inv13", 1);
											resAvailabilityFinalRes 		= 	rr.inv13ReadFinalRes(inv13resPageSource, ReportPrinter);
					System.out.println("test -- 11");
					//------verify------
					//verify req against confirmation page
					finalVeri.inv13ReqAgainstConWeb(reqAvailabilityFinalReq, conHotel, ReportPrinter);
					System.out.println("test -- 12");
					//------verify------
					//verify res against confirmation page
					finalVeri.inv13ResAgainstCon(resAvailabilityFinalRes, conHotel, ReportPrinter, defaultCurrency, currency);
					System.out.println("test -- 13");
					//reservation report
					reservationReport resReport = new reservationReport();	
					resReport = nav.reservationReport(txtProperty.get("reservationReport"), driverReadAndWrite, conHotel);
					System.out.println("test -- 14");
					//booking list report
					bookingListReport blr = new bookingListReport();
					blr = nav.bookingListReport(driverReadAndWrite, txtProperty.get("bookingListReport"), conHotel);
					System.out.println("test -- 15");
					//------verify------
					//verify reservation report
					reservationReportVerify resReportVeri = new reservationReportVerify();
					resReportVeri.inv13Verify(resAvailabilityFinalRes, resReport, conHotel, ReportPrinter, "Tourico", "web", searchDetails, defaultCurrency, currency);
					System.out.println("test -- 16");
					//------verify------
					//verify booking list report
					bookingListVerification blv = new bookingListVerification();
					blv.inv13Verify(resAvailabilityFinalRes, blr, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails);
					System.out.println("test -- 17");
					//profit and lose report
					profitAndLostReport profitNloss = new profitAndLostReport();
					profitNloss = nav.profitAndLossReport(txtProperty.get("profitNlossReportUrl"), driverReadAndWrite, conHotel, searchDetails);
					System.out.println("test -- 18");
					//------verify------
					//verify profit and loss report
					//resReportVeri.inv13profitAndLossVerify(resAvailabilityFinalRes, profitNloss, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, currency, defaultCurrency);
					System.out.println("test -- 19");
					//read 3rd party supplier payable report
					thirdPartySupplierReport tpsr = new thirdPartySupplierReport();
					tpsr = nav.read3rdPartySupplierPayableReport(driverReadAndWrite, txtProperty.get("thirdPartySupplierPayableUrl"), conHotel);
					System.out.println("test -- 20");
					//------verify------
					//verify 3rd party supplier payable report
					thirdpartySupReportVerification tpsrv = new thirdpartySupReportVerification();
					tpsrv.inv13Verify(resAvailabilityFinalRes, tpsr, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, defaultCurrency);
					System.out.println("test -- 21");
					//read customer confirmation mail
					customerConfirmationEmail cusCon = new customerConfirmationEmail();
					cusCon	=	nav.customerConfirmationMail(driverReadAndWrite, txtProperty.get("confirmationreportUrl"), conHotel);
					System.out.println("test -- 22");
					//read customer voucher email
					voucherMail  voucher = new voucherMail();
					nav.voucherMail(driverReadAndWrite, conHotel);
					System.out.println("test -- 23");
					//------verify------
					// verify customer confirmation email
					resReportVeri.inv13customerConfirmationmail(cusCon, resAvailabilityFinalRes, conHotel, ReportPrinter, currency, defaultCurrency, profitMarkUp);
					System.out.println("test -- 24");
					//------verify------
					//verify customer voucher email
					resReportVeri.inv13CustomerVoucher(voucher, resAvailabilityFinalRes, conHotel, ReportPrinter, defaultCurrency, currency);
					System.out.println("test -- 25");
					if(cusCon.getFlag().equals("null"))
					{
						
					}
					else
					{
						//verifyn customer confirmation mail
						
						
						/*//verifyn customer confirmation mail
						resReportVeri.customerConfirmationmail(cusCon, resAvailabilityFinalRes, conHotel, ReportPrinter);
						
						resReportVeri.hbCustomerVoucher(voucher, resAvailabilityFinalRes, conHotel, ReportPrinter);*/
					}
					//cancellation process
					nav.cancellation(driverReadAndWrite, txtProperty.get("cancellationUrl"), conHotel.getReservationNumber(), cancellation);
				}					
		}
	}
	
	public void inv13(xmlNavigator xmlNav, readRequest rr, Map<String, String> currency, String defaultCurrency, navigator nav, String searchUrl, String hotel2, String sup, String[] searchDetails) throws FileNotFoundException, InterruptedException
	{
		// search and get search details
		String url;
		readSearchDetails 		rsd	 							= 	nav.navigateToSearch(driverReadAndWrite, searchUrl, searchDetails);
		
		//Read results
		ArrayList<readHotelResults> hotelList					= new ArrayList<readHotelResults>();
									hotelList 					= nav.readResults(driverReadAndWrite, rsd.getRoomCount(), "inv13");
		
		//chose from results
		ArrayList<String> 		tracerAndHotel 					= nav.navigateToResults(driverReadAndWrite, hotel2, sup);
		String 					tracer 							= tracerAndHotel.get(0);
		String					hotelName 						= tracerAndHotel.get(1);
		String 					error 							= tracerAndHotel.get(2);
		String 					errorType 						= tracerAndHotel.get(3);
		System.out.println(error);
		
		//get xml url
		xmlHome 				xmlUrl 							= new xmlHome();
		String 					xmlHomeUrl	 					= xmlUrl.generateUrl();
		
		
		
		
		if(error.equals("*Sorry, rates got changed. please search again!"))
		{
			//read room availability res
			resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
			String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
			String					hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "inv13", 1);
			try {
				
				resRoomAvailability 			= 	rr.readResponse(hbRoomAvailabilityResPageSource);
				} catch (Exception e) {
					// TODO: handle exception
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>Xml not available</span>"
							+ "<br>");
				}
		}
		
		if(error.equals("*Sorry! This product is no longer available. Please choose another."))
		{
			//read room availability res
			resHotelAvailability 	resRoomAvailability 			= 	new resHotelAvailability();
			String					hbRoomAvailabilityRes 			= 	xmlUrl.hbRoomAvailabilityRes(tracer);
			String					hbRoomAvailabilityResPageSource = 	xmlNav.navigater(hbRoomAvailabilityRes, xmlHomeUrl, "inv13", 1);	
			try {
						
									resRoomAvailability 			= 	rr.readResponse(hbRoomAvailabilityResPageSource);
			} catch (Exception e) {
				// TODO: handle exception
				ReportPrinter.append("<br>"
						+ "<span>"+ error +"</span>"
						+ "<br>"
						+ "<span>Xml Error</span>"
						+ "<br>");
			}
			
			try {
				System.out.println(resRoomAvailability.getTotalItems());
				System.out.println(error);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			try {
				if(resRoomAvailability.getTotalItems().equals("0") && error.equals("*Sorry! This product is no longer available. Please choose another."))
				{
					ReportPrinter.append("<br>"
							+ "<span>"+ error +"</span>"
							+ "<br>"
							+ "<span>No results available</span>"
							+ "<br>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		else
		{
			// read search request xml
			hbPage1Req				page1Req= null;
			String 					inv13SearchUrl					= xmlUrl.inv13SearchReq(tracer);
			System.out.println(inv13SearchUrl);
			try {
				String 				inv13SearchPageSource			= xmlNav.navigater(inv13SearchUrl, xmlHomeUrl, "inv13", 1);
									page1Req						= rr.inv13AvailabilityReq(inv13SearchPageSource);
			} catch (Exception e) {
				ReportPrinter.append("<span>Search Verification</span>");
				
				ReportPrinter.append("<table border=1 style=width:800px>"
						+ "<tr><th>Error</th></tr>");
				ReportPrinter.append	(	"<tr><td>Availability request xml not found</td></tr>");
				
			}
			
			
			// verify search 1
			hbpage1Req				searchReq 						= new hbpage1Req();
			//temp
			//searchReq.verify(page1Req, rsd, ReportPrinter, "inv13");
			
			//Hotel count
			countHotels 			hotelCount						= new countHotels();
			hotelCount.hotelCount(hotelList);
			
			//read room availability req
			hbPage1Req  			roomAvailabilityReq 			=  new hbPage1Req();
			String 					inv13RoomAvailabilityReq		= xmlUrl.inv13RoomAvailabilityReq(tracer);
			System.out.println(inv13RoomAvailabilityReq);
			String					inv13RoomAvailabilityReqPageSource 	= "" ; 
			try {
				inv13RoomAvailabilityReqPageSource 						= xmlNav.navigater(inv13RoomAvailabilityReq, xmlHomeUrl, "inv13", 1);
				roomAvailabilityReq 									= rr.inv13RoomAvailabilityReq(inv13RoomAvailabilityReqPageSource);
			} catch (Exception e) {
				ReportPrinter.append("<span>Search Verification</span>");
				
				ReportPrinter.append("<table border=1 style=width:800px>"
						+ "<tr><th>Error</th></tr>");
				ReportPrinter.append	(	"<tr><td>Room Availability request xml not found</td></tr>");
			}
				
			//Read page1 xml response
			ArrayList<resHotelAvailability>	resAvailability			=  	new ArrayList<resHotelAvailability>();
			String 					hbPage1ResUrl 					= 	xmlUrl.inv13Page1Res(tracer);
			String 					resPageSource					=	xmlNav.navigater(hbPage1ResUrl, xmlHomeUrl, "inv13", 1);
									resAvailability 				= 	rr.inv13AvailabilityResponse(resPageSource);
									
			//verify number of hotels 1.1
			verifyNumberOfHotels numHotels = new verifyNumberOfHotels();
			numHotels.verify(hotelList.size(), resAvailability.size(), ReportPrinter);
			
			//read room availability res
			resHotelAvailability 	resRoomAvailability 			= new resHotelAvailability();
			String					inv13RoomAvailabilityRes 		= xmlUrl.inv13RoomAvailabilityRes(tracer);
			System.out.println(tracer);
			String					inv13RoomAvailabilityResPageSource = 	xmlNav.navigater(inv13RoomAvailabilityRes, xmlHomeUrl, "inv13", 1);			
									resRoomAvailability 			= rr.inv13RoomAvailabilityRes(inv13RoomAvailabilityResPageSource);
			
			//verify room availability req against web 2
			hbRoomAvailability 		hbRmAvailability 				= new hbRoomAvailability();
			hbRmAvailability.inv13AvailabilityReqAgainstWeb(ReportPrinter, roomAvailabilityReq, hotelList, hotelName, searchDetails);
			
			//verify room availability req against res 2.1
			//temp
			//hbRmAvailability.inv13AvailabilityReqAgainstRes(ReportPrinter, roomAvailabilityReq, resRoomAvailability, error, errorType, defaultCurrency, currency);
			
			
			
			//verify search req against res 3
			//temp
			//searchReq.inv13SearchReqAgainstRes(ReportPrinter, page1Req, resAvailability);
			
			//check whether results available
			int 					inv13HotelCount 				= rr.inv13HotelCount(resPageSource);
			
			//verify page1 response 4
			hbPage1Res 				hbPage1verify 					= new hbPage1Res();
			hbPage1verify.inv13VerifyAvailabilityResponse(hotelList, resAvailability, ReportPrinter, defaultCurrency, currency, inv13HotelCount, resRoomAvailability);
	        
			// get inv13 payment request url 
			String 					hbReqUrl 						= xmlUrl.inv13ReqUrl(tracer);
			System.out.println(hbReqUrl);
			
			//Read and fill payment page
			paymentReadAndWrite 	payment							= 	new paymentReadAndWrite();
			hotel					hotel							= 	new hotel();
									hotel 							= 	payment.paymentRead(driverReadAndWrite);
			System.out.println(hotel.getCurrencyCode());
			
			//INV13 verification 5
			hbRmAvailability.inv13AvailablityResAgainstWeb(hotel, ReportPrinter, resRoomAvailability, currency, defaultCurrency);
			
			//reservation response
			resHotelAvailability 	resAvailability2 				= new resHotelAvailability();
			// get INV13 payment response url
			String 					hbResUrl 						= 	xmlUrl.inv13ResUrl(tracer);
									resPageSource					=	xmlNav.navigater(hbResUrl, xmlHomeUrl, "inv13", 1);
									resAvailability2 				= 	rr.inv13ReadPaymentResponse(resPageSource);
			hotelBedsPaymentResponse	hbPaymentResponse			=  new hotelBedsPaymentResponse();
			
			//pre reservation request
			hbPage1Req 				preReqAvailability 				= new hbPage1Req();
			String					preReqUrl						= xmlUrl.inv13PreReq(tracer);
									resPageSource 					= xmlNav.navigater(preReqUrl, xmlHomeUrl, "inv13", 1);
									preReqAvailability 				= rr.inv13ReadPreRequest(resPageSource);
			
			//pre reservation response
			resHotelAvailability 	preResAvailability 				= 	new resHotelAvailability();
			String					preResUrl						= 	xmlUrl.inv13PreRes(tracer);
									resPageSource 					= 	xmlNav.navigater(preResUrl, xmlHomeUrl, "inv13", 1);
									preResAvailability				=	rr.inv13ReadPreResponse(resPageSource);
			
			//pre reservation verification 6
			hbPreReservation 		hbPre 							= 	new hbPreReservation();
			hbPre.inv13VerifyPreReservation(preReqAvailability, preResAvailability, ReportPrinter);
			
			//read the bill
			confirmationRead 		conRead 						= 	new confirmationRead();
			conhotel				conHotel 						= 	new conhotel();
			conHotel												= 	conRead.comfirmationRead(driverReadAndWrite, resAvailability2, ReportPrinter, searchDetails);
			resHotelAvailability	resAvailabilityFinalRes 		= 	new resHotelAvailability();
			hbPage1Req 				reqAvailabilityFinalReq 		=	new hbPage1Req();
			hbFinalVerification 	finalVeri 						= 	new hbFinalVerification();
			
			//final reservation request
			String 					finalReqUrl 					=	xmlUrl.inv13FinalReq(tracer);
									resPageSource 					= 	xmlNav.navigater(finalReqUrl, xmlHomeUrl, "inv13", 1);
									reqAvailabilityFinalReq 		= 	rr.inv13FinalAvailability(resPageSource);
			
			//final reservation response
			String 					finalResUrl 					= 	xmlUrl.inv13FinalRes(tracer);
									resPageSource 					= 	xmlNav.navigater(finalResUrl, xmlHomeUrl, "inv13", 1);
									resAvailabilityFinalRes 		= 	rr.inv13ReadFinalRes(resPageSource, ReportPrinter);
									
			//cancellation deadline request
			canDeadRes 				pen 							= 	new canDeadRes();
			String					canDeadReqUrl					=	xmlUrl.canDeadReq(tracer);
									resPageSource 					= 	xmlNav.navigater(canDeadReqUrl, xmlHomeUrl, "inv13", 1);
									pen 							= 	rr.inv13canDeadRes(resPageSource, ReportPrinter);
			
			try {
				//verify req against confirmation page 7
				finalVeri.inv13ReqAgainstCon(reqAvailabilityFinalReq, conHotel, ReportPrinter);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//try {
				//verify res against confirmation page 8
				finalVeri.inv13ResAgainstCon(resAvailabilityFinalRes, conHotel, ReportPrinter, defaultCurrency, currency);
			//} catch (Exception e) {
				// TODO: handle exception
			//}
			
			//reservation report
			reservationReport resReport = new reservationReport();	
			try {
				resReport = nav.reservationReport(txtProperty.get("reservationReport"), driverReadAndWrite, conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//booking list report
			bookingListReport blr = new bookingListReport();
			try {
				blr = nav.bookingListReport(driverReadAndWrite, txtProperty.get("bookingListReport"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//verify reservation report 9
			reservationReportVerify resReportVeri = new reservationReportVerify();
			try {
				resReportVeri.verify(resAvailabilityFinalRes, resReport, conHotel, ReportPrinter, "Tourico", "Call Center", searchDetails, currency, defaultCurrency, currency);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//verify booking list report 10
			bookingListVerification blv = new bookingListVerification();
			try {
				blv.verify(resAvailabilityFinalRes, blr, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, currency, defaultCurrency, pen, profitMarkUp);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//profit and lose report
			profitAndLostReport profitNloss = new profitAndLostReport();
			try {
				profitNloss = nav.profitAndLossReport(txtProperty.get("profitNlossReportUrl"), driverReadAndWrite, conHotel, searchDetails);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//verify profit and loss report 11
			try {
				resReportVeri.profitAndLossVerify(resAvailabilityFinalRes, profitNloss, conHotel, ReportPrinter, "hotelbeds", "web", searchDetails, finalResUrl, currency, profitMarkUp);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			//read 3rd party supplier payable report
			thirdPartySupplierReport tpsr = new thirdPartySupplierReport();
			try {
				tpsr = nav.read3rdPartySupplierPayableReport(driverReadAndWrite, txtProperty.get("thirdPartySupplierPayableUrl"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			//verify 3rd party supplier payable report 12
			thirdpartySupReportVerification tpsrv = new thirdpartySupReportVerification();
			try {
				tpsrv.verify(resAvailabilityFinalRes, tpsr, conHotel, ReportPrinter, "Tourico", "web", searchDetails, defaultCurrency,currency, profitMarkUp, bookingFee);	
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//read customer confirmation mail
			customerConfirmationEmail cusCon = new customerConfirmationEmail();
			try {
				cusCon	=	nav.customerConfirmationMail(driverReadAndWrite, txtProperty.get("confirmationreportUrl"), conHotel);	
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			//verifyn customer confirmation mail 13
			try {
				resReportVeri.inv13customerConfirmationmail(cusCon, resAvailabilityFinalRes, conHotel, ReportPrinter, currency, finalResUrl, profitMarkUp);	
			} catch (Exception e) {
				// TODO: handle exception
			}
		
			cancellationDetails cd = new cancellationDetails();
			//cancellation process
			cd = nav.cancellation(driverReadAndWrite, txtProperty.get("cancellationUrl"), conHotel.getReservationNumber(), cancellation);
			
			//read cancellation request
			cancellationReq 				canReq 								= 	new cancellationReq();
			String							canReqUrl							= 	xmlUrl.inv13cancellationRequest(tracer);
			String							canReqPageSource					= 	xmlNav.navigater(canReqUrl, xmlHomeUrl, "white", 0);
											canReq								= 	rr.cancelReq(canReqPageSource);
											
			//read cancellation response
			cancellationRes 				canRes 								= 	new cancellationRes();
			String							canResUrl							= 	xmlUrl.cancellationResponse(tracer);
			String							canResPageSource					= 	xmlNav.navigater(canResUrl, xmlHomeUrl, "white", 0);
											canRes								= 	rr.cancelres(canResPageSource);		
		
			//reservation response after issue is been fixed
			resHotelAvailability rha = new resHotelAvailability();
			//read Cancellation report
											cancellationReport cr = new cancellationReport();	
			try {
									
				cr	=	nav.cancellationreport(driverReadAndWrite, conHotel.getReservationNumber(), txtProperty.get("cancellationReportUrl"));
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("cancellation report error");
			}
			
			//verify cancellation report 14
			cancellationVerify cv = new cancellationVerify();
			cv.hb(ReportPrinter, cd, rha, conHotel, "Call Center", defaultCurrency, currency, cr);
			
			//read 3rd party supplier payable report
			thirdPartySupplierReport tpsr2 = new thirdPartySupplierReport();
			try {
				tpsr2 = nav.read3rdPartySupplierPayableReport(driverReadAndWrite, txtProperty.get("thirdPartySupplierPayableUrl"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//reservation report
			reservationReport resReport2 = new reservationReport();	
			String resReportAvailability = "available";
			try {
				resReport2 = nav.reservationReport(txtProperty.get("reservationReport"), driverReadAndWrite, conHotel);
			} catch (Exception e) {
				// TODO: handle exception
				resReportAvailability = "not available";
			}
			
			System.out.println(resReportAvailability);
			//booking list report
			bookingListReport blr2 = new bookingListReport();
			try {
				blr2 = nav.bookingListReport(driverReadAndWrite, txtProperty.get("bookingListReport"), conHotel);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
}

