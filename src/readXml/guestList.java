package readXml;

public class guestList {

	String customerType ="";
	String customerAge =""; 
	String name ="";
	String lastName =""; 
	String custoemrId ="";

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	public String getCustoemrId() {
		return custoemrId;
	}

	public void setCustoemrId(String custoemrId) {
		this.custoemrId = custoemrId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the customerAge
	 */
	public String getCustomerAge() {
		return customerAge;
	}

	/**
	 * @param customerAge the customerAge to set
	 */
	public void setCustomerAge(String customerAge) {
		this.customerAge = customerAge;
	}
	
}
