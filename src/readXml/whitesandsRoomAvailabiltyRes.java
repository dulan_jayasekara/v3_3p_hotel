package readXml;

import java.util.ArrayList;

public class whitesandsRoomAvailabiltyRes {

	String units				= ""; 
	String nyts					= ""; 
	String fromDate				= ""; 
	String toDate				= ""; 
	String timeLimit			= ""; 
	String maxAdult				= ""; 
	String freeNyts				= ""; 
	String availability			= ""; 
	String rateType				= ""; 
	String creditLimit			= ""; 
	String minimumNyts			= ""; 
	String childAgeStart		= ""; 
	String adultAgeStart		= ""; 
	String childPolicy			= ""; 
	String cancellationPolicy	= ""; 
	String fromDate2			= ""; 
	String toDate2				= ""; 
	String price2				= ""; 
	String freenyts2			= ""; 
	String nyts2				= ""; 
	String plistcode			= ""; 
	String plistlineno			= "";
	String maxChildren			= "";
	
	ArrayList<whitesandsRoomOccupancy> room  = new ArrayList<whitesandsRoomOccupancy>();
	
	
	
	
	public String getMaxChildren() {
		return maxChildren;
	}
	public void setMaxChildren(String maxChildren) {
		this.maxChildren = maxChildren;
	}
	public ArrayList<whitesandsRoomOccupancy> getRoom() {
		return room;
	}
	public void setRoom(ArrayList<whitesandsRoomOccupancy> room) {
		this.room = room;
	}
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	public String getNyts() {
		return nyts;
	}
	public void setNyts(String nyts) {
		this.nyts = nyts;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getTimeLimit() {
		return timeLimit;
	}
	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}
	public String getMaxAdult() {
		return maxAdult;
	}
	public void setMaxAdult(String maxAdult) {
		this.maxAdult = maxAdult;
	}
	public String getFreeNyts() {
		return freeNyts;
	}
	public void setFreeNyts(String freeNyts) {
		this.freeNyts = freeNyts;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}
	public String getMinimumNyts() {
		return minimumNyts;
	}
	public void setMinimumNyts(String minimumNyts) {
		this.minimumNyts = minimumNyts;
	}
	public String getChildAgeStart() {
		return childAgeStart;
	}
	public void setChildAgeStart(String childAgeStart) {
		this.childAgeStart = childAgeStart;
	}
	public String getAdultAgeStart() {
		return adultAgeStart;
	}
	public void setAdultAgeStart(String adultAgeStart) {
		this.adultAgeStart = adultAgeStart;
	}
	public String getChildPolicy() {
		return childPolicy;
	}
	public void setChildPolicy(String childPolicy) {
		this.childPolicy = childPolicy;
	}
	public String getCancellationPolicy() {
		return cancellationPolicy;
	}
	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}
	public String getFromDate2() {
		return fromDate2;
	}
	public void setFromDate2(String fromDate2) {
		this.fromDate2 = fromDate2;
	}
	public String getToDate2() {
		return toDate2;
	}
	public void setToDate2(String toDate2) {
		this.toDate2 = toDate2;
	}
	public String getPrice2() {
		return price2;
	}
	public void setPrice2(String price2) {
		this.price2 = price2;
	}
	public String getFreenyts2() {
		return freenyts2;
	}
	public void setFreenyts2(String freenyts2) {
		this.freenyts2 = freenyts2;
	}
	public String getNyts2() {
		return nyts2;
	}
	public void setNyts2(String nyts2) {
		this.nyts2 = nyts2;
	}
	public String getPlistcode() {
		return plistcode;
	}
	public void setPlistcode(String plistcode) {
		this.plistcode = plistcode;
	}
	public String getPlistlineno() {
		return plistlineno;
	}
	public void setPlistlineno(String plistlineno) {
		this.plistlineno = plistlineno;
	}
	
	
	
}
