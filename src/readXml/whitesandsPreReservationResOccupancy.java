package readXml;

public class whitesandsPreReservationResOccupancy {

	String fromdate = ""; 
	String toDate = ""; 
	String roomCode = ""; 
	String roomname = ""; 
	String CalcByPax = ""; 
	String CalcByUnits = ""; 
	String autoconfirm = "";
	String price  = "";
	
	
	
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getFromdate() {
		return fromdate;
	}
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getRoomCode() {
		return roomCode;
	}
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}
	public String getRoomname() {
		return roomname;
	}
	public void setRoomname(String roomname) {
		this.roomname = roomname;
	}
	public String getCalcByPax() {
		return CalcByPax;
	}
	public void setCalcByPax(String calcByPax) {
		CalcByPax = calcByPax;
	}
	public String getCalcByUnits() {
		return CalcByUnits;
	}
	public void setCalcByUnits(String calcByUnits) {
		CalcByUnits = calcByUnits;
	}
	public String getAutoconfirm() {
		return autoconfirm;
	}
	public void setAutoconfirm(String autoconfirm) {
		this.autoconfirm = autoconfirm;
	}
	
	
	
}
