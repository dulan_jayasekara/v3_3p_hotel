package readXml;

import java.util.ArrayList;

public class whitesandsReservationReq {

	String reqId = ""; 
	String partyCode = ""; 
	String roomCode = ""; 
	String mealCode = ""; 
	String numOfRooms = ""; 
	String numOfAdults = ""; 
	String numOfChilds = ""; 
	String checkin = ""; 
	String checkout = ""; 
	String nyts = ""; 
	String reqDate = ""; 
	String availability = ""; 
	String timeLimit = ""; 
	String minNyts = ""; 
	String reqId2 = ""; 
	String lineNum2 = ""; 
	String roomCode2 = ""; 
	String units2 = ""; 
	String lineValue2 = ""; 
	String plistcode2 = ""; 
	String plistlineno2 = ""; 
	String ratetype2 = ""; 
	String pkgnights2 = ""; 
	String freenights2 = ""; 
	String agentsellprice2 = ""; 
	
	ArrayList<String> title 		= 	new ArrayList<String>();
	ArrayList<String> lastName 		= 	new ArrayList<String>();
	ArrayList<String> firstname 	= 	new ArrayList<String>();
	ArrayList<String> age		 	= 	new ArrayList<String>();
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getPartyCode() {
		return partyCode;
	}
	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}
	public String getRoomCode() {
		return roomCode;
	}
	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}
	public String getMealCode() {
		return mealCode;
	}
	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}
	public String getNumOfRooms() {
		return numOfRooms;
	}
	public void setNumOfRooms(String numOfRooms) {
		this.numOfRooms = numOfRooms;
	}
	public String getNumOfAdults() {
		return numOfAdults;
	}
	public void setNumOfAdults(String numOfAdults) {
		this.numOfAdults = numOfAdults;
	}
	public String getNumOfChilds() {
		return numOfChilds;
	}
	public void setNumOfChilds(String numOfChilds) {
		this.numOfChilds = numOfChilds;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getNyts() {
		return nyts;
	}
	public void setNyts(String nyts) {
		this.nyts = nyts;
	}
	public String getReqDate() {
		return reqDate;
	}
	public void setReqDate(String reqDate) {
		this.reqDate = reqDate;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getTimeLimit() {
		return timeLimit;
	}
	public void setTimeLimit(String timeLimit) {
		this.timeLimit = timeLimit;
	}
	public String getMinNyts() {
		return minNyts;
	}
	public void setMinNyts(String minNyts) {
		this.minNyts = minNyts;
	}
	public String getReqId2() {
		return reqId2;
	}
	public void setReqId2(String reqId2) {
		this.reqId2 = reqId2;
	}
	public String getLineNum2() {
		return lineNum2;
	}
	public void setLineNum2(String lineNum2) {
		this.lineNum2 = lineNum2;
	}
	public String getRoomCode2() {
		return roomCode2;
	}
	public void setRoomCode2(String roomCode2) {
		this.roomCode2 = roomCode2;
	}
	public String getUnits2() {
		return units2;
	}
	public void setUnits2(String units2) {
		this.units2 = units2;
	}
	public String getLineValue2() {
		return lineValue2;
	}
	public void setLineValue2(String lineValue2) {
		this.lineValue2 = lineValue2;
	}
	public String getPlistcode2() {
		return plistcode2;
	}
	public void setPlistcode2(String plistcode2) {
		this.plistcode2 = plistcode2;
	}
	public String getPlistlineno2() {
		return plistlineno2;
	}
	public void setPlistlineno2(String plistlineno2) {
		this.plistlineno2 = plistlineno2;
	}
	public String getRatetype2() {
		return ratetype2;
	}
	public void setRatetype2(String ratetype2) {
		this.ratetype2 = ratetype2;
	}
	public String getPkgnights2() {
		return pkgnights2;
	}
	public void setPkgnights2(String pkgnights2) {
		this.pkgnights2 = pkgnights2;
	}
	public String getFreenights2() {
		return freenights2;
	}
	public void setFreenights2(String freenights2) {
		this.freenights2 = freenights2;
	}
	public String getAgentsellprice2() {
		return agentsellprice2;
	}
	public void setAgentsellprice2(String agentsellprice2) {
		this.agentsellprice2 = agentsellprice2;
	}
	public ArrayList<String> getTitle() {
		return title;
	}
	public void setTitle(ArrayList<String> title) {
		this.title = title;
	}
	public ArrayList<String> getLastName() {
		return lastName;
	}
	public void setLastName(ArrayList<String> lastName) {
		this.lastName = lastName;
	}
	public ArrayList<String> getFirstname() {
		return firstname;
	}
	public void setFirstname(ArrayList<String> firstname) {
		this.firstname = firstname;
	}
	public ArrayList<String> getAge() {
		return age;
	}
	public void setAge(ArrayList<String> age) {
		this.age = age;
	}
	
	
}
