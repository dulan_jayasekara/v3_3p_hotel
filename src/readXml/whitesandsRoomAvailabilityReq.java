package readXml;

public class whitesandsRoomAvailabilityReq {

	String agentCode		= ""; 
	String xmlId			= ""; 
	String cartid			= ""; 
	String partyCode		= ""; 
	String roomTypeCode		= ""; 
	String roomCatCode		= ""; 
	String mealCode			= ""; 
	String rateType			= ""; 
	String flag				= "";
	
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getXmlId() {
		return xmlId;
	}
	public void setXmlId(String xmlId) {
		this.xmlId = xmlId;
	}
	public String getCartid() {
		return cartid;
	}
	public void setCartid(String cartid) {
		this.cartid = cartid;
	}
	public String getPartyCode() {
		return partyCode;
	}
	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}
	public String getRoomTypeCode() {
		return roomTypeCode;
	}
	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}
	public String getRoomCatCode() {
		return roomCatCode;
	}
	public void setRoomCatCode(String roomCatCode) {
		this.roomCatCode = roomCatCode;
	}
	public String getMealCode() {
		return mealCode;
	}
	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	
}
