package readXml;

import java.util.ArrayList;

import readConfirmation.customerDetails;

public class hbPage1Req {

	String user =""; 
	String password =""; 
	String checkin =""; 
	String checkout =""; 
	String destination =""; 
	String roomCount =""; 
	String adultCount =""; 
	String childCount =""; 
	String cityName =""; 
	String hotelId ="";
	String email ="";
	String roomCode ="";
	String numberOfCots ="";
	String duration = ""; 
	String hotelName = "";
	String bookingreference = "";
	String bookingDepartureDate = "";
	String itemType = "";
	String itemReference = "";
	String itemCity = "";
	String itemCode = "";
	
	
	ArrayList<inv27rooms> room = new ArrayList<inv27rooms>();
	ArrayList<resGuestList> guest	=  new ArrayList<resGuestList>();
	ArrayList<customerDetails> cus = new ArrayList<customerDetails>();
	/**
	 * @return the user
	 */
	
	public String getUser() {
		return user;
	}
	
	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemReference() {
		return itemReference;
	}

	public void setItemReference(String itemReference) {
		this.itemReference = itemReference;
	}

	public String getItemCity() {
		return itemCity;
	}

	public void setItemCity(String itemCity) {
		this.itemCity = itemCity;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBookingreference() {
		return bookingreference;
	}

	public void setBookingreference(String bookingreference) {
		this.bookingreference = bookingreference;
	}

	public String getBookingDepartureDate() {
		return bookingDepartureDate;
	}

	public void setBookingDepartureDate(String bookingDepartureDate) {
		this.bookingDepartureDate = bookingDepartureDate;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public ArrayList<inv27rooms> getRoom() {
		return room;
	}

	public void setRoom(ArrayList<inv27rooms> room) {
		this.room = room;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public String getNumberOfCots() {
		return numberOfCots;
	}

	public void setNumberOfCots(String numberOfCots) {
		this.numberOfCots = numberOfCots;
	}

	/**
	 * @return the hotelId
	 */
	public String getHotelId() {
		return hotelId;
	}

	/**
	 * @param hotelId the hotelId to set
	 */
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the cus
	 */
	public ArrayList<customerDetails> getCus() {
		return cus;
	}

	/**
	 * @param cus the cus to set
	 */
	public void setCus(ArrayList<customerDetails> cus) {
		this.cus = cus;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the checkin
	 */
	public String getCheckin() {
		return checkin;
	}
	/**
	 * @param checkin the checkin to set
	 */
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	/**
	 * @return the checkout
	 */
	public String getCheckout() {
		return checkout;
	}
	/**
	 * @param checkout the checkout to set
	 */
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	/**
	 * @return the roomCount
	 */
	public String getRoomCount() {
		return roomCount;
	}
	/**
	 * @param roomCount the roomCount to set
	 */
	public void setRoomCount(String roomCount) {
		this.roomCount = roomCount;
	}
	/**
	 * @return the adultCount
	 */
	public String getAdultCount() {
		return adultCount;
	}
	/**
	 * @param adultCount the adultCount to set
	 */
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}
	/**
	 * @return the childCount
	 */
	public String getChildCount() {
		return childCount;
	}
	/**
	 * @param childCount the childCount to set
	 */
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}
	/**
	 * @return the guest
	 */
	public ArrayList<resGuestList> getGuest() {
		return guest;
	}
	/**
	 * @param guest the guest to set
	 */
	public void setGuest(ArrayList<resGuestList> guest) {
		this.guest = guest;
	}
	
	
}
