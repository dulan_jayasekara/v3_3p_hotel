package readXml;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import readConfirmation.customerDetails;
import readConfirmation.roomOccupancy;

import com.sun.jna.platform.win32.WinDef.HDC;

public class readRequest {

	
	
	public hbPage1Req inv13ReadPreRequest(String pageSource)
	{

		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		hbPage1Req				hb				= new hbPage1Req();
		hb.setCheckout							(doc.getElementsByTag("dtCheckOut").first().text());
		hb.setCheckin							(doc.getElementsByTag("dtCheckIn").first().text());
		
		Iterator<Element> 		guestElements		= doc.getElementsByTag("RoomInfo").iterator();
		ArrayList<resGuestList> 	guest			=  new ArrayList<resGuestList>();
		while(guestElements.hasNext())
		{
			Element	currentElement							= guestElements.next();
			resGuestList 	gst	=  new resGuestList();
			gst.setAdultCount					(currentElement.getElementsByTag("AdultsNum").first().text());
			gst.setChildCount					(currentElement.getElementsByTag("ChildNum").first().text());
			Iterator<Element> 			customerElements	= currentElement.getElementsByTag("ChildAge").iterator();
			ArrayList<customer>    		customer = new ArrayList<customer>();
			while(customerElements.hasNext())
			{
				Element	customerElement							= customerElements.next();
				customer			cus = new  customer();
				cus.setAge						(customerElement.getElementsByTag("ChildAge").first().text());
				customer.add(cus);
			}
			gst.setCus(customer);
			guest.add(gst);
		}
		hb.setGuest(guest);
		
		return hb;
	
	}
	
	public hbPage1Req inv13FinalAvailability(String pageSource)
	{

		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		hbPage1Req				hb				= new hbPage1Req();
		hb.setCheckout							(doc.getElementsByTag("m:CheckOut").first().text());
		hb.setCheckin							(doc.getElementsByTag("m:CheckIn").first().text());
		
		Iterator<Element> 		guestElements		= doc.getElementsByTag("m:RoomReserveInfo").iterator();
		ArrayList<resGuestList> 	guest			=  new ArrayList<resGuestList>();
		while(guestElements.hasNext())
		{
			Element	currentElement							= guestElements.next();
			resGuestList 	gst	=  new resGuestList();
			gst.setName							(currentElement.getElementsByTag("m:firstName").first().text());
			gst.setLastName						(currentElement.getElementsByTag("m:lastName").first().text());
			gst.setBedType						(currentElement.getElementsByTag("m:bedType").first().text());
			gst.setAdultCount					(currentElement.getElementsByTag("m:AdultsNum").first().text());
			gst.setChildCount					(currentElement.getElementsByTag("m:ChildNum").first().text());
			Iterator<Element> 			customerElements	= currentElement.getElementsByTag("m:ChildAge").iterator();
			ArrayList<customer>    		customer = new ArrayList<customer>();
			while(customerElements.hasNext())
			{
				Element	customerElement							= customerElements.next();
				customer			cus = new  customer();
				cus.setAge						(customerElement.getElementsByTag("m:ChildAge").first().text());
				customer.add(cus);
			}
			gst.setCus(customer);
			guest.add(gst);
		}
		hb.setGuest(guest);
		
		return hb;
	
	}
	
	public hbPage1Req inv27PreReq(String pageSource)
	{
		hbPage1Req 				hotel 			= new hbPage1Req();
		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		
		hotel.setUser			(doc.getElementsByTag("RequestorID").first().attr("Client"));
		hotel.setEmail			(doc.getElementsByTag("RequestorID").first().attr("EMailAddress"));
		hotel.setHotelName		(doc.getElementsByTag("ItemName").first().text());
		hotel.setHotelId		(doc.getElementsByTag("ItemCode").first().text());
		hotel.setCheckin		(doc.getElementsByTag("CheckInDate").first().text());
		hotel.setDuration		(doc.getElementsByTag("Duration").first().text());
		
		Iterator<Element> roomIterator = doc.getElementsByTag("Room").iterator();
		ArrayList<inv27rooms> roomList = new ArrayList<inv27rooms>();
		while(roomIterator.hasNext())
		{
			Element currentElement = roomIterator.next();
			inv27rooms room = new inv27rooms();
			room.setRoomCode		(currentElement.getElementsByTag("Room").first().attr("Code"));
			room.setRoomId			(currentElement.getElementsByTag("Room").first().attr("id"));
			room.setNumOfRooms		(currentElement.getElementsByTag("Room").first().attr("NumberOfRooms"));
			room.setNumOfCots		(currentElement.getElementsByTag("Room").first().attr("NumberOfCots"));
			
			roomList.add(room);
		}
		hotel.setRoom(roomList);
		
		
		return hotel;
	}
	
	public hotelAvailability readPreRequest(String pageSource)
	{

		Document doc  				= Jsoup.parse(pageSource, "UTF-8");
		hotelAvailability	hotel	= new hotelAvailability(); 
		Element availability		= doc.getElementsByTag("ServiceAddRQ").first();
		
		
		hotel.setUser				(availability.getElementsByTag("User").first().text());
		hotel.setPassword			(availability.getElementsByTag("Password").first().text());
		hotel.setContractname		(availability.getElementsByTag("Name").first().text());
		hotel.setIncomingOffice		(availability.getElementsByTag("IncomingOffice").first().attr("code"));
		hotel.setDateFromDate		(availability.getElementsByTag("DateFrom").first().attr("date"));
		hotel.setDateTodate			(availability.getElementsByTag("DateTo").first().attr("date"));
		hotel.setHotelType			(availability.getElementsByTag("HotelInfo").first().attr("xsi:type"));
		hotel.setHotelCode			(availability.getElementsByTag("Code").first().text());
		hotel.setDestination		(availability.getElementsByTag("Destination ").first().attr("code"));
		//System.out.println(hotel.getDestination());
		hotel.setDestinationType	(availability.getElementsByTag("Destination ").first().attr("type"));
		
		ArrayList<availableRoom>  	roomAvailability = new ArrayList<availableRoom>();
		Iterator<Element> roomIterator		= doc.getElementsByTag("AvailableRoom").iterator();
		while(roomIterator.hasNext())
		{
			Element	room					= roomIterator.next();
			availableRoom			aRoom	= new availableRoom();
			
			aRoom.setRoomCount			(room.getElementsByTag("RoomCount").first().text());
			aRoom.setAdultCount			(room.getElementsByTag("AdultCount").first().text());
			aRoom.setChildCount			(room.getElementsByTag("ChildCount").first().text());
			aRoom.setShrui				(room.getElementsByTag("HotelRoom").first().attr("SHRUI"));
			aRoom.setBoardCode			(room.getElementsByTag("Board").first().attr("code"));
			aRoom.setBoardType			(room.getElementsByTag("Board").first().attr("type"));
			aRoom.setRoomCode			(room.getElementsByTag("RoomType").first().attr("code"));
			aRoom.setRoomType			(room.getElementsByTag("RoomType").first().attr("type"));
			
			ArrayList<guestList> guestList		= new ArrayList<guestList>();
			Iterator<Element> guestIterator 	= room.getElementsByTag("Customer").iterator();
			while(guestIterator.hasNext())
			{
				Element				guest		= guestIterator.next();
				guestList			list		= new  guestList();
				
				list.setCustomerAge			(guest.getElementsByTag("Age").first().text());
				list.setCustomerType		(guest.getElementsByTag("Customer").first().attr("type"));
				guestList.add(list);
			}
			aRoom.setGuestList(guestList);
			//System.out.println(aRoom.getGuestList().get(0).getCustomerAge());
			roomAvailability.add(aRoom);
			
		}
		
		hotel.setRoomAvailability(roomAvailability);
		
		//System.out.println(hotel.getDateFromDate());
		//System.out.println(hotel.getRoomAvailability().get(0).getGuestList().get(0).getCustomerAge());
		return hotel;
		
	
	}
	
	public hbPage1Req inv27FinalReq(String pageSource)
	{
		Document doc  				= Jsoup.parse(pageSource, "UTF-8");
		hbPage1Req 					hotel 			= new hbPage1Req(); 
	 hotel.setUser				(doc.getElementsByTag("RequestorID").first().attr("Client"));
		hotel.setEmail				(doc.getElementsByTag("RequestorID").first().attr("EMailAddress"));
		try {
			hotel.setHotelName			(doc.getElementsByTag("ItemName").first().text());
			hotel.setHotelId			(doc.getElementsByTag("ItemCode").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		hotel.setCheckin			(doc.getElementsByTag("CheckInDate").first().text());
		try {
			hotel.setDuration			(doc.getElementsByTag("Duration").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		hotel.setBookingreference	(doc.getElementsByTag("BookingReference").first().text());
		hotel.setBookingDepartureDate(doc.getElementsByTag("BookingDepartureDate").first().text());
		
		Iterator<Element> paxIterator = doc.getElementsByTag("PaxName").iterator();
		ArrayList<resGuestList> gList = new ArrayList<resGuestList>();
		while(paxIterator.hasNext())
		{
			Element currentElement = paxIterator.next();
			resGuestList guest = new resGuestList();
			guest.setName			(currentElement.getElementsByTag("PaxName").first().attr("PaxId"));
			
			gList.add(guest);
		}
		hotel.setGuest(gList);
		
		hotel.setItemType			(doc.getElementsByTag("BookingItem").first().attr("ItemType"));
		hotel.setItemReference		(doc.getElementsByTag("ItemReference").first().text());
		hotel.setItemCity			(doc.getElementsByTag("ItemCity").first().attr("Code"));
		hotel.setCheckin			(doc.getElementsByTag("CheckInDate").first().text());
		hotel.setCheckout			(doc.getElementsByTag("CheckOutDate").first().text());
		
		
		Iterator<Element> roomIterator = doc.getElementsByTag("Room").iterator();
		ArrayList<inv27rooms> roomList = new ArrayList<inv27rooms>();
		while(roomIterator.hasNext())
		{
			Element currentElement = roomIterator.next();
			inv27rooms room = new inv27rooms();
			room.setRoomCode		(currentElement.getElementsByTag("Room").first().attr("Code"));
			room.setRoomId			(currentElement.getElementsByTag("Room").first().attr("id"));
			room.setNumOfRooms		(currentElement.getElementsByTag("Room").first().attr("NumberOfRooms"));
			room.setNumOfCots		(currentElement.getElementsByTag("Room").first().attr("NumberOfCots"));
			
			roomList.add(room);
		}
		hotel.setRoom(roomList);
		
		
		return hotel;
	}
	
	public hotelAvailability readRequest(String pageSource)
	{
		Document doc  				= Jsoup.parse(pageSource, "UTF-8");
		hotelAvailability	hotel	= new hotelAvailability(); 
		Element availability		= doc.getElementsByTag("PurchaseConfirmRQ").first();
		
		try {
			hotel.setUser				(availability.getElementsByTag("User").first().text());
			hotel.setPassword			(availability.getElementsByTag("Password").first().text());
			hotel.setContractname		(availability.getElementsByTag("Name").first().text());
			hotel.setIncomingOffice		(availability.getElementsByTag("IncomingOffice").first().attr("code"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		hotel.setHolderName			(availability.getElementsByTag("Name").first().text());
		hotel.setLastName			(availability.getElementsByTag("LastName").first().text());
		
		ArrayList<guestList>  	guestList = new ArrayList<guestList>();
		Iterator<Element> roomIterator		= doc.getElementsByTag("Customer").iterator();
		while(roomIterator.hasNext())
		{
			Element	room					= roomIterator.next();
			guestList			guest		= new guestList();
			
			guest.setCustomerType		(room.getElementsByTag("Customer").first().attr("type"));
			guest.setCustoemrId			(room.getElementsByTag("CustomerId").first().text());
			guest.setCustomerAge		(room.getElementsByTag("Age").first().text());
			guest.setName				(room.getElementsByTag("Name").first().text());
			guest.setLastName			(room.getElementsByTag("LastName").first().text());	
			guestList.add(guest);
		}
		
		hotel.setGuestList(guestList);
		
		//System.out.println(hotel.getDateFromDate());
		//System.out.println(hotel.getRoomAvailability().get(0).getGuestList().get(0).getCustomerAge());
		return hotel;
		
	}
	
	public hbPage1Req inv13AvailabilityReq(String pageSource)
	{
		Document doc  				= Jsoup.parse(pageSource, "UTF-8");
		hbPage1Req				hb				= new hbPage1Req();
		
		hb.setDestination		(doc.getElementsByTag("m:sDestination").first().text());
		hb.setCityName			(doc.getElementsByTag("m:sHotelCityName").first().text());
		hb.setCheckin			(doc.getElementsByTag("m:dtCheckIn").first().text());
		hb.setCheckout			(doc.getElementsByTag("m:dtCheckOut").first().text());
		
		Iterator<Element> 		guestElements		= doc.getElementsByTag("m:RoomInfo").iterator();
		ArrayList<resGuestList> 	guest			=  new ArrayList<resGuestList>();
		while(guestElements.hasNext())
		{
			Element	currentElement							= guestElements.next();
			resGuestList 	gst			=  new resGuestList();
			gst.setAdultCount			(currentElement.getElementsByTag("m:AdultsNum").first().text());
			try {
				gst.setChildCount			(currentElement.getElementsByTag("m:ChildNum").first().text());

			} catch (Exception e) {
				// TODO: handle exception
			}
			
			Iterator<Element> 			customerElements	= currentElement.getElementsByTag("m:ChildAge").iterator();
			ArrayList<customer>    		customer 			= new ArrayList<customer>();
			try {
				while(customerElements.hasNext())
				{
					Element	customerElement				= customerElements.next();
					customer			cus 			= new  customer();
					cus.setAge					(customerElement.getElementsByTag("m:ChildAge").first().text());
					//System.out.println(cus.getAge());
					customer.add(cus);
				}
				gst.setCus(customer);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			guest.add(gst);
		}
		hb.setGuest(guest);
		return hb;
	}
	
	public hbPage1Req inv27page1Req(String hbSearchPageSource)
	{
		Document 				doc  			= Jsoup.parse(hbSearchPageSource, "UTF-8");
		hbPage1Req				request				= new hbPage1Req();
		request.setUser			(doc.getElementsByTag("RequestorID").first().attr("Client"));
		request.setEmail		(doc.getElementsByTag("RequestorID").first().attr("EMailAddress"));
		request.setDestination	(doc.getElementsByTag("ItemDestination").first().attr("DestinationType"));
		request.setCheckin		(doc.getElementsByTag("CheckInDate").first().text());
		
		Iterator<Element> roomIterator = doc.getElementsByTag("Room").iterator();
		ArrayList<inv27rooms> roomList = new ArrayList<inv27rooms>();
		
		while(roomIterator.hasNext())
		{
			Element	currentElement							= roomIterator.next();
			inv27rooms room = new inv27rooms();
			room.setRoomCode		(currentElement.getElementsByTag("Room").first().attr("Code"));
			room.setNumOfRooms		(currentElement.getElementsByTag("Room").first().attr("NumberOfRooms"));
			room.setNumOfCots		(currentElement.getElementsByTag("Room").first().attr("NumberOfCots"));
			roomList.add(room);
		}
		request.setRoom(roomList);
		
		
		return request;
	}
	
	public hbPage1Req page1Req(String hbSearchPageSource)
	{
		Document 				doc  			= Jsoup.parse(hbSearchPageSource, "UTF-8");
		hbPage1Req				hb				= new hbPage1Req();
		
		hb.setUser				(doc.getElementsByTag("User").first().text());
		hb.setPassword			(doc.getElementsByTag("Password").first().text());
		hb.setCheckin			(doc.getElementsByTag("CheckInDate").first().attr("date"));
		//System.out.println(hb.getCheckin());
		hb.setCheckout			(doc.getElementsByTag("CheckOutDate").first().attr("date"));
		//System.out.println(hb.getCheckout());
		hb.setDestination		(doc.getElementsByTag("Destination").first().attr("code"));
		hb.setRoomCount			(String.valueOf(doc.getElementsByTag("RoomCount").size()));
		//hb.setAdultCount		(doc.getElementsByTag("AdultCount").first().text());
		//hb.setChildCount		(doc.getElementsByTag("ChildCount").first().text());
		ArrayList<String> a =  new ArrayList<String>();
		Iterator<Element> 		guestElements		= doc.getElementsByTag("HotelOccupancy").iterator();
		ArrayList<resGuestList> 	guest			=  new ArrayList<resGuestList>();
		while(guestElements.hasNext())
		{
			Element	currentElement							= guestElements.next();
			resGuestList 	gst	=  new resGuestList();
			gst.setAdultCount			(currentElement.getElementsByTag("AdultCount").first().text());
			gst.setChildCount			(currentElement.getElementsByTag("ChildCount").first().text());
			Iterator<Element> 			customerElements	= currentElement.getElementsByTag("Customer").iterator();
			ArrayList<customer>    		customer = new ArrayList<customer>();
			while(customerElements.hasNext())
			{
				Element	customerElement							= customerElements.next();
				customer			cus = new  customer();
				cus.setType					(customerElement.getElementsByTag("Customer").first().attr("type"));
				cus.setAge					(customerElement.getElementsByTag("Age").first().text());
				customer.add(cus);
			}
			gst.setCus(customer);
			guest.add(gst);
		}
		hb.setGuest(guest);
		return hb;
	}
	
	public int inv13HotelCount(String resPageSource)
	{
		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		int count = 0 ; 
		try {
			count = doc.getElementsByTag("Hotel").size();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return count;
	}
	
	public String hbhotelCount(String inv13RoomAvailabilityReqPageSource)
	{
		Document 				doc  			= Jsoup.parse(inv13RoomAvailabilityReqPageSource, "UTF-8");
		String count = null ; 
		count = doc.getElementsByTag("HotelValuedAvailRS").first().attr("totalItems");
		/*
		try {
			count = doc.getElementsByTag("Hotel").size();
		} catch (Exception e) {
			// TODO: handle exception
		}*/
		return count;
	}
	
	public hbPage1Req inv13RoomAvailabilityReq(String inv13RoomAvailabilityReqPageSource)
	{
		Document 				doc  			= Jsoup.parse(inv13RoomAvailabilityReqPageSource, "UTF-8");
		hbPage1Req				hb				= new hbPage1Req();
		hb.setCheckout							(doc.getElementsByTag("dtCheckOut").first().text());
		hb.setCheckin							(doc.getElementsByTag("dtCheckIn").first().text());
		
		Iterator<Element> 		guestElements		= doc.getElementsByTag("RoomInfo").iterator();
		ArrayList<resGuestList> 	guest			=  new ArrayList<resGuestList>();
		while(guestElements.hasNext())
		{
			Element	currentElement							= guestElements.next();
			resGuestList 	gst	=  new resGuestList();
			gst.setAdultCount					(currentElement.getElementsByTag("AdultsNum").first().text());
			gst.setChildCount					(currentElement.getElementsByTag("ChildNum").first().text());
			Iterator<Element> 			customerElements	= currentElement.getElementsByTag("ChildAge").iterator();
			ArrayList<customer>    		customer = new ArrayList<customer>();
			while(customerElements.hasNext())
			{
				Element	customerElement							= customerElements.next();
				customer			cus = new  customer();
				cus.setAge						(customerElement.getElementsByTag("ChildAge").first().text());
				customer.add(cus);
			}
			gst.setCus(customer);
			guest.add(gst);
		}
		hb.setGuest(guest);
		
		return hb;
	}
	
	public hbPage1Req inv27RoomaAvailabilityReq(String pageSource)
	{
		hbPage1Req 				hotel 			= new hbPage1Req();
		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		
		hotel.setUser			(doc.getElementsByTag("RequestorID").first().attr("Client"));
		hotel.setEmail			(doc.getElementsByTag("RequestorID").first().attr("EMailAddress"));
		hotel.setHotelName		(doc.getElementsByTag("ItemName").first().text());
		hotel.setHotelId		(doc.getElementsByTag("ItemCode").first().text());
		hotel.setCheckin		(doc.getElementsByTag("CheckInDate").first().text());
		hotel.setDuration		(doc.getElementsByTag("Duration").first().text());
		
		Iterator<Element> roomIterator = doc.getElementsByTag("Room").iterator();
		ArrayList<inv27rooms> roomList = new ArrayList<inv27rooms>();
		while(roomIterator.hasNext())
		{
			Element currentElement = roomIterator.next();
			inv27rooms room = new inv27rooms();
			room.setRoomCode		(currentElement.getElementsByTag("Room").first().attr("Code"));
			room.setRoomId			(currentElement.getElementsByTag("Room").first().attr("id"));
			room.setNumOfRooms		(currentElement.getElementsByTag("Room").first().attr("NumberOfRooms"));
			room.setNumOfCots		(currentElement.getElementsByTag("Room").first().attr("NumberOfCots"));
			
			roomList.add(room);
		}
		hotel.setRoom(roomList);
		
		
		return hotel;
	}
	
	public hbPage1Req hbRoomaAvailabilityReq(String hbRoomAvailabilityReqPageSource)
	{
		Document 				doc  			= Jsoup.parse(hbRoomAvailabilityReqPageSource, "UTF-8");
		hbPage1Req				hb				= new hbPage1Req();
		hb.setUser				(doc.getElementsByTag("User").first().text());
		//System.out.println(hb.getUser());
		hb.setPassword			(doc.getElementsByTag("Password").first().text());
		//System.out.println(hb.getPassword());
		hb.setCheckin			(doc.getElementsByTag("DateFrom").first().attr("date"));
		//System.out.println(hb.getCheckin());
		hb.setCheckout			(doc.getElementsByTag("DateTo").first().attr("date"));
		//System.out.println(hb.getCheckout());
		hb.setDestination		(doc.getElementsByTag("Destination").first().attr("code"));
		//System.out.println(hb.getDestination());
		hb.setRoomCount			(String.valueOf(doc.getElementsByTag("RoomCount").size()));
		//System.out.println(hb.getRoomCount());
		//hb.setAdultCount		(doc.getElementsByTag("AdultCount").first().text());
		//hb.setChildCount		(doc.getElementsByTag("ChildCount").first().text());
		ArrayList<String> a =  new ArrayList<String>();
		Iterator<Element> 		guestElements		= doc.getElementsByTag("HotelOccupancy").iterator();
		ArrayList<resGuestList> 	guest			=  new ArrayList<resGuestList>();
		while(guestElements.hasNext())
		{
			Element	currentElement							= guestElements.next();
			resGuestList 	gst	=  new resGuestList();
			gst.setAdultCount			(currentElement.getElementsByTag("AdultCount").first().text());
			//System.out.println(gst.getAdultCount());
			gst.setChildCount			(currentElement.getElementsByTag("ChildCount").first().text());
			//System.out.println(gst.getChildCount());
			Iterator<Element> 			customerElements	= currentElement.getElementsByTag("Customer").iterator();
			ArrayList<customer>    		customer = new ArrayList<customer>();
			while(customerElements.hasNext())
			{
				Element	customerElement							= customerElements.next();
				customer			cus = new  customer();
				cus.setType					(customerElement.getElementsByTag("Customer").first().attr("type"));
				//System.out.println(cus.getType());
				cus.setAge					(customerElement.getElementsByTag("Age").first().text());
				//System.out.println(cus.getAge());
				customer.add(cus);
			}
			gst.setCus(customer);
			guest.add(gst);
		}
		hb.setGuest(guest);
		return hb;
	}
	
	public ArrayList<resHotelAvailability> inv13AvailabilityResponse(String resPageSource)
	{
		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		ArrayList<resHotelAvailability>	hotelList			= new ArrayList<resHotelAvailability>();
		Iterator<Element> 			hotelElement	= doc.getElementsByTag("Hotel").iterator();
		
		while(hotelElement.hasNext())
		{
			Element	hotelInfo							= hotelElement.next();
			resHotelAvailability 	hotel = new resHotelAvailability();
			
			hotel.setHotelName				(hotelInfo.getElementsByTag("Hotel").first().attr("name"));
			hotel.setHotelAddress			(hotelInfo.getElementsByTag("Hotel").first().attr("address"));
			hotel.setHotelCategory			(hotelInfo.getElementsByTag("Hotel").first().attr("category"));
			hotel.setStarLevel				(hotelInfo.getElementsByTag("Hotel").first().attr("starsLevel"));
			hotel.setMinPrice				(hotelInfo.getElementsByTag("Hotel").first().attr("minAverPrice"));
			hotel.setDesdription			(hotelInfo.getElementsByTag("Hotel").first().attr("desc"));
			hotel.setLocation				(hotelInfo.getElementsByTag("Hotel").first().attr("location"));
			hotel.setCurrencyCode			(hotelInfo.getElementsByTag("Hotel").first().attr("currency"));
			
			ArrayList<resHotelRoom>	hotelRoomList			= new ArrayList<resHotelRoom>();
			Iterator<Element> 			roomTypeElement		= hotelInfo.getElementsByTag("RoomType").iterator();
			
			while(roomTypeElement.hasNext())
			{
				Element				roomInfoElement			= roomTypeElement.next();
				resHotelRoom		hotelRoom 				= new resHotelRoom();
				
				hotelRoom.setRoomId				(roomInfoElement.getElementsByTag("RoomType").first().attr("roomId"));
				hotelRoom.setRoomName			(roomInfoElement.getElementsByTag("RoomType").first().attr("name"));
				hotelRoom.setNights				(roomInfoElement.getElementsByTag("RoomType").first().attr("nights"));
				hotelRoom.setDateTimeFromDate	(roomInfoElement.getElementsByTag("RoomType").first().attr("startDate"));
				hotelRoom.setAvailability		(roomInfoElement.getElementsByTag("RoomType").first().attr("isAvailable"));
				hotel.setSingleRoom(hotelRoom);
				
				ArrayList<resavailableRoom>  	roomInfo 		= new ArrayList<resavailableRoom>();
				Iterator<Element> 				roomIterator	= hotelInfo.getElementsByTag("Occup").iterator();
				//System.out.println(roomInfoElement.getElementsByTag("Occup").size());
				while(roomIterator.hasNext())
				{
					Element						roomElement		= roomIterator.next();
					resavailableRoom 			room			= new resavailableRoom();
					
					room.setMaxAdult			(roomElement.getElementsByTag("Occup").first().attr("maxAdult"));
					room.setMaxChild			(roomElement.getElementsByTag("Occup").first().attr("maxChild"));
					room.setPrice				(roomElement.getElementsByTag("Occup").first().attr("price"));
					room.setTax					(roomElement.getElementsByTag("Occup").first().attr("tax"));
					room.setDoubleBed			(roomElement.getElementsByTag("Occup").first().attr("dblBed"));
					room.setAvgPrice			(roomElement.getElementsByTag("Occup").first().attr("avrNightPrice"));
					//System.out.println(room.getAvgPrice());	
					try {
						room.setBoardName			(roomElement.getElementsByTag("Board").first().attr("name"));
						room.setBoardPrice			(roomElement.getElementsByTag("Board").first().attr("price"));
					} catch (Exception e) {
						// TODO: handle exception
					}
					room.setRoomNumber			(roomElement.getElementsByTag("Room").first().attr("seqNum"));
					//System.out.println(room.getRoomNumber());
					room.setAdultCount			(roomElement.getElementsByTag("Room").first().attr("adultNum"));
					room.setChildCount			(roomElement.getElementsByTag("Room").first().attr("childNum"));
					room.setRoomPrice			(roomElement.getElementsByTag("Price").first().text());
					
					ArrayList<resGuestList>  	guestInfo 			= new ArrayList<resGuestList>();
					Iterator<Element> 			guestIterator		= roomElement.getElementsByTag("Child").iterator();
					int qwe = 0 ;
					while(guestIterator.hasNext())
					{
						Element					guestElement		= guestIterator.next();
						resGuestList 			guest				= new resGuestList();
						
						guest.setCustomerAge	(guestElement.getElementsByTag("Child").first().attr("age"));
						guestInfo.add(guest);
						qwe++;
					}
					room.setGuest(guestInfo);
					roomInfo.add(room);
					hotelRoom.setAvailableRoom(roomInfo);
					hotelRoomList.add(hotelRoom);
				}
				hotel.setHotelRoom(hotelRoomList);
				//System.out.println(hotel.getHotelRoom().size());
				hotelList.add(hotel);
			}
			
		}
		/*System.out.println(hotelList.get(0).getHotelRoom().get(0).getAvailableRoom().get(0).getGuest().get(0).getCustomerAge());
		System.out.println(hotelList.size());	
		System.out.println(hotelList.get(0).getHotelRoom().get(0).getRoomName());*/
		return hotelList;
	
		
	}
	
	public resHotelAvailability whitesandsRoomAvailabilityRes(String roomAvailabilityResPageSource)
	{
		resHotelAvailability 	res 		= 	new resHotelAvailability();
		Document 						doc  		= Jsoup.parse(roomAvailabilityResPageSource, "UTF-8");
		//System.out.println(doc);
		res.setUnits					(doc.getElementsByTag("CalcByUnits").first().text());
		res.setNyts						(doc.getElementsByTag("nights").first().text());
		res.setFromDate					(doc.getElementsByTag("frmdate").first().text());
		res.setToDate					(doc.getElementsByTag("todate").first().text());
		res.setMaxAdult					(doc.getElementsByTag("maxadults").first().text());
		res.setMaxChildren				(doc.getElementsByTag("maxchildfree").first().text());
		res.setAvailability				(doc.getElementsByTag("available").first().text());
		res.setRateType					(doc.getElementsByTag("ratetype").first().text());
		res.setCreditLimit				(doc.getElementsByTag("Crlimit").first().text());
		res.setMinimumNyts				(doc.getElementsByTag("minnights").first().text());
		res.setChildAgeStart			(doc.getElementsByTag("chldagestart").first().text());
		res.setAdultAgeStart			(doc.getElementsByTag("adultagestart").first().text());
		res.setChildPolicy				(doc.getElementsByTag("childpolicy").first().text());
		res.setCancellationPolicy		(doc.getElementsByTag("cancellationpolicy").first().text());
		Element 	ele 		= 		doc.getElementsByTag("getAvailabiltyDet1").first();
		res.setFromDate2				(ele.getElementsByTag("frmdate").first().text());
		res.setToDate2					(ele.getElementsByTag("todate").first().text());
		res.setPrice2					(ele.getElementsByTag("price").first().text());
		res.setNyts2					(ele.getElementsByTag("nights").first().text());
		res.setFreenyts2				(ele.getElementsByTag("freenights").first().text());
		res.setPlistcode				(ele.getElementsByTag("plistcode").first().text());
		res.setPlistlineno				(ele.getElementsByTag("plistlineno").first().text());
		
		ArrayList<whitesandsPreReservationResOccupancy>		roomList		= new ArrayList<whitesandsPreReservationResOccupancy>();
		Iterator<Element> 						room			= doc.getElementsByTag("getAvailabiltyDet2").iterator();
		
		while(room.hasNext())
		{
			Element								roomElement		= room.next();
			whitesandsPreReservationResOccupancy 			currentRoom		= new whitesandsPreReservationResOccupancy();
			
			currentRoom.setFromdate			(roomElement.getElementsByTag("frmdate").first().text());
			currentRoom.setToDate			(roomElement.getElementsByTag("todate").first().text());
			currentRoom.setRoomCode			(roomElement.getElementsByTag("rmcatcode").first().text());
			currentRoom.setRoomname			(roomElement.getElementsByTag("rmcatname").first().text());
			currentRoom.setPrice			(roomElement.getElementsByTag("price").first().text());
			currentRoom.setCalcByPax		(roomElement.getElementsByTag("CalcByPax").first().text());
			currentRoom.setCalcByUnits		(roomElement.getElementsByTag("CalcByUnits").first().text());
			currentRoom.setAutoconfirm		(roomElement.getElementsByTag("autoconfirm").first().text());
			roomList.add(currentRoom);
		}
		res.setRoom(roomList);
		return res;
	}
	
	public ArrayList<whitesandsReservationReq> whitesandsReservationReq(String pageSource)
	{
		Document 		doc  	= Jsoup.parse(pageSource, "UTF-8");
		ArrayList<whitesandsReservationReq> list = new ArrayList<whitesandsReservationReq>();
		Iterator<Element> 						room		=	 doc.getElementsByTag("htlrequestdet").iterator();
		while(room.hasNext())
		{
			Element							currentRoom		= room.next();
			whitesandsReservationReq		req 			= new whitesandsReservationReq();
			req.setReqId					(currentRoom.getElementsByTag("requestid").first().text());
			req.setPartyCode				(currentRoom.getElementsByTag("partycode").first().text());
			req.setRoomCode					(currentRoom.getElementsByTag("rmtypcode").first().text());
			req.setMealCode					(currentRoom.getElementsByTag("mealcode").first().text());
			req.setNumOfRooms				(currentRoom.getElementsByTag("noofrooms").first().text());
			req.setNumOfAdults				(currentRoom.getElementsByTag("noofadults").first().text());
			req.setNumOfChilds				(currentRoom.getElementsByTag("noofchild").first().text());
			ArrayList<String> 	title 		= new ArrayList<String>();
			ArrayList<String> 	firstName 	= new ArrayList<String>();
			ArrayList<String> 	lastName 	= new ArrayList<String>();
			ArrayList<String> 	age			= new ArrayList<String>();
			for(int i = 1 ; i > 0 ; i++)
			{
				try {
					title.add(currentRoom.getElementsByTag("title" + i).first().text());
					lastName.add(currentRoom.getElementsByTag("familyname" + i).first().text());
					firstName.add(currentRoom.getElementsByTag("firstname" + i).first().text());
					age.add(currentRoom.getElementsByTag("age" + i).first().text());
				} catch (Exception e) {
					// TODO: handle exception
					break;
				}
				
			}
			req.setTitle(title);
			req.setFirstname(firstName);
			req.setLastName(firstName);
			req.setAge(age);
			req.setCheckin					(currentRoom.getElementsByTag("chkindate").first().text());
			req.setCheckout					(currentRoom.getElementsByTag("chkoutdate").first().text());
			req.setNyts						(currentRoom.getElementsByTag("nights").first().text());
			req.setReqDate					(currentRoom.getElementsByTag("requestdate").first().text());
			req.setAvailability				(currentRoom.getElementsByTag("availableyesno").first().text());
			req.setTimeLimit				(currentRoom.getElementsByTag("time_limit").first().text());
			req.setMinNyts					(currentRoom.getElementsByTag("minnights").first().text());
			
			Element			more			= currentRoom.getElementsByTag("htlrequestsubdet").first();
			req.setReqId2					(more.getElementsByTag("requestid").first().text());
			req.setLineNum2					(more.getElementsByTag("line_no").first().text());
			req.setRoomCode2				(more.getElementsByTag("rmcatcode").first().text());
			req.setUnits2					(more.getElementsByTag("units").first().text());
			req.setLineValue2				(more.getElementsByTag("linevalue").first().text());
			req.setPlistcode2				(more.getElementsByTag("plistcode").first().text());
			req.setPlistlineno2				(more.getElementsByTag("plistlineno").first().text());
			req.setRatetype2				(more.getElementsByTag("ratetype").first().text());
			req.setPkgnights2				(more.getElementsByTag("pkgnights").first().text());
			req.setFreenights2				(more.getElementsByTag("freenights").first().text());
			req.setAgentsellprice2			(more.getElementsByTag("agentsellprice").first().text());
			
			list.add(req);
		}
		
		return list;
	}
	
	public whitesandsPreReservationRes whitesandsPreReservetionresponse(String roomAvailabilityPreReqPageSource)
	{
		Document 							doc  			= Jsoup.parse(roomAvailabilityPreReqPageSource, "UTF-8");
		whitesandsPreReservationRes res = new whitesandsPreReservationRes();
		
		res.setFromdate						(doc.getElementsByTag("frmdate").first().text());
		res.setToDate						(doc.getElementsByTag("todate").first().text());
		res.setNyts							(doc.getElementsByTag("nights").first().text());
		res.setTimeLimit					(doc.getElementsByTag("timelimit").first().text());
		res.setMaxAdults					(doc.getElementsByTag("maxadults").first().text());
		res.setMaxeb						(doc.getElementsByTag("maxeb").first().text());
		res.setMaxChildFree					(doc.getElementsByTag("maxchildfree").first().text());
		res.setFreeNyts						(doc.getElementsByTag("freenights").first().text());
		res.setAvaialability				(doc.getElementsByTag("available").first().text());
		res.setRateType						(doc.getElementsByTag("ratetype").first().text());
		res.setCreditLimit					(doc.getElementsByTag("Crlimit").first().text());
		res.setPromotionid					(doc.getElementsByTag("promotionid").first().text());
		res.setMinimumNyts					(doc.getElementsByTag("minnights").first().text());
		res.setChildAgeStart				(doc.getElementsByTag("chldagestart").first().text());
		res.setAdultAgeStart				(doc.getElementsByTag("adultagestart").first().text());
		res.setChildPolicy					(doc.getElementsByTag("childpolicy").first().text());
		res.setCancellationPolicy			(doc.getElementsByTag("cancellationpolicy").first().text());
		res.setPriceremarks					(doc.getElementsByTag("priceremarks").first().text());
		res.setProductType					(doc.getElementsByTag("spTypeCode").first().text());
		
		Element 			aaa 	= 		doc.getElementsByTag("getAvailabiltyDet1").first();
		
		res.setFromDate2					(aaa.getElementsByTag("frmdate").first().text());
		res.setToDate2						(aaa.getElementsByTag("todate").first().text());
		res.setPrice2						(aaa.getElementsByTag("price").first().text());
		res.setNyts2						(aaa.getElementsByTag("nights").first().text());
		res.setFreeNyts2					(aaa.getElementsByTag("freenights").first().text());
		res.setPlistcode					(aaa.getElementsByTag("plistcode").first().text());
		res.setPlistlineno					(aaa.getElementsByTag("plistlineno").first().text());
		
		Iterator<Element> 					room		=	 doc.getElementsByTag("getAvailabiltyDet2").iterator();
		ArrayList<whitesandsPreReservationResOccupancy> occupancy = new ArrayList<whitesandsPreReservationResOccupancy>();
		while(room.hasNext())
		{
			Element				currentRoom		= room.next();
			whitesandsPreReservationResOccupancy occ = new whitesandsPreReservationResOccupancy();
			occ.setFromdate					(currentRoom.getElementsByTag("frmdate").first().text());
			occ.setToDate					(currentRoom.getElementsByTag("todate").first().text());
			occ.setRoomCode					(currentRoom.getElementsByTag("rmcatcode").first().text());
			occ.setRoomname					(currentRoom.getElementsByTag("rmcatname").first().text());
			occ.setPrice					(currentRoom.getElementsByTag("price").first().text());
			occ.setCalcByPax				(currentRoom.getElementsByTag("CalcByPax").first().text());
			occ.setCalcByUnits				(currentRoom.getElementsByTag("CalcByUnits").first().text());
			occ.setAutoconfirm				(currentRoom.getElementsByTag("autoconfirm").first().text());
			occupancy.add(occ);
		}
		res.setRoom(occupancy);
		return res;
	}
	
	public cancellationRes cancelres(String pageSource) 
	{
		Document 								doc  			= Jsoup.parse(pageSource, "UTF-8");
		cancellationRes cr = new cancellationRes();
		cr.setFaultCode						(doc.getElementsByTag("faultcode").first().text());
		cr.setFaultString					(doc.getElementsByTag("faultstring").first().text());
		return cr;
	}
	
	public cancellationReq cancelReq(String canReqPageSource)
	{
		Document 								doc  			= Jsoup.parse(canReqPageSource, "UTF-8");
		cancellationReq cr = new cancellationReq();
		cr.setPassword						(doc.getElementsByTag("m0:username").first().text());
		cr.setPassword						(doc.getElementsByTag("m0:password").first().text());
		return cr;
	}
	
	public whitesandsRoomAvailabilityReq whitesandsRoomAvailabilityReq(String roomAvailabilityReqPageSource)
	{
		Document 								doc  			= Jsoup.parse(roomAvailabilityReqPageSource, "UTF-8");
		//System.out.println(doc);
		Element aaa = doc.getElementsByTag("getAvailabiltyDet").first();
		whitesandsRoomAvailabilityReq req = new whitesandsRoomAvailabilityReq();
		
		req.setAgentCode					(aaa.getElementsByTag("agentregcode").first().text());
		req.setXmlId						(aaa.getElementsByTag("xml_id").first().text());
		req.setCartid						(aaa.getElementsByTag("cartid").first().text());
		req.setPartyCode					(aaa.getElementsByTag("partycode").first().text());
		req.setRoomTypeCode					(aaa.getElementsByTag("rmtypcode").first().text());
		req.setRoomCatCode					(aaa.getElementsByTag("rmcatcode").first().text());
		req.setMealCode						(aaa.getElementsByTag("mealcode").first().text());
		req.setRateType						(aaa.getElementsByTag("ratetype").first().text());
		req.setFlag							(aaa.getElementsByTag("flag").first().text());
		
		return req;
	}
	
	public ArrayList<whiteSandsAvailabilityRes> whitesandshotelAvailabilityRes(String resPageSource)
	{
		Document 								doc  			= Jsoup.parse(resPageSource, "UTF-8");
		Iterator<Element> 						hotel			= doc.getElementsByTag("getavailabilty").iterator();
		ArrayList<whiteSandsAvailabilityRes> 	hotelList		= new ArrayList<whiteSandsAvailabilityRes>();
		
		while(hotel.hasNext())
		{
			Element								list				= hotel.next();
			whiteSandsAvailabilityRes			ws 				= new whiteSandsAvailabilityRes();
			ws.setHotelCode					(list.getElementsByTag("partycode").first().text());
			ws.setHotelName					(list.getElementsByTag("partyname").first().text());
			ws.setStarCategory				(list.getElementsByTag("catname").first().text());
			ws.setRoomCode					(list.getElementsByTag("rmcatcode").first().text());
			ws.setRoomTpeCode				(list.getElementsByTag("rmtypcode").first().text());
			ws.setRoomName					(list.getElementsByTag("rmtypname").first().text());
			ws.setMealCode					(list.getElementsByTag("mealcode").first().text());
			ws.setMealname					(list.getElementsByTag("mealname").first().text());
			ws.setRateType					(list.getElementsByTag("ratetype").first().text());
			ws.setPrice						(list.getElementsByTag("price").first().text());
			ws.setAvailability				(list.getElementsByTag("available").first().text());
			ws.setSpecial					(list.getElementsByTag("special").first().text());
			ws.setFromDate					(list.getElementsByTag("frmdate").first().text());
			ws.setToDate					(list.getElementsByTag("todate").first().text());
			ws.setTimeLimit					(list.getElementsByTag("timelimit").first().text());
			ws.setXmlid						(list.getElementsByTag("XML_ID").first().text());
			ws.setCartId					(list.getElementsByTag("Cartid").first().text());
			ws.setCancelDays				(list.getElementsByTag("canceldays").first().text());
			
			hotelList.add(ws);
		}
		
		return hotelList;
	}
	
	public ArrayList<resHotelAvailability> inv27Page1Response(String resPageSource)
	{
		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		ArrayList<resHotelAvailability>	hotel	= new ArrayList<resHotelAvailability>(); 
		Iterator<Element> 		hotelIterator 	= doc.getElementsByTag("Hotel").iterator();
		while(hotelIterator.hasNext())
		{
			Element currentElement = hotelIterator.next();
			resHotelAvailability	hDetails			= new resHotelAvailability(); 
			hDetails.setLocation			(currentElement.getElementsByTag("City").first().text());
			hDetails.setHotelName			(currentElement.getElementsByTag("Item").first().text());
			
			Iterator<Element> categoryIterator = currentElement.getElementsByTag("RoomCategory").iterator();
			ArrayList<roomCategory> rcList = new ArrayList<roomCategory>();
			while(categoryIterator.hasNext())
			{
				Element categoryElement = categoryIterator.next();
				roomCategory rc = new roomCategory();
				rc.setDescription			(categoryElement.getElementsByTag("Description").first().text());
				rc.setPrice					(categoryElement.getElementsByTag("ItemPrice").first().text());
				rc.setCurrency				(categoryElement.getElementsByTag("ItemPrice").first().attr("Currency"));
				rc.setAvailability			(categoryElement.getElementsByTag("Confirmation").first().text());
				rc.setMealBasis				(categoryElement.getElementsByTag("Basis").first().text());
				try {
					rc.setMealBreakFast			(categoryElement.getElementsByTag("Breakfast").first().text());
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				Iterator<Element> roomIterator = categoryElement.getElementsByTag("HotelRoom").iterator();
				ArrayList<resHotelRoom> rhrList = new ArrayList<resHotelRoom>();
				while(roomIterator.hasNext())
				{
					Element roomElement = roomIterator.next();
					resHotelRoom rhr = new resHotelRoom();
					rhr.setRoomCode			(roomElement.getElementsByTag("HotelRoom").first().attr("Code"));
					rhr.setPrice			(roomElement.getElementsByTag("RoomPrice ").first().attr("Gross"));
					rhr.setDateTimeFromDate	(roomElement.getElementsByTag("FromDate").first().text());
					rhr.setDateTimeToDate	(roomElement.getElementsByTag("ToDate").first().text());
					rhr.setNights			(roomElement.getElementsByTag("Price ").first().attr("Nights"));
					rhrList.add(rhr);
				}
				rc.setRoom(rhrList);
				rcList.add(rc);
			}
			hDetails.setRc(rcList);
			hotel.add(hDetails);
		}
		return hotel;
	}
	
	public resHotelAvailability hbPage1Response(String resPageSource)
	{
		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		
		String					count			= doc.getElementsByTag("HotelValuedAvailRS").first().attr("totalItems");
		int     				icount			= Integer.parseInt(count);
		if(icount != 0)
		{
			Element 				auditData	= doc.getElementsByTag("AuditData").first();
			
			hotel.setProcessTime			(auditData.getElementsByTag("ProcessTime").first().text());
			hotel.setTimeStamp				(auditData.getElementsByTag("Timestamp").first().text());
			hotel.setRequestHost			(auditData.getElementsByTag("RequestHost").first().text());
			hotel.setServerName				(auditData.getElementsByTag("ServerName").first().text());
			hotel.setServerId				(auditData.getElementsByTag("ServerId").first().text());
			hotel.setSchemaRelease			(auditData.getElementsByTag("SchemaRelease").first().text());
			hotel.setHydraCodeRelease		(auditData.getElementsByTag("HydraCoreRelease").first().text());
			hotel.setHydraERelease			(auditData.getElementsByTag("HydraEnumerationsRelease").first().text());
			hotel.setMerlineRelease			(auditData.getElementsByTag("MerlinRelease").first().text());
			Iterator<Element> serviceHotelIterator			= doc.getElementsByTag("ServiceHotel").iterator();
			ArrayList<resHotelDetails> 		hotelDetails	= new ArrayList<resHotelDetails>();
			while(serviceHotelIterator.hasNext())
			{
				Element	aaa							= serviceHotelIterator.next();
				resHotelDetails		hDetails 		= new resHotelDetails();
				Element hotelInfo					= aaa.getElementsByTag("HotelInfo").first();
				Element	destinationInfo				= hotelInfo.getElementsByTag("Destination").first();
				
				hDetails.setHotelTypr		(hotelInfo.getElementsByTag("HotelInfo").first().attr("xsi:type"));
				hDetails.setHotelCode		(hotelInfo.getElementsByTag("Code").first().text());
				hDetails.setHotelName		(hotelInfo.getElementsByTag("Name").first().text());
				hDetails.setStarCategory	(hotelInfo.getElementsByTag("Category").first().text());
				hDetails.setCurrencyCode	(aaa.getElementsByTag("Currency").first().attr("code"));
				hDetails.setCheckin			(aaa.getElementsByTag("DateFrom").first().attr("date"));
				hDetails.setCheckout		(aaa.getElementsByTag("DateTo").first().attr("date"));
				
				
				hDetails.setDestinationCode	(destinationInfo.getElementsByTag("Destination").first().attr("code"));
				hDetails.setDestination		(destinationInfo.getElementsByTag("Destination").first().attr("Name"));
				hDetails.setZone			(destinationInfo.getElementsByTag("Zone").first().text());
				hDetails.setChildAgrFrom	(hotelInfo.getElementsByTag("ChildAge").first().attr("ageFrom"));
				hDetails.setChildAgeTo		(hotelInfo.getElementsByTag("ChildAge").first().attr("ageTo"));
				
				Iterator<Element> roomOccupancy		= aaa.getElementsByTag("AvailableRoom").iterator();
				ArrayList<resavailableRoom>	availRoom		= new ArrayList<resavailableRoom>();
				ArrayList<resHotelRoom> 	resHRoom 		= new ArrayList<resHotelRoom>();
				while(roomOccupancy.hasNext())
				{
					Element	occupancyDetails = roomOccupancy.next();
					resavailableRoom 	resRoom		=  new resavailableRoom();
					resHotelRoom		reshRoom	= new resHotelRoom();
					resRoom.setRoomCount		(String.valueOf(aaa.getElementsByTag("RoomCount").size()));
					resRoom.setAdultCount		(occupancyDetails.getElementsByTag("AdultCount").first().text());
					resRoom.setChildCount		(occupancyDetails.getElementsByTag("ChildCount").first().text());
					reshRoom.setStatus			(occupancyDetails.getElementsByTag("HotelRoom").first().attr("onRequest"));
					reshRoom.setBoard			(occupancyDetails.getElementsByTag("Board").first().text());
					reshRoom.setRoom			(occupancyDetails.getElementsByTag("RoomType").first().text());
					reshRoom.setPrice			(occupancyDetails.getElementsByTag("Amount").first().text());
					availRoom.add(resRoom);
					resHRoom.add(reshRoom);
				}
				hDetails.setAvailRoom(availRoom);
				hDetails.setRhRoom(resHRoom);
				hotelDetails.add(hDetails);
			}	
			hotel.setHotelDetails(hotelDetails);
		}
		else
		{
			System.out.println("No results available");
		}
		
	
		
		return hotel;
	}
	
	public resHotelAvailability inv13RoomAvailabilityRes(String pageSource)
	{
		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		Element					hotelInfo		= doc.getElementsByTag("TWS_HotelList").first();
		
		hotel.setHotelName				(hotelInfo.getElementsByTag("Hotel").first().attr("name"));
		hotel.setHotelAddress			(hotelInfo.getElementsByTag("Hotel").first().attr("address"));
		hotel.setHotelCategory			(hotelInfo.getElementsByTag("Hotel").first().attr("category"));
		hotel.setStarLevel				(hotelInfo.getElementsByTag("Hotel").first().attr("starsLevel"));
		hotel.setMinPrice				(hotelInfo.getElementsByTag("Hotel").first().attr("minAverPrice"));
		hotel.setDesdription			(hotelInfo.getElementsByTag("Hotel").first().attr("desc"));
		hotel.setLocation				(hotelInfo.getElementsByTag("Hotel").first().attr("location"));
		hotel.setCurrencyCode			(hotelInfo.getElementsByTag("Hotel").first().attr("currency"));
		
		resHotelRoom	hotelRoom = new resHotelRoom();
		hotelRoom.setRoomId				(hotelInfo.getElementsByTag("RoomType").first().attr("roomId"));
		hotelRoom.setRoomName			(hotelInfo.getElementsByTag("RoomType").first().attr("name"));
		//System.out.println(hotelRoom.getRoomName());
		hotelRoom.setNights				(hotelInfo.getElementsByTag("RoomType").first().attr("nights"));
		hotelRoom.setDateTimeFromDate	(hotelInfo.getElementsByTag("RoomType").first().attr("startDate"));
		hotelRoom.setAvailability		(hotelInfo.getElementsByTag("RoomType").first().attr("isAvailable"));
		hotel.setSingleRoom(hotelRoom);
		
		ArrayList<resavailableRoom>  	roomInfo 		= new ArrayList<resavailableRoom>();
		Iterator<Element> 				roomIterator	= doc.getElementsByTag("Occup").iterator();
		while(roomIterator.hasNext())
		{
			Element						roomElement		= roomIterator.next();
			resavailableRoom 			room			= new resavailableRoom();
			
			room.setMaxAdult			(roomElement.getElementsByTag("Occup").first().attr("maxAdult"));
			room.setMaxChild			(roomElement.getElementsByTag("Occup").first().attr("maxChild"));
			room.setPrice				(roomElement.getElementsByTag("Occup").first().attr("price"));
			room.setTax					(roomElement.getElementsByTag("Occup").first().attr("tax"));
			room.setDoubleBed			(roomElement.getElementsByTag("Occup").first().attr("dblBed"));
			room.setAvgPrice			(roomElement.getElementsByTag("Occup").first().attr("avrNightPrice"));
			try {
				room.setBoardName			(roomElement.getElementsByTag("Board").first().attr("name"));
				room.setBoardPrice			(roomElement.getElementsByTag("Board").first().attr("price"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			room.setRoomNumber			(roomElement.getElementsByTag("Room").first().attr("seqNum"));
			room.setAdultCount			(roomElement.getElementsByTag("Room").first().attr("adultNum"));
			room.setChildCount			(roomElement.getElementsByTag("Room").first().attr("childNum"));
			room.setRoomPrice			(roomElement.getElementsByTag("Price").first().text());
			
			ArrayList<resGuestList>  	guestInfo 			= new ArrayList<resGuestList>();
			Iterator<Element> 			guestIterator		= roomElement.getElementsByTag("Child").iterator();
			int qwe = 0 ;
			while(guestIterator.hasNext())
			{
				Element					guestElement		= guestIterator.next();
				resGuestList 			guest				= new resGuestList();
				
				guest.setCustomerAge	(guestElement.getElementsByTag("Child").first().attr("age"));
				guestInfo.add(guest);
				qwe++;
			}
			room.setGuest(guestInfo);
			roomInfo.add(room);
		}
		hotel.setRoomAvailability(roomInfo);
		
		return hotel;
	}
	
	public resHotelAvailability inv13ReadPaymentResponse(String pageSource)
	{

		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		Element					hotelInfo		= doc.getElementsByTag("TWS_HotelList").first();
		
		hotel.setHotelName				(hotelInfo.getElementsByTag("Hotel").first().attr("name"));
		hotel.setHotelAddress			(hotelInfo.getElementsByTag("Hotel").first().attr("address"));
		hotel.setHotelCategory			(hotelInfo.getElementsByTag("Hotel").first().attr("category"));
		hotel.setStarLevel				(hotelInfo.getElementsByTag("Hotel").first().attr("starsLevel"));
		hotel.setMinPrice				(hotelInfo.getElementsByTag("Hotel").first().attr("minAverPrice"));
		hotel.setDesdription			(hotelInfo.getElementsByTag("Hotel").first().attr("desc"));
		hotel.setLocation				(hotelInfo.getElementsByTag("Hotel").first().attr("location"));
		hotel.setCurrencyCode			(hotelInfo.getElementsByTag("Hotel").first().attr("currency"));
		
		resHotelRoom	hotelRoom = new resHotelRoom();
		hotelRoom.setRoomId				(hotelInfo.getElementsByTag("RoomType").first().attr("roomId"));
		hotelRoom.setRoomName			(hotelInfo.getElementsByTag("RoomType").first().attr("name"));
		//System.out.println(hotelRoom.getRoomName());
		hotelRoom.setNights				(hotelInfo.getElementsByTag("RoomType").first().attr("nights"));
		hotelRoom.setDateTimeFromDate	(hotelInfo.getElementsByTag("RoomType").first().attr("startDate"));
		hotelRoom.setAvailability		(hotelInfo.getElementsByTag("RoomType").first().attr("isAvailable"));
		hotel.setSingleRoom(hotelRoom);
		
		ArrayList<resavailableRoom>  	roomInfo 		= new ArrayList<resavailableRoom>();
		Iterator<Element> 				roomIterator	= doc.getElementsByTag("Occup").iterator();
		while(roomIterator.hasNext())
		{
			Element						roomElement		= roomIterator.next();
			resavailableRoom 			room			= new resavailableRoom();
			
			room.setMaxAdult			(roomElement.getElementsByTag("Occup").first().attr("maxAdult"));
			room.setMaxChild			(roomElement.getElementsByTag("Occup").first().attr("maxChild"));
			room.setPrice				(roomElement.getElementsByTag("Occup").first().attr("price"));
			room.setTax					(roomElement.getElementsByTag("Occup").first().attr("tax"));
			room.setDoubleBed			(roomElement.getElementsByTag("Occup").first().attr("dblBed"));
			room.setAvgPrice			(roomElement.getElementsByTag("Occup").first().attr("avrNightPrice"));
			try {
				room.setBoardName			(roomElement.getElementsByTag("Board").first().attr("name"));
				room.setBoardPrice			(roomElement.getElementsByTag("Board").first().attr("price"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			room.setRoomNumber			(roomElement.getElementsByTag("Room").first().attr("seqNum"));
			room.setAdultCount			(roomElement.getElementsByTag("Room").first().attr("adultNum"));
			room.setChildCount			(roomElement.getElementsByTag("Room").first().attr("childNum"));
			room.setRoomPrice			(roomElement.getElementsByTag("Price").first().text());
			
			ArrayList<resGuestList>  	guestInfo 			= new ArrayList<resGuestList>();
			Iterator<Element> 			guestIterator		= roomElement.getElementsByTag("Child").iterator();
			int qwe = 0 ;
			while(guestIterator.hasNext())
			{
				Element					guestElement		= guestIterator.next();
				resGuestList 			guest				= new resGuestList();
				
				guest.setCustomerAge	(guestElement.getElementsByTag("Child").first().attr("age"));
				guestInfo.add(guest);
				qwe++;
			}
			room.setGuest(guestInfo);
			roomInfo.add(room);
		}
		hotel.setRoomAvailability(roomInfo);
		
		return hotel;
	
	}
	
	public resHotelAvailability inv13ReadPreResponse(String pageSource)
	{


		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		Element					hotelInfo		= doc.getElementsByTag("TWS_HotelList").first();
		
		hotel.setHotelName				(hotelInfo.getElementsByTag("Hotel").first().attr("name"));
		hotel.setHotelAddress			(hotelInfo.getElementsByTag("Hotel").first().attr("address"));
		hotel.setHotelCategory			(hotelInfo.getElementsByTag("Hotel").first().attr("category"));
		hotel.setStarLevel				(hotelInfo.getElementsByTag("Hotel").first().attr("starsLevel"));
		hotel.setMinPrice				(hotelInfo.getElementsByTag("Hotel").first().attr("minAverPrice"));
		hotel.setDesdription			(hotelInfo.getElementsByTag("Hotel").first().attr("desc"));
		hotel.setLocation				(hotelInfo.getElementsByTag("Hotel").first().attr("location"));
		hotel.setCurrencyCode			(hotelInfo.getElementsByTag("Hotel").first().attr("currency"));
		
		resHotelRoom	hotelRoom = new resHotelRoom();
		hotelRoom.setRoomId				(hotelInfo.getElementsByTag("RoomType").first().attr("roomId"));
		hotelRoom.setRoomName			(hotelInfo.getElementsByTag("RoomType").first().attr("name"));
		//System.out.println(hotelRoom.getRoomName());
		hotelRoom.setNights				(hotelInfo.getElementsByTag("RoomType").first().attr("nights"));
		hotelRoom.setDateTimeFromDate	(hotelInfo.getElementsByTag("RoomType").first().attr("startDate"));
		hotelRoom.setAvailability		(hotelInfo.getElementsByTag("RoomType").first().attr("isAvailable"));
		hotel.setSingleRoom(hotelRoom);
		
		ArrayList<resavailableRoom>  	roomInfo 		= new ArrayList<resavailableRoom>();
		Iterator<Element> 				roomIterator	= doc.getElementsByTag("Occup").iterator();
		while(roomIterator.hasNext())
		{
			Element						roomElement		= roomIterator.next();
			resavailableRoom 			room			= new resavailableRoom();
			
			room.setMaxAdult			(roomElement.getElementsByTag("Occup").first().attr("maxAdult"));
			room.setMaxChild			(roomElement.getElementsByTag("Occup").first().attr("maxChild"));
			room.setPrice				(roomElement.getElementsByTag("Occup").first().attr("price"));
			room.setTax					(roomElement.getElementsByTag("Occup").first().attr("tax"));
			room.setDoubleBed			(roomElement.getElementsByTag("Occup").first().attr("dblBed"));
			room.setAvgPrice			(roomElement.getElementsByTag("Occup").first().attr("avrNightPrice"));
			try {
				room.setBoardName			(roomElement.getElementsByTag("Board").first().attr("name"));
				room.setBoardPrice			(roomElement.getElementsByTag("Board").first().attr("price"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			room.setRoomNumber			(roomElement.getElementsByTag("Room").first().attr("seqNum"));
			room.setAdultCount			(roomElement.getElementsByTag("Room").first().attr("adultNum"));
			room.setChildCount			(roomElement.getElementsByTag("Room").first().attr("childNum"));
			room.setRoomPrice			(roomElement.getElementsByTag("Price").first().text());
			
			ArrayList<resGuestList>  	guestInfo 			= new ArrayList<resGuestList>();
			Iterator<Element> 			guestIterator		= roomElement.getElementsByTag("Child").iterator();
			int qwe = 0 ;
			while(guestIterator.hasNext())
			{
				Element					guestElement		= guestIterator.next();
				resGuestList 			guest				= new resGuestList();
				
				guest.setCustomerAge	(guestElement.getElementsByTag("Child").first().attr("age"));
				guestInfo.add(guest);
				qwe++;
			}
			room.setGuest(guestInfo);
			roomInfo.add(room);
		}
		hotel.setRoomAvailability(roomInfo);
		
		return hotel;
	
	
	}
	
	public canDeadRes inv13canDeadRes(String pageSource, StringBuffer reportPrinter)
	{
		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		canDeadRes 				res 			= new canDeadRes();
		res.setHotelId					(doc.getElementsByTag("HotelPolicy").first().attr("hotelId"));
		res.setCheckin					(doc.getElementsByTag("RoomTypePolicy").first().attr("CheckIn"));
		res.setCheckout					(doc.getElementsByTag("RoomTypePolicy").first().attr("CheckOut"));
		
		Iterator<Element> 	penaltyIterator = doc.getElementsByTag("CancelPenalty").iterator();
		ArrayList<penalty> 	penaltyList 	= new ArrayList<penalty>();
		while(penaltyIterator.hasNext())
		{
			Element penalty = penaltyIterator.next();
			penalty pen 	= new penalty();
			
			pen.setTimeunit				(doc.getElementsByTag("Deadline").attr("OffsetTimeUnit"));
			pen.setMultiplier			(doc.getElementsByTag("Deadline").attr("OffsetUnitMultiplier"));
			pen.setDropTime				(doc.getElementsByTag("Deadline").attr("OffsetDropTime"));
			pen.setNpNumOfNyts			(doc.getElementsByTag("AmountPercent").attr("NmbrOfNights"));
			pen.setBasisType			(doc.getElementsByTag("AmountPercent").attr("BasisType"));
			
			penaltyList.add(pen);
		}
		
		res.setPenalty(penaltyList);
		return res;
	}
	
	public resHotelAvailability inv13ReadFinalRes(String pageSource, StringBuffer ReportPrinter)
	{


		//System.out.println(pageSource.toString());
		Document 				doc  			= Jsoup.parse(pageSource, "UTF-8");
		//System.out.println(doc.getElementsByTag("ResGroup").first().attr("currency"));
		String error = "noError";
		try { 
			error = doc.getElementsByTag("faultstring").first().text();
		} catch (Exception e) {
			// TODO: handle exception
		}
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		if(error.equals("noError"))
		{
			Element					hotelInfo		= doc.getElementsByTag("TWS_RGInfo").first();
			
			try {
				hotel.setXmlError				(doc.getElementsByTag("faultcode").first().text());
				if(hotel.getXmlError().equals("Confirm reservation failed. RESERVE_ONLY_AVAIL_VIOLATED"))
				{
					ReportPrinter.append("<span>Final response xml error</span>");
					
					ReportPrinter.append("<table border=1 style=width:800px>"
						+ "<tr><th>Error</th></tr>");
					
					
					ReportPrinter.append	(	"<tr><td>" + hotel.getXmlError() + "</td></tr>");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				System.out.println(doc.getElementsByTag("ResGroup").first().attr("currency"));
				hotel.setFinalcurrency				(hotelInfo.getElementsByTag("ResGroup").first().attr("currency"));
				//System.out.println(hotel.getFinalcurrency());
				hotel.setFinalPrice					(hotelInfo.getElementsByTag("ResGroup").first().attr("totalPrice"));
				//System.out.println(hotel.getFinalPrice());
				
				ArrayList<resHotelDetails>  	reservationDetails 	= new ArrayList<resHotelDetails>();
				Iterator<Element> 		reservationIterator		= doc.getElementsByTag("Reservation").iterator();
				while(reservationIterator.hasNext())
				{
					Element						currentElement		= reservationIterator.next();
					resHotelDetails 			reservation			= new resHotelDetails();
					
					reservation.setReservationId				(currentElement.getElementsByTag("Reservation").first().attr("reservationID"));
					reservation.setFromDate						(currentElement.getElementsByTag("Reservation").first().attr("fromDate"));
					reservation.setToDate						(currentElement.getElementsByTag("Reservation").first().attr("toDate"));
					reservation.setTotalTax						(currentElement.getElementsByTag("Reservation").first().attr("totalTax"));
					reservation.setPrice						(currentElement.getElementsByTag("Reservation").first().attr("price"));
					System.out.println(reservation.getPrice());
					reservation.setCurrencyCode					(currentElement.getElementsByTag("Reservation").first().attr("currency"));
					reservation.setStatus						(currentElement.getElementsByTag("Reservation").first().attr("status"));
					reservation.setNumOfAdults					(currentElement.getElementsByTag("Reservation").first().attr("numOfAdults"));
					reservation.setNumOfChilds					(currentElement.getElementsByTag("Reservation").first().attr("numOfChildren"));
					
					reservation.setHotelid						(currentElement.getElementsByTag("HotelExtraInfo").first().attr("hotelID"));
					reservation.setHotelName					(currentElement.getElementsByTag("HotelExtraInfo").first().attr("name"));
					reservation.setAddress						(currentElement.getElementsByTag("HotelExtraInfo").first().attr("address"));
					reservation.setCategory						(currentElement.getElementsByTag("HotelExtraInfo").first().attr("category"));
					reservation.setStarLevel					(currentElement.getElementsByTag("HotelExtraInfo").first().attr("numOfStars"));
					reservation.setRoomType						(currentElement.getElementsByTag("HotelExtraInfo").first().attr("roomType"));
					reservation.setDoubleBed					(currentElement.getElementsByTag("HotelExtraInfo").first().attr("dblBed"));
					reservation.setFirstName					(currentElement.getElementsByTag("Passenger").first().attr("firstName"));
					reservation.setLastName						(currentElement.getElementsByTag("Passenger").first().attr("lastName"));

					
					reservationDetails.add(reservation);
				}
				hotel.setHotelDetails(reservationDetails);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		else
		{
			hotel.setXmlError(error);
			System.out.println(hotel.getXmlError());
		}

		Calendar cal = Calendar.getInstance();
    	cal.getTime();
    	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    	String time = sdf.format(cal.getTime());
    	hotel.setReferenceTime(time);
		
		return hotel;
	
	
	
	}
	
	public resHotelAvailability hbcancellationResponse(String resPageSource)
	{


		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		Element 				availability 	= doc.getElementsByTag("PurchaseCancelRS").first();
		
		try {
			hotel.setError				(availability.getElementsByTag("DetailedMessage").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			hotel.setProcessTime			(availability.getElementsByTag("ProcessTime").first().text());
			hotel.setTimeStamp				(availability.getElementsByTag("Timestamp").first().text());
			hotel.setRequestHost			(availability.getElementsByTag("RequestHost").first().text());
			hotel.setServerName				(availability.getElementsByTag("ServerName").first().text());
			hotel.setServerId				(availability.getElementsByTag("ServerId").first().text());
			hotel.setSchemaRelease			(availability.getElementsByTag("SchemaRelease").first().text());
			hotel.setHydraCodeRelease		(availability.getElementsByTag("HydraCoreRelease").first().text());
			hotel.setHydraERelease			(availability.getElementsByTag("HydraEnumerationsRelease").first().text());
			hotel.setMerlineRelease			(availability.getElementsByTag("MerlinRelease").first().text());
			hotel.setPurchaseToken			(availability.getElementsByTag("Purchase").first().attr("purchaseToken"));
			hotel.setTimeToExpiration		(availability.getElementsByTag("Purchase").first().attr("timeToExpiration"));
			hotel.setStatus					(availability.getElementsByTag("Status").first().text());
			hotel.setAgencyCode				(availability.getElementsByTag("Code").first().text());
			hotel.setAgencyBranch			(availability.getElementsByTag("Branch").first().text());
			hotel.setLanguage				(availability.getElementsByTag("Language").first().text());
			hotel.setCreationUser			(availability.getElementsByTag("CreationUser").first().text());
			hotel.setHoldername				(availability.getElementsByTag("Name").first().text());
			//System.out.println(hotel.getHoldername());
			hotel.setHolderlastName			(availability.getElementsByTag("LastName").first().text());
			//System.out.println(hotel.getHolderlastName());
			hotel.setHolderType				(availability.getElementsByTag("Holder").first().attr("type"));
			//System.out.println(hotel.getHolderType());
			
			Element				serviceElement		= doc.getElementsByTag("ServiceList").first(); 
			Element				hotelInfoElement	= doc.getElementsByTag("HotelInfo").first();
			hotel.setServiceType			(serviceElement.getElementsByTag("Service").first().attr("xsi:type"));
			hotel.setSpui					(serviceElement.getElementsByTag("Service").first().attr("SPUI"));
			hotel.setStatus					(serviceElement.getElementsByTag("Status").first().text());
			hotel.setContractName			(serviceElement.getElementsByTag("Name").first().text());
		
			hotel.setIncomingOfficeCode		(serviceElement.getElementsByTag("IncomingOffice").first().text());
			try {
				hotel.setComment			(serviceElement.getElementsByTag("Comment").first().text());
				hotel.setCommentType			(serviceElement.getElementsByTag("Comment").first().attr("type"));

			} catch (Exception e) {
				// TODO: handle exception
			}
			
			hotel.setSupplierName			(serviceElement.getElementsByTag("Supplier").first().attr("name"));
			hotel.setSupplierVatNumber		(serviceElement.getElementsByTag("Supplier").first().attr("vatNumber"));
			hotel.setDateFromDate			(serviceElement.getElementsByTag("DateFrom").first().attr("date"));
			hotel.setDateToDate				(serviceElement.getElementsByTag("DateTo").first().attr("date"));
			hotel.setCurrency				(serviceElement.getElementsByTag("Currency").first().text());
			hotel.setCurrencyCode			(serviceElement.getElementsByTag("Currency").first().attr("code"));
			hotel.setTotalAmount			(serviceElement.getElementsByTag("TotalAmount").first().text());
			
			ArrayList<resAdditionalCost>  	additionalCost 	= new ArrayList<resAdditionalCost>();
			Iterator<Element> additionalCostIterator		= doc.getElementsByTag("AdditionalCost").iterator();
			while(additionalCostIterator.hasNext())
			{
				Element	cost								= additionalCostIterator.next();
				resAdditionalCost 			addcost			= new resAdditionalCost();
				addcost.setAdditionalCostType	(cost.getElementsByTag("AdditionalCost").first().attr("type"));
				addcost.setPrice				(cost.getElementsByTag("Amount").first().text());
				additionalCost.add(addcost);
			}
			hotel.setAditionalCost(additionalCost);
			
			hotel.setModificationPolicyList		(serviceElement.getElementsByTag("ModificationPolicy").first().text());
			hotel.setHotelInfoType				(serviceElement.getElementsByTag("HotelInfo").first().attr("xsi:type"));
			hotel.setHotelInfoCode				(serviceElement.getElementsByTag("Code").first().text());
			hotel.setHotelInfoname				(hotelInfoElement.getElementsByTag("Name").first().text());
			hotel.setHotelInfoCategory			(serviceElement.getElementsByTag("Category").first().text());
			hotel.setDestinationCode			(serviceElement.getElementsByTag("Destination").first().attr("code"));
			Element destinationElement			= serviceElement.getElementsByTag("Destination").first();
			hotel.setDestinationName			(destinationElement.getElementsByTag("Name").first().text());
			hotel.setDestinationType			(serviceElement.getElementsByTag("Destination").first().attr("type"));
			hotel.setZoneCode					(serviceElement.getElementsByTag("Zone").first().attr("code"));
			hotel.setZoneType					(serviceElement.getElementsByTag("Zone").first().attr("type"));
			
			ArrayList<resavailableRoom>  	availableRoom 	= new ArrayList<resavailableRoom>();
			Iterator<Element> 				avaialableRoomIterator		= doc.getElementsByTag("HotelOccupancy").iterator();
			//System.out.println(doc.getElementsByTag("HotelOccupancy").size());
			while(avaialableRoomIterator.hasNext())
			{
				Element						currentElement	= avaialableRoomIterator.next();
				resavailableRoom			room			= new resavailableRoom();
				room.setAdultCount				(currentElement.getElementsByTag("AdultCount").first().text());
				room.setChildCount				(currentElement.getElementsByTag("ChildCount").first().text());
				room.setRoomCount				(currentElement.getElementsByTag("RoomCount").first().text());
				
				
				ArrayList<resGuestList>  			guestList		= new ArrayList<resGuestList>();
				Iterator<Element> 				guestIterator	= currentElement.getElementsByTag("Customer").iterator();
				while(guestIterator.hasNext())
				{
												currentElement	= guestIterator.next();
					resGuestList					guest			= new resGuestList();
					
					guest.setCustomerAge		(currentElement.getElementsByTag("Age").first().text());
					//System.out.println(guest.getCustomerAge());
					guest.setCustomerid			(currentElement.getElementsByTag("CustomerId").first().text());
					//System.out.println(guest.getCustomerid());
					guest.setCustomerType		(currentElement.getElementsByTag("Customer").first().attr("type"));
					//System.out.println(guest.getCustomerType());
					guest.setName				(currentElement.getElementsByTag("Name").first().text());
					guest.setLastName			(currentElement.getElementsByTag("LastName").first().text());
					
					guestList.add(guest);
				}
				room.setGuest(guestList);
				
				availableRoom.add(room);
			}
			
			hotel.setRoomAvailability(availableRoom);
			
			
			ArrayList<resHotelRoom>  	roomAvailability 	= new ArrayList<resHotelRoom>();
			Iterator<Element> 				roomAvailabilityIterator		= doc.getElementsByTag("HotelRoom").iterator();
			while(roomAvailabilityIterator.hasNext())
			{

				Element						currentElement	= roomAvailabilityIterator.next();
				resavailableRoom			room			= new resavailableRoom();
				resHotelRoom				hotelRoom 		= new resHotelRoom();
				
				
				hotelRoom.setShrui				(currentElement.getElementsByTag("HotelRoom").first().attr("SHRUI"));
				hotelRoom.setAvailableCount		(currentElement.getElementsByTag("HotelRoom").first().attr("availCount"));
				hotelRoom.setStatus				(currentElement.getElementsByTag("HotelRoom").first().attr("status"));
				hotelRoom.setBoard				(currentElement.getElementsByTag("Board").first().text());
				hotelRoom.setBoardCode			(currentElement.getElementsByTag("Board").first().attr("code"));
				hotelRoom.setBoardType			(currentElement.getElementsByTag("Board").first().attr("type"));
				hotelRoom.setRoomType			(currentElement.getElementsByTag("RoomType").first().attr("type"));
				hotelRoom.setRoomCode			(currentElement.getElementsByTag("RoomType").first().attr("code"));
				hotelRoom.setCharacteristic		(currentElement.getElementsByTag("RoomType").first().attr("characteristic"));
				hotelRoom.setRoom				(currentElement.getElementsByTag("RoomType").first().text());
				hotelRoom.setPrice				(currentElement.getElementsByTag("Amount").first().text());
				try {
					Element cancelElement			= currentElement.getElementsByTag("CancellationPolicy").first();
					hotelRoom.setCancellationCodet	(cancelElement.getElementsByTag("Amount").first().text());
					hotelRoom.setDateTimeFromDate	(cancelElement.getElementsByTag("DateTimeFrom").first().attr("date"));
					//System.out.println(hotelRoom.getDateTimeFromDate());
					hotelRoom.setDateTimeToDate		(cancelElement.getElementsByTag("DateTimeTo").first().attr("date"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				roomAvailability.add(hotelRoom);
			}
				hotel.setHotelRoom(roomAvailability);	

		} catch (Exception e) {
			// TODO: handle exception
		}
					//System.out.println(roomAvailability.get(0).getCancellationCodet());
//			
//			System.out.println(hotel.getAgencyCode());
//			System.out.println(hotel.getHotelRoom().get(0).getPrice());
			return hotel;
	
	
	}
	
	public resHotelAvailability readFinalResponse(String resPageSource)
	{

		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		Element 				availability 	= doc.getElementsByTag("PurchaseConfirmRS").first();
		
		try {
			hotel.setError				(availability.getElementsByTag("DetailedMessage").first().text());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			hotel.setProcessTime			(availability.getElementsByTag("ProcessTime").first().text());
			hotel.setTimeStamp				(availability.getElementsByTag("Timestamp").first().text());
			hotel.setRequestHost			(availability.getElementsByTag("RequestHost").first().text());
			hotel.setServerName				(availability.getElementsByTag("ServerName").first().text());
			hotel.setServerId				(availability.getElementsByTag("ServerId").first().text());
			hotel.setSchemaRelease			(availability.getElementsByTag("SchemaRelease").first().text());
			hotel.setHydraCodeRelease		(availability.getElementsByTag("HydraCoreRelease").first().text());
			hotel.setHydraERelease			(availability.getElementsByTag("HydraEnumerationsRelease").first().text());
			hotel.setMerlineRelease			(availability.getElementsByTag("MerlinRelease").first().text());
			hotel.setPurchaseToken			(availability.getElementsByTag("Purchase").first().attr("purchaseToken"));
			hotel.setTimeToExpiration		(availability.getElementsByTag("Purchase").first().attr("timeToExpiration"));
			hotel.setStatus					(availability.getElementsByTag("Status").first().text());
			hotel.setAgencyCode				(availability.getElementsByTag("Code").first().text());
			hotel.setAgencyBranch			(availability.getElementsByTag("Branch").first().text());
			hotel.setLanguage				(availability.getElementsByTag("Language").first().text());
			hotel.setCreationUser			(availability.getElementsByTag("CreationUser").first().text());
			hotel.setHoldername				(availability.getElementsByTag("Name").first().text());
			//System.out.println(hotel.getHoldername());
			hotel.setHolderlastName			(availability.getElementsByTag("LastName").first().text());
			//System.out.println(hotel.getHolderlastName());
			hotel.setHolderType				(availability.getElementsByTag("Holder").first().attr("type"));
			//System.out.println(hotel.getHolderType());
			
			Element				serviceElement		= doc.getElementsByTag("ServiceList").first(); 
			Element				hotelInfoElement	= doc.getElementsByTag("HotelInfo").first();
			hotel.setServiceType			(serviceElement.getElementsByTag("Service").first().attr("xsi:type"));
			hotel.setSpui					(serviceElement.getElementsByTag("Service").first().attr("SPUI"));
			hotel.setStatus					(serviceElement.getElementsByTag("Status").first().text());
			hotel.setContractName			(serviceElement.getElementsByTag("Name").first().text());
		
			hotel.setIncomingOfficeCode		(serviceElement.getElementsByTag("IncomingOffice").first().text());
			try {
				hotel.setComment			(serviceElement.getElementsByTag("Comment").first().text());
				hotel.setCommentType			(serviceElement.getElementsByTag("Comment").first().attr("type"));

			} catch (Exception e) {
				// TODO: handle exception
			}
			
			hotel.setSupplierName			(serviceElement.getElementsByTag("Supplier").first().attr("name"));
			hotel.setSupplierVatNumber		(serviceElement.getElementsByTag("Supplier").first().attr("vatNumber"));
			hotel.setDateFromDate			(serviceElement.getElementsByTag("DateFrom").first().attr("date"));
			hotel.setDateToDate				(serviceElement.getElementsByTag("DateTo").first().attr("date"));
			hotel.setCurrency				(serviceElement.getElementsByTag("Currency").first().text());
			hotel.setCurrencyCode			(serviceElement.getElementsByTag("Currency").first().attr("code"));
			hotel.setTotalAmount			(serviceElement.getElementsByTag("TotalAmount").first().text());
			
			ArrayList<resAdditionalCost>  	additionalCost 	= new ArrayList<resAdditionalCost>();
			Iterator<Element> additionalCostIterator		= doc.getElementsByTag("AdditionalCost").iterator();
			while(additionalCostIterator.hasNext())
			{
				Element	cost								= additionalCostIterator.next();
				resAdditionalCost 			addcost			= new resAdditionalCost();
				addcost.setAdditionalCostType	(cost.getElementsByTag("AdditionalCost").first().attr("type"));
				addcost.setPrice				(cost.getElementsByTag("Amount").first().text());
				additionalCost.add(addcost);
			}
			hotel.setAditionalCost(additionalCost);
			
			hotel.setModificationPolicyList		(serviceElement.getElementsByTag("ModificationPolicy").first().text());
			hotel.setHotelInfoType				(serviceElement.getElementsByTag("HotelInfo").first().attr("xsi:type"));
			hotel.setHotelInfoCode				(serviceElement.getElementsByTag("Code").first().text());
			hotel.setHotelInfoname				(hotelInfoElement.getElementsByTag("Name").first().text());
			hotel.setHotelInfoCategory			(serviceElement.getElementsByTag("Category").first().text());
			hotel.setDestinationCode			(serviceElement.getElementsByTag("Destination").first().attr("code"));
			Element destinationElement			= serviceElement.getElementsByTag("Destination").first();
			hotel.setDestinationName			(destinationElement.getElementsByTag("Name").first().text());
			hotel.setDestinationType			(serviceElement.getElementsByTag("Destination").first().attr("type"));
			hotel.setZoneCode					(serviceElement.getElementsByTag("Zone").first().attr("code"));
			hotel.setZoneType					(serviceElement.getElementsByTag("Zone").first().attr("type"));
			
			ArrayList<resavailableRoom>  	availableRoom 	= new ArrayList<resavailableRoom>();
			Iterator<Element> 				avaialableRoomIterator		= doc.getElementsByTag("HotelOccupancy").iterator();
			//System.out.println(doc.getElementsByTag("HotelOccupancy").size());
			while(avaialableRoomIterator.hasNext())
			{
				Element						currentElement	= avaialableRoomIterator.next();
				resavailableRoom			room			= new resavailableRoom();
				room.setAdultCount				(currentElement.getElementsByTag("AdultCount").first().text());
				room.setChildCount				(currentElement.getElementsByTag("ChildCount").first().text());
				room.setRoomCount				(currentElement.getElementsByTag("RoomCount").first().text());
				
				
				ArrayList<resGuestList>  			guestList		= new ArrayList<resGuestList>();
				Iterator<Element> 				guestIterator	= currentElement.getElementsByTag("Customer").iterator();
				while(guestIterator.hasNext())
				{
												currentElement	= guestIterator.next();
					resGuestList					guest			= new resGuestList();
					
					guest.setCustomerAge		(currentElement.getElementsByTag("Age").first().text());
					//System.out.println(guest.getCustomerAge());
					guest.setCustomerid			(currentElement.getElementsByTag("CustomerId").first().text());
					//System.out.println(guest.getCustomerid());
					guest.setCustomerType		(currentElement.getElementsByTag("Customer").first().attr("type"));
					//System.out.println(guest.getCustomerType());
					guest.setName				(currentElement.getElementsByTag("Name").first().text());
					guest.setLastName			(currentElement.getElementsByTag("LastName").first().text());
					
					guestList.add(guest);
				}
				room.setGuest(guestList);
				
				availableRoom.add(room);
			}
			
			hotel.setRoomAvailability(availableRoom);
			
			
			ArrayList<resHotelRoom>  	roomAvailability 	= new ArrayList<resHotelRoom>();
			Iterator<Element> 				roomAvailabilityIterator		= doc.getElementsByTag("HotelRoom").iterator();
			while(roomAvailabilityIterator.hasNext())
			{

				Element						currentElement	= roomAvailabilityIterator.next();
				resavailableRoom			room			= new resavailableRoom();
				resHotelRoom				hotelRoom 		= new resHotelRoom();
				
				
				hotelRoom.setShrui				(currentElement.getElementsByTag("HotelRoom").first().attr("SHRUI"));
				hotelRoom.setAvailableCount		(currentElement.getElementsByTag("HotelRoom").first().attr("availCount"));
				hotelRoom.setStatus				(currentElement.getElementsByTag("HotelRoom").first().attr("status"));
				hotelRoom.setBoard				(currentElement.getElementsByTag("Board").first().text());
				hotelRoom.setBoardCode			(currentElement.getElementsByTag("Board").first().attr("code"));
				hotelRoom.setBoardType			(currentElement.getElementsByTag("Board").first().attr("type"));
				hotelRoom.setRoomType			(currentElement.getElementsByTag("RoomType").first().attr("type"));
				hotelRoom.setRoomCode			(currentElement.getElementsByTag("RoomType").first().attr("code"));
				hotelRoom.setCharacteristic		(currentElement.getElementsByTag("RoomType").first().attr("characteristic"));
				hotelRoom.setRoom				(currentElement.getElementsByTag("RoomType").first().text());
				hotelRoom.setPrice				(currentElement.getElementsByTag("Amount").first().text());
				try {
					Element cancelElement			= currentElement.getElementsByTag("CancellationPolicy").first();
					hotelRoom.setCancellationCodet	(cancelElement.getElementsByTag("Amount").first().text());
					hotelRoom.setDateTimeFromDate	(cancelElement.getElementsByTag("DateTimeFrom").first().attr("date"));
					//System.out.println(hotelRoom.getDateTimeFromDate());
					hotelRoom.setDateTimeToDate		(cancelElement.getElementsByTag("DateTimeTo").first().attr("date"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				roomAvailability.add(hotelRoom);
			}
				hotel.setHotelRoom(roomAvailability);	

		} catch (Exception e) {
			// TODO: handle exception
		}
					//System.out.println(roomAvailability.get(0).getCancellationCodet());
//			
//			System.out.println(hotel.getAgencyCode());
//			System.out.println(hotel.getHotelRoom().get(0).getPrice());
			return hotel;
	
	}
	
	public resHotelAvailability inv27RoomAvailabilityRes(String resPageSource)
	{
		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		
		hotel.setDestinationName				(doc.getElementsByTag("City").first().text());
		hotel.setDestinationCode				(doc.getElementsByTag("City").first().attr("Code"));
		hotel.setItemCode						(doc.getElementsByTag("Item").first().attr("Code"));
		hotel.setItemName						(doc.getElementsByTag("Item").first().text());
		Iterator<Element> 			inv27Iterator 	= doc.getElementsByTag("HotelRoom").iterator();
		ArrayList<inv27rooms>     	inv27RoomList 	= new ArrayList<inv27rooms>();
		while(inv27Iterator.hasNext())
		{
			Element currenteElement = inv27Iterator.next();
			inv27rooms room = new inv27rooms();
			room.setRoomCode					(currenteElement.getElementsByTag("HotelRoom").first().attr("Code"));
			room.setNumOfRooms					(currenteElement.getElementsByTag("HotelRoom").first().attr("NumberOfRooms"));
			try {
				room.setExtraBed				(currenteElement.getElementsByTag("HotelRoom").first().attr("ExtraBed"));
				room.setNumOfExtraBeds			(currenteElement.getElementsByTag("HotelRoom").first().attr("NumberOfExtraBeds"));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			inv27RoomList.add(room);
		}
		hotel.setInv27Room(inv27RoomList);
		hotel.setDesdription					(doc.getElementsByTag("Description").first().text());
		hotel.setPrice2							(doc.getElementsByTag("ItemPrice ").first().text());
		hotel.setCurrencyCode					(doc.getElementsByTag("ItemPrice").first().attr("Currency"));
		hotel.setConfirmation					(doc.getElementsByTag("Confirmation").first().text());
		
		Element hotelRoomPrices = doc.getElementsByTag("HotelRoomPrices").first();
		Iterator<Element> roomIterator = hotelRoomPrices.getElementsByTag("HotelRoom ").iterator();
		ArrayList<resHotelRoom> roomList = new ArrayList<resHotelRoom>();
		while(roomIterator.hasNext())
		{
			Element currentElement = roomIterator.next();
			resHotelRoom room = new resHotelRoom();
			room.setRoomCode					(currentElement.getElementsByTag("HotelRoom").first().attr("Code"));
			room.setPrice						(currentElement.getElementsByTag("RoomPrice").first().attr("Gross"));
			room.setDateTimeFromDate			(currentElement.getElementsByTag("FromDate").first().text());
			room.setDateTimeToDate				(currentElement.getElementsByTag("ToDate").first().text());
			try {
				room.setNumOfRooms					(currentElement.getElementsByTag("HotelRoom").first().attr("NumberOfRooms"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			room.setNights						(currentElement.getElementsByTag("Price").first().attr("Nights"));
			roomList.add(room);
		}
		hotel.setHotelRoom(roomList);
		
		Iterator<Element> cancelationIterator = doc.getElementsByTag("Condition").iterator();
		ArrayList<inv27Cancellation> cancellationlist = new ArrayList<inv27Cancellation>();
		while(cancelationIterator.hasNext())
		{
			Element currentElement = cancelationIterator.next();
			inv27Cancellation can = new inv27Cancellation();
			
			//can.setType							(currentElement.getElementsByTag("ChargeCondition").first().attr("Type"));
			can.setCharge						(currentElement.getElementsByTag("Condition").first().attr("Charge"));
			try {
				can.setAmount						(currentElement.getElementsByTag("Condition").first().attr("ChargeAmount"));
				can.setCurrency						(currentElement.getElementsByTag("Condition").first().attr("Currency"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			can.setFromdate						(currentElement.getElementsByTag("Condition").first().attr("ToDate"));
			can.setTodate						(currentElement.getElementsByTag("Condition").first().attr("FromDate"));
			cancellationlist.add(can);
		}
		hotel.setCancel(cancellationlist);
		return hotel;
	}
	
	public resHotelAvailability inv27PreRes(String resPageSource)
	{

		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		
		hotel.setDestinationName				(doc.getElementsByTag("City").first().text());
		hotel.setDestinationCode				(doc.getElementsByTag("City").first().attr("Code"));
		
		Iterator<Element> 			inv27Iterator 	= doc.getElementsByTag("HotelRoom").iterator();
		ArrayList<inv27rooms>     	inv27RoomList 	= new ArrayList<inv27rooms>();
		while(inv27Iterator.hasNext())
		{
			Element currenteElement = inv27Iterator.next();
			inv27rooms room = new inv27rooms();
			room.setRoomCode					(currenteElement.getElementsByTag("HotelRoom").first().attr("Code"));
			room.setNumOfRooms					(currenteElement.getElementsByTag("HotelRoom").first().attr("NumberOfRooms"));
			try {
				room.setExtraBed				(currenteElement.getElementsByTag("HotelRoom").first().attr("ExtraBed"));
				room.setNumOfExtraBeds			(currenteElement.getElementsByTag("HotelRoom").first().attr("NumberOfExtraBeds"));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			inv27RoomList.add(room);
		}
		hotel.setInv27Room(inv27RoomList);
		hotel.setDesdription					(doc.getElementsByTag("Description").first().text());
		hotel.setPrice2							(doc.getElementsByTag("ItemPrice ").first().text());
		hotel.setCurrencyCode					(doc.getElementsByTag("ItemPrice").first().attr("Currency"));
		hotel.setConfirmation					(doc.getElementsByTag("Confirmation").first().text());
		
		Element hotelRoomPrices = doc.getElementsByTag("HotelRoomPrices").first();
		Iterator<Element> roomIterator = hotelRoomPrices.getElementsByTag("HotelRoom ").iterator();
		ArrayList<resHotelRoom> roomList = new ArrayList<resHotelRoom>();
		while(roomIterator.hasNext())
		{
			Element currentElement = roomIterator.next();
			resHotelRoom room = new resHotelRoom();
			room.setRoomCode					(currentElement.getElementsByTag("HotelRoom").first().attr("Code"));
			room.setPrice						(currentElement.getElementsByTag("RoomPrice").first().attr("Gross"));
			room.setDateTimeFromDate			(currentElement.getElementsByTag("FromDate").first().text());
			room.setDateTimeToDate				(currentElement.getElementsByTag("ToDate").first().text());
			try {
				room.setNumOfRooms					(currentElement.getElementsByTag("HotelRoom").first().attr("NumberOfRooms"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			room.setNights						(currentElement.getElementsByTag("Price").first().attr("Nights"));
			roomList.add(room);
		}
		hotel.setHotelRoom(roomList);
		
		Iterator<Element> cancelationIterator = doc.getElementsByTag("ChargeCondition").iterator();
		ArrayList<inv27Cancellation> cancellationlist = new ArrayList<inv27Cancellation>();
		while(cancelationIterator.hasNext())
		{
			Element currentElement = cancelationIterator.next();
			inv27Cancellation can = new inv27Cancellation();
			
			can.setType							(currentElement.getElementsByTag("ChargeCondition").first().attr("Type"));
			can.setCharge						(currentElement.getElementsByTag("Condition ").first().attr("Charge"));
			try {
				can.setAmount						(currentElement.getElementsByTag("Condition").first().attr("ChargeAmount"));
				can.setCurrency						(currentElement.getElementsByTag("Condition").first().attr("Currency"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			can.setFromdate						(currentElement.getElementsByTag("Condition").first().attr("FromDate"));
			
			cancellationlist.add(can);
		}
		hotel.setCancel(cancellationlist);
		return hotel;
	
	}
	
	public resHotelAvailability readResponse(String resPageSource)
	{
		//System.out.println("test -- 3.0.1");
		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		Element 				availability	= doc.getElementsByTag("ServiceAddRS").first();
		//System.out.println("test -- 3.0.2");
		try {
			try {
				hotel.setTotalItems				(doc.getElementsByTag("HotelValuedAvailRS").first().attr("totalItems"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				hotel.setError					(doc.getElementsByTag("DetailedMessage").first().text());
				//System.out.println(hotel.getError());
			} catch (Exception e) {
				// TODO: handle exception
			}
			//System.out.println("test -- 3.0.3");
			hotel.setProcessTime			(availability.getElementsByTag("ProcessTime").first().text());
			hotel.setTimeStamp				(availability.getElementsByTag("Timestamp").first().text());
			hotel.setRequestHost			(availability.getElementsByTag("RequestHost").first().text());
			hotel.setServerName				(availability.getElementsByTag("ServerName").first().text());
			hotel.setServerId				(availability.getElementsByTag("ServerId").first().text());
			hotel.setSchemaRelease			(availability.getElementsByTag("SchemaRelease").first().text());
			hotel.setHydraCodeRelease		(availability.getElementsByTag("HydraCoreRelease").first().text());
			hotel.setHydraERelease			(availability.getElementsByTag("HydraEnumerationsRelease").first().text());
			hotel.setMerlineRelease			(availability.getElementsByTag("MerlinRelease").first().text());
			hotel.setPurchaseToken			(availability.getElementsByTag("Purchase").first().attr("purchaseToken"));
			hotel.setTimeToExpiration		(availability.getElementsByTag("Purchase").first().attr("timeToExpiration"));
			hotel.setStatus					(availability.getElementsByTag("Status").first().text());
			hotel.setAgencyCode				(availability.getElementsByTag("Code").first().text());
			hotel.setAgencyBranch			(availability.getElementsByTag("Branch").first().text());
			hotel.setLanguage				(availability.getElementsByTag("Language").first().text());
			hotel.setCreationUser			(availability.getElementsByTag("CreationUser").first().text());
			//System.out.println("test -- 3.0.4");
			
			Element				serviceElement		= doc.getElementsByTag("ServiceList").first(); 
			Element				hotelInfoElement	= doc.getElementsByTag("HotelInfo").first();
			hotel.setServiceType			(serviceElement.getElementsByTag("Service").first().attr("xsi:type"));
			hotel.setSpui					(serviceElement.getElementsByTag("Service").first().attr("SPUI"));
			hotel.setStatus					(serviceElement.getElementsByTag("Status").first().text());
			hotel.setContractName			(serviceElement.getElementsByTag("Name").first().text());
			//System.out.println("test -- 3.0.5");
			hotel.setIncomingOfficeCode		(serviceElement.getElementsByTag("IncomingOffice").first().text());
			try {
				hotel.setComment			(serviceElement.getElementsByTag("Comment").first().text());
				hotel.setCommentType			(serviceElement.getElementsByTag("Comment").first().attr("type"));

			} catch (Exception e) {
				// TODO: handle exception
			}
			//System.out.println("test -- 3.0.6");
			try {
				hotel.setSupplierName			(serviceElement.getElementsByTag("Supplier").first().attr("name"));
				hotel.setSupplierVatNumber		(serviceElement.getElementsByTag("Supplier").first().attr("vatNumber"));
				hotel.setDateFromDate			(serviceElement.getElementsByTag("DateFrom").first().attr("date"));
				hotel.setDateToDate				(serviceElement.getElementsByTag("DateTo").first().attr("date"));
				hotel.setCurrency				(serviceElement.getElementsByTag("Currency").first().text());
				hotel.setCurrencyCode			(serviceElement.getElementsByTag("Currency").first().attr("code"));
				hotel.setTotalAmount			(serviceElement.getElementsByTag("TotalAmount").first().text());
				System.out.println(hotel.getTotalAmount());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//System.out.println("test -- 3.0.7");
			ArrayList<resAdditionalCost>  	additionalCost 	= new ArrayList<resAdditionalCost>();
			Iterator<Element> additionalCostIterator		= doc.getElementsByTag("AdditionalCost").iterator();
			while(additionalCostIterator.hasNext())
			{
				Element	cost								= additionalCostIterator.next();
				resAdditionalCost 			addcost			= new resAdditionalCost();
				addcost.setAdditionalCostType	(cost.getElementsByTag("AdditionalCost").first().attr("type"));
				addcost.setPrice				(cost.getElementsByTag("Amount").first().text());
				additionalCost.add(addcost);
			}
			hotel.setAditionalCost(additionalCost);
			//System.out.println("test -- 3.0.8");
			try {
				hotel.setModificationPolicyList		(serviceElement.getElementsByTag("ModificationPolicy").first().text());
				hotel.setHotelInfoType				(serviceElement.getElementsByTag("HotelInfo").first().attr("xsi:type"));
				hotel.setHotelInfoCode				(serviceElement.getElementsByTag("Code").first().text());
				hotel.setHotelInfoname				(hotelInfoElement.getElementsByTag("Name").first().text());
				hotel.setHotelInfoCategory			(serviceElement.getElementsByTag("Category").first().text());
				hotel.setDestinationCode			(serviceElement.getElementsByTag("Destination").first().attr("code"));
				hotel.setDestinationName			(serviceElement.getElementsByTag("Name").first().text());
				hotel.setDestinationType			(serviceElement.getElementsByTag("Destination").first().attr("type"));
				hotel.setZoneCode					(serviceElement.getElementsByTag("Zone").first().attr("code"));
				hotel.setZoneType					(serviceElement.getElementsByTag("Zone").first().attr("type"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			//System.out.println("test -- 3.0.9");
			ArrayList<resavailableRoom>  	availableRoom 	= new ArrayList<resavailableRoom>();
			Iterator<Element> 				avaialableRoomIterator		= doc.getElementsByTag("HotelOccupancy").iterator();
			//System.out.println(doc.getElementsByTag("HotelOccupancy").size());
			while(avaialableRoomIterator.hasNext())
			{
				Element						currentElement	= avaialableRoomIterator.next();
				resavailableRoom			room			= new resavailableRoom();
				room.setAdultCount				(currentElement.getElementsByTag("AdultCount").first().text());
				room.setChildCount				(currentElement.getElementsByTag("ChildCount").first().text());
				room.setRoomCount				(currentElement.getElementsByTag("RoomCount").first().text());
				
				
				ArrayList<resGuestList>  			guestList		= new ArrayList<resGuestList>();
				Iterator<Element> 				guestIterator	= currentElement.getElementsByTag("Customer").iterator();
				while(guestIterator.hasNext())
				{
												currentElement	= guestIterator.next();
					resGuestList					guest			= new resGuestList();
					
					guest.setCustomerAge		(currentElement.getElementsByTag("Age").first().text());
					//System.out.println(guest.getCustomerAge());
					guest.setCustomerid			(currentElement.getElementsByTag("CustomerId").first().text());
					//System.out.println(guest.getCustomerid());
					guest.setCustomerType		(currentElement.getElementsByTag("Customer").first().attr("type"));
					//System.out.println(guest.getCustomerType());
					
					guestList.add(guest);
				}
				room.setGuest(guestList);
				
				availableRoom.add(room);
			}
			//System.out.println("test -- 3.0.10");
			hotel.setRoomAvailability(availableRoom);
			
			
			ArrayList<resHotelRoom>  	roomAvailability 	= new ArrayList<resHotelRoom>();
			Iterator<Element> 				roomAvailabilityIterator		= doc.getElementsByTag("HotelRoom").iterator();
			while(roomAvailabilityIterator.hasNext())
			{

				Element						currentElement	= roomAvailabilityIterator.next();
				resavailableRoom			room			= new resavailableRoom();
				resHotelRoom				hotelRoom 		= new resHotelRoom();
				
				
				hotelRoom.setShrui				(currentElement.getElementsByTag("HotelRoom").first().attr("SHRUI"));
				hotelRoom.setAvailableCount		(currentElement.getElementsByTag("HotelRoom").first().attr("availCount"));
				hotelRoom.setStatus				(currentElement.getElementsByTag("HotelRoom").first().attr("status"));
				hotelRoom.setBoard				(currentElement.getElementsByTag("Board").first().text());
				hotelRoom.setBoardCode			(currentElement.getElementsByTag("Board").first().attr("code"));
				hotelRoom.setBoardType			(currentElement.getElementsByTag("Board").first().attr("type"));
				hotelRoom.setRoomType			(currentElement.getElementsByTag("RoomType").first().attr("type"));
				hotelRoom.setRoomCode			(currentElement.getElementsByTag("RoomType").first().attr("code"));
				hotelRoom.setCharacteristic		(currentElement.getElementsByTag("RoomType").first().attr("characteristic"));
				hotelRoom.setRoom				(currentElement.getElementsByTag("RoomType").first().text());
				hotelRoom.setPrice				(currentElement.getElementsByTag("Amount").first().text());
				//System.out.println(hotelRoom.getPrice());
				
				Element cancelElement			= currentElement.getElementsByTag("CancellationPolicy").first();
				hotelRoom.setCancellationCodet	(cancelElement.getElementsByTag("Amount").first().text());
				hotelRoom.setDateTimeFromDate	(cancelElement.getElementsByTag("DateTimeFrom").first().attr("date"));
				//System.out.println(hotelRoom.getDateTimeFromDate());
				hotelRoom.setDateTimeToDate		(cancelElement.getElementsByTag("DateTimeTo").first().attr("date"));
				roomAvailability.add(hotelRoom);
			}
			//System.out.println("test -- 3.0.11");
				hotel.setHotelRoom(roomAvailability);	
				//System.out.println(roomAvailability.get(0).getCancellationCodet());
//				
//				System.out.println(hotel.getAgencyCode());
//				System.out.println(hotel.getHotelRoom().get(0).getPrice());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
			return hotel;
	}
	
	public hotelAvailability finalRequest(String pageSource)
	{

		Document doc  				= Jsoup.parse(pageSource, "UTF-8");
		hotelAvailability	hotel	= new hotelAvailability(); 
		//Element availability		= doc.getElementsByTag("PurchaseConfirmRQ").first();
		
		hotel.setUser				(doc.getElementsByTag("User").first().text());
		hotel.setPassword			(doc.getElementsByTag("Password").first().text());
		hotel.setConfirmationDatePurchaseToken	(doc.getElementsByTag("ConfirmationData").first().attr("purchaseToken"));
		hotel.setHolderType			(doc.getElementsByTag("Holder ").first().attr("type"));
		hotel.setHolderName			(doc.getElementsByTag("Name").first().text());
		hotel.setLastName			(doc.getElementsByTag("LastName").first().text());
		hotel.setAgencyReference	(doc.getElementsByTag("AgencyReference").first().text());
		
		
		ArrayList<availableRoom>  	roomAvailability = new ArrayList<availableRoom>();
		//Element confirmationElement			= doc.getElementsByTag("ConfirmationServiceDataList").first();
		Iterator<Element> roomIterator		= doc.getElementsByTag("ConfirmationServiceDataList").iterator();
		while(roomIterator.hasNext())
		{
			Element	room					= roomIterator.next();
			availableRoom			aRoom	= new availableRoom();
			
			//aRoom.setRoomCount			(room.getElementsByTag("RoomCount").first().text());
			aRoom.setServiceDateType		(room.getElementsByTag("ServiceData").first().attr("xsi:type"));
			aRoom.setSpui					(room.getElementsByTag("ServiceData ").first().attr("SPUI"));
			
			ArrayList<guestList> guestList		= new ArrayList<guestList>();
			Iterator<Element> guestIterator 	= room.getElementsByTag("Customer").iterator();
			while(guestIterator.hasNext())
			{
				Element				guest		= guestIterator.next();
				guestList			list		= new  guestList();
				
				list.setCustomerAge			(guest.getElementsByTag("Age").first().text());
				list.setCustomerType		(guest.getElementsByTag("Customer").first().attr("type"));
				list.setCustoemrId			(guest.getElementsByTag("CustomerId").first().text());
				list.setName				(guest.getElementsByTag("Name").first().text());
				list.setLastName			(guest.getElementsByTag("LastName").first().text());
				guestList.add(list);
			}
			aRoom.setGuestList(guestList);
			//System.out.println(aRoom.getGuestList().get(0).getCustomerAge());
			roomAvailability.add(aRoom);
			
		}
		
		hotel.setRoomAvailability(roomAvailability);
		

		return hotel;
		
	
	}
	
	public resHotelAvailability finalResponse(String resPageSource)
	{

		Document 				doc  			= Jsoup.parse(resPageSource, "UTF-8");
		resHotelAvailability	hotel			= new resHotelAvailability(); 
		Element 				availability	= doc.getElementsByTag("PurchaseConfirmRS").first();
		
		hotel.setProcessTime			(availability.getElementsByTag("ProcessTime").first().text());
		hotel.setTimeStamp				(availability.getElementsByTag("Timestamp").first().text());
		hotel.setRequestHost			(availability.getElementsByTag("RequestHost").first().text());
		hotel.setServerName				(availability.getElementsByTag("ServerName").first().text());
		hotel.setServerId				(availability.getElementsByTag("ServerId").first().text());
		hotel.setSchemaRelease			(availability.getElementsByTag("SchemaRelease").first().text());
		hotel.setHydraCodeRelease		(availability.getElementsByTag("HydraCoreRelease").first().text());
		hotel.setHydraERelease			(availability.getElementsByTag("HydraEnumerationsRelease").first().text());
		hotel.setMerlineRelease			(availability.getElementsByTag("MerlinRelease").first().text());
		hotel.setPurchaseToken			(availability.getElementsByTag("Purchase").first().attr("purchaseToken"));
		hotel.setTimeToExpiration		(availability.getElementsByTag("Purchase").first().attr("timeToExpiration"));
		hotel.setReferenceFileNumber	(availability.getElementsByTag("FileNumber").first().text());
		hotel.setReferenceincomingofficeCode	(availability.getElementsByTag("IncomingOffice ").first().text());
		hotel.setStatus					(availability.getElementsByTag("Status ").first().text());
		hotel.setAgencyCode				(availability.getElementsByTag("Code ").first().text());
		hotel.setAgencyBranch			(availability.getElementsByTag("Branch ").first().text());
		hotel.setCreationdate			(availability.getElementsByTag("CreationDate").first().text());
		hotel.setCreationUser			(availability.getElementsByTag("CreationUser").first().text());
		hotel.setHolderType				(availability.getElementsByTag("Holder ").first().attr("type"));
		hotel.setHolderAge				(availability.getElementsByTag("Age").first().text());
		hotel.setHolderlastName			(availability.getElementsByTag("Name").first().text());
		hotel.setHolderlastName			(availability.getElementsByTag("LastName").first().text());

		
		
		Element				serviceElement		= doc.getElementsByTag("ServiceList").first(); 
		hotel.setServiceType			(serviceElement.getElementsByTag("Service").first().attr("xsi:type"));
		hotel.setSpui					(serviceElement.getElementsByTag("Service").first().attr("SPUI"));
		hotel.setStatus					(serviceElement.getElementsByTag("Status").first().text());
		hotel.setContractName			(serviceElement.getElementsByTag("Name").first().text());
	
		hotel.setIncomingOfficeCode		(serviceElement.getElementsByTag("IncomingOffice").first().text());
		try {
			hotel.setComment			(serviceElement.getElementsByTag("Comment").first().text());
			hotel.setCommentType			(serviceElement.getElementsByTag("Comment").first().attr("type"));

		} catch (Exception e) {
			// TODO: handle exception
		}
		
		hotel.setSupplierName			(serviceElement.getElementsByTag("Supplier").first().attr("name"));
		hotel.setSupplierVatNumber		(serviceElement.getElementsByTag("Supplier").first().attr("vatNumber"));
		hotel.setDateFromDate			(serviceElement.getElementsByTag("DateFrom").first().attr("date"));
		hotel.setDateToDate				(serviceElement.getElementsByTag("DateTo").first().attr("date"));
		hotel.setCurrency				(serviceElement.getElementsByTag("Currency").first().text());
		hotel.setCurrencyCode			(serviceElement.getElementsByTag("Currency").first().attr("code"));
		hotel.setTotalAmount			(serviceElement.getElementsByTag("TotalAmount").first().text());
		
		ArrayList<resAdditionalCost>  	additionalCost 	= new ArrayList<resAdditionalCost>();
		Iterator<Element> additionalCostIterator		= doc.getElementsByTag("AdditionalCost").iterator();
		while(additionalCostIterator.hasNext())
		{
			Element	cost								= additionalCostIterator.next();
			resAdditionalCost 			addcost			= new resAdditionalCost();
			addcost.setAdditionalCostType	(cost.getElementsByTag("AdditionalCost").first().attr("type"));
			addcost.setPrice				(cost.getElementsByTag("Amount").first().text());
			additionalCost.add(addcost);
		}
		hotel.setAditionalCost(additionalCost);
		
		hotel.setModificationPolicyList		(serviceElement.getElementsByTag("ModificationPolicy").first().text());
		hotel.setHotelInfoType				(serviceElement.getElementsByTag("HotelInfo").first().attr("xsi:type"));
		hotel.setHotelInfoCode				(serviceElement.getElementsByTag("Code").first().text());
		hotel.setHotelInfoname				(serviceElement.getElementsByTag("Name").first().text());
		hotel.setHotelInfoCategory			(serviceElement.getElementsByTag("Category").first().text());
		hotel.setDestinationCode			(serviceElement.getElementsByTag("Destination").first().attr("code"));
		hotel.setDestinationName			(serviceElement.getElementsByTag("Name").first().text());
		hotel.setDestinationType			(serviceElement.getElementsByTag("Destination").first().attr("type"));
		hotel.setZoneCode					(serviceElement.getElementsByTag("Zone").first().attr("code"));
		hotel.setZoneType					(serviceElement.getElementsByTag("Zone").first().attr("type"));
		
		ArrayList<resavailableRoom>  	availableRoom 	= new ArrayList<resavailableRoom>();
		Iterator<Element> 				avaialableRoomIterator		= doc.getElementsByTag("HotelOccupancy").iterator();
		//System.out.println(doc.getElementsByTag("HotelOccupancy").size());
		while(avaialableRoomIterator.hasNext())
		{
			Element						currentElement	= avaialableRoomIterator.next();
			resavailableRoom			room			= new resavailableRoom();
			room.setAdultCount				(currentElement.getElementsByTag("AdultCount").first().text());
			room.setChildCount				(currentElement.getElementsByTag("ChildCount").first().text());
			room.setRoomCount				(currentElement.getElementsByTag("RoomCount").first().text());
			
			
//--------------------------------------------------------------------------------------------------------------------------------------------			
			
			
			
//--------------------------------------------------------------------------------------------------------------------------------------------			
			//room.setName					(hotelElement.getElementsByTag("Name").first().text());
			//room.setValue					(hotelElement.getElementsByTag("Value").first().text());
			ArrayList<resGuestList>  			guestList		= new ArrayList<resGuestList>();
			Iterator<Element> 				guestIterator	= currentElement.getElementsByTag("Customer").iterator();
			while(guestIterator.hasNext())
			{
											currentElement	= guestIterator.next();
				resGuestList					guest			= new resGuestList();
				
				guest.setCustomerAge		(currentElement.getElementsByTag("Age").first().text());
				guest.setCustomerid			(currentElement.getElementsByTag("CustomerId").first().text());
				guest.setCustomerType		(currentElement.getElementsByTag("Customer").first().attr("type"));
				guest.setLastName			(currentElement.getElementsByTag("Name").first().text());
				guest.setLastName			(currentElement.getElementsByTag("LastName").first().text());
				
				guestList.add(guest);
			}
			room.setGuest(guestList);
			
			availableRoom.add(room);
		}
		
		hotel.setRoomAvailability(availableRoom);
		
		
		ArrayList<resHotelRoom>  	roomAvailability 	= new ArrayList<resHotelRoom>();
		Iterator<Element> 				roomAvailabilityIterator		= doc.getElementsByTag("HotelRoom").iterator();
		while(roomAvailabilityIterator.hasNext())
		{

			Element						currentElement	= roomAvailabilityIterator.next();
			resavailableRoom			room			= new resavailableRoom();
			resHotelRoom				hotelRoom 		= new resHotelRoom();
			
			
			hotelRoom.setShrui				(currentElement.getElementsByTag("HotelRoom").first().attr("SHRUI"));
			hotelRoom.setAvailableCount		(currentElement.getElementsByTag("HotelRoom").first().attr("availCount"));
			hotelRoom.setStatus				(currentElement.getElementsByTag("HotelRoom").first().attr("status"));
			hotelRoom.setBoard				(currentElement.getElementsByTag("Board").first().text());
			hotelRoom.setBoardCode			(currentElement.getElementsByTag("Board").first().attr("code"));
			hotelRoom.setBoardType			(currentElement.getElementsByTag("Board").first().attr("type"));
			hotelRoom.setRoomType			(currentElement.getElementsByTag("RoomType").first().attr("type"));
			hotelRoom.setRoomCode			(currentElement.getElementsByTag("RoomType").first().attr("code"));
			hotelRoom.setCharacteristic		(currentElement.getElementsByTag("RoomType").first().attr("characteristic"));
			hotelRoom.setRoom				(currentElement.getElementsByTag("RoomType").first().text());
			hotelRoom.setPrice				(currentElement.getElementsByTag("Amount").first().text());
			
			Element cancelElement			= currentElement.getElementsByTag("CancellationPolicy").first();
			hotelRoom.setCancellationCodet	(cancelElement.getElementsByTag("Amount").first().text());
			hotelRoom.setDateTimeFromDate	(cancelElement.getElementsByTag("DateTimeFrom").first().text());
			hotelRoom.setDateTimeToDate		(cancelElement.getElementsByTag("DateTimeTo").first().text());
			
			
			
			ArrayList<resExtendedInfo> extended = new ArrayList<resExtendedInfo>();
			Iterator<Element> extendedIterator	= currentElement.getElementsByTag("ExtendedData").iterator();
			while(extendedIterator.hasNext())
			{
				currentElement = extendedIterator.next();
				resExtendedInfo 	infoExtended = new resExtendedInfo();
				infoExtended.setName			(currentElement.getElementsByTag("Name").first().text());
				infoExtended.setValue			(currentElement.getElementsByTag("Value").first().text());
				extended.add(infoExtended);
			}
			
			hotelRoom.setExtended(extended);
			roomAvailability.add(hotelRoom);
			
			
		}
		
			hotel.setHotelRoom(roomAvailability);	
			//System.out.println(roomAvailability.get(0).getCancellationCodet());
			
			
			
//			
//			System.out.println(hotel.getAgencyCode());
//			System.out.println(hotel.getHotelRoom().get(0).getPrice());
			return hotel;
	
	}
}
