package readXml;

import java.util.ArrayList;

public class resavailableRoom {

	String	roomCount =""; 
	String	childCount =""; 
	String	adultCount =""; 
	String	name =""; 
	String	value =""; 
	String	maxAdult =""; 
	String	maxChild =""; 
	String	price =""; 
	String	tax =""; 
	String	avgPrice =""; 
	String	doubleBed =""; 
	String	boardName =""; 
	String	boardPrice =""; 
	String	roomNumber =""; 
	String	roomPrice ="";
	ArrayList<resGuestList> guest = new ArrayList<resGuestList>();
	ArrayList<resHotelRoom> room	= new ArrayList<resHotelRoom>();
	ArrayList<resExtendedInfo> extended = new ArrayList<resExtendedInfo>();
	
	/**
	 * @return the guest
	 */


	/**
	 * @return the room
	 */
	public ArrayList<resHotelRoom> getRoom() {
		return room;
	}

	/**
	 * @return the maxAdult
	 */
	public String getMaxAdult() {
		return maxAdult;
	}

	/**
	 * @param maxAdult the maxAdult to set
	 */
	public void setMaxAdult(String maxAdult) {
		this.maxAdult = maxAdult;
	}

	/**
	 * @return the maxChild
	 */
	public String getMaxChild() {
		return maxChild;
	}

	/**
	 * @param maxChild the maxChild to set
	 */
	public void setMaxChild(String maxChild) {
		this.maxChild = maxChild;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the tax
	 */
	public String getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(String tax) {
		this.tax = tax;
	}

	/**
	 * @return the avgPrice
	 */
	public String getAvgPrice() {
		return avgPrice;
	}

	/**
	 * @param avgPrice the avgPrice to set
	 */
	public void setAvgPrice(String avgPrice) {
		this.avgPrice = avgPrice;
	}

	/**
	 * @return the doubleBed
	 */
	public String getDoubleBed() {
		return doubleBed;
	}

	/**
	 * @param doubleBed the doubleBed to set
	 */
	public void setDoubleBed(String doubleBed) {
		this.doubleBed = doubleBed;
	}

	/**
	 * @return the boardName
	 */
	public String getBoardName() {
		return boardName;
	}

	/**
	 * @param boardName the boardName to set
	 */
	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	/**
	 * @return the boardPrice
	 */
	public String getBoardPrice() {
		return boardPrice;
	}

	/**
	 * @param boardPrice the boardPrice to set
	 */
	public void setBoardPrice(String boardPrice) {
		this.boardPrice = boardPrice;
	}

	/**
	 * @return the roomNumber
	 */
	public String getRoomNumber() {
		return roomNumber;
	}

	/**
	 * @param roomNumber the roomNumber to set
	 */
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	/**
	 * @return the roomPrice
	 */
	public String getRoomPrice() {
		return roomPrice;
	}

	/**
	 * @param roomPrice the roomPrice to set
	 */
	public void setRoomPrice(String roomPrice) {
		this.roomPrice = roomPrice;
	}

	public ArrayList<resExtendedInfo> getExtended() {
		return extended;
	}

	public void setExtended(ArrayList<resExtendedInfo> extended) {
		this.extended = extended;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @param room the room to set
	 */
	public void setRoom(ArrayList<resHotelRoom> room) {
		this.room = room;
	}

	/**
	 * @return the roomCount
	 */
	public String getRoomCount() {
		return roomCount;
	}

	/**
	 * @return the guest
	 */
	public ArrayList<resGuestList> getGuest() {
		return guest;
	}

	/**
	 * @param guest the guest to set
	 */
	public void setGuest(ArrayList<resGuestList> guest) {
		this.guest = guest;
	}

	/**
	 * @param roomCount the roomCount to set
	 */
	public void setRoomCount(String roomCount) {
		this.roomCount = roomCount;
	}

	/**
	 * @return the childCount
	 */
	public String getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount the childCount to set
	 */
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}

	/**
	 * @return the adultCount
	 */
	public String getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount the adultCount to set
	 */
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}
		
}
