package urlGenarator;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class xmlHome {
	private static Map<String, String>  txtProperty = new HashMap<String, String>();
		String part1 				= "http://xml-test.secure-reservation.com/jsp/Browser.jsp?sort=-3&dir=%2Fvar%2Flog%2Frezg%2Fapp%2Fhotelxml%2F" ;
		String part2 				= "%2Fstaging%2Frezpackage";

		public String generateUrl()
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		today 		= 	getToday();
			String 		date 		=	urls.get("xmlHomeUrl1").concat(today).concat(urls.get("xmlHomeUrl2"));
			return date;
		}
		public String getToday()
		{
			DateFormat 	dateFormat 	= new SimpleDateFormat("dd-MM-yyyy");
			Calendar 	cal 		= Calendar.getInstance();
			String 		date 		= dateFormat.format(cal.getTime());
			System.out.println(date);
			return date;
		}
		
		public String inv13ReqUrl(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl 		= urls.get("inv13RoomAvailabilityReq").concat(tracer);
			/*String[] 	date 		= getToday().split("-");
			String 		day			= date[0];
			String		month		= date[1];
			String 		year		= date[2];
			String		newDate		= year.concat("-").concat(month).concat("-").concat(day);
						hbUrl		= hbUrl.concat("_").concat(newDate);*/
			return		hbUrl;
		}
		
		public String hotelBedsReqUrl(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl 		= urls.get("hbRoomAvailabilityReq").concat(tracer);
			return		hbUrl;
		}
		
		public String inv13ResUrl(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("inv13RoomAvailabilityRes").concat(tracer);
			/*String[] 	date 		= getToday().split("-");
			String 		day			= date[0];
			String		month		= date[1];
			String 		year		= date[2];
			String		newDate		= year.concat("-").concat(month).concat("-").concat(day);
						hbUrl		= hbUrl.concat("_").concat(newDate);*/
			System.out.println(hbUrl);
			return		hbUrl;	 	 
		}
		
		public String hotelBedsResUrl(String tracer)
		{
			System.out.println("test -- 3.0.0.1");
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("hbRoomAvailabilityRes").concat(tracer);
			return		hbUrl;	 	 
		}
		
		public String inv13PreReq(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("inv13PreReq").concat(tracer);
			return		hbUrl;	 	 
		}
		
		public String inv27PreReq(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("inv27PreReq").concat(tracer);
			return		hbUrl;	 
		}
		
		public String hbPreReq(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("hbPreReq").concat(tracer);
			return		hbUrl;	 	 
		}
		
		public String inv13PreRes(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("inv13PreRes").concat(tracer);
			return		hbUrl;	
		}
		
		public String inv27PreRes(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("inv27PreRes").concat(tracer);
			return		hbUrl;	
		}
		
		public String hbPreRes(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("hbPreRes").concat(tracer);
			return		hbUrl;	
		}
		
		public String inv13FinalReq(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("inv13FinalReq").concat(tracer);
			return		hbUrl;	
		}
		
		public String inv27FinalReq(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= urls.get("inv27FinalReq").concat(tracer);
			return		hbUrl;	
		}
		
		public String hbFinalReq(String tracer)
		{
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("hbFinalReq").concat(tracer);
			return		hbUrl;	
		}
		
		public String canDeadReq(String tracer)
		{
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("inv13CanDeadRes").concat(tracer);
			return		hbUrl;	
		} 
		
		public String inv13FinalRes(String tracer)
		{
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("inv13FinalRes").concat(tracer);
			return		hbUrl;	
		}
		
		public String hbCancellationRes(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= 	urls.get("hbCancellationRes").concat(tracer);
			return		hbUrl;	
		}
		
		public String hbFinalRes(String tracer)
		{
			Map<String, String>  urls = new HashMap<String, String>();
			urls					=	getUrls();
			String 		hbUrl		= 	urls.get("inv27FinalRes").concat(tracer);
			return		hbUrl;	
		}
		
		public String inv13RoomAvailabilityReq(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						=	getUrls();
			String		hbUrl		= urls.get("inv13RoomAvailabilityReq").concat(tracer);
			return		hbUrl;	 
		}
		
		public String inv27RoomAvailabilityReq(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						=	getUrls();
			String		hbUrl		= urls.get("inv27RoomAvailabilityReq").concat(tracer);
			return		hbUrl;	 
		}
		
		public String hbRoomAvailabilityReq(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						=	getUrls();
			String		hbUrl		= urls.get("hbRoomAvailabilityReq").concat(tracer);
			return		hbUrl;	 
		}
		
		public String inv13RoomAvailabilityRes(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						= getUrls();
			String		hbUrl			= urls.get("inv13RoomAvailabilityRes").concat(tracer);
			return		hbUrl;	 
		}
		
		public String inv27roomAvailabilityRes(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						=	getUrls();
			String		hbUrl		= urls.get("inv27RoomAvailabilityRes").concat(tracer);
			return		hbUrl;	
		}
		
		public String hbRoomAvailabilityRes(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						=	getUrls();
			String		hbUrl		= urls.get("hbRoomAvailabilityRes").concat(tracer);
			return		hbUrl;	 
		}
		
		public String inv13SearchReq(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						=	getUrls();
			String		hbUrl			= urls.get("inv13Page1Req").concat(tracer);
			return		hbUrl;	 	 
		}
		
		public String inv27SearchReq(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						= getUrls();
			String		hbUrl			= urls.get("inv27Page1Req").concat(tracer);
			return		hbUrl;	 	 
		}
		
		public String hbSearchReq(String tracer)
		{
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						= getUrls();
			String		hbUrl			= urls.get("hbPage1Req").concat(tracer);
			return		hbUrl;	 		 
		}
		
		public String inv13Page1Res(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						= getUrls();
			String 		hbUrl			= urls.get("inv13AvailabilityRes").concat(tracer);
			return		hbUrl;
		}
		
		public String whitesandsroomAvailabilityRes(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("whitesandsRoomAvailabilityRes").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String whitesandsReservationReq(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("whitesandsReservationReq").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String whitesandsroomAvailabilityPreRes(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("whitesandsRoomAvailabilityPreRes").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String cancellationResponse(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("inv13CancellationRes").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String hbcancellationReq(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("hbCancellationReq").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String inv13cancellationRequest(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("inv13CancellationReq").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String whitesandsroomAvailabilityPreReq(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("whitesandsRoomAvailabilityPreReq").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String whitesandsroomAvailabilityReq(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("whitesandsRoomAvailabilityReq").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String whitesandsPage1Res(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= 	new HashMap<String, String>();
			urls						=	getUrls();
			String 		hbUrl			= 	urls.get("whitesandspage1").concat(tracer);
			//String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String inv27Page1Res(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						= getUrls();
			String 		hbUrl			= urls.get("inv27page1Res").concat(tracer);
			return		hbUrl;
		}
		
		public String hbPage1Res(String tracer)
		{
			System.out.println(tracer);
			Map<String, String>  urls 	= new HashMap<String, String>();
			urls						= getUrls();
			String 		hbUrl			= urls.get("hbpage1Res").concat(tracer);
			return		hbUrl;
		}
		public Map<String, String> getUrls()
		{
			Properties properties = new Properties();
			try 
			{
				//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
				FileReader reader =new FileReader(new File("C://Users//Akila//git//v3_3p_hotel//xmlUrls.properties"));
				
				properties.load(reader);
			} 
			catch (IOException e)
			{
				 System.out.println(e.toString());
			}
			for (String key : properties.stringPropertyNames()) 
			{
			    String value = properties.getProperty(key);
			    //System.out.print(key + " --- ");
			    //System.out.println(value);
			    txtProperty.put(key, value);
			}
			return txtProperty;
		}
}
