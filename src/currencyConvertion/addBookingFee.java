package currencyConvertion;

import java.math.BigDecimal;

public class addBookingFee {

	public int bookingFee(String value, String bookingFee)
	{

		int rtnValue = 0;
		if(bookingFee.contains("%"))
		{
			String[] bookingFeePercentage = bookingFee.split("%");
			float percentage = Float.parseFloat(bookingFeePercentage[0]);
			float fvalue	= Float.parseFloat(value);
			fvalue = (fvalue/100)*percentage;
			BigDecimal roundfinalPrice 	= new BigDecimal(fvalue).setScale(0,BigDecimal.ROUND_CEILING);
			value = String.valueOf(roundfinalPrice);
			int bookingFeeAddedPrice	= Integer.parseInt(value);
			rtnValue		= bookingFeeAddedPrice;
			//System.out.println(bookingFeeAddedPrice);
		}
		else
		{
			float fvalue	=	Float.parseFloat(value);
			float fbookingFee		= 	Float.parseFloat(bookingFee);
			fvalue			= 	fvalue + fbookingFee;
			BigDecimal roundfinalPrice 	= new BigDecimal(fvalue).setScale(0,BigDecimal.ROUND_CEILING);
			value = String.valueOf(roundfinalPrice);
			int bookingFeeAddedPrice	= Integer.parseInt(value);
			rtnValue		= bookingFeeAddedPrice;
		}
		//System.out.println(rtnValue);
		return rtnValue;
	}
	
}
