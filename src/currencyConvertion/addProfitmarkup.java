package currencyConvertion;

import java.math.BigDecimal;

public class addProfitmarkup {

	public int profitMarkup(String value, String pm)
	{
		int rtnValue = 0;
		if(pm.contains("%"))
		{
			String[] pmPercentage = pm.split("%");
			float percentage = Float.parseFloat(pmPercentage[0]);
			float fvalue	= Float.parseFloat(value);
			fvalue = (fvalue/100)*percentage;
			BigDecimal roundfinalPrice 	= new BigDecimal(fvalue).setScale(0,BigDecimal.ROUND_CEILING);
			value = String.valueOf(roundfinalPrice);
			int pmAddedPrice	= Integer.parseInt(value);
			rtnValue		= pmAddedPrice;
			//System.out.println(pmAddedPrice);
		}
		else
		{
			float fvalue	=	Float.parseFloat(value);
			float fpm		= 	Float.parseFloat(pm);
			fvalue			= 	fpm;
			BigDecimal roundfinalPrice 	= new BigDecimal(fvalue).setScale(0,BigDecimal.ROUND_CEILING);
			value = String.valueOf(roundfinalPrice);
			int pmAddedPrice	= Integer.parseInt(value);
			rtnValue		= pmAddedPrice;
		}
		//System.out.println(rtnValue);
		return rtnValue;
	}
}
