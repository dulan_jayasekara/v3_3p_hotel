package currencyConvertion;

import java.math.BigDecimal;
import java.util.Map;

public class currencyConveter {

	public String convert(String defaultCC, String value, String customerCC, Map<String, String> currency, String suppliercc)
	{
		
		//System.out.println(customerCC);
		//System.out.println(defaultCC);
		//System.out.println(suppliercc);
		//System.out.println(value);
		//System.out.println(currency.get(suppliercc));
		String 		returnValue 	= 	"0";
		if(currency.get(suppliercc).equals("null"))
		{
			
		}
		else
		{
			if(defaultCC.equals(customerCC) && defaultCC.equals(suppliercc))
			{
				float	fvalue				= Float.parseFloat(value);
						//fvalue				= fvalue + (fvalue/100)*10;
				//System.out.println(fvalue);
				BigDecimal roundfinalPrice 	= new BigDecimal(fvalue).setScale(0,BigDecimal.ROUND_CEILING);
						returnValue			= String.valueOf(roundfinalPrice);
			}
			if(defaultCC.equals(customerCC) && !defaultCC.equals(suppliercc))
			{
				//System.out.println(currency.get(suppliercc));
				float 	rate				= Float.parseFloat(currency.get(suppliercc)); 
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue/rate;				//cvalue	= conveted final value
						//cValue				= cValue + (cValue/100)*10;
				//System.out.println(cValue);
				BigDecimal roundfinalPrice 	= new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
						returnValue			= String.valueOf(roundfinalPrice);			
			}
			if(defaultCC.equals(suppliercc) && !defaultCC.equals(customerCC))
			{
				//System.out.println(currency.get(customerCC));
				float 	rate				= Float.parseFloat(currency.get(customerCC));
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue*rate;				//cvalue	= conveted final value
						//cValue				= cValue + (cValue/100)*10;
				//System.out.println(cValue);
				BigDecimal roundfinalPrice 	= new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
						returnValue			= String.valueOf(roundfinalPrice);			
			}
			if(!defaultCC.equals(suppliercc) && !defaultCC.equals(customerCC))
			{
				//System.out.println(currency.get(customerCC));
				float 	crate				= Float.parseFloat(currency.get(customerCC));
				float 	srate				= Float.parseFloat(currency.get(suppliercc));
				float 	fValue				= Float.parseFloat(value);	//fvalue	= converted value to float
				float 	cValue				= fValue/srate;	
				//System.out.println(cValue);
						cValue				= cValue*crate;
						//System.out.println(cValue);
						//cValue				= cValue + (cValue/100)*10;
						//System.out.println(cValue);
				BigDecimal roundfinalPrice 	= new BigDecimal(cValue).setScale(0,BigDecimal.ROUND_CEILING);
						returnValue			= String.valueOf(roundfinalPrice);
			}
		}
		
		
		return returnValue;
	}
}
