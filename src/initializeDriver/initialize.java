package initializeDriver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class initialize {

	private static WebDriver driver;
	private static Map<String, String>  	txtProperty 		= new HashMap<String, String>();
	public WebDriver initialize()
	{
		Properties properties 	= new Properties();
		try 
		{
			//FileInputStream fs = new FileInputStream(new File("/selTest2/src/selTest2/jTestProperties.properties"));
			FileReader reader 	=new FileReader(new File("D://workSpace//hotelXmlValidation//logginInfo.properties"));
			
			properties.load(reader);
		} 
		catch (IOException e)
		{
			 System.out.println(e.toString());
		}
		for (String key : properties.stringPropertyNames()) 
		{
		    String value 		= properties.getProperty(key);
		    txtProperty.put(key, value);
		}
		
		String profile= txtProperty.get("firefoxProfile");
		FirefoxProfile prof		= new FirefoxProfile(new File(profile));
		driver 					= new FirefoxDriver(prof);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		return driver;
	}
}
